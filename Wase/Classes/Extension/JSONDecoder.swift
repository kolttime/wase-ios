//
//  JSONDecoder.swift
//  MDW
//
//  Created by Roman Makeev on 05/08/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

public extension JSONDecoder {
    
    static func makeCamelDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }
}
