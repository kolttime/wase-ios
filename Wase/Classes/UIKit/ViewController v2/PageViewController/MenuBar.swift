//
//  MenuBar.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class MenuBar : AirdronView {
    
    var dots : [UIView] = []
    var emptyColor : UIColor
    var currentColor : UIColor
    var count : Int
    var currentIndex = 0
     init(frame: CGRect, count : Int, emptyColor : UIColor, currentColor : UIColor) {
        self.emptyColor = emptyColor
        self.currentColor = currentColor
        self.count = count
        super.init(frame: frame)
        
    }
    
    override func initialSetup() {
        super.initialSetup()
        for i in 0...self.count - 1 {
            let view = UIView()
            view.backgroundColor = emptyColor
            
           
            self.dots.append(view)
            self.addSubview(view)
        }
        self.dots[0].backgroundColor = currentColor
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        self.dots[0].snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.left.equalToSuperview()
            $0.height.equalTo(8)
            $0.width.equalTo(8)
        }
         dots[0].layer.cornerRadius = 4
         dots[0].clipsToBounds = true
        for i in 1...self.count - 1 {
            self.dots[i].snp.makeConstraints{
                $0.centerY.equalToSuperview()
                $0.left.equalTo(self.dots[i - 1].snp.right).offset(10)
                $0.height.equalTo(8)
                $0.width.equalTo(8)
                
            }
            
        
            dots[i].layer.cornerRadius = 4
            dots[i].clipsToBounds = true
        }
        
    }
    func next(){
        self.dots[currentIndex].backgroundColor = emptyColor
        currentIndex += 1
        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseOut, animations: {
            self.dots[self.currentIndex].backgroundColor = self.currentColor
        }, completion: { finished in
           
        })
    }
    func previous(){
        self.dots[currentIndex].backgroundColor = emptyColor
        self.currentIndex -= 1
        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseOut, animations: {
            self.dots[self.currentIndex].backgroundColor = self.currentColor
        }, completion: { finished in
            
        })
        
    }
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
