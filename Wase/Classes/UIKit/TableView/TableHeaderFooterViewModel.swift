//
//  TableHeaderFooterViewModel.swift
//  AirdronKit
//
//  Created by Roman Makeev on 19/07/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

public protocol TableHeaderFooterViewModel {
    
    var viewType: TableHeaderFooterView.Type { get }
}
