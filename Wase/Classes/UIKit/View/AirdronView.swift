//
//  AirdronView.swift
//  MDW
//
//  Created by Roman Makeev on 20/07/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

open class AirdronView: UIView {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupInitialState()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupInitialState()
    }
    
    open func initialSetup() {
        
    }
    
    open func setupInitialState() {
        self.initialSetup()
        self.setupConstraints()
        self.setNeedsUpdateConstraints()
    }
    
    open func setupConstraints() {
        
    }
}
