//hs,f
//
//  ApiService.swift
//  OMG
//
//  Created by Roman Makeev on 06/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import Alamofire
import Photos

protocol ApiService {
    
    
    
    func licenseAgreement(completion: ((Result<StaticContent>) -> Void)?)
    
    func postFeedback(feedback : String, completion : ((Result<Success>) -> Void)?)
    
    func instagramCode(url: URL, completion: ((Result<Success>) -> Void)?)
    
    func instagramAuth(url: URL, completion: ((Result<VerificationResponse>) -> Void)?)
    
    func auth(byPhone phone: String, agreed : Bool, completion: ((Result<KeyCode>) -> Void)?)
    
    func verification(byKey key: String, code: String, completion: ((Result<VerificationResponse>) -> Void)?)
    
    func createPhotosession(CreatePhoto : CreatePhotosession, competition : ((Result<Photosession>) -> Void)?)
    
    func activation(userActivation: UserCreation, email : String ,
                    cityName : String ,
                    professionName : String ,
                    completion: ((Result<User>) -> Void)?)
    func getProfile(userId : String?, completion : ((Result<User>) -> Void)?)
    
    func getAlbumId(id : String, completion : ((Result<Album>) -> Void)?)
    
    func getAlbums(id : String?, completion : ((Result<[ShortAlbum]>) -> Void)?)
    
    func getCheckNickname(nickname : String, completion : ((Result<CheckNicknameResponse>) -> Void)?)
    
    func specialization(completion : ((Result<[Specialization]>) -> Void)?)
    
    func getCities(completion: ((Result<[City]>) -> Void)?)
    
    func uploadImage(byAsset asset: PHAsset, completion: ((Result<Image>) -> Void)?)
    
    func uploadImage(byFileUrl url: URL,
                     requestCompletionHandler: ((Result<AirdronDataRequestWrapper>) -> Void)?,
                     progressHandler: ((Double) -> Void)?,
                     completion: ((Result<Image>) -> Void)?)
    func uploadImagesInAlbum(albumImages: [Image], completion: ((Result<[Image]>) -> Void)?)
    
    func uploadAlbum(albumImages: [Image], album: AlbumUpload, completion: ((Result<Album>) -> Void)?)
    
    func privacyPolicy(completion: ((Result<StaticContent>) -> Void)?)
    
    func getReadyPageClient(completion: ((Result<StaticContent>) -> Void)?)
    func getReadyPageSupplier(completion: ((Result<StaticContent>) -> Void)?)
    
    
    func termsOfUse(completion: ((Result<StaticContent>) -> Void)?)
    
    func getFeed(specialization : String? ,  completion : ((Result<[FeedSection]>) -> Void)?)
    
    func getImages(id : String? , completion : ((Result<[Image]>) -> Void)?)
    
    func getAlbum(userId : String?, id : String, completion : ((Result<Album>) -> Void)?)
    
    func takeDistricts(id : String, completion : ((Result<[District]>) -> Void)?)
    
    func getBudgest(specialization : String, duration : Int, type : [String], completion : ((Result<[Budget]>) -> Void)?)
    
    func getCandidates(type : UserType, id : String, completion : ((Result<[CandidatesResponse2]>) -> Void)?)
    
    func getUserPhotosessions(active : Bool, completion : ((Result<[Photosession]>) -> Void)?)
    
    func postOffers(photoId : String, users : [String], competition: ((Result<[UserOffers]>) -> Void)?)
    
    func postParticipants(photoId : String, users : [String], competition: ((Result<[UserOffers]>) -> Void)?)
    
    func patchProfile(userModel : User, completion : ((Result<User>) -> Void)?)
    
    func postPrepayment(photoId : String, completion : ((Result<SuccessModel>) -> Void)?)
    
    func getCheckPromocode(code : String, completion : ((Result<CheckPromocode>) -> Void)?)
    
    func getPhotoPhotosessions(tabIndex : TabIndex, id : String?, completion : ((Result<[Photosession]>) -> Void)?)
    
    func getPhotoPhotosession(id : String, completion : ((Result<LongPhotosession>) -> Void)?)
    
    func patchPhotosessionsoffer(id : String, price : Int? , comment : String, status : String, completion : ((Result<LongPhotosession>) -> Void)?)
    
    func getPhotosessionsOffers(type : UserType, id : String ,completion : ((Result<[UserOffers]>) -> Void)?)
    
    func patchPhotosessions(photosession : LongPhotosession, completion : ((Result<LongPhotosession>) -> Void)?)
    
    func deletePhotosessionsOffers(photoId : String, comment : String, completion : ((Result<Success>) -> Void)?)
    
    func putPhotosessionOffer(photoId : String, comment : String, completion : ((Result<Success>) -> Void)?)
    
    func postConfirmation(photoId : String, rating : Int?, comment : String?, resultDate : Int?, completion : ((Result<LongPhotosession>) -> Void)? )
    
    func uploadResults(id : String, albumImages: [Image], storage : String, visibility : Bool, completion: ((Result<Album>) -> Void)?)
    
    func postProfileCard(completion: ((Result<LinkModel>) -> Void)?)
    
    func getProfileCard(completion: ((Result<Finances>) -> Void)?)
    
    func getStories(completion: ((Result<[ShortStories]>) -> Void)?)
    
    func getPhotoCount(completion : ((Result<PhotoCount>) -> Void)?)
    
    func getStaticPayments(completion : ((Result<staticPayments>) -> Void)?)
    
    func patchPhotosessionsAlbum(id : String, visibility : Bool, completion: ((Result<Album>) -> Void)?)
    
    func getRefferalDiscount(completion : ((Result<RefferalDiscount>) -> Void)?)
    
    func getStories(id : String, completion: ((Result<Stories>) -> Void)?)
    
    func getRefferalLink(completion : ((Result<LinkModel>) -> Void)?)
    
    func getProfilePromocode(completion : ((Result<Promocode>) -> Void)?)
    
    func putPhotoPhotosession(id : String, comment : String, completion : ((Result<Empty>) -> Void)?)
    
    func logout()
    
}

class ApiServiceImp: ApiService {
    
    
    private let networkManager: NetworkManager
    private let uploadManager: UploadManager
    private let backgroundUploadManager: UploadManager
    private let sessionManager: TaokaUserSessionManager
    private var credential: CredentialStorage
    
    init(networkManager: NetworkManager,
         uploadManager: UploadManager,
         backgroundUploadManager: UploadManager,
         sessionManager: TaokaUserSessionManager,
         credential: CredentialStorage) {
        self.networkManager = networkManager
        self.credential = credential
        self.backgroundUploadManager = backgroundUploadManager
        self.sessionManager = sessionManager
        self.uploadManager = uploadManager
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.notAuthorizedHandler),
                                               name: Notification.Name.notAuthorized,
                                               object: nil)
    }
    
    func makeTokenHeader() -> [String: String] {
        if let token = self.credential.getToken() {
            return ["Token": token]
        }
        return [:]
    }
    
    
    private let userParametres : [String : Any] = ["q" : String.self , "type" : UserType.self, "cout" : Int.self]
    private let langParameters: String = "?lang=\(LanguageManager.defaultLanguage().rawValue)"
    private let langDictParameters: [String : Any] = ["lang" : LanguageManager.defaultLanguage().rawValue]
    
    @objc
    func notAuthorizedHandler() {
        self.logout()
    }
    
    
    func logout() {
        self.sessionManager.logout()
        
        NotificationCenter.default.post(name: Notification.Name.TaokaNotificationLogout, object: nil)
    }
    
    
    
    
}

// MARK: Login
extension ApiServiceImp {
    
    
    func licenseAgreement(completion: ((Result<StaticContent>) -> Void)?) {
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.pages(name: "license_agreement").url)
        func processData(data: Data) throws -> StaticContent {
            let response = try StaticContent(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func instagramCode(url: URL, completion: ((Result<Success>) -> Void)?) {
        let request = AirdronRequest(method: .get,
                                     endpoint: url.absoluteString,
                                     headers: self.makeTokenHeader())
        self.networkManager.perform(request: request,
                                    processData: { data in try Success(data: data) },
                                    completionHandler: completion)
    }
    
    func instagramAuth(url: URL, completion: ((Result<VerificationResponse>) -> Void)?) {
        print("Url : \(url.absoluteString)")
        let request = AirdronRequest(method: .get,
                                     endpoint: url.absoluteString)
        self.networkManager.perform(request: request,
                                    processData: { data in try VerificationResponse(data: data) },
                                    completionHandler: completion)
    }
    
    func getCities(completion: ((Result<[City]>) -> Void)?) {
        
        let request = AirdronRequest(method: .get, endpoint: Endpoints.cities.url)
        func processData(data: Data) throws -> [City] {
            let response = try [City](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    
    func auth(byPhone phone: String, agreed : Bool, completion: ((Result<KeyCode>) -> Void)?) {
        let parameters: [String: Any] = ["phone": phone]
        let url = agreed ? Endpoints.putPhone.url : Endpoints.auth.url
        let request : AirdronRequest
        if !agreed {
            request = AirdronRequest(method: .post, endpoint: url, parameters: parameters)
        } else {
            request = AirdronRequest(method: .put, endpoint: url, parameters: parameters, headers: self.makeTokenHeader())
        }
       // let request = AirdronRequest(method: !agreed ? .post : .put, endpoint: url, parameters: parameters)
        self.networkManager.perform(request: request,
                                    processData: { data in try KeyCode(data: data) },
                                    completionHandler: completion)
    }
    
    
    
    func verification(byKey key: String, code: String, completion: ((Result<VerificationResponse>) -> Void)?) {
        let parameters: [String: Any] = ["key": key, "code": code]
        let request = AirdronRequest(method: .post, endpoint: Endpoints.verification.url, parameters: parameters)
        func processData(data: Data) throws -> VerificationResponse {
            let response = try VerificationResponse(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func activation(userActivation: UserCreation,
                    email : String,
                    cityName : String,
                    professionName : String,
                    completion: ((Result<User>) -> Void)?) {
        var parameters: [String: Any] = userActivation.toDictionary()
        if email != "" {
            parameters["email"] = email
            
        }
        if  cityName != "" {
            parameters["city_name"] = cityName
        }
        if professionName != "" {
            parameters["profession"] = professionName
        }
        print("params : \(parameters)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.activation.url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> User {
            let response = try User(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func getCheckNickname(nickname : String, completion : ((Result<CheckNicknameResponse>) -> Void)?){
        // let parameters: [String: Any] = userActivation.toDictionary()
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.nickname(name: nickname).url,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> CheckNicknameResponse {
            let response = try CheckNicknameResponse(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func postProfileCard(completion: ((Result<LinkModel>) -> Void)?){
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.profileCard.url,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> LinkModel {
            let response = try LinkModel(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getProfileCard(completion: ((Result<Finances>) -> Void)?){
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.profileCard.url,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Finances {
            let response = try Finances(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func getStories(completion: ((Result<[ShortStories]>) -> Void)?){
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.stories.url,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> [ShortStories] {
            let response = try [ShortStories](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func postFeedback(feedback : String, completion : ((Result<Success>) -> Void)?) {
        var parameters: [String: Any] = [:]
        parameters["message"] = feedback
        // print("HERERERE \n\n\n\(parameters)\n\n\nUrl : \(Endpoints.createPhotosession.url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.feedback.url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Success {
            let response = try Success(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func getStories(id : String, completion: ((Result<Stories>) -> Void)?){
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.Stories(id: id).url,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Stories {
            let response = try Stories(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getReadyPageClient(completion: ((Result<StaticContent>) -> Void)?) {
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.pages(name: "intro_client").url)
        func processData(data: Data) throws -> StaticContent {
            let response = try StaticContent(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func getReadyPageSupplier(completion: ((Result<StaticContent>) -> Void)?) {
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.pages(name: "intro_supplier").url)
        func processData(data: Data) throws -> StaticContent {
            let response = try StaticContent(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    
    func patchProfile(userModel : User, completion : ((Result<User>) -> Void)?) {
        var parameters: [String: Any] = userModel.toDictionary()
        
        
        print("HERERERE \n\n\nBODY : \(parameters)\nUrl : \(Endpoints.profile.url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .patch,
                                     endpoint: Endpoints.profile.url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader()
                                     )
        func processData(data: Data) throws -> User {
            let response = try User(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    
}

// MARK: Privacy
extension ApiServiceImp {
    
    func privacyPolicy(completion: ((Result<StaticContent>) -> Void)?) {
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.pages(name: "privacy_policy").url)
        func processData(data: Data) throws -> StaticContent {
            let response = try StaticContent(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func termsOfUse(completion: ((Result<StaticContent>) -> Void)?) {
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.pages(name: "terms_of_use").url)
        func processData(data: Data) throws -> StaticContent {
            let response = try StaticContent(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
}

// MARK: UserMiddle
extension ApiServiceImp {
    func specialization(completion: ((Result<[Specialization]>) -> Void)?) {
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.specialization.url)
        func processData(data: Data) throws -> [Specialization] {
            let response = try [Specialization](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func postConfirmation(photoId : String, rating : Int?, comment : String?, resultDate : Int?, completion : ((Result<LongPhotosession>) -> Void)? ) {
        var parameters: [String: Any] = [:]
        parameters["rating"] = rating
        parameters["comment"] = comment
        //parameters["results_date"] = resultDate
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let date = Date()
        if resultDate != nil {
        let today = Date()
        let tomorrow = Calendar.current.date(byAdding: .day, value: resultDate!, to: today)
        
            parameters["results_date"] = dateFormatter.string(from: tomorrow!)
        }
        print("HERERERE \n\n\nBODY : \(parameters)\nUrl : \(Endpoints.confirmation(id: photoId).url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.confirmation(id: photoId).url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> LongPhotosession {
            let response = try LongPhotosession(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getCandidates(type : UserType, id : String, completion : ((Result<[CandidatesResponse2]>) -> Void)?) {
        var parameters: [String: Any] = [:]
        parameters["type"] = type.rawValue
        parameters["count"] = 5
        
        print("HERERERE \n\n\nBODY : \(parameters)\nUrl : \(Endpoints.candidates(id: id).url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.candidates(id: id).url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [CandidatesResponse2] {
            let response = try [CandidatesResponse2](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func getUserPhotosessions(active : Bool, completion : ((Result<[Photosession]>) -> Void)?) {
        var parameters: [String: Any] = [:]
        if active {
            parameters["active"] = "true"
        } else {
            parameters["active"] = "false"
        }
        // parameters["count"] = 5
        
        print("HERERERE \n\n\nBODY : \(parameters)\nUrl : \(Endpoints.photosessions.url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.photosessions.url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [Photosession] {
            let response = try [Photosession](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getPhotoPhotosessions(tabIndex : TabIndex, id : String?, completion : ((Result<[Photosession]>) -> Void)?) {
        var parameters: [String: Any] = [:]
        if id != nil {
            parameters["for_user"] = id!
        } else {
        parameters["tab"] = tabIndex.rawValue
        }
        // parameters["count"] = 5
        
        print("HERERERE \n\n\nBODY : \(parameters)\nUrl : \(Endpoints.photosessions.url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.photosessions.url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [Photosession] {
            let response = try [Photosession](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func patchPhotosessions(photosession : LongPhotosession, completion : ((Result<LongPhotosession>) -> Void)?) {
        var parameters: [String: Any] = photosession.toDictionary()
      // parameters["tab"] = tabIndex.rawValue
        // parameters["count"] = 5
        
        print("HERERERE \n\n\nBODY : \(parameters)\nUrl : \(Endpoints.patchPhotosessions(id: photosession.id).url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .patch,
                                     endpoint: Endpoints.patchPhotosessions(id: photosession.id).url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> LongPhotosession {
            let response = try LongPhotosession(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getPhotoPhotosession(id : String, completion : ((Result<LongPhotosession>) -> Void)?) {
        
        print("URL : \(Endpoints.longPhotosession(id: id))\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.longPhotosession(id: id).url,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> LongPhotosession {
            let response = try LongPhotosession(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func putPhotoPhotosession(id : String, comment : String, completion : ((Result<Empty>) -> Void)?) {
        
        print("URL : \(Endpoints.longPhotosession(id: id).url)\nToken : \(self.makeTokenHeader())")
        var parameters : [String : Any] = [:]
        parameters["comment"] = comment
        let request = AirdronRequest(method: .put,
                                     endpoint: Endpoints.longPhotosession(id: id).url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> Empty {
            let response = try Empty(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getPhotoCount(completion : ((Result<PhotoCount>) -> Void)?) {
        
        print("URL : \(Endpoints.counterPhotosessions.url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.counterPhotosessions.url,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> PhotoCount {
            let response = try PhotoCount(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getProfilePromocode(completion : ((Result<Promocode>) -> Void)?) {
        
        print("URL : \(Endpoints.profilePromocode.url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.profilePromocode.url,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> Promocode {
            let response = try Promocode(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getCheckPromocode(code : String, completion : ((Result<CheckPromocode>) -> Void)?) {
        
        print("URL : \(Endpoints.checkPromocode(code: code).url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.checkPromocode(code: code).url,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> CheckPromocode {
            let response = try CheckPromocode(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getRefferalDiscount(completion : ((Result<RefferalDiscount>) -> Void)?) {
        
        print("URL : \(Endpoints.staticDiscount.url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.staticDiscount.url,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> RefferalDiscount {
            let response = try RefferalDiscount(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getRefferalLink(completion : ((Result<LinkModel>) -> Void)?) {
        
        print("URL : \(Endpoints.staticDiscount.url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.refferalLink.url,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> LinkModel {
            let response = try LinkModel(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getStaticPayments(completion : ((Result<staticPayments>) -> Void)?) {
        
        print("URL : \(Endpoints.staticPayments.url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.staticPayments.url,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> staticPayments {
            let response = try staticPayments(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
}

// MARK: Profile
extension ApiServiceImp {
    
    func uploadImage(byAsset asset: PHAsset, completion: ((Result<Image>) -> Void)?) {
        DispatchQueue.global().async {
            asset.resolveEditingImageAndCopy { [weak self] _, error, tempUrl, _ in
                guard let self = self else { return }
                if let error = error {
                    DispatchQueue.main.async {
                        completion?(.failure(AirdronError(error: error)))
                    }
                    return
                }
                guard let temp = tempUrl else {
                    DispatchQueue.main.async {
                        completion?(.failure(AirdronError.dataIsEmptyError()))
                    }
                    return
                }
                let request = AirdronRequest(method: .put, endpoint: Endpoints.upload.url, headers: self.makeTokenHeader())
                print("TOKEN: \(self.makeTokenHeader())")
                self.backgroundUploadManager.upload(request: request,
                                                    data: AirdronUrlResource(meta: .image(fileURL: temp)),
                                                    processData: { data in
                                                        let image = try Image.init(data: data)
                                                        return image
                },
                                                    completionHandler: completion)
            }
        }
    }
    func uploadImage(byFileUrl url: URL,
                     requestCompletionHandler: ((Result<AirdronDataRequestWrapper>) -> Void)?,
                     progressHandler: ((Double) -> Void)?,
                     completion: ((Result<Image>) -> Void)?) {
        let request = AirdronRequest(method: .put, endpoint: Endpoints.upload.url, headers: self.makeTokenHeader())
        
        self.backgroundUploadManager.upload(request: request,
                                            data: AirdronUrlResource(meta: .image(fileURL: url)),
                                            processData: { data in
                                                let image = try Image.init(data: data)
                                                return image
        },
                                            
                                            completionHandler: completion,
                                            requestCompletionHandler: requestCompletionHandler,
                                            progressHandler: progressHandler)
    }
    func uploadImagesInAlbum(albumImages: [Image], completion: ((Result<[Image]>) -> Void)?) {
        var parameters: [String: Any] = [:]
        parameters["images"] = albumImages.map { $0.id }
        
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.profileImages.url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> [Image] {
            let response = try [Image](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func uploadResults(id : String, albumImages: [Image], storage : String, visibility : Bool, completion: ((Result<Album>) -> Void)?) {
        var parameters: [String: Any] = [:]
        parameters["images"] = albumImages.map { $0.id }
        parameters["storage"] = storage
        parameters["visibility"] = visibility ? "PENDING" : "PRIVATE"
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.photoAlbum(id: id).url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Album {
            let response = try Album(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func patchPhotosessionsAlbum(id : String, visibility : Bool, completion: ((Result<Album>) -> Void)?) {
        var parameters: [String: Any] = [:]
        
        parameters["visibility"] = visibility ? "PUBLIC" : "PRIVATE"
        print("Token : \(self.makeTokenHeader())\nUrl : \(Endpoints.photosessionAlbum(id: id).url)\nparams : \(parameters)")
        let request = AirdronRequest(method: .patch,
                                     endpoint: Endpoints.photosessionAlbum(id: id).url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Album {
            let response = try Album(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func uploadAlbum(albumImages: [Image], album: AlbumUpload, completion: ((Result<Album>) -> Void)?) {
        var parameters: [String: Any] = [:]
        parameters["images"] = albumImages.map { $0.id }
        parameters["album"] = album.toDictionary()
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.profileImages.url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Album {
            let response = try Album(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func getProfile(userId : String?, completion : ((Result<User>) -> Void)?) {
        let parameters: [String: Any] = [:]
        
        let url = userId == nil ? Endpoints.profile.url : Endpoints.users(id: userId!).url
        print("HERE ZAPROS url : \(url)")
        let request = AirdronRequest(method: .get,
                                     endpoint: url,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> User {
            let response = try User(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func getAlbums(id : String?, completion : ((Result<[ShortAlbum]>) -> Void)?) {
        
        
        let url = id == nil ? Endpoints.profileAlbums.url : Endpoints.profileUserAlbums(id: id!).url
        print("Token : \(self.makeTokenHeader())\nURL : \(url)")
        let request = AirdronRequest(method: .get,
                                     endpoint: url,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> [ShortAlbum] {
            let response = try [ShortAlbum](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getFeed(specialization : String? ,  completion : ((Result<[FeedSection]>) -> Void)?) {
        //let url = id == nil ? Endpoints.profileImages.url : Endpoints.profileUserImages(id: id!).url
        print("Token : \(self.makeTokenHeader())\nUrl: \(Endpoints.feed.url)")
        var parameters : [String : Any] = [:]
        if specialization != nil {
            parameters["specialization"] = specialization
        }
        print(parameters)
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.feed.url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [FeedSection] {
            let response = try [FeedSection](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getImages(id : String?, completion : ((Result<[Image]>) -> Void)?) {
        
        
        
        let url = id == nil ? Endpoints.profileImages.url : Endpoints.profileUserImages(id: id!).url
        print("Token : \(self.makeTokenHeader())\nUrl: \(Endpoints.profileUserImages(id: id ?? "").url)")
        let request = AirdronRequest(method: .get,
                                     endpoint: url,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> [Image] {
            let response = try [Image](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func getAlbum(userId : String?, id : String, completion : ((Result<Album>) -> Void)?) {
        // print("Token : \(self.makeTokenHeader())\nUrl : \(Endpoints.profileUserAlbum(userId: userId!, id: id).url)")
        let url = userId == nil ? Endpoints.profileAlbum(id: id).url : Endpoints.profileUserAlbum(userId: userId!, id: id).url
        let request = AirdronRequest(method: .get,
                                     endpoint: url,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Album {
            let response = try Album(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getAlbumId(id : String, completion : ((Result<Album>) -> Void)?) {
        // print("Token : \(self.makeTokenHeader())\nUrl : \(Endpoints.profileUserAlbum(userId: userId!, id: id).url)")
       // let url = userId == nil ? Endpoints.profileAlbum(id: id).url : Endpoints.profileUserAlbum(userId: userId!, id: id).url
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.albumId(id: id).url,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Album {
            let response = try Album(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func getPhotosessionsOffers(type : UserType, id : String ,completion : ((Result<[UserOffers]>) -> Void)?) {
        
        let parameters : [String: Any] = ["type" : type.rawValue]
        print("URL \(Endpoints.photosessionsOffers(id: id).url)\nBody : \(parameters)\n Token : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.photosessionsOffers(id: id).url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [UserOffers] {
            let response = try [UserOffers](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func deletePhotosessionsOffers(photoId : String, comment : String, completion : ((Result<Success>) -> Void)?){
        let parameters : [String: Any] = ["comment" : comment]
        print("URL \(Endpoints.photosessionsOffers(id: photoId).url)\nBody : \(parameters)\n Token : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .put,
                                     endpoint: Endpoints.photosessionsOffers(id: photoId).url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> Success {
            let response = try Success(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func putPhotosessionOffer(photoId : String, comment : String, completion : ((Result<Success>) -> Void)?){
        let parameters : [String: Any] = ["comment" : comment]
        print("URL \(Endpoints.photoOffer(id: photoId).url)\nBody : \(parameters)\n Token : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .put,
                                     endpoint: Endpoints.photoOffer(id: photoId).url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> Success {
            let response = try Success(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func patchPhotosessionsoffer(id : String, price : Int? , comment : String, status : String, completion : ((Result<LongPhotosession>) -> Void)?) {
        var parameters : [String : Any] = [:]
        parameters["status"] = status
        parameters["price"] = price
        parameters["comment"] = comment
        print(parameters)
        let request = AirdronRequest(method: .patch,
                                     endpoint: Endpoints.photoOffer(id: id).url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> LongPhotosession {
            let response = try LongPhotosession(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }

    
}


// MARK: Portfolio
extension ApiServiceImp {
    
    func createPhotosession(CreatePhoto: CreatePhotosession, competition: ((Result<Photosession>) -> Void)?) {
        let parameters: [String: Any] = CreatePhoto.toDictionary()
        print("HERERERE \n\n\n\(parameters)\n\n\nUrl : \(Endpoints.createPhotosession.url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.createPhotosession.url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Photosession {
            let response = try Photosession(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: competition)
    }
    func postPrepayment(photoId : String, completion : ((Result<SuccessModel>) -> Void)?) {
        print("URL : \(Endpoints.photosessionPrepayment(id: photoId).url)\nToken : \(self.makeTokenHeader())")
        let parameters : [String : Any] = ["payment_type" : "CARD"]
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.photosessionPrepayment(id: photoId).url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> SuccessModel {
            let response = try SuccessModel(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func postParticipants(photoId : String, users : [String], competition: ((Result<[UserOffers]>) -> Void)?) {
        let parameters: [String: Any] = ["users": users]
        
        // [ id : "id1", id : "id2", id : "id3" ]
        print("HERERERE \n\n\n\(parameters)\n\n\nUrl : \(Endpoints.photosessionPartitipants(id: photoId).url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.photosessionPartitipants(id: photoId).url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> [UserOffers] {
            let response = try [UserOffers](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: competition)
    }
    func postOffers(photoId : String, users : [String], competition: ((Result<[UserOffers]>) -> Void)?) {
        let parameters: [String: Any] = ["users": users]
        
        // [ id : "id1", id : "id2", id : "id3" ]
        print("HERERERE \n\n\n\(parameters)\n\n\nUrl : \(Endpoints.photosessionsOffers(id: photoId).url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.photosessionsOffers(id: photoId).url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> [UserOffers] {
            let response = try [UserOffers](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: competition)
    }
    
    func takeDistricts(id: String, completion: ((Result<[District]>) -> Void)?) {
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.takeDistricts(id: id).url,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> [District] {
            let response = try [District](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func getBudgest(specialization : String, duration : Int, type : [String], completion : ((Result<[Budget]>) -> Void)?) {
        var parameters: [String: Any] = [:]
        parameters["specialization"] = specialization
        parameters["duration"] = duration
        parameters["participants_types"] = type
        // print("HERERERE \n\n\n\(parameters)\n\n\nUrl : \(Endpoints.createPhotosession.url)\nToken : \(self.makeTokenHeader())")
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.takeBudget.url,
                                     parameters : parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> [Budget] {
            let response = try [Budget](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
}


