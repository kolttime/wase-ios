//
//  staticPayments.swift
//  Wase
//
//  Created by Роман Макеев on 21/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation

struct staticPayments: Codable {
    
    let payPercent : Int
}

extension staticPayments {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(staticPayments.self, from: data)
    }
}
