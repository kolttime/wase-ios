//
//  LongPhotosession.swift
//  Taoka
//
//  Created by Роман Макеев on 06/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct LongPhotosession: Codable {
    
    var participantsTypes : [UserType]
    var description : String?
    var freedom : Bool?
    var comment : String?
    var budget : [Int]
    var id : String
    var offers : [Offers]?
    var status : PhotosessionStatus
    var time : String
    var duration : Int
    var title : String
    var date : String
    var author : UserShort?
    var album : ShortAlbum?
    var price : Int?
    var createdAt : String
    var specialization : Specialization
    var place : Place
    init(participantsTypes : [UserType], description : String? , freedom : Bool?, comment : String? ,budget : [Int] ,id : String, offers : [Offers], status : PhotosessionStatus, title : String, duration : Int, time : String, date : String, author : UserShort, price : Int, createdAt : String, specialization: Specialization, place : Place, album : ShortAlbum?) {
        self.participantsTypes =  participantsTypes
        self.description = description
        self.freedom = freedom
        self.album = album
        self.comment = comment
        self.budget = budget
        self.id = id
        self.author = author
        self.price = price
        self.offers = offers
        self.status = status
        self.title = title
        self.duration = duration
        self.time = time
        self.date = date
        self.createdAt = createdAt
        self.specialization = specialization
        self.place = place
    }
    func toDictionary() -> JsonDictionary {
        var dict : [String : Any] = [:]
        dict["date"] = self.date
        dict["time"] = self.time
        dict["duration"] = self.duration
        var placeDict : [String : Any] = [:]
        placeDict["type"] = self.place.type.rawValue
        if self.place.address != nil {placeDict["address"] = self.place.address!}
        if self.place.district != nil {placeDict["district"] = self.place.district!.id!}
        dict["place"] = placeDict
        dict["participants_types"] = self.participantsTypes.map({$0.rawValue})
        dict["budget"] = self.budget
        dict["title"] = self.title
        if self.description != nil {dict["description"] = self.description!}
        return dict
    }
    func toShort() -> Photosession {
        return Photosession(id: self.id, offers: self.offers, status: self.status, title: self.title, duration: self.duration, time: self.time, date: self.date, author: self.author, price: self.price, createdAt: self.createdAt, specialization: self.specialization, place: self.place, album: self.album)
    }
    
}
extension LongPhotosession {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(LongPhotosession.self, from: data)
    }
}

extension Array where Element == LongPhotosession {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([LongPhotosession].self, from: data)
    }
}


