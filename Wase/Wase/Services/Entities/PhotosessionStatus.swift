//
//  UserType.swift
//  OMG
//
//  Created by Roman Makeev on 15/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

public enum PhotosessionStatus: String {
    
    case inbox = "INBOX"
    case waiting = "WAITING"
    case created = "CREATED"
    case selection = "SELECTION"
    case prepayment = "PREPAYMENT"
    case execution = "EXECUTION"
    case unselected = "UNSELECTED"
    case confirmation = "CONFIRMATION"
    case pretension = "PRETENSION"
    case results = "RESULTS"
    case canceled = "CANCELLED"
    case annuled = "ANNULED"
    case completed = "COMPLETED"
    case nostatus = "NO STATUS"
    case payProcess = "PAYPROCESS"
    
    
 
}

extension PhotosessionStatus: Codable {
    
}

public enum OffersStatus : String {
    //("INBOX", "REJECTED", "ACCEPTED", "PARTICIPANT")
    case inbox = "INBOX"
    case rejected = "REJECTED"
    case accepted = "ACCEPTED"
    case participant = "PARTICIPANT"
}
extension OffersStatus: Codable {
    
}


