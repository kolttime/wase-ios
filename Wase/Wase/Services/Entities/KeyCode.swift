//
// User.swift
// OMG
//
// Created by Roman Makeev on 06/10/2018.
// Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct KeyCode: Codable {
    
    let key: String
    
    
    init(key: String
        ) {
        self.key = key }
    
}

extension KeyCode {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(KeyCode.self, from: data)
    }
}

extension Array where Element == KeyCode {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([KeyCode].self, from: data)
    }
}

