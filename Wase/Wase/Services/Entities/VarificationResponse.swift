//
//  VarificationResponse.swift
//  Taoka
//
//  Created by Роман Макеев on 14/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct VerificationResponse: Codable {
    
    let success: Bool
    let token: String?
    let user: User?
}

extension VerificationResponse {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        //decoder.dateDecodingStrategy = .formatted(DateFormatter.dateDateFormatter())
        self = try decoder.decode(VerificationResponse.self, from: data)
    }
}
