//
//  SuccessModel.swift
//  Taoka
//
//  Created by Роман Макеев on 06/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//



import Foundation

struct SuccessModel: Codable {
    
    let link : String
    let price : Int
}

extension SuccessModel {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(SuccessModel.self, from: data)
    }
}
