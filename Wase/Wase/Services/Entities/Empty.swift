//
//  Empty.swift
//  Wase
//
//  Created by Роман Макеев on 28/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation

struct Empty: Codable {
    
    
    
}

extension Empty {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Empty.self, from: data)
    }
}
