//
//  UserType.swift
//  OMG
//
//  Created by Roman Makeev on 15/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

public enum UserType: String {
    
    case photographer = "PHOTOGRAPHER"
    case client = "CLIENT"
    case makeUpArtist = "MAKEUP_ARTIST"
    case other = "OTHER"
}

extension UserType: Codable {
    

    
}
