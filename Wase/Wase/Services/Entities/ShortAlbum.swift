//
//  ShortAlbum.swift
//  Taoka
//
//  Created by Роман Макеев on 16/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct ShortAlbum: Codable {
    
    
    let id: String
    let title : String
    let specialization : Specialization
    let date : String
    let images : [Image]
    
    
    
    init(id : String, title : String, specialization : Specialization, date : String, images : [Image]  ){
        self.id = id
        self.title = title
        self.images = images
        self.date = date
        self.specialization = specialization
    }
}

extension ShortAlbum {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(ShortAlbum.self, from: data)
    }
}

extension Array where Element == ShortAlbum {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([ShortAlbum].self, from: data)
    }
}
