//
//  LinkModel.swift
//  Wase
//
//  Created by Роман Макеев on 17/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation



struct LinkModel: Codable {
    
    let link: String
}

extension LinkModel {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(LinkModel.self, from: data)
    }
}
