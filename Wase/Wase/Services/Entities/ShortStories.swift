import Foundation

struct ShortStories: Codable {
    
    var id : String
    var title : String?
    var cover : Image
    var slidesCount : Int
    
    
    
    init(id : String, title : String?, cover : Image, slidesCount : Int){
        self.id = id
        self.title = title
        self.cover = cover
        self.slidesCount = slidesCount
    }
    
    
   
}

extension ShortStories {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(ShortStories.self, from: data)
    }
}

extension Array where Element == ShortStories {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([ShortStories].self, from: data)
    }
}
