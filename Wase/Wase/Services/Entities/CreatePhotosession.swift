//
//  userCreation.swift
//  Taoka
//
//  Created by Роман Макеев on 14/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

class CreatePhotosession: DictionaryMappable {
    
    
    var specialization : Specialization?
    var date : String?
    var time : String?
    var duration : Int?
    var place : PlaceTypes = PlaceTypes()
    var participantsTypes : [ParticipantsTypes]?
    var budget : [Int]?
    var title : String?
    var description : String?
    
    
    init() {
        self.specialization = nil
        self.date = nil
        self.time = nil
        self.duration = nil
        //self.place =
        self.participantsTypes = nil
        self.budget = nil
        self.title = nil
        self.description = nil
    }
    
    init(specialization : Specialization?, date : String?, time : String?, duration : Int?, place : PlaceTypes, participantsTypes : [ParticipantsTypes], budget : [Int]?, title : String?, description : String?) {
        self.specialization = specialization
        self.date = date
        self.time = time
        self.duration = duration
        self.place = place
        self.participantsTypes = participantsTypes
        self.budget = budget
        self.title = title
        self.description = description
    }
    
    func toDictionary() -> JsonDictionary {
        
        print("PARAMETERS \n\n\nSpecialization : \(self.specialization)\n")
        print("Date :  \(self.date)\n")
        print("Time :  \(self.time)\n")
        print("Duration :  \(self.duration)\n")
        print("Place :  \(self.place)\n")
        print("ParticipantsTypes :  \(self.participantsTypes)\n")
        print("Budget :  \(self.budget)\n")
        print("Title :  \(self.title)\n")
        print("Description :  \(self.description)\n")
        
        guard let specialization  = self.specialization,
            let date = self.date,
            let time = self.time,
            let duration = self.duration,
            //let place = place.toDictionary(),
            let participantsTypes  = self.participantsTypes,
            let budget = self.budget,
            let title = self.title else { return [:] }
            let description = self.description ?? ""
        var types : [String] = []
        for type in participantsTypes {
            types.append(type.rawValue)
        }
            let parameters: [String: Any] = ["specialization": specialization.id,
                                         "date": date,
                                         "time": time,
                                         "duration": duration,
                                         "place": self.place.toDictionary(),
                                         "participants_types" : types,
                                         "budget": budget,
                                         "title": title,
                                         "description": description
                                        ]
            return parameters
    }
    
}

