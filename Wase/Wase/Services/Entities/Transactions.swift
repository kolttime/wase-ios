//
//  Finances.swift
//  Wase
//
//  Created by Роман Макеев on 19/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation

struct Transactions: Codable {
    
    let datetime : String
    let from : UserShort
    let sent : Float
    
    
    init(datetime : String, from : UserShort, sent : Float) {
        self.datetime = datetime
        self.from = from
        self.sent = sent
    }
    
}

extension Transactions {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Transactions.self, from: data)
    }
}

extension Array where Element == Transactions {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Transactions].self, from: data)
    }
}
