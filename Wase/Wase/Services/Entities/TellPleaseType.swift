//
//  TellPleaseType.swift
//  Wase
//
//  Created by Роман Макеев on 26/09/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation


public enum TellPleaseType: String {
    
    case photoCancel = "PHOTOCANCEL"
    case clientCancel = "CLIENTCANCEL"
    case feedBack = "FEEDBACK"
    case confirmation = "CONFIRMATION"
}
