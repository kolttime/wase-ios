//
//  Promocode.swift
//  Wase
//
//  Created by Роман Макеев on 23/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation

struct Promocode: Codable {
    
    let _id : String?
    let code : String?
    let discount : Int?
    let title : String?
}

extension Promocode {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Promocode.self, from: data)
    }
}

struct CheckPromocode: Codable {
    
    let promocode : Promocode?
    let available : Bool
    let reason : String?
}

extension CheckPromocode {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(CheckPromocode.self, from: data)
    }
}
