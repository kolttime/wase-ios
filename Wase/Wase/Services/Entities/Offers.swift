//
//  Specializations.swift
//  Taoka
//
//  Created by Роман Макеев on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct Offers: Codable {
    
    var type : UserType?
    var pending : Int?
    var accepted : Int?
    var participant : UserShort?
    var price : Int?
    var new : Int?
    
    
    init() {
        self.type = nil
        self.pending = nil
        self.participant = nil
        self.accepted = nil
        self.price = nil
        self.new = nil
    }
    
    init(type : UserType?, pending : Int?, participant : UserShort, accepted : Int?, price : Int?, new : Int?) {
        self.type = type
        self.pending = pending
        self.participant = participant
        self.accepted = accepted
        self.price = price
        self.new = new
    }
    
    
    //    func toDictionary() -> JsonDictionary {
    //
    //        var dict : [String : Any] = [:]
    //        dict["type"] = self.type?.rawValue
    //        return dict
    //
    //    }
}

extension Offers {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Offers.self, from: data)
    }
}

extension Array where Element == Offers {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Offers].self, from: data)
    }
}




