//
//  Specializations.swift
//  Taoka
//
//  Created by Роман Макеев on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct Place: Codable {
    
    
    var district : District?
    var address : String?
    var type : PlaceType
    
    
    init(district : District?, address: String?, type : PlaceType) {
        self.district = district
        self.address = address
        self.type = type
    }
    
    
    //    func toDictionary() -> JsonDictionary {
    //
    //        var dict : [String : Any] = [:]
    //        dict["type"] = self.type?.rawValue
    //        return dict
    //
    //    }
}


public enum PlaceType: String {
    
    case home = "HOME"
    case outdoors = "OUTDOORS"
    case studio = "STUDIO"
}

extension PlaceType: Codable {
    
    
    
}
extension Place {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Place.self, from: data)
    }
}

extension Array where Element == Place {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Place].self, from: data)
    }
}




