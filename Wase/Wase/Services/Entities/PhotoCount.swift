//
//  PhotoCount.swift
//  Wase
//
//  Created by Роман Макеев on 21/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation

struct PhotoCount: Codable {
    
    let photosessions : Int
}

extension PhotoCount {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(PhotoCount.self, from: data)
    }
}
struct RefferalDiscount: Codable {
    
    let referralDiscount : Int
}

extension RefferalDiscount {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(RefferalDiscount.self, from: data)
    }
}
