//
//  ProfileCard.swift
//  Wase
//
//  Created by Роман Макеев on 19/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//


import Foundation

struct Card: Codable {
    
    let number : String
    let expiration : String
    let holder : String
    
    init(number : String, expiration : String, holder : String) {
        self.number = number
        self.expiration = expiration
        self.holder = holder
    }
    
}

extension Card {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Card.self, from: data)
    }
}

extension Array where Element == Card {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Card].self, from: data)
    }
}

