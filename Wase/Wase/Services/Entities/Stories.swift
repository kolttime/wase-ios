//
//  Stories.swift
//  Wase
//
//  Created by Роман Макеев on 30/08/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation

struct Stories: Codable {
    
    var id : String
    var title : String?
    var cover : Image?
    var slides : [Image]
    
    
    
    
    init(id : String, title : String?, cover : Image?, slides : [Image]){
        self.id = id
        self.title = title
        self.cover = cover
        self.slides = slides
    }
    
    
    
}

extension Stories {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Stories.self, from: data)
    }
}

extension Array where Element == Stories {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Stories].self, from: data)
    }
}

