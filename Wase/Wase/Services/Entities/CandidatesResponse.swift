//
//  Finances.swift
//  Wase
//
//  Created by Роман Макеев on 19/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation

struct CandidatesResponse: Codable {
    
    var user : UserShort
    var offer : Bool
    
    
    init(user : UserShort, offer : Bool) {
        self.user = user
        self.offer = offer
    }
    
}

extension CandidatesResponse {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(CandidatesResponse.self, from: data)
    }
}

extension Array where Element == CandidatesResponse {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([CandidatesResponse].self, from: data)
    }
}

struct CandidatesResponse2: Codable {
    
    let candidate : CandidatesResponse
    
    
    init(candidate : CandidatesResponse) {
        self.candidate = candidate
    }
    
}

extension CandidatesResponse2 {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(CandidatesResponse2.self, from: data)
    }
}

extension Array where Element == CandidatesResponse2 {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([CandidatesResponse2].self, from: data)
    }
}
