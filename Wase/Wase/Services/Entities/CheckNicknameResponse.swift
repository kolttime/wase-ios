//
// User.swift
// OMG
//
// Created by Roman Makeev on 06/10/2018.
// Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct CheckNicknameResponse: Codable {
    
    let available: Bool
    
    
    init(available: Bool
        ) {
        self.available = available }
    
}

extension CheckNicknameResponse {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(CheckNicknameResponse.self, from: data)
    }
}

extension Array where Element == CheckNicknameResponse {

    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([CheckNicknameResponse].self, from: data)
    }
}

