//
//  Specializations.swift
//  Taoka
//
//  Created by Роман Макеев on 15/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation

struct UserOffers: Codable {
    
    var user : UserShort
    var status : OffersStatus
    var price : Int?
    var comment : String?
    var createdAt : String
    var acceptedAt : String?
    var viewed : Bool
    
    
    
    init(user : UserShort, status : OffersStatus, price : Int?, comment : String?, createdAt : String, acceptedAt : String?, viewed : Bool) {
      
        self.user = user
        self.status = status
        self.price = price
        self.comment = comment
        self.createdAt = createdAt
        self.acceptedAt = acceptedAt
        self.viewed = viewed
    }
    
    
    //    func toDictionary() -> JsonDictionary {
    //
    //        var dict : [String : Any] = [:]
    //        dict["type"] = self.type?.rawValue
    //        return dict
    //
    //    }
}

extension UserOffers {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(UserOffers.self, from: data)
    }
}

extension Array where Element == UserOffers {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([UserOffers].self, from: data)
    }
}




