//
//  Finances.swift
//  Wase
//
//  Created by Роман Макеев on 19/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation

struct Finances: Codable {
    
    let card : Card?
    let transactions : [Transactions]?
    
    
    init(card : Card?, transactions : [Transactions]?) {
        self.card = card
        self.transactions = transactions
    }
    
}

extension Finances {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Finances.self, from: data)
    }
}

extension Array where Element == Finances {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Finances].self, from: data)
    }
}
