//
//  FeedSection.swift
//  Wase
//
//  Created by Роман Макеев on 26/09/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct FeedSection: Codable {
    
    let type : FeedType
    let title : String
    //let items : AnyObject
    let albums : [ShortAlbum]?
    let users : [UserShort]?
    
    init(type : FeedType, title : String, albums : [ShortAlbum]?, users : [UserShort]?) {
        self.type = type
        self.title = title
        self.albums = albums
        self.users = users
        
    }
    
}

extension FeedSection {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(FeedSection.self, from: data)
    }
}

extension Array where Element == FeedSection {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([FeedSection].self, from: data)
    }
}
public enum FeedType: String {
    
    case albums = "ALBUMS"
    case users = "USERS"
}

extension FeedType: Codable {
    
    
    
}
