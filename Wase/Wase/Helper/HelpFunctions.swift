//
//  Functions.swift
//  Taoka
//
//  Created by Minic Relocusov on 30/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


public func roundNumber(num : Float) -> Int{
    let full : Int = Int(num)
    if Float(full) == num {
        return full
    } else {
        return full + 1
    }
}

public func getCardHeight() -> CGFloat {
    var height : CGFloat = Device.isIphoneXSmax || Device.isIphoneX ? 180 : 160
    if Device.isIphone5 {
        height = 145
    }
    return height
}


class HelpFunctions  {
    static public func setButtonOffset(view : UIView, scrollView : UIView, frameView : UIView, navBar : UIView, btnHeight : CGFloat = 56, bottomOffset : CGFloat = 58, standartOffset : CGFloat = 40) -> CGFloat {
        let navheight = navBar.frame.height + UIApplication.shared.statusBarFrame.height
        let height = UIScreen.main.bounds.height
        let frame : CGRect = view.convert(frameView.frame, from: scrollView)
        let maxY = frameView.frame.maxY
        let interval = height - maxY - bottomOffset - btnHeight - navheight
        print("maxY : \(frameView.frame.maxY)\nheight : \(height)\ninterval : \(interval)\nframe \(frame)\nframeFrameView : \(frameView.frame)")
        if interval < standartOffset {
            return standartOffset
        } else {
            return interval
        }
    }
    
    static public func getNavigationHeight(navBar : UIView) -> CGFloat {
        return UIApplication.shared.statusBarFrame.height + navBar.frame.height
    }
    static func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "+X (XXX) XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    static func setScrollOffset(scrollView : UIScrollView, view : UIView ,activeView : UIView, keyHeight : CGFloat = 500, offset : CGFloat = 0, navBar : UIView = UIView(frame: CGRect.zero)){
        let textMaxY = scrollView.convert(activeView.frame, to : view).maxY + offset - navBar.frame.height
        let keyMinHeight = view.frame.height - keyHeight
        print("textMaxY : \(textMaxY)\nKeyHeight : \(keyHeight)\nkeyMinHeigh : \(keyMinHeight)")
        
        if textMaxY > keyMinHeight {
            let scrollOffset = textMaxY - keyMinHeight
            print("scrollOffset : \(scrollOffset)")
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollOffset), animated: true)
        } else {
           // scrollView.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    
    
    static func checkPhotosessionChanged(first : LongPhotosession, second : LongPhotosession) -> Bool{
        print(first.time)
        print(second.time)
        if first.title != second.title {return true}
        if first.budget != second.budget {return true}
        if first.comment != second.comment {return true}
        if first.description != second.description {return true}
        if first.time != second.time {return true}
        if first.specialization.id != second.specialization.id {return true}
        if first.date != second.date {return true}
        if first.participantsTypes.count != second.participantsTypes.count {return true}
        if first.place.address != second.place.address {return true}
        if first.duration != second.duration {return true}
        
        
        return false
    }
    
    static public func userModelChanged(first : User, second : User) -> Bool {
        
        if first.givenName != second.givenName {return true}
        if first.familyName != second.familyName {return true}
        if first.nickname != second.nickname {return true}
        if first.avatar != second.avatar {return true}
        if first.city.id != second.city.id {return true}
        if first.description != second.description {return true}
        if checkSpecializations(first: first.services?.specializations ?? [], second: second.services?.specializations ?? []) {
            return true
        }
        if first.services?.workDays != second.services?.workDays {
            return true
        }
        if first.services?.vacant != second.services?.vacant {
            return true
        }
        if first.services?.mobile != second.services?.vacant{
            return true
        }
        if first.services?.price != second.services?.price {
            return true
        }
        if first.services?.workHours != second.services?.workHours {
            return true
        }
        
        return false
    }
    
    static func checkSpecializations(first : [Specialization], second : [Specialization]) -> Bool {
        
        if first.count != second.count {
            return true
        }
        
        for spec in first {
            if !second.contains(spec) {
                return true
            }
        }
        
        return false
    }
    

    static func convertModelToPhotosessionCell(photoModels : [Photosession], settingSelection : ((String) -> Void)?, candidatesSelection : ((UserType, String) -> Void)?, userSelection : ((String, String?) -> Void)?, accepcSelection : ((String, Int) -> Void)? , approveSelection : ((UserType, String) -> Void)?, openAlbumSelection : ((String) -> Void)?, openConfirmation : ((String) -> Void)?      ) -> [TableCellViewModel]{
        var viewModels : [TableCellViewModel] = []
        
            for model in photoModels {
                if model.status == .canceled {
                    continue
                }
            var viewModel : TableCellViewModel
            var stage = 0
            switch model.status {
            case .created:
            stage = 0
            case .prepayment:
            stage = 2
            case .selection:
            stage = 1
            case .execution:
                stage = 3
            case .confirmation:
                stage = 4
            case .results:
                stage = 5
            case .completed:
                stage = 6
            case .payProcess:
                stage = -2
                
            case .pretension:
                stage = -1
            default:
            print("ИИИИИ")
        
            }
            let dateFormatter = DateFormatter()
            let calendar = Calendar.current
            //let DATE = Date()
            let year = calendar.component(.year, from: Date())
            dateFormatter.locale =  Locale(identifier: "ru_MD")
            dateFormatter.dateFormat = "YYYY-MM-dd"
            let dates = dateFormatter.date(from: model.date)
            let year2 = calendar.component(.year, from: dates!)
            if year != year2 {
            dateFormatter.dateFormat = "dd MMMM YYYY"
            } else {
            dateFormatter.dateFormat = "dd MMMM"
            }
            let stringDate = dateFormatter.string(from: dates!)
        
        
                let bottomSelection : ((String) -> Void)? = {id in
                    if model.status == .prepayment {
                        accepcSelection?(id, model.price ?? 0)
                    } else if model.status == .execution {
                        openAlbumSelection?(id)
                    } else if model.status == .confirmation {
                        openConfirmation?(id)
                    } else if model.status == .completed {
                        openAlbumSelection?(model.album!.id)
                    }
                }
            let selection : ((UserType, String) -> Void)? = {type, id in
           // self?.selection?(type, id)
                
                if type == .photographer {
                    var phoneNumber : String? = nil
                    if model.offers![0].participant?.phone != nil {
                        phoneNumber = model.offers![0].participant!.phone!
                    }
                    if model.offers![0].participant != nil {
                        if model.status == .execution || model.status == .results || model.status == .completed || model.status == .confirmation {
                            userSelection?(model.offers![0].participant!.id, phoneNumber)
                        } else {
                            if model.status != .annuled && model.status != .canceled && model.status != .pretension {
                                approveSelection?(type, id)
                            }
                        }
                    } else if model.offers![0].pending != nil && model.offers![0].pending != 0 || model.offers![0].accepted != nil && model.offers![0].accepted != 0 {
                        
                        approveSelection?(type, id)
                        
                    } else {
                    
                        candidatesSelection?(type , id)
                    }
                    
                } else {
                    if model.offers![1].participant != nil {
                        var phoneNumber : String? = nil
                        if model.offers![1].participant?.phone != nil {
                            phoneNumber = model.offers![1].participant!.phone!
                        }
                        if model.status == .execution || model.status == .results || model.status == .completed || model.status == .confirmation {
                            userSelection?(model.offers![1].participant!.id, phoneNumber)
                        } else {
                            if model.status != .annuled && model.status != .canceled && model.status != .pretension {
                                approveSelection?(type, id)
                            }
                        }
                    } else if model.offers![1].pending != nil && model.offers![1].pending != 0 || model.offers![1].accepted != nil && model.offers![1].accepted != 0 {
                        
                        approveSelection?(type, id)
                        
                    } else {
                        
                        candidatesSelection?(type , id)
                    }
                }
                
            }
            var durationString = ""
            switch model.duration {
            case 1:
            durationString = "1 час"
            case 2:
            durationString = "2 часа"
            case 3:
            durationString = "3 часа"
            case 4:
            durationString = "4 часа"
            default:
            durationString = "\(model.duration) часов"
        
            }
                let title = model.title
                let date = "\(stringDate) c \(model.time), \(durationString)"
                
                var urls = ["", ""]
                var count1 : NSMutableAttributedString
                var proffessionName1 : String
                var count2 : NSMutableAttributedString
                var proffesionName2 : String
                var new2 : Int? = nil
                var new1 : Int? = nil
                if model.offers![0].participant != nil {
                    urls[0] = model.offers![0].participant?.avatar?.original.absoluteString ?? ""
                    count1 = NSMutableAttributedString.init(attributedString: CustomFont.bodyRegular17.attributesWithParagraph.make(string: "\(model.offers![0].participant!.givenName) \(model.offers![0].participant!.familyName)"))
                    proffessionName1 = "Фотограф, \(model.offers![0].price!) ₽"
                } else {
                    var zayavka = ""
                    switch model.offers![0].pending! {
                    case 0:
                        zayavka = "Заявок"
                        break
                    case 1:
                        zayavka = "Заявка"
                        break
                    case 2:
                        zayavka = "Заявки"
                        break
                    case 3:
                        zayavka = "Заявки"
                        break
                    case 4:
                        zayavka = "Заявки"
                        break
                    case 11 :
                        zayavka = "Заявок"
                        break
                    case 12 :
                        zayavka = "Заявок"
                        break
                    case 13 :
                        zayavka = "Заявок"
                        break
                    case 14 :
                        zayavka = "Заявок"
                        break
                    default :
                        zayavka = "Заявок"
                    }
                    proffessionName1 = "Фотограф"
                    if model.offers![0].new != 0 {
                        new1 = model.offers![0].new
                    }
                    count1 = NSMutableAttributedString.init(attributedString: CustomFont.bodyRegular17.attributesWithParagraph.make(string: "\(model.offers![0].pending!) \(zayavka)"))
                    if model.offers![0].accepted != nil && model.offers![0].accepted != 0 {
                        count1.append(CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: " ･ "))
                        var otcklick = ""
                        switch model.offers![0].accepted! {
                        case 1:
                            otcklick = "1 Отклик"
                        case 2:
                            otcklick = "2 Отклика"
                        case 3:
                            otcklick = "3 Отклика"
                        case 4:
                            otcklick = "4 Отклика"
                        default:
                            otcklick = "\(model.offers![0].accepted!) Отклика"
                        }
                        count1.append(CustomFont.bodyRegular17.attributesWithParagraph.make(string: otcklick))
                    }
                    
                }
                if model.offers!.count == 1 {
                    if model.status == .created || model.status == .selection || model.status == .execution || model.status == .results || model.status == .pretension || model.status == .payProcess {
                        var new  = 0
                        if model.status == .selection {
                            if model.offers != nil {
                                new = model.offers![0].new ?? 0
                            }
                        }
                        var onCall : ((UserType) -> Void)? = {type in
                            if type == .photographer {
                                print(model.offers![0].participant!.phone!)
                                guard let number = URL(string: "tel://" + model.offers![0].participant!.phone!) else { return }
                                UIApplication.shared.open(number)
                                print("call")
                            } else {
                                guard let number = URL(string: "tel://" + model.offers![1].participant!.phone!) else { return }
                                UIApplication.shared.open(number)
                            }
                        }
                        let viewModel = UserPhotosessionsCellModelType1.init(name: title, date: date, urls: urls, type: "", count: count1, proffesionName: proffessionName1, stage: stage, id: model.id, selection: selection, settingsSelection: settingSelection , new: new, onCall: onCall)
                        viewModels.append(viewModel)
                    } else {
                        let viewModel = UserPhotosessionsCellModelType4.init(name: title, date: date, urls: urls, stage: stage, count: count1, id: model.id, selection: selection, settingsSelection: settingSelection, bottomSelection: bottomSelection, proffesionName: proffessionName1, onCall: {type in
                            if type == .photographer {
                                print(model.offers![0].participant!.phone!)
                                guard let number = URL(string: "tel://" + model.offers![0].participant!.phone!) else { return }
                                UIApplication.shared.open(number)
                                print("call")
                            } else {
                                guard let number = URL(string: "tel://" + model.offers![1].participant!.phone!) else { return }
                                UIApplication.shared.open(number)
                            }
                        }, new: 0)
                        viewModels.append(viewModel)
                        
                    }
                } else {
                    if model.offers![1].participant != nil {
                        urls[1] = model.offers![1].participant?.avatar?.original.absoluteString ?? ""
                        count2 = NSMutableAttributedString.init(attributedString: CustomFont.bodyRegular17.attributesWithParagraph.make(string: "\(model.offers![1].participant!.givenName) \(model.offers![1].participant!.familyName)"))
                        proffesionName2 = "Визажист, \(model.offers![1].price!) ₽"
                    } else {
                        var zayavka = ""
                        switch model.offers![1].pending! {
                        case 0:
                            zayavka = "Заявок"
                            break
                        case 1:
                            zayavka = "Заявка"
                            break
                        case 2:
                            zayavka = "Заявки"
                            break
                        case 3:
                            zayavka = "Заявки"
                            break
                        case 4:
                            zayavka = "Заявки"
                            break
                        case 11 :
                            zayavka = "Заявок"
                            break
                        case 12 :
                            zayavka = "Заявок"
                            break
                        case 13 :
                            zayavka = "Заявок"
                            break
                        case 14 :
                            zayavka = "Заявок"
                            break
                        default :
                            zayavka = "Заявок"
                        }
                        proffesionName2 = "Визажист"
                        if model.offers![1].new != 0 {
                            new2 = model.offers![1].new
                        }
                        count2 = NSMutableAttributedString.init(attributedString: CustomFont.bodyRegular17.attributesWithParagraph.make(string: "\(model.offers![1].pending!) \(zayavka)"))
                        if model.offers![1].accepted != nil && model.offers![1].accepted != 0 {
                            count2.append(CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: " ･ "))
                            var otcklick = ""
                            switch model.offers![1].accepted! {
                            case 1:
                                otcklick = "1 Отклик"
                            case 2:
                                otcklick = "2 Отклика"
                            case 3:
                                otcklick = "3 Отклика"
                            case 4:
                                otcklick = "4 Отклика"
                            default:
                                otcklick = "\(model.offers![1].accepted!) Отклика"
                            }
                            count2.append(CustomFont.bodyRegular17.attributesWithParagraph.make(string: otcklick))
                        }
                        
                    }
                        
                        if model.status == .created || model.status == .selection || model.status == .execution || model.status == .results || model.status == .pretension || model.status == .payProcess{
                            var new1 = 0
                            var new2 = 0
                            if model.offers != nil {
                                new1 = model.offers![0].new ?? 0
                                new2 = model.offers![1].new ?? 0
                            }
                            let viewModel = UserPhotosessionsCellModelType2.init(name: title, date: date, urls: urls, count1: count1, proffesionName1: proffessionName1, count2: count2, proffesionName2: proffesionName2, stage: stage, id: model.id, selection: selection, settingsSelection: settingSelection, new1: new1, new2: new2, onCall: {type in
                                if type == .photographer {
                                    print(model.offers![0].participant!.phone!)
                                    guard let number = URL(string: "tel://" + model.offers![0].participant!.phone!) else { return }
                                    UIApplication.shared.open(number)
                                    print("call")
                                } else {
                                    guard let number = URL(string: "tel://" + model.offers![1].participant!.phone!) else { return }
                                    UIApplication.shared.open(number)
                                }
                            })
                            viewModels.append(viewModel)
                        } else {
                            let viewModel = UserPhotosessionsCellModelType3.init(name: title, date: date, urls: urls, count1: count1, proffesionName1: proffessionName1, count2: count2, proffesionName2: proffesionName2, stage: stage, id: model.id, selection: selection, settingsSelection: settingSelection, bottomSelection: bottomSelection, onCall: {type in
                                if type == .photographer {
                                    guard let number = URL(string: "tel://" + model.offers![0].participant!.phone!) else { return }
                                    UIApplication.shared.open(number)
                                    print("Call")
                                } else {
                                    guard let number = URL(string: "tel://" + model.offers![1].participant!.phone!) else { return }
                                    UIApplication.shared.open(number)
                                }
                            }, new1: 0, new2: 0)
                            viewModels.append(viewModel)
                            
                        }
                        
                    
                }
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
           
        
        
        
            }
        
        return viewModels
        
    }
}
