//
//  Offsets.swift
//  Taoka
//
//  Created by Minic Relocusov on 11/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class ViewSize {
    
    static var sideOffset: CGFloat {
        if Device.isIphone5 {
            return 19
        } else {
            return 24
        }
    }
}

