//
//  StoriesView.swift
//  Wase
//
//  Created by Роман Макеев on 30/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class StoriesView : AirdronView, UIGestureRecognizerDelegate {
    var images : [Image] = []
    var top : Constraint?
    var centerX : Constraint?
    var left : Constraint?
    var width : Constraint?
    var height : Constraint?
    let imageView = UIImageView()
    var stories : Stories?
    var id : String
    var close : Action?
    var animate : Action?
    var storyBarview = StoryBarView()
    var onNextBlock : Action?
    var onPrevBlock : Action?
    var count : Int
    private var apiService : ApiService
    init(id : String, count : Int, apiService : ApiService) {
        self.id = id
        self.apiService = apiService
        self.count = count
        super.init(frame: .zero)
    }
    var contentView = UIButton()
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var maxTopOffset : CGFloat = 0
    var maxWidthOffset : CGFloat = 0
    
    override func initialSetup() {
        super.initialSetup()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = Color.red.value
        self.addSubview(self.contentView)
        //contentView.frame = self.frame
        self.contentView.backgroundColor = Color.black.value
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.recognizing(recognizer:)))
        pan.delegate = self
        let tap = UILongPressGestureRecognizer(target: self, action: #selector(self.tapecognizing(recognizer:)))
        tap.delegate = self
        self.contentView.addGestureRecognizer(tap)
        self.contentView.addSubview(self.imageView)
        
        //self.contentView.addGestureRecognizer(tap)
        self.contentView.addGestureRecognizer(pan)
        //self.contentView.frame.equalTo(self.frame)
        maxTopOffset = UIScreen.main.bounds.size.height / 9
        maxWidthOffset = UIScreen.main.bounds.size.width / 5
        self.contentView.addSubview(self.storyBarview)
        //self.storyBarview.startAnimate()
        self.storyBarview.setLines(count: count)
        self.storyBarview.onNext = {[weak self] index in
            if index < self?.images.count ?? 0 {
                if self?.images.count ?? 0 > 0 {
                    //self?.imageView.kf.setImage(with: URL(string: self?.images[index] ?? ""))
                   // self?.imageView.kf.setImage(with: URL(string : self?.images[index].preview ?? ""))
                    self?.imageView.kf.setImage(with: self?.images[index].original)
                }
            }
        }
        self.storyBarview.onPrev = {[weak self] index in
            if index >=  0 {
                if self?.images.count ?? 0 > 0 {
                    //self?.imageView.kf.setImage(with: URL(string : self?.images[index].preview ?? ""))
                    self?.imageView.kf.setImage(with: self?.images[index].original)
                }
            }
        }
        self.storyBarview.onNextBlock = {[weak self] in
            self?.onNextBlock?()
        }
        self.storyBarview.onPrevBlock = {[weak self] in
            self?.onPrevBlock?()
        }
        self.setupConstraints()
        contentView.addTarget(self, action: #selector(self.contentTouchBegan), for: .touchDown)
        contentView.addTarget(self, action: #selector(self.contentTouchCanceled), for: .touchDragExit)
        contentView.addTarget(self, action: #selector(self.contentTouchCanceled), for: .touchCancel)
        let tapButton = UITapGestureRecognizer(target: self, action: #selector(self.tapButton(recognizer:)))
        self.contentView.addGestureRecognizer(tapButton)
        self.imageView.contentMode = .scaleAspectFill
        self.imageView.layer.masksToBounds = true
        self.render()
        //contentView.addTarget(self, action: #selector(self.contentTouchUp(sender:)), for: .touchUpInside)
    }
    func render(){
        self.apiService.getStories(id: self.id) {[weak self] response in
            guard let self = self else {return}
            switch response {
            case .success(let result):
                self.images = result.slides
                self.imageView.image = nil
               // self.imageView.kf.setImage(with: URL(string: self.images[0].preview ?? ""))
                self.imageView.kf.setImage(with: self.images[0].original)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
    }
    var topOffset : CGFloat = 0
    var heightOffset : CGFloat = 0
    var widthOffset : CGFloat = 0
    var leftOffset : CGFloat = 0
    var firstClose = true
    var canScroll = true
    func toStart(){
        //self.storyBarview.toStart()
        //self.storyBarview.layer.removeAllAnimations()
        //self.storyBarview.currentStory = 0
        self.storyBarview.removeFromSuperview()
        self.storyBarview = StoryBarView()
        self.contentView.addSubview(self.storyBarview)
        self.storyBarview.onNextBlock = {[weak self] in
            //self?.storyBarview.currentStory = 0
            self?.onNextBlock?()
            self?.storyBarview.currentStory = 0
            //self?.imageView.kf.setImage(with: URL(string: self?.images[0].preview ?? ""))
            self?.imageView.kf.setImage(with: self?.images[0].original)
        }
        self.storyBarview.onPrevBlock = {[weak self] in
            self?.onPrevBlock?()
            self?.storyBarview.currentStory = 0
            //self?.imageView.kf.setImage(with: URL(string: self?.images[0].preview ?? ""))
            self?.imageView.kf.setImage(with: self?.images[0].original)
        }
        self.storyBarview.onNext = {[weak self] index in
            if index < self?.images.count ?? 0 {
                //self?.imageView.kf.setImage(with: URL(string: self?.images[index].preview ?? ""))
                self?.imageView.kf.setImage(with: self?.images[index].original)
            }
        }
        self.storyBarview.onPrev = {[weak self] index in
            if index >=  0 {
               // self?.imageView.kf.setImage(with: URL(string: self?.images[index].preview ?? ""))
                self?.imageView.kf.setImage(with: self?.images[index].original)
            }
        }
        self.storyBarview.setLines(count: self.count)
        
        self.storyBarview.snp.makeConstraints{
            $0.top.equalToSuperview().offset(52)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(2)
        }
        self.contentView.layoutSubviews()
    }
    @objc func contentTouchBegan(){
        print("Began")
        self.storyBarview.stopAnimation()
    }
    @objc func contentTouchCanceled(){
        print("Cancel")
        //self.storyBarview.resumeAnimation()
    }
    
    @objc func tapButton(recognizer : UITapGestureRecognizer){
        self.storyBarview.resumeAnimation()
        if recognizer.location(in: self.contentView).x >= self.contentView.center.x{
            self.storyBarview.nextAnimation()
        } else {
            print("PREV")
            self.storyBarview.prev()
            
        }
    }
    @objc func tapecognizing(recognizer : UILongPressGestureRecognizer){
        if recognizer.state == .began {
            self.storyBarview.stopAnimation()
        }
        if recognizer.state == .ended {
            self.storyBarview.resumeAnimation()
        }
        if recognizer.state == .cancelled {
            print("CANCELLED")
            self.storyBarview.stopAnimation()
        }
    }
    @objc func recognizing(recognizer: UIPanGestureRecognizer) {
        
        //print(recognizer.translation(in: self.contentView).x)
        //self.top?.update(offset: recognizer.translation(in: self.contentView).y)
        //return
        
        if recognizer.state == .began {
            self.storyBarview.stopAnimation()
            if abs(recognizer.translation(in: self.contentView).x) > abs(recognizer.translation(in: self.contentView).y) {
                //ReviewHelperClass.shared.horizontalScroll = true
                //return
                canScroll = false
            } else {
                canScroll = true
            }
        }
        if !canScroll {
            return
        }
        
        let translation = recognizer.translation(in: self.contentView)
        //self.storyBarview.stopAnimation()
        if firstClose {
        topOffset += translation.y
        //widthOffset -= translation.y / 10
        self.top?.update(offset: topOffset)
        if topOffset >= 0 && widthOffset <= 0{
            width?.update(offset: widthOffset)
            top?.update(offset: topOffset)
            
        } else {
            self.topOffset = 0
            self.widthOffset = 0
            self.width?.update(offset: 0)
            self.top?.update(offset: 0)
        }
        
        //print(topOffset)
        recognizer.setTranslation(CGPoint.zero, in: self.contentView)
            
        }
        if recognizer.state == .ended {
            self.storyBarview.resumeAnimation()
            if topOffset > maxWidthOffset + 20 {
                topOffset = UIScreen.main.bounds.size.height
                //widthOffset = 0
                self.top?.update(offset: topOffset)
                self.animate?()
                self.topOffset = 0
                self.widthOffset = 0
                //self.top?.update(offset: 0)
                //self.width?.update(offset: 0)
                self.close?()
                
            } else {
                topOffset = 0
                widthOffset = 0
                self.top?.update(offset: topOffset)
                self.width?.update(offset: widthOffset)
                self.animate?()
            }
        }
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.contentView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.imageView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.storyBarview.snp.makeConstraints{
            $0.top.equalToSuperview().offset(52)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(2)
        }
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isKind(of: UIPanGestureRecognizer.self) {
            let recognizer = gestureRecognizer as! UIPanGestureRecognizer
            
            if abs(recognizer.translation(in: self.contentView).x) > abs(recognizer.translation(in: self.contentView).y)  {
                //print("TRUE")
                return false
            } else {
               // print("FALSE")
                return true
            }
        } else {
            return true
        }
        
    }
    
}
