//
//  StoryLine.swift
//  Wase
//
//  Created by Роман Макеев on 31/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class StoryLine : AirdronView {
    
    
    private var firstView = UIView()
    var onNext : Action?
    var widthConstraint : Constraint?
    var width : CGFloat
    init(width : CGFloat){
        self.width = width
        super.init(frame: .zero)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.firstView)
        self.backgroundColor = Color.grey.value
        self.firstView.backgroundColor = Color.white.value
        
        self.setupConstraints()
    }
    func prevAnimation(){
        self.layer.removeAllAnimations()
        self.layoutIfNeeded()
        // self.setNeedsDisplay()
        let frame = CGRect(x:  self.firstView.frame.minX, y:  self.firstView.frame.minY, width:  0, height: self.firstView.frame.height )
        
        self.firstView.removeFromSuperview()
        
        self.firstView = UIView()
        self.addSubview(self.firstView)
        self.firstView.frame = frame
        self.firstView.backgroundColor = Color.white.value
        self.layoutSubviews()
        self.setNeedsDisplay()
    }
    func forceAnimation(){
        self.layer.removeAllAnimations()
        self.layoutIfNeeded()
       // self.setNeedsDisplay()
        let frame = CGRect(x:  self.firstView.frame.minX, y:  self.firstView.frame.minY, width:  self.width, height: self.firstView.frame.height )
        
        self.firstView.removeFromSuperview()
        
        self.firstView = UIView()
        self.addSubview(self.firstView)
        self.firstView.frame = frame
        self.firstView.backgroundColor = Color.white.value
        self.layoutSubviews()
        self.setNeedsDisplay()
    }
    func stopAnimation(){
        self.layer.removeAllAnimations()
        self.layoutIfNeeded()
    }
    func toStart(){
        ff = false
        self.layer.removeAllAnimations()
        self.layoutIfNeeded()
        ff = true
        let frame = self.firstView.frame
        self.firstView.frame = CGRect(x: frame.minX, y: frame.minY, width: 0, height: frame.height)
    }
    func pauseAnimation(){
        var pausedTime = layer.convertTime(CACurrentMediaTime(), from: nil)
        layer.speed = 0.0
        layer.timeOffset = pausedTime
    }
    var ff = true
    func resumeAnimation(){
        var pausedTime = layer.timeOffset
        layer.speed = 1.0
        layer.timeOffset = 0.0
        layer.beginTime = 0.0
        let timeSincePause = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        layer.beginTime = timeSincePause
    }
    func startAnimation(){
        //self.widthConstraint?.update(offset: self.width)
        UIView.animate(withDuration: 5, delay: 0, options: .curveLinear, animations: {
            self.firstView.frame = CGRect(x:  self.firstView.frame.minX, y:  self.firstView.frame.minY, width:  self.width, height: self.firstView.frame.height )
        }, completion: {[weak self] ff in
            if ff {
                if self?.ff ?? false {
                    self?.onNext?()
                }
            }
        })
        
        //self.layoutIfNeeded()
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.firstView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.bottom.equalToSuperview()
            self.widthConstraint = $0.width.equalTo(0).constraint
            //$0.width.equalTo(0).offset(100)
            //self.widthConstraint?.update(offset: 200)
            
            
        }
        
    }
}
