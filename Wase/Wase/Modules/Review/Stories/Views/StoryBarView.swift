//
//  StoryBarView.swift
//  Wase
//
//  Created by Роман Макеев on 31/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class StoryBarView: AirdronView {
    
    var storyLines : [StoryLine] = []
    var currentStory = 0
    var onNextBlock : Action?
    var onPrevBlock : Action?
    var count : Int = 0
    var onNext : ((Int) -> Void)?
    var onPrev : ((Int) -> Void)?
    var width : CGFloat = 0
    override func initialSetup() {
        super.initialSetup()
        //self.addSubview(self.storyLines[0])
        //self.setupConstraints()
    }
    func setLines(count : Int){
        self.count = count
        let fullWidth = UIScreen.main.bounds.width - 16
        let width = (fullWidth - (CGFloat(count) - 1) * 8) / CGFloat(count)
        self.width = width
        for _ in 0...(count - 1) {
            let storyLine = StoryLine(width: width)
            storyLine.onNext = {[weak self] in
                self?.currentStory += 1
                
                //self?.onNext?()
                guard let self = self else {return}
                self.onNext?(self.currentStory)
                if self.currentStory <= self.storyLines.count - 1{
                    self.storyLines[self.currentStory].startAnimation()
                } else {
                    self.onNextBlock?()
                }
            }
            self.storyLines.append(storyLine)
            self.addSubview(storyLine)
            
        }
        self.setupConstraints()
        
    }
    func startAnimate(){
        self.storyLines[0].startAnimation()
        //self.animateLayout(duration: 10)
    }
    func stopAnimation(){
        //self.storyLines[0].layer.anim
        if self.currentStory == self.count {
            self.currentStory = self.count - 1
        }
        self.storyLines[self.currentStory].pauseAnimation()
    }
    func resumeAnimation(){
        self.storyLines[self.currentStory].resumeAnimation()
    }
    func nextAnimation(){
        //self.resumeAnimation()
        self.storyLines[self.currentStory].forceAnimation()
        if self.currentStory + 1 < self.count {
            self.currentStory += 1
            self.storyLines[self.currentStory].startAnimation()
            self.onNext?(self.currentStory)
        } else {
            self.onNextBlock?()
            self.currentStory = 0
        }
        
    }
    func prev(){
        //self.storyLines[self.currentStory].toStart()
        self.storyLines[self.currentStory].prevAnimation()
        if self.currentStory == 0 {
            self.onPrevBlock?()
        } else {
            self.storyLines[self.currentStory - 1].prevAnimation()
            self.currentStory -= 1
            self.storyLines[self.currentStory].startAnimation()
            self.onPrev?(self.currentStory)
        }
        //self.startAnimate()
        
    }
    func toStart(){
        self.currentStory = 0
        for line in self.self.storyLines {
            //line.layer.removeAllAnimations()
            line.ff = false
            line.toStart()
            line.layoutSubviews()
            line.ff = true
            
            
        }
        //self.layer.removeAllAnimations()
    }
    override func setupConstraints() {
        super.setupConstraints()
        let count = self.storyLines.count
        if count > 0 {
            self.storyLines[0].snp.makeConstraints{
                $0.top.equalToSuperview()
                $0.bottom.equalToSuperview()
                $0.left.equalToSuperview().offset(8)
                //$0.width.equalTo(self.width)
            }
            if count > 1 {
                for i in 1...(count - 1) {
                    self.storyLines[i].snp.makeConstraints{
                        $0.top.equalToSuperview()
                        $0.bottom.equalToSuperview()
                        $0.left.equalTo(self.storyLines[i-1].snp.right).offset(8)
                        $0.width.equalTo(self.storyLines[i-1])
                    }
                }
            }
            self.storyLines[count - 1].snp.makeConstraints{
                $0.right.equalToSuperview().offset(-8)
            }
        }
    }
}
