//
//  StoriesCollectionViewCell.swift
//  Wase
//
//  Created by Роман Макеев on 29/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class StoriesCollectionViewCell : CollectionViewCell {
    
    var onSelect : Action?
    let title = UILabel()
    let coverImageView = UIImageView()
    let containerView = UIView()
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.containerView)
        self.containerView.addSubview(self.coverImageView)
        self.containerView.addSubview(self.title)
        self.title.numberOfLines = 0
        //self.containerView.addSubview(self.)
        self.coverImageView.frame.equalTo(self.containerView.frame)
        // self.containerView.addSubview(self.nameLabel)
        self.backgroundColor = Color.white.value
        self.containerView.backgroundColor = Color.white.value
        self.contentView.layoutIfNeeded()
        self.containerView.layer.shadowColor = UIColor.init(red: 0, green: 0, blue: 25/255.0, alpha: 0.1).cgColor
        self.containerView.layer.shadowRadius = 16
        self.containerView.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.containerView.layer.shadowOpacity = 0.1
        self.containerView.layer.cornerRadius = 10
        self.coverImageView.backgroundColor = Color.white.value
        //self.containerView.layer.shadowPath = UIBezierPath(rect: containerView.bounds).cgPath
        self.containerView.clipsToBounds = true
        self.containerView.layer.masksToBounds = false
        self.containerView.layer.shouldRasterize = true
        self.containerView.layer.rasterizationScale = UIScreen.main.scale
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selected))
        self.coverImageView.addGestureRecognizer(tap)
        self.coverImageView.isUserInteractionEnabled = true
        self.coverImageView.layer.cornerRadius = 10
        self.coverImageView.clipsToBounds = true
        self.coverImageView.contentMode = .scaleAspectFill
        // self.layoutIfNeeded()
        self.setupConstraints()
    }
    @objc func selected(){
        self.onSelect?()
    }
    override func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! StoriesCollectionViewCellModel
        self.onSelect = viewModel.selectionHandler
        self.coverImageView.kf.setImage(with: URL(string: viewModel.cover.preview))
        self.coverImageView.kf.setImage(with: viewModel.cover.original)
        self.title.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.white.value).make(string: viewModel.title)
       // self.nameLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: viewModel.name)
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.containerView.snp.makeConstraints{
            //$0.top.equalToSuperview().offset(8)
            //$0.centerY.equalToSuperview()
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(6.25)
            $0.right.equalToSuperview().offset(-6.25)
            //$0.bottom.equalToSuperview().offset(-8)
            $0.height.equalTo(188)
        }
        self.title.snp.makeConstraints{
            $0.left.equalToSuperview().offset(10)
            $0.right.equalToSuperview().offset(-10)
            $0.bottom.equalToSuperview().offset(-12)
        }
        self.coverImageView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
    
    
}
extension StoriesCollectionViewCell: WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel, width: CGFloat) -> CGFloat {
        return 204
    }
    
}
struct StoriesCollectionViewCellModel : CollectionCellViewModel {
    var cellType: CollectionViewCell.Type { return StoriesCollectionViewCell.self }
    var title : String
    var cover : Image
    var selectionHandler : Action?
}
