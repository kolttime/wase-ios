//
//  AuthModuleBuilder.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class PhotographReviewModuleBuilder{
    private var apiService : ApiService
    private var sessionManager : TaokaUserSessionManager
    init(apiService : ApiService, sessionManager : TaokaUserSessionManager){
        self.apiService = apiService
        self.sessionManager = sessionManager
    }
    func makePhotographReviewModule() -> UserReviewViewController {
        return UserReviewViewController(apiService: self.apiService, sessionManager: self.sessionManager)
    }
    
}

