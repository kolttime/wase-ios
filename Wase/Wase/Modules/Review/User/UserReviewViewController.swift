//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class UserReviewViewController: SimpleNavigationBar{
    
    
    
    private var loadingView = TaokaLoadingView()
    var currentStoryViewConstraint : Constraint?
    var nextStoryViewConstraint : Constraint?
    var prevStoryViewConstraint : Constraint?
    var currentStoryIndex = 0
    private var storiesViews : [StoriesView?] = []
    private lazy var tableViewController = TableViewController()
    private var storiesModels : [ShortStories] = []
    //var currentStoryView : StoriesView?
    //var nextStoryView : StoriesView?
    //var prevStoryView : StoriesView?
    private var apiService : ApiService
    private var sessionManager : TaokaUserSessionManager
    init(apiService : ApiService, sessionManager : TaokaUserSessionManager){
        self.apiService = apiService
        self.sessionManager = sessionManager
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func initialSetup() {
        super.initialSetup()
        self.add(self.tableViewController)
        self.setTitle(title: "Обзор")
        self.setTabBar()
        self.setLargeTitle()
        self.toastPresenter.targetView = self.view
        //self.showToastErrorAlert(AirdronError.init(code: 1, reason: "fdjksbfgdsfdsb fiodshfjkl dsjkfhjds fm,dsnfjds fdmnsklf dsklflmds f"))
        self.tableViewController.tableView.register(cellClass: ReviewTableViewCell.self)
        self.tableViewController.tableView.register(cellClass: ReviewTableViewCell2.self)
        self.tableViewController.tableView.register(cellClass: ReviewTableViewCell3.self)
        self.tableViewController.tableView.register(cellClass: ReviewHeaderLargeTableViewCell.self)
        self.tableViewController.tableView.register(cellClass: ReviewHeaderNormalTableViewCell.self)
        self.tableViewController.tableView.register(cellClass: ReviewDiscountTableViewCell.self)
        self.tableViewController.tableView.register(cellClass: ReviewTableViewCell4.self)
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = false
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.setUpConstraints()
        //self.render()
        self.fetchData()
    }
    var canScroll = true
    var nextCreated = false
    var prevCreated = false
    var centerOffset : CGFloat = 0
    var onDiscount : ((Int) -> Void)!
    var onUserUser : ((String) -> Void)?
    @objc func storyRecongnizer(recognizer: UIPanGestureRecognizer) {
        let mainCenter = UIScreen.main.bounds.size.width / 2.0
        let translation = recognizer.translation(in: self.view)
       // print("GoingWell : x : \(translation.x) y : \(translation.y)")
        guard let navController = self.navigationController else {return}
        if recognizer.state == .began {
            if abs(recognizer.translation(in: navController.view).x) <= abs(recognizer.translation(in: navController.view).y) {
                canScroll = false
            } else {
                canScroll = true
            }
        }
        if !canScroll {
            return
        }
      
        
        //guard let currentStoryView = self.currentStoryView else {return}
        
        if translation.x < 0 {
            if self.currentStoryIndex + 1 < self.storiesViews.count {
              
                    self.createStory(storyIndex: self.currentStoryIndex + 1, next: true)
                    self.navigationController?.view.layoutSubviews()
                    //self.nextStoryView?.center.x += UIScreen.main.bounds.size.width
                   
                   
                //self.currentStoryView?.center.x += translation.x
                centerOffset += translation.x
                self.storiesViews[self.currentStoryIndex]?.centerX?.update(offset: centerOffset)
                
            } else if centerOffset > 0 {
               // self.currentStoryView?.center.x += translation.x
                centerOffset += translation.x
            }
        } else {
            if self.currentStoryIndex - 1 >= 0 {
               
                    self.createStory(storyIndex: self.currentStoryIndex - 1, next: false)
                    self.navigationController?.view.layoutSubviews()
                    //self.prevStoryView?.center.x  += -UIScreen.main.bounds.size.width
                
                
                //self.currentStoryView?.center.x += translation.x
                centerOffset += translation.x
                self.storiesViews[self.currentStoryIndex]?.centerX?.update(offset: centerOffset)
            } else if centerOffset  < 0 {
                //self.currentStoryView?.center.x += translation.x
                centerOffset += translation.x
                self.storiesViews[self.currentStoryIndex]?.centerX?.update(offset: centerOffset)
            }
            
        }
       // print("Width : \(mainCenter * 2) centerOffset : \(centerOffset)")
        //self.nextStoryView?.center.x += translation.x
        //self.prevStoryView?.center.x += translation.x
        recognizer.setTranslation(.zero, in: self.view)
        if recognizer.state == .ended {
            let width = UIScreen.main.bounds.size.width
            if centerOffset > width / 4.0 {
                self.storiesViews[self.currentStoryIndex]?.centerX?.update(offset: (width) + 5)
                self.storiesViews[self.currentStoryIndex]?.toStart()
                self.navigationController?.view.animateLayout(duration: 0.5)
                //self.nextStoryView = self.currentStoryView
                //self.currentStoryView = self.prevStoryView
                //self.currentStoryView?.storyBarview.startAnimate()
                
                //self.prevStoryView?.removeFromSuperview()
               // self.prevStoryView = nil
               // self.prevCreated = false
                self.currentStoryIndex -= 1
                self.storiesViews[self.currentStoryIndex]?.storyBarview.startAnimate()
                //self.storiesViews[self.currentStoryIndex]?.snp.removeConstraints()
                //self.storiesViews[self.currentStoryIndex + 1]?.snp.removeConstraints()
                //self.prevStoryView?.snp.removeConstraints()
                self.storiesViews[self.currentStoryIndex]?.snp.remakeConstraints{
                    self.storiesViews[self.currentStoryIndex]?.top =  $0.top.equalToSuperview().constraint
                    self.storiesViews[self.currentStoryIndex]?.height = $0.height.equalToSuperview().constraint
                    self.storiesViews[self.currentStoryIndex]?.width = $0.width.equalToSuperview().constraint
                    self.storiesViews[self.currentStoryIndex]?.centerX = $0.centerX.equalToSuperview().constraint
                    
                }
                self.storiesViews[self.currentStoryIndex + 1]?.snp.remakeConstraints{
                    $0.top.equalToSuperview()
                    $0.bottom.equalToSuperview()
                    //$0.height.equalToSuperview()
                    $0.width.equalToSuperview()
                    $0.left.equalTo(self.storiesViews[self.currentStoryIndex]!.snp.right).offset(5)
                }
                
                
            } else if centerOffset < -width / 4.0 {
                self.storiesViews[self.currentStoryIndex]?.centerX?.update(offset: -(width) - 5)
                self.storiesViews[self.currentStoryIndex]?.toStart()
                self.navigationController?.view.animateLayout(duration: 0.5)
                //self.nextStoryView = self.currentStoryView
                //self.currentStoryView = self.prevStoryView
                //self.currentStoryView?.storyBarview.startAnimate()
                
                //self.prevStoryView?.removeFromSuperview()
                // self.prevStoryView = nil
                // self.prevCreated = false
                self.currentStoryIndex += 1
                self.storiesViews[self.currentStoryIndex]?.storyBarview.startAnimate()
                //self.storiesViews[self.currentStoryIndex]?.snp.removeConstraints()
                //self.storiesViews[self.currentStoryIndex - 1]?.snp.removeConstraints()
                //self.prevStoryView?.snp.removeConstraints()
                self.storiesViews[self.currentStoryIndex]?.snp.remakeConstraints{
                    self.storiesViews[self.currentStoryIndex]?.top =  $0.top.equalToSuperview().constraint
                    self.storiesViews[self.currentStoryIndex]?.height = $0.height.equalToSuperview().constraint
                    self.storiesViews[self.currentStoryIndex]?.width = $0.width.equalToSuperview().constraint
                    self.storiesViews[self.currentStoryIndex]?.centerX = $0.centerX.equalToSuperview().constraint
                    
                }
                self.storiesViews[self.currentStoryIndex - 1]?.snp.remakeConstraints{
                    $0.top.equalToSuperview()
                    $0.bottom.equalToSuperview()
                    //$0.height.equalToSuperview()
                    $0.width.equalToSuperview()
                    $0.right.equalTo(self.storiesViews[self.currentStoryIndex]!.snp.left).offset(-5)
                }
                
                //sleep(5)
                
            } else {
                self.storiesViews[self.currentStoryIndex]?.centerX?.update(offset: 0)
                self.navigationController?.view.animateLayout(duration: 0.5)
            }



            centerOffset = 0
        }
        
    }
    private var feedModels : [FeedSection] = []
    func fetchData(){
    
        
        self.loadingView.isHidden = false
        var fetched1 = false
        var fetched2 = false
        self.apiService.getStories{
            [weak self] response in
            guard let self = self else {return}
            switch response {
            case .success(let result):
                fetched1 = true
                //self.loadingView.isHidden = false
                self.storiesModels = result
                for story in self.storiesModels {
                    
                    
                    self.storiesViews.append(nil)
                }
                if fetched1 && fetched2 {
                    self.loadingView.isHidden = true
                    self.render()
                }
            case .failure(let error):
                self.showToastErrorAlert(error)
            }
        }
        
        self.apiService.getFeed(specialization: nil){
            [weak self] response in
            guard let self = self else {return}
            switch response {
            case .success(let result):
                print(result)
                self.feedModels = result
                fetched2 = true
                if fetched1 && fetched2 {
                    self.loadingView.isHidden = true
                    self.render()
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.showToastErrorAlert(error)
            }
        }
    }
    func onNext(){
        if self.currentStoryIndex + 1 < self.storiesModels.count {
        let width = UIScreen.main.bounds.size.width
        self.createStory(storyIndex: self.currentStoryIndex + 1 , next: true)
            self.storiesViews[self.currentStoryIndex + 1]?.contentView.layoutSubviews()
        self.storiesViews[self.currentStoryIndex + 1]?.layoutSubviews()
        self.navigationController?.view.layoutSubviews()
        self.storiesViews[self.currentStoryIndex]?.centerX?.update(offset: -(width) - 5)
        self.storiesViews[self.currentStoryIndex]?.toStart()
        self.navigationController?.view.animateLayout(duration: 0.5)
        //self.nextStoryView = self.currentStoryView
        //self.currentStoryView = self.prevStoryView
        //self.currentStoryView?.storyBarview.startAnimate()
        
        //self.prevStoryView?.removeFromSuperview()
        // self.prevStoryView = nil
        // self.prevCreated = false
        self.currentStoryIndex += 1
        self.storiesViews[self.currentStoryIndex]?.storyBarview.startAnimate()
        //self.storiesViews[self.currentStoryIndex]?.snp.removeConstraints()
        //self.storiesViews[self.currentStoryIndex - 1]?.snp.removeConstraints()
        //self.prevStoryView?.snp.removeConstraints()
        self.storiesViews[self.currentStoryIndex]?.snp.remakeConstraints{
            self.storiesViews[self.currentStoryIndex]?.top =  $0.top.equalToSuperview().constraint
            self.storiesViews[self.currentStoryIndex]?.height = $0.height.equalToSuperview().constraint
            self.storiesViews[self.currentStoryIndex]?.width = $0.width.equalToSuperview().constraint
            self.storiesViews[self.currentStoryIndex]?.centerX = $0.centerX.equalToSuperview().constraint
            
        }
        self.storiesViews[self.currentStoryIndex - 1]?.snp.remakeConstraints{
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview()
            //$0.height.equalToSuperview()
            $0.width.equalToSuperview()
            $0.right.equalTo(self.storiesViews[self.currentStoryIndex]!.snp.left).offset(-5)
        }
        } else {
            print("Hey")
            self.storiesViews[self.currentStoryIndex]?.close?()
            
        }
    }
    private var selectedCells : [String] = []
    func onPrev(){
        if self.currentStoryIndex - 1 >= 0 {
        
        
        let width = UIScreen.main.bounds.size.width
        self.createStory(storyIndex: self.currentStoryIndex - 1, next: false)
        self.storiesViews[self.currentStoryIndex - 1]?.contentView.layoutSubviews()
        self.storiesViews[self.currentStoryIndex - 1]?.layoutSubviews()
        
        self.navigationController?.view.layoutSubviews()
        self.storiesViews[self.currentStoryIndex]?.centerX?.update(offset: (width) + 5)
        self.storiesViews[self.currentStoryIndex]?.toStart()
        self.navigationController?.view.animateLayout(duration: 0.5)
        //self.nextStoryView = self.currentStoryView
        //self.currentStoryView = self.prevStoryView
        //self.currentStoryView?.storyBarview.startAnimate()
        
        //self.prevStoryView?.removeFromSuperview()
        // self.prevStoryView = nil
        // self.prevCreated = false
        self.currentStoryIndex -= 1
        self.storiesViews[self.currentStoryIndex]?.storyBarview.startAnimate()
        //self.storiesViews[self.currentStoryIndex]?.snp.removeConstraints()
        //self.storiesViews[self.currentStoryIndex + 1]?.snp.removeConstraints()
        //self.prevStoryView?.snp.removeConstraints()
        self.storiesViews[self.currentStoryIndex]?.snp.remakeConstraints{
            self.storiesViews[self.currentStoryIndex]?.top =  $0.top.equalToSuperview().constraint
            self.storiesViews[self.currentStoryIndex]?.height = $0.height.equalToSuperview().constraint
            self.storiesViews[self.currentStoryIndex]?.width = $0.width.equalToSuperview().constraint
            self.storiesViews[self.currentStoryIndex]?.centerX = $0.centerX.equalToSuperview().constraint
            
        }
        self.storiesViews[self.currentStoryIndex + 1]?.snp.remakeConstraints{
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview()
            //$0.height.equalToSuperview()
            $0.width.equalToSuperview()
            $0.left.equalTo(self.storiesViews[self.currentStoryIndex]!.snp.right).offset(5)
        }
        } else {
            self.storiesViews[self.currentStoryIndex]?.close?()
        }
    }
    func createStory(storyIndex : Int, next : Bool){
        guard let navController = self.navigationController else {return}
        //guard let index = self.storiesModels.firstIndex(where: {$0.id == id}) else {return}
        if storyIndex == self.storiesModels.count {return}
        if self.storiesViews[storyIndex] == nil {
            let storyView = StoriesView.init(id: self.storiesModels[storyIndex].id, count : self.storiesModels[storyIndex].slidesCount, apiService: self.apiService)
            let pan = UIPanGestureRecognizer(target: self, action: #selector(self.storyRecongnizer(recognizer:)))
            pan.delegate = self
            storyView.addGestureRecognizer(pan)
            storyView.animate = {[weak self] in
                self?.navigationController?.view.animateLayout(duration: 0.5)
            }
            storyView.onNextBlock = {[weak self] in
                guard let self = self else {return}
                self.onNext()
            }
            storyView.onPrevBlock = {[weak self] in
                guard let self = self else {return}
                self.onPrev()
            }
            storyView.close = {[weak self] in
                storyView.storyBarview.currentStory = 0
                UIView.animate(withDuration: 0.5, animations: {
                    self?.tabBarController?.tabBar.alpha = 1
                }, completion: { (ff) in
                    if ff {
                       // storyView.firstClose = true
                       // storyView.removeFromSuperview()
                        guard let self = self else {return}
                        for i in 0...self.storiesViews.count - 1 {
                            self.storiesViews[i]?.removeFromSuperview()
                            self.storiesViews[i] = nil
                            
                        }
                    }
                })
                
                
            }
            navController.view.addSubview(storyView)
            self.storiesViews[storyIndex] = storyView
            //storyView.center.x = 0
            if next {
                //storyView.center.x += UIScreen.main.bounds.size.width
                //storyView.contentView.backgroundColor = UIColor.purple
                storyView.snp.makeConstraints{
                    $0.top.equalToSuperview()
                    $0.bottom.equalToSuperview()
                    $0.left.equalTo(self.storiesViews[storyIndex - 1]!.snp.right).offset(5)
                    $0.width.equalToSuperview()
                    
                }
                //self.nextStoryView = storyView
                self.storiesViews[storyIndex] = storyView
                
            } else {
                //storyView.center.x -= UIScreen.main.bounds.size.width
                storyView.snp.makeConstraints{
                    $0.top.equalToSuperview()
                    $0.bottom.equalToSuperview()
                    $0.right.equalTo(self.storiesViews[storyIndex + 1]!.snp.left).offset(-5)
                    $0.width.equalToSuperview()
                    
                }
                //self.prevStoryView = storyView
                self.storiesViews[storyIndex] = storyView
            }
        }
        
    }
    func updateFilters(){
        var spString : String? = nil
        if self.selectedCells.count > 0 {
            spString = self.selectedCells[0]
        }
        self.apiService.getFeed(specialization: spString) {
            [weak self] response in
            switch response {
            case .success(let result):
                print(result)
                self?.feedModels = result
                self?.updateRender()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    func updateRender(){

        var section : DefaultTableSectionViewModel
        //let section = DefaultTableSectionViewModel(cellModels: [storiesModel])
        //sections.append(section)
        var cellModels : [TableCellViewModel] = []
        for model in self.feedModels {
            if model.albums != nil {
                var albums = model.albums!
                
                let albumModel = ReviewTableViewCellModel2(stories: albums) { (id) in
                    print("Заебок \(id)")
                    self.onAlbum?(id)
                }
                
                
//                let section = DefaultTableSectionViewModel(cellModels: [ReviewHeaderLargeTableViewCellModel(mainLabel: model.title), albumModel])
//                sections.append(section)
                cellModels.append(ReviewHeaderLargeTableViewCellModel(mainLabel: model.title))
                cellModels.append(albumModel)
                
                
                
                
            } else if model.users != nil {
                var users = model.users!
                
                let userModel = ReviewTableViewCellModel3(stories: users) { (id) in
                    print("Заебок \(id)")
                    if self.sessionManager.user?.type != .client {
                        self.onPhotouser?(id)
                    } else {
                        self.onUserUser?(id)
                    }
                }
                
                cellModels.append(ReviewHeaderLargeTableViewCellModel(mainLabel: model.title))
                cellModels.append(userModel)
                
            }
        }
        if self.selectedCells.count == 0 {
                    let viewModel = ReviewDiscountTableViewCellModel.init(discount: self.refferalDiscount!, selection: {
                        print("Click!")
                        self.onDiscount(self.refferalDiscount!)
                    })
                   cellModels.insert(viewModel, at: 2)
        }
        section = DefaultTableSectionViewModel(cellModels: cellModels)
        self.tableViewController.update(section, inSection: 2)
        //self.tableViewController.update
    }
    func updateTableView(){
        let selectionHandlerr : ((String) -> Void) =
        { (id) in
            print(id)
            if let index = self.selectedCells.firstIndex(where: {$0 == id}) {
                self.selectedCells.remove(at: index)
            } else {
                self.selectedCells = [id]
            }
            //self.tableViewController.update(ReviewTableViewCellModel4(stories: result, selectedCells: self.selectedCells, selectionHandler: ), inSection: 3)
            self.updateTableView()
            
        }
        
        let viewModel = ReviewTableViewCellModel4(stories: self.specializations, selectedCells: self.selectedCells, selectionHandler: selectionHandlerr)
        let section = DefaultTableSectionViewModel(cellModels: [viewModel])
        self.tableViewController.update(section, inSection: 1, animation: .none, reload: false)
        //self.tableViewController.update(section, inSection: 1)
        self.updateFilters()
        print(self.selectedCells.count)
    }
    func openStory(id : String, frame : CGRect){
        guard let index = self.storiesModels.firstIndex(where: {$0.id == id}) else {return}
        self.currentStoryIndex = index
        let storyView = StoriesView.init(id: id, count : self.storiesModels[index].slidesCount, apiService: self.apiService)
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.storyRecongnizer(recognizer:)))
        pan.delegate = self
        storyView.addGestureRecognizer(pan)
        storyView.onNextBlock = {[weak self] in
            guard let self = self else {return}
            self.onNext()
        }
        storyView.onPrevBlock = {[weak self] in
            guard let self = self else {return}
            self.onPrev()
        }
        storyView.animate = {[weak self] in
            self?.navigationController?.view.animateLayout(duration: 0.5)
        }
        storyView.close = {[weak self] in
            storyView.storyBarview.currentStory = 0
            UIView.animate(withDuration: 0.5, animations: {
                self?.tabBarController?.tabBar.alpha = 1
            }, completion: { (ff) in
                if ff {
                    guard let self = self else {return}
                    for i in 0...self.storiesViews.count - 1 {
                        self.storiesViews[i]?.removeFromSuperview()
                        self.storiesViews[i] = nil
                        
                    }
                }
            })
            
            
        }
        //self.currentStoryView = storyView
        self.navigationController?.view.addSubview(storyView)
        storyView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(frame.minY)
            $0.left.equalToSuperview().offset(frame.minX)
            $0.height.equalTo(frame.height)
            $0.width.equalTo(frame.width)
        }
        self.navigationController?.view.layoutIfNeeded()
        storyView.snp.removeConstraints()
        storyView.snp.makeConstraints{
            storyView.top = $0.top.equalToSuperview().constraint
            //storyView.left = $0.left.equalToSuperview().constraint
            storyView.centerX =  $0.centerX.equalToSuperview().constraint
            storyView.height = $0.height.equalToSuperview().constraint
            storyView.width = $0.width.equalToSuperview().constraint
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.navigationController?.view.layoutIfNeeded()
            self.tabBarController?.tabBar.alpha = 0
        }) { (ff) in
            if ff {
                storyView.storyBarview.startAnimate()
            }
        }
        self.storiesViews[index] = storyView
//        storyView.layer.zPosition = 9999
//        UIView.animate(withDuration: 5) {
//            storyView.frame = UIScreen.main.bounds
//            self.tabBarController?.tabBar.alpha = 0
//
//        }
    }
    var specializations : [Specialization] = []
    var onAlbum : ((String) -> Void)?
    var onPhotouser : ((String) -> Void)?
    func render(){
        var last = 0
        let selectionHandler : ((String, CGRect) -> Void)? = {[weak self] id, frame in
            print(frame)
            guard let self = self else {return}
            let convertedFrame = self.tableViewController.tableView.convert(frame , to : self.navigationController?.view)
            print(convertedFrame)
            self.openStory(id: id, frame: convertedFrame)
        }
        let storiesModel = ReviewTableViewCellModel(stories: self.storiesModels, selectionHandler: selectionHandler)
        var sections : [DefaultTableSectionViewModel] = []
        let section = DefaultTableSectionViewModel(cellModels: [storiesModel])
        sections.append(section)
        var cellModels : [TableCellViewModel] = []
        for model in self.feedModels {
            if model.albums != nil {
                var albums = model.albums!
                
                let albumModel = ReviewTableViewCellModel2(stories: albums) { (id) in
                    print("Заебок \(id)")
                    self.onAlbum?(id)
                }
                
                    
                    //let section = DefaultTableSectionViewModel(cellModels: [ReviewHeaderLargeTableViewCellModel(mainLabel: model.title), albumModel])
                    //sections.append(section)
                cellModels.append(ReviewHeaderLargeTableViewCellModel(mainLabel: model.title))
                cellModels.append(albumModel)
                
                
                
                
            } else if model.users != nil {
                var users = model.users!
                
                let userModel = ReviewTableViewCellModel3(stories: users) { (id) in
                    print("Заебок \(id)")
                    if self.sessionManager.user?.type != .client {
                        self.onPhotouser?(id)
                    } else {
                        self.onUserUser?(id)
                    }
                }
                
                cellModels.append(ReviewHeaderLargeTableViewCellModel(mainLabel: model.title))
                cellModels.append(userModel)
                
            }
        }
        let section2 = DefaultTableSectionViewModel(cellModels: cellModels)
        sections.append(section2)
        self.tableViewController.update(viewModels: sections)
        
        
        self.apiService.specialization { [weak self] (response) in
            guard let self = self else {return}
            switch response {
            case .success(let result):
                self.specializations = result
                //self.selectedCells = result.map({$0.id!})
                self.apiService.getRefferalDiscount{
                    [weak self] response in
                    guard let self = self else {return}
                    switch response {
                    case .success(let result):
                        print("")
                        self.refferalDiscount = result.referralDiscount
                        let viewModel = ReviewDiscountTableViewCellModel.init(discount: result.referralDiscount, selection: {
                            print("Click!")
                            self.onDiscount(result.referralDiscount)
                        })
                        self.tableViewController.insert(viewModel, for: 2, in: 2)
                    case .failure(let error):
                        print(error.localizedDescription)
                        
                    }
                }
                let selectionHandlerr : ((String) -> Void) =
                { (id) in
                    print(id)
                        if let index = self.selectedCells.firstIndex(where: {$0 == id}) {
                            self.selectedCells.remove(at: index)
                        } else {
                            self.selectedCells = [id]
                        }
                        //self.tableViewController.update(ReviewTableViewCellModel4(stories: result, selectedCells: self.selectedCells, selectionHandler: ), inSection: 3)
                        self.updateTableView()
                    }
                
                let viewModel = ReviewTableViewCellModel4(stories: result, selectedCells: self.selectedCells, selectionHandler: selectionHandlerr)
                let section = DefaultTableSectionViewModel(cellModels: [viewModel])
                self.tableViewController.insert(section, in: 1)
            case .failure(let error):
                self.showToastErrorAlert(error)
            }
        }
        
       
    }
    
    
    var refferalDiscount : Int?
    override func setUpConstraints() {
        super.setUpConstraints()
        self.tableViewController.view.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
    
   // var canScroll = true
}

extension UserReviewViewController : UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true

    }
}

