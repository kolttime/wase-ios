//
//  AuthModuleBuilder.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class UserReviewModuleBuilder{
    private var apiService : ApiService
    private var sessionManager : TaokaUserSessionManager
    init(apiService : ApiService, sessionManager : TaokaUserSessionManager){
        self.apiService = apiService
        self.sessionManager = sessionManager
    }
    func makeUserReviewModule() -> UserReviewViewController {
        return UserReviewViewController(apiService: self.apiService, sessionManager: self.sessionManager)
    }
    
    func makeDescountViewController() -> DiscountViewController{
        return DiscountViewController(apiService: self.apiService)
    }
    func makeAlbummodule(id : String) -> ReviewAlbumViewController{
        return ReviewAlbumViewController(id: id, apiService: self.apiService)
    }
    func makePhotoProfileModule(id : String) -> UserProfileViewController{
        return UserProfileViewController(apiService: self.apiService, userId: id, isReview: true)
    }
    func makePhotoAlbum(images : [Image]?, id : String?, scrollTo : Int?, userId : String?) -> PhotoAlbumViewController {
        //return PhotoAlbumViewController( images : images,  id: id, sctollTo: scrollTo , apiService: self.apiService )
        return PhotoAlbumViewController(userId: userId, images: images, id: id, sctollTo: scrollTo, apiService: self.apiService)
        
    }
    func makeUserProfileModule(id : String) -> PhotographProfileViewController{
        return PhotographProfileViewController(apiService: self.apiService, userId: id, photoId: nil, status: nil, phoneNumber: nil, isReview: true)
    }
    func mskeUserCreateModule() -> UserCreatePhotoViewController {
        return UserCreatePhotoViewController(apiService: self.apiService)
    }
    
    func makeTineModule() -> TimeViewController {
        return TimeViewController()
    }
    
    func makePlaceModule() -> BringPlaceViewController {
        return BringPlaceViewController(apiService: self.apiService, userModel: self.sessionManager.user!)
    }
    
    func makeWorkerModule() -> WorkerTypeViewController {
        return WorkerTypeViewController()
    }
    
    func makeBudgetModule(photoCreation : CreatePhotosession) -> BudgetViewController {
        return BudgetViewController(photoCreation: photoCreation, apiService: self.apiService, userModel: self.sessionManager.user!)
    }
    
    func makeSecondStepModule(photoCreation : CreatePhotosession) -> ReadySecondStepViewController {
        return ReadySecondStepViewController(photoCreation: photoCreation, apiService: self.apiService)
    }
    
    func makeSelectionModule(type : UserType, id : String) -> SelectionViewController {
        return SelectionViewController(apiService: self.apiService, type: type, id: id)
    }
    
    func makePageUserCreateViewModule(initialViewControllers : [AirdronViewController], count : Int) -> AirdronPageViewController {
        let PageViewControllers = AirdronPageViewController(count: count)
        PageViewControllers.setViewContrtollers(viewControllers: initialViewControllers, count: count)
        return PageViewControllers
    }
}
