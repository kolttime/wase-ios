//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class ReviewTableViewCell3: TableViewCell{
    
    
    private var cellHeight = 64
    var storiesCollectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 24
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    var viewModel : ReviewTableViewCellModel3?
    var feedModels : [UserShort] = []
    override func initialSetup() {
        super.initialSetup()
        self.storiesCollectionView.contentInset = UIEdgeInsets(top: 0, left: ViewSize.sideOffset, bottom: 0, right: ViewSize.sideOffset)
        self.storiesCollectionView.backgroundColor = Color.white.value
        self.storiesCollectionView.showsHorizontalScrollIndicator = false
        self.storiesCollectionView.register(SelectionCollectionViewCell.self, forCellWithReuseIdentifier: "hello3")
        self.storiesCollectionView.delegate = self
        self.storiesCollectionView.dataSource = self
        self.backgroundColor = Color.white.value
        self.addSubview(self.storiesCollectionView)
        self.setUpConstraints()
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! ReviewTableViewCellModel3
        self.viewModel = viewModel
        self.feedModels = viewModel.stories
        self.storiesCollectionView.reloadData()
        
    }
    
    func setUpConstraints() {
        
        self.storiesCollectionView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            //$0.height.equalTo((self.cellHeight * 3) + 10 + 16 + 16 + 24)
            $0.bottom.equalToSuperview().offset(-24)
        }
    }
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
       // return 234
        guard let model = viewModel as? ReviewTableViewCellModel3 else {return 258}
        if model.stories.count == 1 {
            return 64 + 66
        } else if model.stories.count == 2 {
            return 64 + 64 + 66
        } else {
            return 258
        }
        
    }
    
}
struct ReviewTableViewCellModel3 : TableCellViewModel {
    var cellType: TableViewCell.Type {return ReviewTableViewCell3.self}
    var stories : [UserShort]
    var selectionHandler : ((String) -> Void)?
}

extension ReviewTableViewCell3 : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.feedModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.storiesCollectionView.dequeueReusableCell(withReuseIdentifier: "hello3", for: indexPath) as! SelectionCollectionViewCell
        let model = self.feedModels[indexPath.row]
        //        let viewModel : StoriesCollectionViewCellModel = StoriesCollectionViewCellModel(title: model.title ?? "", cover: model.cover, selectionHandler: {[weak self] in
        //            guard let self = self else {return}
        //            guard let viewModel = self.viewModel else {return}
        //            let convertedFrame = self.storiesCollectionView.convert(cell.frame, to: self.contentView)
        //            viewModel.selectionHandler?(self.storiesModels[indexPath.row].id, convertedFrame)
        //        })
        let viewModel : SelectionCollectionViewCellModel = SelectionCollectionViewCellModel(mainLabel: "\(model.givenName) \(model.familyName)", countLabel: "\(model.description ?? "")", id: model.id, image: model.avatar) { [weak self] (id) in
            guard let self = self else {return}
            guard let viewModel = self.viewModel else {return}
            viewModel.selectionHandler?(id)
        }
        cell.configure(viewModel: viewModel)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: 311, height: self.cellHeight)
    }
}

