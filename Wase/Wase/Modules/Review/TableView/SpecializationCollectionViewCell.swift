//
//  SubCollectionViewCell.swift
//  Taoka
//
//  Created by Роман Макеев on 13/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class SpecializationCollectionViewCell : CollectionViewCell {
    
    var onSelect : ((String) -> Void)?
    let nameLabel = UILabel()
    let containerView = UIView()
    var id : String?
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.containerView)
        self.contentView.addSubview(self.nameLabel)
        // self.containerView.addSubview(self.nameLabel)
        self.backgroundColor = Color.white.value
        self.containerView.backgroundColor = Color.light.value
        self.containerView.layer.masksToBounds = true
        self.containerView.layer.cornerRadius = 20
        self.contentView.layoutIfNeeded()
        // self.layoutIfNeeded()
        self.setupConstraints()
    }
    
    override func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! SpecializationCollectionViewCellModel
        self.onSelect = viewModel.selectionHandler
        self.nameLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: viewModel.name)
        self.id = viewModel.id
        if viewModel.isSelected {
            self.containerView.backgroundColor = Color.grey.value
        } else {
            self.containerView.backgroundColor = Color.light.value
        }
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.containerView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.nameLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
    }
    
    override func didSelect() {
        guard let id = self.id else {return}
        self.onSelect?(id)
    }
}
extension SpecializationCollectionViewCell: WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel, width: CGFloat) -> CGFloat {
        return 40
    }
    
}
struct SpecializationCollectionViewCellModel : CollectionCellViewModel {
    var cellType: CollectionViewCell.Type { return SpecializationCollectionViewCell.self }
    var name : String
    var id : String
    var isSelected : Bool
    var selectionHandler : ((String) -> Void)?
}
