//
//  SubTableViewCell.swift
//  Taoka
//
//  Created by Minic Relocusov on 12/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class Feed1CollectionViewCell : CollectionViewCell {
    
    
    var id : String?
    var selectionhandler : ((String) -> Void)?
    let nameLabel = UILabel()
    let datelabel = UILabel()
    let mainImageView = UIImageView()
    let containerView = UIView()
    
    override func initialSetup() {
        super.initialSetup()
        
        self.backgroundColor = Color.white.value
        // self.contentView.addSubview(self.label)
        self.contentView.addSubview(self.containerView)
        // self.contentView.addSubview(self.mainImageView)
        self.mainImageView.addSubview(self.nameLabel)
        self.mainImageView.addSubview(self.datelabel)
        self.containerView.addSubview(self.mainImageView)
        
        
        self.containerView.layer.shadowColor = UIColor.black.cgColor
        self.containerView.layer.shadowRadius = 8
        self.containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.containerView.layer.shadowOpacity = 0.2
        self.containerView.layer.cornerRadius = 10
        
        //self.containerView.layer.shadowPath = UIBezierPath(rect: containerView.bounds).cgPath
        self.containerView.clipsToBounds = true
        self.containerView.layer.masksToBounds = false
        self.containerView.layer.shouldRasterize = true
        self.containerView.layer.rasterizationScale = UIScreen.main.scale
        
        
        self.mainImageView.layer.cornerRadius = 10
        self.mainImageView.clipsToBounds = true
        self.mainImageView.contentMode = .scaleAspectFill
        
        self.nameLabel.textColor = .white
        self.datelabel.textColor = .white
        self.nameLabel.font = UIFont.boldSystemFont(ofSize: 17)
        self.datelabel.font = UIFont.systemFont(ofSize: 13)
        self.contentView.backgroundColor = Color.white.value
        //self.containerView.dropShadow()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selected))
        self.mainImageView.addGestureRecognizer(tap)
        self.mainImageView.isUserInteractionEnabled = true
        self.layoutIfNeeded()
        
        // set the shadow properties
        
        
        self.setupConstraints()
    }
    
    @objc func selected(){
       // self.selectionhandler?(self.id ?? "")
        guard let id = self.id else {return}
        self.selectionhandler?(id)
    }
    
    override func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! Feed1CollectionViewCellModel
        self.mainImageView.kf.setImage(with: URL(string: viewModel.imgUrl))
        self.nameLabel.text = viewModel.nameText
        self.datelabel.text = viewModel.dateText
        self.selectionhandler = viewModel.selectionHandler
        self.id = viewModel.id
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.containerView.snp.makeConstraints{
            $0.top.equalTo(self.contentView).offset(10)
            $0.left.equalTo(self.contentView).offset(6)
            $0.right.equalTo(self.contentView).offset(-6)
            $0.bottom.equalTo(self.contentView).offset(-24)
            $0.height.equalTo(getCardHeight())
        }
        self.mainImageView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        let hh : CGFloat = 64
        
        let view = UIView(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width - (2 * (ViewSize.sideOffset)), height: hh))
        
        let gradient = CAGradientLayer()
        print("FRAME : \(view.frame)")
        gradient.frame = view.frame
        
        gradient.colors = [
            UIColor(red: 0, green: 0, blue: 0, alpha: 0.4).cgColor,
            UIColor(red: 0, green: 0, blue: 0, alpha: 0).cgColor
        ]
        
        gradient.locations = [0.0, 1.0]
        
        view.layer.insertSublayer(gradient, at: 0)
        view.backgroundColor = UIColor.clear
        
        self.mainImageView.addSubview(view)
        view.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.mainImageView.bringSubviewToFront(view)
        self.mainImageView.bringSubviewToFront(self.nameLabel)
        self.mainImageView.bringSubviewToFront(self.datelabel)
        self.nameLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(12)
            $0.left.equalToSuperview().offset(12)
        }
        self.datelabel.snp.makeConstraints{
            $0.top.equalTo(self.nameLabel.snp.bottom)
            $0.left.equalToSuperview().offset(12)
        }
        
        
        
    }
    
    
    override func didSelect() {
        self.selectionhandler?(self.id!)
    }
    
//    override class func height(for viewModel: ViewModelType,
//                               tableView: UITableView) -> CGFloat {
//        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
//        if Device.isIphoneX || Device.isIphoneXSmax {
//            return 176
//        } else {
//            return 176
//        }
//
//    }
    
    
    
    
}
struct Feed1CollectionViewCellModel : CollectionCellViewModel {
    var cellType: CollectionViewCell.Type {return Feed1CollectionViewCell.self}
    var imgUrl : String
    var nameText : String
    var dateText : String
    var id : String
    var selectionHandler : ((String) -> Void)?
}


