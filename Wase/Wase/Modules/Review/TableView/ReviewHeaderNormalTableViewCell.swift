
import Foundation
import UIKit


class ReviewHeaderNormalTableViewCell : TableViewCell {
    
    
    
    private var viewModel : ReviewHeaderNormalTableViewCellModel?
    private lazy var titleLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(titleLabel)
        self.setupConstraints()
    }
    
    
    
    
     override func configure(viewModel: TableViewCell.ViewModelType) {
        
        let viewModel = viewModel as! ReviewHeaderNormalTableViewCellModel
        
        self.viewModel = viewModel
        self.titleLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: viewModel.mainLabel)
        self.titleLabel.numberOfLines = 0
        
    }
    
    
        override class func height(for viewModel: ViewModelType,
                                   tableView: UITableView) -> CGFloat {
            return 22
        }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.titleLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            //$0.top.equalToSuperview()
            $0.bottom.equalToSuperview()
            //$0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
    }
}


struct ReviewHeaderNormalTableViewCellModel : TableCellViewModel {
    var cellType: TableViewCell.Type { return ReviewHeaderNormalTableViewCell.self }
    var mainLabel: String
}
