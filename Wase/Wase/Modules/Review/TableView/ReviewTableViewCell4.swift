//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class ReviewTableViewCell4: TableViewCell{
    
    var prevSelectedCell : SpecializationCollectionViewCell?
    private var cellHeight = 40
    var storiesCollectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 4
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    var selectedId : String?
    var viewModel : ReviewTableViewCellModel4?
    var storiesModels : [Specialization] = []
    override func initialSetup() {
        super.initialSetup()
        self.storiesCollectionView.contentInset = UIEdgeInsets(top: 0, left: ViewSize.sideOffset , bottom: 0, right: ViewSize.sideOffset )
        self.storiesCollectionView.backgroundColor = Color.white.value
        self.storiesCollectionView.showsHorizontalScrollIndicator = false
        self.storiesCollectionView.register(SpecializationCollectionViewCell.self, forCellWithReuseIdentifier: "hello4")
        self.storiesCollectionView.delegate = self
        self.storiesCollectionView.dataSource = self
        self.backgroundColor = Color.white.value
        self.addSubview(self.storiesCollectionView)
        self.setUpConstraints()
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! ReviewTableViewCellModel4
        self.viewModel = viewModel
        self.storiesModels = viewModel.stories
        self.storiesCollectionView.reloadData()
        
    }
    
    func setUpConstraints() {
        
        self.storiesCollectionView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(self.cellHeight)
        }
    }
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        return 40 + 24
        
    }
    
    
}
struct ReviewTableViewCellModel4 : TableCellViewModel {
    var cellType: TableViewCell.Type {return ReviewTableViewCell4.self}
    var stories : [Specialization]
    var selectedCells :  [String]
    var selectionHandler : ((String) -> Void)?
}

extension ReviewTableViewCell4 : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.storiesModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.storiesCollectionView.dequeueReusableCell(withReuseIdentifier: "hello4", for: indexPath) as! SpecializationCollectionViewCell
        let model = self.storiesModels[indexPath.row]
//        let viewModel : StoriesCollectionViewCellModel = StoriesCollectionViewCellModel(title: model.title ?? "", cover: model.cover, selectionHandler: {[weak self] in
//            guard let self = self else {return}
//            guard let viewModel = self.viewModel else {return}
//            let convertedFrame = self.storiesCollectionView.convert(cell.frame, to: self.contentView)
//            viewModel.selectionHandler?(self.storiesModels[indexPath.row].id, convertedFrame)
//        })
        var isSelected = false
        if let id = self.viewModel?.selectedCells.firstIndex(where: {$0 == model.id}) {
            isSelected = true
        }
        let viewModel : SpecializationCollectionViewCellModel = SpecializationCollectionViewCellModel(name: model.name!, id: model.id!, isSelected: isSelected) { [weak self] (id) in
            guard let self = self else {return}
            guard let viewModel = self.viewModel else {return}
            //viewModel.selectionHandler?(self)
        }
        cell.configure(viewModel: viewModel)
//        if cell.id == nil || cell.id != self.selectedId {
//            cell.containerView.backgroundColor = Color.light.value
//        } else {
//            cell.containerView.backgroundColor = Color.grey.value
//        }
       // if cell.id == self.viewModel?.selectedCells.
        if self.viewModel?.selectedCells.count ?? 0 > 0 {
            if cell.id == self.viewModel?.selectedCells[0] {
                self.prevSelectedCell = cell
            }
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       // for cell in collectionView.ce
        
        let selectedCell = collectionView.cellForItem(at: indexPath) as! SpecializationCollectionViewCell
        
        if let index = self.viewModel?.selectedCells.firstIndex(where: {$0 == selectedCell.id!}) {
            UIView.animate(withDuration: 0.3, animations: {
                selectedCell.containerView.backgroundColor = Color.light.value
            }) { (ff) in
                if ff {
                    self.viewModel?.selectionHandler?(selectedCell.id!)
                    self.viewModel?.selectedCells.remove(at: index)
                }
            }
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.prevSelectedCell?.containerView.backgroundColor = Color.light.value
                selectedCell.containerView.backgroundColor = Color.grey.value
                collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                
            }) { (ff) in
                if ff {
                    self.viewModel?.selectionHandler?(selectedCell.id!)
                    self.viewModel?.selectedCells = [selectedCell.id!]
                    self.prevSelectedCell = selectedCell
                }
            }
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if let model = self.viewModel {
            let textLabel = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "\(model.stories[indexPath.item].name!)")
            let label = UILabel()
            label.attributedText = textLabel
            let textWidth = label.textWidth()
            return CGSize.init(width: Int(textWidth + 48), height: self.cellHeight)
        } else {
            return CGSize.init(width: 50, height: 50)
        }
    }
}

