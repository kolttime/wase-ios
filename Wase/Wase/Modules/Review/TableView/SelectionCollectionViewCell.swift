//
//  SearchableCountryTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 13/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class SelectionCollectionViewCell: CollectionViewCell {
    var selection : ((String) -> Void)?
    var selectModel: SelectionCollectionViewCellModel?
    var id : String?
    var imageq = UIImageView()
    
    private lazy var mainLabel = UILabel()
    private lazy var countLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.imageq)
        self.contentView.addSubview(self.mainLabel)
        self.contentView.addSubview(self.countLabel)
        self.countLabel.numberOfLines = 0
        self.imageq.backgroundColor = Color.light.value
        self.imageq.contentMode = .scaleAspectFill
        self.imageq.layer.cornerRadius = 32
        self.imageq.layer.masksToBounds = true
        //self.contentView.backgroundColor = Color.red.value
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selected))
        self.contentView.addGestureRecognizer(tap)
        self.setupConstraints()
    }
    @objc func selected(){
        guard let id = self.id else {return}
        self.selection?(id)
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.imageq.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.top.equalToSuperview()
            $0.height.equalTo(64)
            $0.width.equalTo(64)
            //$0.bottom.equalToSuperview()
        }
        self.mainLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8)
            $0.left.equalTo(imageq.snp.right).offset(12)
        }
        
        self.countLabel.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(0)
            $0.left.equalTo(imageq.snp.right).offset(12)
            $0.right.equalToSuperview()
        }
    }
    
    override func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! SelectionCollectionViewCellModel
        self.selectModel = viewModel
        self.imageq.kf.setImage(with: (viewModel.image?.original) ?? URL(string: ""))
        self.id = viewModel.id
        self.selection = viewModel.selectionHandler
        self.mainLabel.text = viewModel.mainLabel
        self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: self.mainLabel.text ?? "")
        self.countLabel.text = viewModel.countLabel
        self.countLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: self.countLabel.text ?? "")
    }
    
//    override class func height(for viewModel: ViewModelType,
//                               tableView: UITableView) -> CGFloat {
//        return 64
//    }
    
    override func didSelect() {
        self.selection?(self.id!)
    }
}

struct SelectionCollectionViewCellModel : CollectionCellViewModel {
    var cellType: CollectionViewCell.Type { return SelectionCollectionViewCell.self }
    var mainLabel: String
    var countLabel : String
    var id : String
    var image : Image?
    let selectionHandler: ((String) -> Void)?
}




