//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class ReviewTableViewCell2: TableViewCell{
    
    
    private var cellHeight = 194
    var storiesCollectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    var viewModel : ReviewTableViewCellModel2?
    var feedModels : [ShortAlbum] = []
    override func initialSetup() {
        super.initialSetup()
        self.storiesCollectionView.contentInset = UIEdgeInsets(top: 0, left: ViewSize.sideOffset - 6.25, bottom: 0, right: ViewSize.sideOffset - 6.25)
        self.storiesCollectionView.backgroundColor = Color.white.value
        self.storiesCollectionView.showsHorizontalScrollIndicator = false
        self.storiesCollectionView.register(Feed1CollectionViewCell.self, forCellWithReuseIdentifier: "hello2")
        self.storiesCollectionView.delegate = self
        self.storiesCollectionView.dataSource = self
        self.backgroundColor = Color.white.value
        self.addSubview(self.storiesCollectionView)
        self.setUpConstraints()
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! ReviewTableViewCellModel2
        self.viewModel = viewModel
        self.feedModels = viewModel.stories
        self.storiesCollectionView.reloadData()
        
    }
    
    func setUpConstraints() {
        
        self.storiesCollectionView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(self.cellHeight)
        }
    }
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        return 194
        
    }
    
}
struct ReviewTableViewCellModel2 : TableCellViewModel {
    var cellType: TableViewCell.Type {return ReviewTableViewCell2.self}
    var stories : [ShortAlbum]
    var selectionHandler : ((String) -> Void)?
}

extension ReviewTableViewCell2 : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.feedModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.storiesCollectionView.dequeueReusableCell(withReuseIdentifier: "hello2", for: indexPath) as! Feed1CollectionViewCell
        let model = self.feedModels[indexPath.row]
//        let viewModel : StoriesCollectionViewCellModel = StoriesCollectionViewCellModel(title: model.title ?? "", cover: model.cover, selectionHandler: {[weak self] in
//            guard let self = self else {return}
//            guard let viewModel = self.viewModel else {return}
//            let convertedFrame = self.storiesCollectionView.convert(cell.frame, to: self.contentView)
//            viewModel.selectionHandler?(self.storiesModels[indexPath.row].id, convertedFrame)
//        })
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let date = dateFormatter.date(from: model.date)!
        dateFormatter.dateFormat = "dd MMMM"
        let dateStr = dateFormatter.string(from: date)
        let viewModel : Feed1CollectionViewCellModel = Feed1CollectionViewCellModel(imgUrl: model.images[0].original.absoluteString, nameText: "\(model.title)", dateText: "\(model.specialization.name!), \(dateStr)", id: model.id) { [weak self] (id) in
            guard let self = self else {return}
            guard let viewModel = self.viewModel else {return}
            viewModel.selectionHandler?(id)
        }
        cell.configure(viewModel: viewModel)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: 335, height: self.cellHeight)
    }
}

