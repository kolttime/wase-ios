//
//  ReviewLargeHeaderViewCell.swift
//  Wase
//
//  Created by Роман Макеев on 22/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//


import Foundation
import UIKit


class ReviewHeaderLargeTableViewCell : TableViewCell {
    
    
   
    private var viewModel : ReviewHeaderLargeTableViewCellModel?
    private lazy var titleLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        //self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(self.titleLabel)
        //self.contentView.addSubview(self.titleLabel)
        self.titleLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(6)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }

        self.setupConstraints()
    }
    
    
    
    
     override func configure(viewModel: TableViewCell.ViewModelType) {
        
        let viewModel = viewModel as! ReviewHeaderLargeTableViewCellModel
        
        self.viewModel = viewModel
        self.titleLabel.attributedText = CustomFont.bodyBold20.attributesWithParagraph.colored(color: Color.black.value).make(string: viewModel.mainLabel)
        self.titleLabel.numberOfLines = 0
        //print("HEERRERERRERERE \n\n\n\n\n\n\n\n \(viewModel.mainLabel) \n\n\n\n\n\n")
        
    }
    
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 25 + 8 + 6
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        
//        self.titleLabel.snp.makeConstraints{
//            //$0.left.equalToSuperview().offset(ViewSize.sideOffset)
//            $0.centerX.equalToSuperview()
//            $0.centerY.equalToSuperview()
//            //$0.top.equalToSuperview()
//            //$0.bottom.equalToSuperview()
//            //$0.right.equalToSuperview().offset(-ViewSize.sideOffset)
//        }
        
    }
}


struct ReviewHeaderLargeTableViewCellModel : TableCellViewModel {
    var cellType: TableViewCell.Type { return ReviewHeaderLargeTableViewCell.self }
    var mainLabel: String
}
