//
//  UserPhotosessionCell.swift
//  Taoka
//
//  Created by Minic Relocusov on 18/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class ReviewDiscountTableViewCell : TableViewCell {
    
    
    
    var selection : Action?
    var viewModel : ReviewDiscountTableViewCellModel?
    let baseView = UIView()
    let discountLabel = UILabel()
    let mainLabel = UILabel()
    private let helloLabel : UILabel = {
        let hello = UILabel()
        hello.font = UIFont.systemFont(ofSize: 37)
        hello.text = "👏"
        return hello
    }()
    private let arrowImageView : UIImageView = {
        let img = UIImageView()
        img.image = UIImage.init(named: "Arrow2")
        return img
    }()
    let containerView = UIView()
    
    override func initialSetup() {
        super.initialSetup()
        
        self.backgroundColor = Color.white.value
        // self.contentView.addSubview(self.label)
        self.contentView.addSubview(self.baseView)
        // self.contentView.addSubview(self.mainImageView)
        
        baseView.backgroundColor = UIColor.clear
        baseView.layer.shadowColor = UIColor.black.cgColor
        baseView.layer.shadowOffset = CGSize(width: 0, height: 0)
        baseView.layer.shadowOpacity = 0.1
        baseView.layer.shadowRadius = 8.0
        self.containerView.frame = baseView.bounds
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.masksToBounds = true
        baseView.addSubview(self.containerView)
        //baseView.layer.shadowPath = UIBezierPath(roundedRect: baseView.bounds, cornerRadius: 10).cgPath
        
        baseView.layer.shouldRasterize = true
        baseView.layer.rasterizationScale = UIScreen.main.scale
        self.containerView.backgroundColor = Color.white.value
        
        self.contentView.backgroundColor = Color.white.value
        //self.containerView.dropShadow()
        //self.containerView.add
       // self.containerView.addSubview(self.helloLabel)
        self.containerView.addSubview(discountLabel)
        self.containerView.addSubview(self.arrowImageView)
        self.baseView.addSubview(self.helloLabel)
        self.setUpConstarints()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selecT))
        self.discountLabel.numberOfLines = 0
        self.containerView.addGestureRecognizer(tap)
        // set the shadow properties
        
        
        // self.setupConstraints()
    }
    @objc func selecT(){
        self.selection?()
    }
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! ReviewDiscountTableViewCellModel
        self.viewModel = viewModel
        self.selection = viewModel.selection
        var combination : NSMutableAttributedString = NSMutableAttributedString()
        let str1 = CustomFont.bodySemibold17.attributesWithParagraph.make(string: "Скидка \(viewModel.discount) % ")
        let str2 = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "на фотосессию за приглашение друга")
        combination.append(str1)
        combination.append(str2)
        self.discountLabel.attributedText = combination
        
        
        
    }
    func setUpConstarints(){
        self.baseView.snp.makeConstraints{
            $0.top.equalTo(self.contentView).offset(20)
            $0.left.equalTo(self.contentView).offset(ViewSize.sideOffset)
            $0.right.equalTo(self.contentView).offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.contentView).offset(-24)
            // $0.height.equalTo(169)
        }
        self.containerView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.arrowImageView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(18.5)
            $0.right.equalToSuperview().offset(-15)
            $0.height.equalTo(13)
            $0.width.equalTo(8)
        }
        self.discountLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(28)
            $0.right.equalTo(self.arrowImageView.snp.left).offset(-5)
            $0.centerY.equalToSuperview()
        }
        self.helloLabel.snp.makeConstraints{
            $0.top.equalTo(self.containerView.snp.top).offset(3)
            $0.centerX.equalTo(self.containerView.snp.left)
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        return 104
        
    }
    
    
    
    
}
struct ReviewDiscountTableViewCellModel : TableCellViewModel {
    var cellType: TableViewCell.Type {return ReviewDiscountTableViewCell.self}
    var discount : Int
    var selection : Action?
    
}

