//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class ReviewTableViewCell: TableViewCell{
    
    
    private var cellHeight = 205
    var storiesCollectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    var viewModel : ReviewTableViewCellModel?
    var storiesModels : [ShortStories] = []
    override func initialSetup() {
        super.initialSetup()
        self.storiesCollectionView.contentInset = UIEdgeInsets(top: 0, left: ViewSize.sideOffset - 6.25, bottom: 0, right: ViewSize.sideOffset - 6.25)
        self.storiesCollectionView.backgroundColor = Color.white.value
        self.storiesCollectionView.showsHorizontalScrollIndicator = false
        self.storiesCollectionView.register(StoriesCollectionViewCell.self, forCellWithReuseIdentifier: "hello")
        self.storiesCollectionView.delegate = self
        self.storiesCollectionView.dataSource = self
        self.backgroundColor = Color.white.value
        self.addSubview(self.storiesCollectionView)
        self.setUpConstraints()
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! ReviewTableViewCellModel
        self.viewModel = viewModel
        self.storiesModels = viewModel.stories
        self.storiesCollectionView.reloadData()
        
    }
    
     func setUpConstraints() {
       
        self.storiesCollectionView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(self.cellHeight)
        }
    }
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        return 220
        
    }
    
}
struct ReviewTableViewCellModel : TableCellViewModel {
    var cellType: TableViewCell.Type {return ReviewTableViewCell.self}
    var stories : [ShortStories]
    var selectionHandler : ((String, CGRect) -> Void)?
}

extension ReviewTableViewCell : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.storiesModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.storiesCollectionView.dequeueReusableCell(withReuseIdentifier: "hello", for: indexPath) as! StoriesCollectionViewCell
        let model = self.storiesModels[indexPath.row]
        let viewModel : StoriesCollectionViewCellModel = StoriesCollectionViewCellModel(title: model.title ?? "", cover: model.cover, selectionHandler: {[weak self] in
            guard let self = self else {return}
            guard let viewModel = self.viewModel else {return}
            let convertedFrame = self.storiesCollectionView.convert(cell.frame, to: self.contentView)
            viewModel.selectionHandler?(self.storiesModels[indexPath.row].id, convertedFrame)
        })
        cell.configure(viewModel: viewModel)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: 171, height: self.cellHeight)
    }
}

