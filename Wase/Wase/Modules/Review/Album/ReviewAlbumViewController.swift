//
//  PhotoAlbumViewController.swift
//  Taoka
//
//  Created by Minic Relocusov on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class ReviewAlbumViewController : SimpleNavigationBar {
    
    
    lazy var tableViewController = TableViewController()
    
    let approveView = AlbumApproveView()
    var id : String
   
    private var apiService : ApiService
    var images : [Image]?
    let loadingView = TaokaLoadingView()
    //var userId : String?
    init(id : String, apiService : ApiService){
        self.id = id
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        //self.tabBarController?.tabBar.isHidden = true
        self.setTabBarHidden()
        // self.setLargeTitle()
        //self.setTitle(title: "Портфолио")
        self.add(self.tableViewController)
        //self.scrollingView = self.tableViewController.scrollView
        //self.scrollingView.scrollView = self.tableViewController.scrollView
        self.view.backgroundColor = Color.white.value
        //self.tableViewController.scrollView.delegate = self
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = true
        self.onLargetitle = {[weak self] in
            //  self?.setOffset()
        }
        self.onNormalTitle = {[weak self] in
            //   self?.setOffset()
        }
        self.tableViewController.tableView.register(cellClass: PhotoAlbumViewCell.self)
        self.tableViewController.tableView.register(headerFooterViewClass: ReviewHeaderType1.self)
        self.tableViewController.tableView.register(headerFooterViewClass: ReviewHeaderType2.self)
        // self.render()
        self.approveView.layer.cornerRadius = 10
        self.approveView.layer.masksToBounds = true
        self.setUpConstraints(approve: false)
        self.fetchData()
    }
    // override func scrollViewDidScroll(_ scrollView: UIScrollView) {
    
    //    }
    
    
    
    func fetchData(){
        
            self.loadingView.isHidden = false
            self.apiService.getAlbumId(id : self.id) {
                [weak self] response in
                switch response {
                case .success(let album):
                    self?.images = album.images
                    self?.setTitle(title: album.title)
                    //self?.setMultilineNavigationBar(topText: "Игорь", bottomText: "сука")
                    //self?.setLargeTitle(subTitle: "Игорь")
                    self?.setLargeTitleHidden()
                    self?.loadingView.isHidden = true
                    
                    
                    self?.render(date: album.participants)
                    
                case .failure(let error):
                    print(error)
                    
                }
            }
        
    }
    var onUser : ((String) -> Void)?
    func render(date : [UserShort]){
        var viewModels : [PhotoAlbumViewCellModel] = []
        for image in self.images! {
            let viewModel = PhotoAlbumViewCellModel.init(image: image)
            viewModels.append(viewModel)
        }
        
        var section : DefaultTableSectionViewModel
        if date.count == 1 {
            section = DefaultTableSectionViewModel(cellModels: viewModels, headerViewModel: ReviewHeaderType1Model(user: date[0], selectionhandler: { (id) in
                self.onUser?(id)
                print("Click!")
            }), footerViewModel: nil)
            self.tableViewController.update(viewModels: [section])
        } else {
            section = DefaultTableSectionViewModel(cellModels: viewModels, headerViewModel: ReviewHeaderType2Model(user: date, selectionhandler: { (id) in
                self.onUser?(id)
                print("Click!")
            }), footerViewModel: nil)
            self.tableViewController.update(viewModels: [section])
        }
        //section = DefaultTableSectionViewModel(cellModels: viewModels)
        
        
        
    }
    private var offsetView = UIView()
    func setUpConstraints(approve : Bool) {
        super.setUpConstraints()
        self.approveView.snp.removeConstraints()
        self.tableViewController.view.snp.removeConstraints()
        self.tableViewController.tableView.snp.removeConstraints()
        
        if !approve {
            // self.tableViewController.tableView.snp.removeConstraints()
            self.approveView.snp.makeConstraints{
                $0.height.equalTo(0)
            }
            self.tableViewController.view.snp.makeConstraints{
                $0.top.equalToSuperview()
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
                $0.bottom.equalToSuperview()
            }
            self.tableViewController.tableView.snp.makeConstraints{
                $0.edges.equalTo(self.tableViewController.view)
            }
            self.loadingView.snp.makeConstraints{
                $0.edges.equalToSuperview()
            }
        } else {
            self.approveView.snp.makeConstraints{
                $0.top.equalToSuperview().offset(10)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(149)
            }
            self.tableViewController.view.snp.makeConstraints{
                $0.top.equalTo(self.approveView.snp.bottom).offset(24)
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
                $0.bottom.equalToSuperview()
            }
            self.tableViewController.tableView.snp.makeConstraints{
                $0.edges.equalTo(self.tableViewController.view)
            }
            self.loadingView.snp.makeConstraints{
                $0.edges.equalToSuperview()
            }
        }
    }
    
}
