//
//  PortfolioHeaderCollectionView.swift
//  OMG
//
//  Created by Minic Relocusov on 26/03/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit


class ReviewHeaderType2 : TableHeaderFooterView {
    
    
    private let view1 = UIView()
    private let view2 = UIView()
    private var viewModel : ReviewHeaderType2Model?
    private let avatar : UIImageView = {
        let img = UIImageView()
        img.contentMode = UIView.ContentMode.scaleAspectFill
        img.layer.masksToBounds = true
        img.layer.cornerRadius = 22
        return img
    }()
    let nameLabel = UILabel()
    let dLabel = UILabel()
    private let avatar2 : UIImageView = {
        let img = UIImageView()
        img.contentMode = UIView.ContentMode.scaleAspectFill
        img.layer.masksToBounds = true
        img.layer.cornerRadius = 22
        return img
    }()
    let nameLabel2 = UILabel()
    let dLabel2 = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = Color.white.value
        self.addSubview(self.view1)
        self.addSubview(self.view2)
        self.view1.addSubview(self.avatar)
        self.view1.addSubview(self.nameLabel)
        self.view1.addSubview(self.dLabel)
        self.view2.addSubview(self.avatar2)
        self.view2.addSubview(self.nameLabel2)
        self.view2.addSubview(self.dLabel2)
        self.dLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Фотограф")
        self.dLabel2.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Визажист")
        self.setupConstraints()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selected))
        self.view1.addGestureRecognizer(tap)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.selected2))
        self.view2.addGestureRecognizer(tap2)
    }
    
    @objc func selected2(){
        guard let model = self.viewModel else {return}
        model.selectionhandler?(model.user[1].id)
    }
    @objc func selected(){
        guard let model = self.viewModel else {return}
        model.selectionhandler?(model.user[0].id)
    }
    
    override func configure(viewModel: TableHeaderFooterView.ViewModelType) {
        
        let viewModel = viewModel as! ReviewHeaderType2Model
        
        self.viewModel = viewModel
        
        self.nameLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "\(viewModel.user[0].givenName) \(viewModel.user[0].familyName)")
        self.avatar.kf.setImage(with: viewModel.user[0].avatar?.original)
        
        self.nameLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "\(viewModel.user[1].givenName) \(viewModel.user[1].familyName)")
        self.avatar.kf.setImage(with: viewModel.user[1].avatar?.original)
        
        
        
        
    }
    
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 138
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        self.view1.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(64)
        }
        self.view2.snp.makeConstraints{
            $0.top.equalTo(self.view1.snp.bottom).offset(1)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(64)
        }
        self.avatar.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(16)
            $0.height.equalTo(44)
            $0.width.equalTo(44)
        }
        self.nameLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(13)
            $0.left.equalTo(self.avatar.snp.right).offset(8)
        }
        self.dLabel.snp.makeConstraints{
            $0.top.equalTo(self.nameLabel.snp.bottom)
            $0.left.equalTo(self.avatar.snp.right).offset(8)
        }
        self.avatar2.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(16)
            $0.height.equalTo(44)
            $0.width.equalTo(44)
        }
        self.nameLabel2.snp.makeConstraints{
            $0.top.equalToSuperview().offset(13)
            $0.left.equalTo(self.avatar.snp.right).offset(8)
        }
        self.dLabel2.snp.makeConstraints{
            $0.top.equalTo(self.nameLabel.snp.bottom)
            $0.left.equalTo(self.avatar.snp.right).offset(8)
        }
    }
}


struct ReviewHeaderType2Model : TableHeaderFooterViewModel {
    var viewType: TableHeaderFooterView.Type { return ReviewHeaderType2.self }
    var user : [UserShort]
    var selectionhandler : ((String) -> Void)?
}
