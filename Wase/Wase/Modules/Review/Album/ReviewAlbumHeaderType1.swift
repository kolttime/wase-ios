//
//  PortfolioHeaderCollectionView.swift
//  OMG
//
//  Created by Minic Relocusov on 26/03/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit


class ReviewHeaderType1 : TableHeaderFooterView {
    
    
    
    private var viewModel : ReviewHeaderType1Model?
    private let avatar : UIImageView = {
        let img = UIImageView()
        img.contentMode = UIView.ContentMode.scaleAspectFill
        img.layer.masksToBounds = true
        img.layer.cornerRadius = 22
        return img
    }()
    let nameLabel = UILabel()
    let dLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = Color.white.value
        self.addSubview(self.avatar)
        self.addSubview(self.nameLabel)
        self.addSubview(self.dLabel)
        self.dLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Фотограф")
        self.setupConstraints()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selected))
        self.addGestureRecognizer(tap)
    }
    
    
    @objc func selected(){
        guard let model = self.viewModel else {return}
        model.selectionhandler?(model.user.id)
    }
    
    override func configure(viewModel: TableHeaderFooterView.ViewModelType) {
        
        let viewModel = viewModel as! ReviewHeaderType1Model
        
        self.viewModel = viewModel
        
        self.nameLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "\(viewModel.user.givenName) \(viewModel.user.familyName)")
        self.avatar.kf.setImage(with: viewModel.user.avatar?.original)
        
        
        
    }
    
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 74
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.avatar.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(16)
            $0.height.equalTo(44)
            $0.width.equalTo(44)
        }
        self.nameLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(13)
            $0.left.equalTo(self.avatar.snp.right).offset(8)
        }
        self.dLabel.snp.makeConstraints{
            $0.top.equalTo(self.nameLabel.snp.bottom)
            $0.left.equalTo(self.avatar.snp.right).offset(8)
        }
    }
}


struct ReviewHeaderType1Model : TableHeaderFooterViewModel {
    var viewType: TableHeaderFooterView.Type { return ReviewHeaderType1.self }
    var user : UserShort
    var selectionhandler : ((String) -> Void)?
}


