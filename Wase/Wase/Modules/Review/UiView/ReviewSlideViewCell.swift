//
//  UserPhotosessionCell.swift
//  Taoka
//
//  Created by Minic Relocusov on 18/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class ReviewSlideViewCell : TableViewCell {
    
    
    
    var selection : ((String) -> Void)?
    var id : String?
    var viewModel : ReviewSlideViewCellModel?
    let baseView = UIView()
    
    let containerView = UIView()
    let toplabel = UILabel()
    let bottomLabel = UILabel()
    let arrowImg = UIImageView()
    override func initialSetup() {
        super.initialSetup()
        
        self.backgroundColor = Color.white.value
        // self.contentView.addSubview(self.label)
        self.contentView.addSubview(self.baseView)
        // self.contentView.addSubview(self.mainImageView)
        
        baseView.backgroundColor = UIColor.clear
        baseView.layer.shadowColor = UIColor.black.cgColor
        baseView.layer.shadowOffset = CGSize(width: 0, height: 0)
        baseView.layer.shadowOpacity = 0.1
        baseView.layer.shadowRadius = 8.0
        self.containerView.frame = baseView.bounds
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.masksToBounds = true
        baseView.addSubview(self.containerView)
        //baseView.layer.shadowPath = UIBezierPath(roundedRect: baseView.bounds, cornerRadius: 10).cgPath
        baseView.layer.shouldRasterize = true
        baseView.layer.rasterizationScale = UIScreen.main.scale
        self.containerView.backgroundColor = Color.white.value
        self.containerView.addSubview(self.toplabel)
        self.containerView.addSubview(self.bottomLabel)
        self.containerView.addSubview(self.arrowImg)
        self.contentView.backgroundColor = Color.white.value
        //self.containerView.dropShadow()
        self.arrowImg.image = UIImage.init(named: "Arrow2")
        self.setUpConstarints()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selecT))
        self.containerView.addGestureRecognizer(tap)
        // set the shadow properties
        
        
        // self.setupConstraints()
    }
    @objc func selecT(){
        guard let model = self.viewModel else {return}
        self.selection?(model.id)
    }
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! ReviewSlideViewCellModel
        self.viewModel = viewModel
        self.selection = viewModel.selection
        self.id = viewModel.id
        self.toplabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: viewModel.topLabel)
        self.bottomLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: viewModel.bottomLabel)
        if id == "иии" {
            self.toplabel.snp.removeConstraints()
            self.toplabel.snp.makeConstraints{
                $0.centerY.equalToSuperview()
                $0.left.equalToSuperview().offset(12)
            }
        }
        
        
        
    }
    func setUpConstarints(){
        self.baseView.snp.makeConstraints{
            $0.top.equalTo(self.contentView).offset(4)
            $0.left.equalTo(self.contentView).offset(ViewSize.sideOffset)
            $0.right.equalTo(self.contentView).offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.contentView).offset(-4)
            // $0.height.equalTo(169)
        }
        self.containerView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        
        self.arrowImg.snp.makeConstraints{
            $0.top.equalToSuperview().offset(15.5)
            $0.right.equalToSuperview().offset(-17)
            $0.width.equalTo(8)
            $0.height.equalTo(13)
        }
        self.toplabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(12)
        }
        self.bottomLabel.snp.makeConstraints{
            $0.top.equalTo(self.toplabel.snp.bottom).offset(2)
            $0.left.equalToSuperview().offset(12)
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        return 72
        
    }
    
    
    
    
}
struct ReviewSlideViewCellModel : TableCellViewModel {
    var cellType: TableViewCell.Type {return ReviewSlideViewCell.self}
    var id : String
    var topLabel : String
    var bottomLabel : String
    var selection : ((String) -> Void)?
    
}

