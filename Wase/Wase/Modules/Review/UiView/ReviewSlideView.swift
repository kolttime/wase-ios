//
//  ReviewSlideView.swift
//  Wase
//
//  Created by Роман Макеев on 23/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit


class ReviewSlideView : SlideView {
    
    
    
    private lazy var tableViewController = TableViewController()
    var titleLabel = UILabel()
    var selection : ((String) -> Void)?
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.titleLabel)
        self.titleLabel.attributedText = CustomFont.bodyBold20.attributesWithParagraph.make(string: "Выберите фотосессию")
        self.titleLabel.numberOfLines = 0
        self.tableViewController.tableView.register(cellClass: ReviewSlideViewCell.self)
        self.addSubview(self.tableViewController.view)
        self.setupConstraintss()
        
        //self.render()
    }
    func render(photos : [Photosession]){
        let selection : ((String) -> Void)? = {[weak self] id in
            self?.selection?(id)
            
        }
        var viewModels : [ReviewSlideViewCellModel] = []
        if photos.count == 0 {
            
            self.titleLabel.attributedText = CustomFont.bodyBold20.attributesWithParagraph.make(string: "Похоже, у вас нет подходящий фотосессий")
            let viewModel = ReviewSlideViewCellModel(id: "иии", topLabel: "Создать новую", bottomLabel: "", selection: selection)
            viewModels.append(viewModel)
        } else {
            for photo in photos {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "YYYY-MM-dd"
                let date = dateFormatter.date(from: photo.date)!
                dateFormatter.dateFormat = "dd MMMM"
                let dateStr = dateFormatter.string(from: date)
                var time = ""
                switch photo.duration {
                case 1:
                    time = "1 час"
                case 2:
                    time = "2 часа"
                case 3:
                    time = "3 часа"
                case 4:
                    time = "4 часа"
                default:
                    time = "\(photo.duration) часов"
                }
                let viewModel = ReviewSlideViewCellModel(id: photo.id, topLabel: photo.title, bottomLabel: "\(dateStr), с \(photo.time), \(time)", selection: selection)
                viewModels.append(viewModel)
            }
        }
        
        let section = DefaultTableSectionViewModel(cellModels: viewModels)
        self.tableViewController.update(viewModels: [section])
    }
    func setupConstraintss() {
        //super.setupConstraints()
        self.titleLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(54)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.tableViewController.view.snp.makeConstraints{
            $0.top.equalTo(self.titleLabel.snp.bottom).offset(12)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
}
