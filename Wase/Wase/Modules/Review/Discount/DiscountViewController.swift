//
//  DiscountViewController.swift
//  Wase
//
//  Created by Роман Макеев on 23/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit


class DiscountViewController : SimpleNavigationBar {
    
    
    private var mainLabel = UILabel()
    private var buttonView = UIView()
    private var buttonlabel = UILabel()
    private var apiService : ApiService
    private var loadingView = TaokaLoadingView()
    private var link : String?
    private var discount : Int?
    init(apiService : ApiService){
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        
        self.setTabBarHidden()
        self.setLargeTitle()
        self.setTitle(title: "Скидка")
        self.view.addSubview(mainLabel)
        self.view.addSubview(self.buttonView)
        self.buttonView.addSubview(self.buttonlabel)
        self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Расскажи своему другу, что мы тут ир-тыш и если он тыр-пыр, то промик на 1000 ₽ забиндим тебе моментально!")
        self.mainLabel.numberOfLines = 0
        self.buttonlabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: "Пригласить друга")
        self.buttonView.layer.masksToBounds = false
        self.buttonView.layer.cornerRadius = 10
        self.buttonView.backgroundColor = Color.light.value
        self.buttonView.isUserInteractionEnabled = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.click))
        self.buttonView.addGestureRecognizer(tap)
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.loadingView)
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.fetchData()
        self.setUpConstraints()
        
    }
    
    func fetchData(){
        self.apiService.getRefferalLink{[weak self] response in
            guard let self = self else {return}
            switch response {
            case .success(let result):
                self.buttonView.isUserInteractionEnabled = true
                self.link = result.link
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        self.apiService.getRefferalDiscount() {[weak self] response in
            guard let self = self else {return}
            self.loadingView.isHidden = true
            switch response {
            case .success(let result):
                self.discount = result.referralDiscount
                self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Расскажи своему другу, что мы поможем ему с поиском фотографов и визажистов, и он получит скидку \(result.referralDiscount)% на первую фотосессию.\n\nТы тоже получишь скидку \(result.referralDiscount)% сразу после того, как он успешно проведет свою первую фотосессию.")
            case .failure(let error):
                print(error.localizedDescription)
                self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Расскажи своему другу, что мы поможем ему с поиском фотографов и визажистов, и он получит скидку \(0)% на первую фотосессию.\n\nТы тоже получишь скидку \(0)% сразу после того, как он успешно проведет свою первую фотосессию.")
            }
        }
    }
    @objc func click(){
        if let name = URL(string: "https://wase.photo"), !name.absoluteString.isEmpty {
            let objectsToShare = ["Рекомендую классное приложение для организации фотосессий Wase. Здесь много классных фотографов и визажистов. Скачивай по моей ссылке \(self.link!) и закажи первую фотосессию со скидкой \(self.discount!)%."] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        }else  {
            // show alert for not available
        }
    }
    override func setUpConstraints() {
        super.setUpConstraints()
        self.mainLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(self.getTopOffset() + 20)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.buttonView.snp.makeConstraints{
            $0.bottom.equalToSuperview().offset(-58)
            $0.height.equalTo(56)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.buttonlabel.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview()
        }
    }
}
