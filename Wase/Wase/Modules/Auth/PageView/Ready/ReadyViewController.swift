//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class ReadyViewController : AirdronViewController {
    
    var onNext : (() -> Void)?
    
    var mainLabel = UILabel()
    
    var readyView = SampleButton()
    
    var readyIntro = UILabel()
    
    var topOffset = 0
    
    private var userCreation : UserCreation
    private var apiService : ApiService
    
    
    init(userCreation : UserCreation, apiService : ApiService){
        self.userCreation = userCreation
        self.apiService = apiService
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func makeIntroSupplier(){
        self.apiService.getReadyPageSupplier(){[weak self] response in
            switch response {
            case .success(let rr):
                self?.readyIntro.text = rr.text
                print(rr)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func makeIntroClien(){
        self.apiService.getReadyPageClient(){[weak self] response in
            switch response {
            case .success(let rr):
                self?.readyIntro.text = rr.text
                print(rr)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fetchData(){
        switch userCreation.type {
        case UserType(rawValue: "PHOTOGRAPHER") as? UserType:
            makeIntroSupplier()
            topOffset = Int(getTopOffset())
        case UserType(rawValue: "MAKEUP_ARTIST") as? UserType:
            makeIntroSupplier()
            topOffset = Int(getTopOffset())
        case UserType(rawValue: "CLIENT") as? UserType:
            makeIntroClien()
        default:
            print("deff")
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.view.backgroundColor = Color.white.value
        self.view.addSubview(mainLabel)
        self.view.addSubview(readyView)
        self.view.addSubview(readyIntro)
        self.mainLabel.font = UIFont.systemFont(ofSize: 35)
        self.mainLabel.textColor = .black
        self.mainLabel.text = "Готово"
        self.mainLabel.numberOfLines = 0
        self.navigationController?.isNavigationBarHidden = true
        self.readyIntro.numberOfLines = 0
        self.readyIntro.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.white.value).make(string: "")
        
        self.readyView.mainLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.white.value).make(string: "Начать")
        
        self.readyView.backgroundColor = Color.accent.value
        self.readyView.layer.cornerRadius = 10
        self.readyView.layer.masksToBounds = true
        
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.nextView))
        self.readyView.addGestureRecognizer(tap1)
        
        self.readyView.backgroundColor = Color.accent.value
        self.fetchData()
        print(userCreation)
        self.setUpConstraints()
        
        
    }
    
    @objc func nextView(){
        print("~~~~~")
        self.onNext?()
    }
    
    func getTopOffset() -> CGFloat {
        return (self.navigationController?.navigationBar.bounds.size.height ?? 0) + ( UIApplication.shared.statusBarFrame.height)
    }
    
    func setUpConstraints(){
        
        mainLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(topOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
        readyView.snp.makeConstraints{
            $0.bottom.equalToSuperview().offset(-50)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
        }
        readyIntro.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
    }
}



