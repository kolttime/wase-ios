//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import CoreTelephony
import libPhoneNumber_iOS
import SwiftPhoneNumberFormatter

class PhoneNumberViewController: SimpleNavigationBar, UITextViewDelegate{
    
    
    private lazy var loadingView  = TaokaLoadingView()
    
    private lazy var phoneNumberTextField = PhoneFormattedTextField()
    
    private let apiService: ApiService
    
    private let keyboardObserver = KeyboardObserver()
    
    var SMSLabel = UILabel()
    
    var regInfoView = UITextView()
    
    var onTextField : ((String, String) -> Void)?
    
    var russia = UILabel()
    
    var scrollViewController = ScrollViewController()
    
    var onUse : (() -> Void)?
    
    var onAgreement : (() -> Void)?
    
    private var activeField : UIView?
    private let textfieldTopOffset: CGFloat = Device.isIphone5 ? 20 : 120
    
    private var bottomConstraint: Constraint?
    
    private var bottomOffset: CGFloat = 10
    private lazy var mainlabel = UILabel()
    private var agreed : Bool
    
    private let phoneUtil = NBPhoneNumberUtil()
    
    init(apiService: ApiService, agreed : Bool = false) {
        self.apiService = apiService
        self.agreed = agreed
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        
        super.initialSetup()
        self.setBackTitle(title: "Назад")
        
        self.setLargeTitleHidden()
        
        self.regInfoView.delegate = self
        let smsString = self.agreed ? "Мы отправим код через СМС для подтверждения номера" : "Мы отправим код через СМС для регистрации или входа"
        self.SMSLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: smsString)
        self.SMSLabel.numberOfLines = 0
        
        self.russia.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Рoccия")
        
        self.view.backgroundColor = Color.white.value
        
        self.phoneNumberTextField.config.defaultConfiguration = PhoneFormat(defaultPhoneFormat: "### ### ## ##")
        self.phoneNumberTextField.prefix = "+7 "
        self.phoneNumberTextField.font = UIFont.systemFont(ofSize: 34)
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        self.view.addSubview(self.scrollViewController.view)
        
        self.scrollViewController.scrollView.showsHorizontalScrollIndicator = false
        self.mainlabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.make(string: "Введите номер телефона")
        self.mainlabel.numberOfLines = 0
        
        
        
        //        self.regInfoLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.black.value).make(string: "")
        
        let strNumber: NSString = "Регистрируясь, вы принимаете условия Лицензионного договора" as NSString
        
        
        let attributedString = NSMutableAttributedString(string: strNumber as String)
        let rangeUse = (strNumber).range(of: "Лицензионного договора")
        //let rangeAgree = (strNumber).range(of: "обработку персональных данных")
        attributedString.addAttribute(.link, value: "rangeUse", range: rangeUse)
        //attributedString.addAttribute(.link, value: "rangeAgree", range: rangeAgree)
        
        self.regInfoView.linkTextAttributes = [
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .underlineColor: Color.black.value
        ]
        self.regInfoView.attributedText = attributedString
        self.regInfoView.isEditable = false
        self.regInfoView.isScrollEnabled = false
        self.regInfoView.isScrollEnabled = false
        
        self.scrollViewController.scrollView.isScrollEnabled = false
        self.scrollViewController.scrollView.addSubview(self.mainlabel)
        self.scrollViewController.scrollView.addSubview(self.SMSLabel)
        self.scrollViewController.scrollView.addSubview(self.phoneNumberTextField)
        self.scrollViewController.scrollView.addSubview(self.russia)
        self.scrollViewController.scrollView.addSubview(self.regInfoView)
        if self.agreed {
            self.setTabBarHidden()
            self.regInfoView.isHidden = true
        }
        print("Scroll: \(self.scrollViewController.scrollView.contentOffset)")
        self.loadingView.isHidden = true
        self.scrollViewController.view.addSubview(self.loadingView)
        self.setupHandlers()
        self.SetupConstraints()
        
    }
    
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString == "rangeUse"{
            self.onUse?()
            print("Use")
        }
        if URL.absoluteString == "rangeAgree"{
            self.onAgreement?()
            print("Agree")
        }
        return false
    }
    
    @objc func textFieldFull(){
        print("done")
        self.loadingView.isHidden = false
        
        let Str = self.phoneNumberTextField.text?.replacingOccurrences(of: " ", with: "")
        let finalStr = String(Str!.dropFirst())
        print(finalStr)
        
            self.apiService.auth(byPhone: finalStr , agreed: self.agreed){[weak self] result in
                switch result{
                case .success(let response):
                    print(response)
                    self?.loadingView.isHidden = true
                    self?.onTextField?((self?.phoneNumberTextField.text!)!, response.key)
                case .failure(let error):
                    print(error)
                    self?.loadingView.isHidden = false
                }
            }
            }
    
    private func SetupConstraints(){
        self.scrollViewController.view.snp.makeConstraints{
            $0.top.equalToSuperview().offset(self.getTopOffset())
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.mainlabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.SMSLabel.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalTo(self.mainlabel.snp.bottom).offset(16)
        }
        
        self.regInfoView.snp.makeConstraints{
            $0.height.equalTo(60)
            $0.right.equalToSuperview().offset(-2*ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            self.bottomConstraint = $0.bottom.equalTo(self.bottomLayoutEdge).offset(-self.bottomOffset).constraint
        }
        
        self.phoneNumberTextField.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalTo(self.SMSLabel.snp.bottom).offset(34)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
        self.russia.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalTo(self.phoneNumberTextField.snp.bottom).offset(8)
        }
        
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.keyboardObserver.isSuspended = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.keyboardObserver.isSuspended = false
        phoneNumberTextField.becomeFirstResponder()
        
    }
    var keyFrame = CGRect.init(x: 0.0, y: 451.0, width: 375.0, height: 216.0)
    func setScrollOffset(offset : CGFloat = 0){
        if Device.isIphone5 {
            let keyMin = UIScreen.main.bounds.height - self.keyFrame.height
            let textFrame  = self.scrollViewController.scrollView.convert(self.regInfoView.frame, to: self.view)
            
            print("KeyMin: \(keyMin)\ntextFrame: \(textFrame.maxY)")
            
            if (textFrame.maxY + offset ) > keyMin {
                
                let scrollOffset = (textFrame.maxY + offset) - keyMin
                self.scrollViewController.scrollView.setContentOffset(CGPoint(x: self.scrollViewController.scrollView.contentOffset.x, y: scrollOffset), animated: true)
            } else {
                self.scrollViewController.scrollView.setContentOffset(CGPoint.zero, animated: true)
            }
        }
    }
    
}


extension String {
    internal func substring(start: Int, offsetBy: Int) -> String? {
        guard let substringStartIndex = self.index(startIndex, offsetBy: start, limitedBy: endIndex) else {
            return nil
        }
        
        guard let substringEndIndex = self.index(startIndex, offsetBy: start + offsetBy, limitedBy: endIndex) else {
            return nil
        }
        
        return String(self[substringStartIndex ..< substringEndIndex])
    }
}

extension PhoneNumberViewController{
    func setupHandlers(){
        phoneNumberTextField.textDidChangeBlock = { field in
            if let text = field?.text, text != "" {
                if text.count == 16{
                    self.textFieldFull()
                }
            } else {
                print("No text")
            }
        }
        
        
        self.keyboardObserver.onKeyboardDidShow = { [weak self] frame, duration in
            guard let self = self else {return}
            self.setScrollOffset(offset: self.regInfoView.frame.height)
            
            // self?.scrollViewController.scrollView.contentSize.height = self?.scrollViewController.scrollView.contentSize.height ?? 0 + y
        }
        
        
        self.keyboardObserver.onKeyboardWillShow = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.keyFrame = frame
            strongSelf.bottomConstraint?.update(offset: -frame.height
                - strongSelf.bottomOffset
                + strongSelf.bottomLayoutEdgeInset)
            strongSelf.view.animateLayout(duration: duration)
        }
        self.keyboardObserver.onKeyboardWillHide = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.bottomConstraint?.update(offset: -strongSelf.bottomOffset)
            strongSelf.view.animateLayout(duration: duration)
        }
    }
    
}


