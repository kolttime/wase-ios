//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import OneSignal

class PhoneCodeViewController: SimpleNavigationBar{
    
    private lazy var codeFiled: OMGCodeTextField = {
        let codeFiled = OMGCodeTextField()
        codeFiled.delegate = self
        return codeFiled
    }()
    
    var onCodeField : (() -> Void)?
    
    var onCompletion : Action?
    
    private let keyboardObserver = KeyboardObserver()
    
    var phoneNumber : String
    
    var mainLabel = UILabel()
    
    var numberInfo = UILabel()
    
    var timerLabel = UILabel()
    
    var timer: Timer?
    
    var timeLeft = 60
    
    var wrongLabel = UILabel()
    
    var agreed : Bool
    private var apiService : ApiService
    private var sessionManager : TaokaUserSessionManager
    private var key : String
    init(phoneNumber : String, apiService : ApiService, key : String, sessionManager : TaokaUserSessionManager, agreed : Bool = false) {
        self.agreed = agreed
        self.phoneNumber = phoneNumber
        self.apiService = apiService
        self.sessionManager = sessionManager
        self.key = key
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func initialSetup() {
        
        super.initialSetup()
        
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
        
        self.view.addSubview(self.mainLabel)
        self.view.addSubview(self.numberInfo)
        self.view.addSubview(self.codeFiled)
        self.view.addSubview(self.timerLabel)
        self.view.addSubview(self.wrongLabel)
        
        
        
        self.setTabBarHidden()

        
        self.mainLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.make(string: "Введите код \nиз СМС")
        self.mainLabel.numberOfLines = 0
        self.setLargeTitleHidden()
        
        self.numberInfo.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Код был отправлен на номер \n\(phoneNumber)")
        self.numberInfo.numberOfLines = 0
        
        self.wrongLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.red.value).make(string: "Неверный код")
        
        UIView.animate(withDuration: 0.01) {
            self.wrongLabel.alpha = 0
        }
        
        self.timerLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "")
        self.timerLabel.numberOfLines = 0
        let tap = UITapGestureRecognizer(target: self, action: #selector(makeSMSRequest))
        self.timerLabel.addGestureRecognizer(tap)
        self.timerLabel.isUserInteractionEnabled = true
        
        self.view.backgroundColor = Color.white.value
        self.SetupConstraints()
        
    }
    
    @objc func textCodeFull(){
        print("done")
        self.onCodeField?()
    }
    
    private func SetupConstraints(){
        self.mainLabel.snp.makeConstraints{
            
            $0.top.equalToSuperview().offset(self.getTopOffset())
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
        self.numberInfo.snp.makeConstraints{
            $0.top.equalTo(self.mainLabel.snp.bottom).offset(16)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-81)
            
        }
        
        self.codeFiled.snp.makeConstraints{
            $0.top.equalTo(self.numberInfo.snp.bottom).offset(32)
            $0.left.equalToSuperview()
        }
        
        self.timerLabel.snp.makeConstraints{
            $0.top.equalTo(self.codeFiled.snp.bottom).offset(17)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
        self.wrongLabel.snp.makeConstraints{
            $0.top.equalTo(self.codeFiled.snp.bottom).offset(-7)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
    }
    
    @objc func resendSMS(){
        print("done")
        var Str = self.phoneNumber.replacingOccurrences(of: " ", with: "")
        let finalStr = String(Str.dropFirst())
        print(finalStr)
        self.apiService.auth(byPhone: finalStr, agreed: self.agreed){[weak self] result in
            switch result{
            case .success(let response):
               // self?.onTimerFires()
                self?.key = response.key
                print(response)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @objc func makeSMSRequest() {
        print("tap working")
        UIView.animate(withDuration: 0.1) {
            self.wrongLabel.alpha = 0
        }
        timeLeft = 60
        timeLeft -= 1
        print(phoneNumber)
        onTimerFires()
        resendSMS()
    }
    
    @objc func onTimerFires()
    {
        timeLeft -= 1
        if timeLeft < 10{
            self.timerLabel.isUserInteractionEnabled = false
            self.timerLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Получить новый код 0:0\(timeLeft)")
        } else {
            self.timerLabel.isUserInteractionEnabled = false
            self.timerLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Получить новый код 0:\(timeLeft)")
        }
        if timeLeft <= 0 {
            timer = nil
            UIView.animate(withDuration: 0.1) {
                self.wrongLabel.alpha = 0
            }
//            UIView.animate(withDuration: 0.1) {
//                self.timerLabel.alpha = 0
//            }
            self.timerLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Получить новый код")
            self.timerLabel.isUserInteractionEnabled = true
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.keyboardObserver.isSuspended = true
        self.wrongLabel.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.keyboardObserver.isSuspended = false
        self.codeFiled.firstResponder()
        timeLeft = 60
        onTimerFires()
    }
    
    var onAgreed : ((User) -> Void)?
    
}
extension PhoneCodeViewController: CodeFieldViewDelegate{
    func codeDidChage(_ code: String) {
    }
    
    func didEnterCode(_ code: String) {
        self.apiService.verification(byKey: self.key, code: code) {[weak self] result in
            switch result {
            case .success(let response):
                print("success")
               // guard let strongself = self else {return}
               // if self?.agreed
                if let token = response.token {
                    
                    guard let strongself = self else {return}
                    if !strongself.agreed {
                        self?.sessionManager.save(token: token)
                        if let user = response.user{
                            OneSignal.sendTag("user_id", value: user.id)
                            self?.sessionManager.save(user: user)
                            self?.sessionManager.setFirstLaunch()
                            self?.onCompletion?()
                        } else {
                            self?.onCodeField?()
                        }
                        
                    } else {
                        guard let user = response.user else {
                            return
                        }
                        strongself.onAgreed?(user)
                    }
                    
                    
                } else {
                    
                    guard let self = self else {return}
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        self.wrongLabel.alpha = 1
                    })
                    
                    UIView.animate(withDuration: 0.1, delay: 1, options: .curveEaseIn, animations: {
                        self.wrongLabel.alpha = 0
                    }, completion: { (true) in
                        UIView.animate(withDuration: 0.1, animations: {
                            self.wrongLabel.alpha = 0
                        })
                    })
                    
                    
                    self.codeFiled.showError()
                    self.codeFiled.firstResponder()
                }
            case .failure(let error):
                print(error)
                guard let self = self else {return}
                self.codeFiled.showError()
                self.codeFiled.firstResponder()
            }
            
        }
        
    }
}


