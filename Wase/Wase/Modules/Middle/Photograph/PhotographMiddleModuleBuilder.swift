//
//  AuthModuleBuilder.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class PhotographMiddleModuleBuilder{
    private let apiService: ApiService
    private let sessionManager: TaokaUserSessionManager
    
    init(apiService: ApiService,
         sessionManager: TaokaUserSessionManager) {
        self.apiService = apiService
        self.sessionManager = sessionManager
    }
    
    
    func makeConfirmationModule(id : String) -> PhotoConfirmationViewController{
        return PhotoConfirmationViewController(id: id, apiService: self.apiService)
    }
    func makeServicesModule() -> PhotoServiceSettingsViewController {
        return PhotoServiceSettingsViewController(apiService: self.apiService, userModel: UserClass.shared.user!, filling: true)
    }
    func makeCancelTextModule(id : String, rating : Int, vc : PhotographMiddleViewController) -> UserCommentConfirmationViewController{
        return UserCommentConfirmationViewController(id: id, rating: 0, apiService: self.apiService, vc: vc, type : .photoCancel)
    }
    func makePhotographMiddleModule() -> PhotographMiddleViewController {
        return PhotographMiddleViewController(apiService: self.apiService, userModel: sessionManager.user!)
    }
    
    func makePhotographResultsModule(id : String) -> PhotoResultsViewController{
        let interactor = AddPhotoInteractor(apiService: self.apiService)
        let vc = PhotoResultsViewController(apiService: self.apiService, id: id)
        vc.interactor = interactor
        interactor.output = vc
        return vc
    }
    func makePhotosessionOfferModule(photo : Photosession) -> PhotosessionOfferView {
        return PhotosessionOfferView(apiService: self.apiService, photo: photo, me: self.sessionManager.user!)
    }
    
    func makeErrorViewController(photoId : String) -> PhotoSendErrorViewController{
        return PhotoSendErrorViewController(apiService: self.apiService, type: .photoError, rating: nil, photoId: photoId)
    }
    
    func makePhoneNumberViewController() -> PhoneNumberViewController{
        return PhoneNumberViewController(apiService: self.apiService, agreed: true)
    }
    
    func makePhoneCodeModule(phonenumber : String, key : String) -> PhoneCodeViewController {
        return PhoneCodeViewController(phoneNumber: phonenumber, apiService : self.apiService, key : key,  sessionManager: self.sessionManager, agreed: true)
    }
}



