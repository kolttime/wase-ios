//
//  PhotoConfirmationViewController.swift
//  Wase
//
//  Created by Роман Макеев on 14/09/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class PhotoConfirmationViewController : SimpleNavigationBar {
    
    
    
    private var confirmButton = UIView()
    private let infoLabel = UILabel()
    var confirmlabel = UILabel()
    private lazy var reviewField = OMGTextField.makePhotoReviewField()
    private lazy var daysField = OMGTextField.makeDaysCountField()
    private var keyObserver = KeyboardObserver()
    var id : String
    var apiService : ApiService
    init(id : String, apiService : ApiService){
        self.id = id
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var onRedy : Action?
    override func initialSetup() {
        super.initialSetup()
        self.view.backgroundColor = Color.white.value
        self.setTitle(title: "Подтверждение")
        self.setLargeTitle()
        self.setTabBarHidden()
        self.view.addSubview(self.confirmButton)
        self.confirmButton.addSubview(self.confirmlabel)
        self.confirmlabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: "Подтвердить")
        self.confirmButton.backgroundColor = Color.light.value
        self.confirmButton.alpha = 0.7
        self.confirmButton.isUserInteractionEnabled = false
        self.confirmButton.layer.cornerRadius = 10
        self.confirmButton.layer.masksToBounds = true
        self.view.addSubview(self.reviewField)
        self.view.addSubview(self.daysField)
        self.view.addSubview(self.infoLabel)
        let tapEdit = UITapGestureRecognizer(target: self, action: #selector(self.endEdditing))
        self.view.addGestureRecognizer(tapEdit)
        self.infoLabel.attributedText = CustomFont.tech11.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Количество дней в течении которых \nклиент получит фотографии")
        self.infoLabel.numberOfLines = 0
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.buttonClick))
        self.confirmButton.addGestureRecognizer(tap)
        self.toastPresenter.targetView = self.view
        self.setUpConstraints()
        self.setupHandlers()
    }
    @objc func endEdditing(){
        self.view.endEditing(true)
    }
    var onConfirm : ((LongPhotosession) -> Void)?
    @objc func buttonClick(){
        print("Clicked!")
        let comment = self.reviewField.text ?? ""
        let days = Int(self.daysField.text ?? "0")
        self.apiService.postConfirmation(photoId: self.id, rating: nil, comment: comment, resultDate: days) {
            [weak self] response in
            guard let self = self else {return}
            switch response {
            case .success(let photo):
                self.onConfirm?(photo)
            case .failure(let error):
                self.showToastErrorAlert(error)
                
            }
        }
        ///self.onRedy?()
    }
    var keyFrame : CGRect?
    func checkButtonState(){
        
        if !(self.reviewField.text?.isEmpty ?? true) && !(self.daysField.text?.isEmpty ?? false) {
            self.confirmButton.isUserInteractionEnabled = true
            self.confirmButton.alpha = 1
        } else {
            self.confirmButton.isUserInteractionEnabled = false
            self.confirmButton.alpha = 0.7
        }
    }
    func setupHandlers(){
        self.reviewField.onTextDidChanged = {[weak self] respose in
            self?.checkButtonState()
        }
        self.daysField.onTextDidChanged = {[weak self] response in
            guard let self = self else {return}
            self.checkButtonState()
        }
    }
    var keyShow = false
    override func setUpConstraints() {
        super.setUpConstraints()
        self.keyObserver.onKeyboardDidShow = {
            [weak self] frame, ff in
            guard let self = self else {return}
            if self.keyFrame == nil {
                self.keyFrame = frame
            }
            if !self.keyShow {
                let keyHeight = self.view.frame.maxY -  frame.height
                print("keyHeight : \(keyHeight)\ninfoMaxY : \(self.infoLabel.frame.maxY)")
                if keyHeight < self.infoLabel.frame.maxY {
                    let offset = -(keyHeight - self.infoLabel.frame.maxY)
                    UIView.animate(withDuration: 0.5, animations: {
                        self.view.center.y -= offset
                    })
                }
                }
            self.keyShow = true
        }
        self.keyObserver.onKeyboardWillHide = {[weak self] frame, ff in
            guard let self = self else {return}
            UIView.animate(withDuration: 0.5, animations: {
                //self?.view.center.y = self?.vi
                self.view.center.y = self.view.frame.height / 2.0
            })
            self.keyShow = false
        }
        self.confirmButton.snp.makeConstraints{
            $0.bottom.equalToSuperview().offset(-45)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
        }
        self.confirmlabel.snp.makeConstraints{
            $0.center.equalToSuperview()
        }
        self.reviewField.snp.makeConstraints{
            $0.top.equalTo(self.view.snp.centerY).offset(-30)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.daysField.snp.makeConstraints{
            $0.top.equalTo(self.reviewField.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.infoLabel.snp.makeConstraints{
            $0.top.equalTo(self.daysField.snp.bottom).offset(6)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
    }
    
}
