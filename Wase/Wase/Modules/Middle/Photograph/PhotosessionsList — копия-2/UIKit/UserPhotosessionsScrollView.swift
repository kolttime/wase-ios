//
//  UserPhotosessionsScrollView.swift
//  Taoka
//
//  Created by Minic Relocusov on 24/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit



class UserPhotosessionsScrollView : AirdronView {
    
    
    let mainView = UIView()
    let views : [UIView] = [UIView(), UIView(), UIView(), UIView()]
    let labels = [UILabel(), UILabel(), UILabel(), UILabel()]
    let scroll = ScrollViewController()
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.scroll.scrollView)
        for i in 0...3
        {
            self.mainView.addSubview(self.views[i])
            self.views[i].addSubview(self.labels[i])
        }
        self.scroll.scrollView.addSubview(self.mainView)
        
        //self.scroll.alwaysBounceHorizontal = true
        self.labels[0].attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Входящие")
        self.labels[1].attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Ожидаемые")
        self.labels[2].attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Активные")
        self.labels[3].attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Завершенные")
        //self.scroll.backgroundColor = Color.black.value
       // self.mainView.backgroundColor = Color.red.value
       // self.views[0].backgroundColor = UIColor.clear
       // self.mainView.backgroundColor = UIColor.clear
        
        self.setupConstraints()
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        self.scroll.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.right.equalToSuperview()
        }
        self.mainView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.views[0].snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.width.equalTo(100)
        }
        for i in 1...3 {
            self.views[i].snp.makeConstraints{
                $0.top.equalToSuperview()
                $0.left.equalTo(self.views[i-1].snp.right)
                $0.bottom.equalToSuperview()
                $0.width.equalTo(100)
            }
        }
        for label in self.labels {
            label.snp.makeConstraints{
                $0.centerX.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
    }
    
    
}
