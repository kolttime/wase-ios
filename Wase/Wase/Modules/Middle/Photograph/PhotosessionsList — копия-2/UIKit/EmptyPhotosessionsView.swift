//
//  EmptyPhotosessionsView.swift
//  Wase
//
//  Created by Роман Макеев on 18/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit


class EmptyPhotosessionsView : AirdronView {
    
   
    private let mainLabel = UILabel()
    private let button = UIView()
    private let tellLabel = UILabel()
    private let arrowImageView = UIImageView()
    var onClick : Action?
    private var mainString : String
    var buttonString : String
    init(mainString : String, buttonString : String){
        self.mainString = mainString
        self.buttonString = buttonString
        super.init(frame: .zero)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
       
        self.addSubview(self.button)
        self.addSubview(self.mainLabel)
        self.button.addSubview(self.tellLabel)
        self.button.addSubview(self.arrowImageView)
        self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: self.mainString)
        self.mainLabel.numberOfLines = 0
        self.tellLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.accent.value).make(string: self.buttonString)
        self.arrowImageView.image = UIImage.init(named: "Arrow2")!
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.Click))
        self.button.addGestureRecognizer(tap)
        self.setupConstraints()
        
        
    }
    
    @objc func Click(){
        self.onClick?()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.mainLabel.snp.makeConstraints{
            $0.centerY.equalTo(self)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.button.snp.makeConstraints{
            $0.top.equalTo(self.mainLabel.snp.bottom).offset(14)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            
        }
        
        self.tellLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.bottom.equalToSuperview().offset(-10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
        self.arrowImageView.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.right.equalToSuperview().offset(-23)
            $0.height.equalTo(13)
            $0.width.equalTo(8)
        }
        
        self.layoutIfNeeded()
        
        self.button.layoutIfNeeded()
    }
}
