//
//  UserPhotosessionCell.swift
//  Taoka
//
//  Created by Minic Relocusov on 18/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit


class PhotoPhotosessionsCellType2 : TableViewCell {
    var selection : ((Photosession) -> Void)?
    let baseView = UIView()
    let containerView = UIView()
    let topView = PhotoPhotosessionsTopView()
    let middleView = PhotoPhotosessionsMiddleView()
    let bottomView = UIView()
    let bottomlabel = UILabel()
    var photosession : Photosession?
    var viewModel : PhotoPhotosessionsCellModelType2?
    override func initialSetup() {
        super.initialSetup()
        
        self.backgroundColor = Color.white.value
        // self.contentView.addSubview(self.label)
        self.contentView.addSubview(self.baseView)
        // self.contentView.addSubview(self.mainImageView)
        
        baseView.backgroundColor = UIColor.clear
        baseView.layer.shadowColor = UIColor.black.cgColor
        baseView.layer.shadowOffset = CGSize(width: 0, height: 0)
        baseView.layer.shadowOpacity = 0.1
        baseView.layer.shadowRadius = 8.0
        self.containerView.frame = baseView.bounds
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.masksToBounds = true
        baseView.addSubview(self.containerView)
        //  baseView.layer.shadowPath = UIBezierPath(roundedRect: baseView.bounds, cornerRadius: 10).cgPath
        baseView.layer.shouldRasterize = true
        baseView.layer.rasterizationScale = UIScreen.main.scale
        self.containerView.backgroundColor = Color.light.value
        self.containerView.addSubview(self.topView)
        self.containerView.addSubview(self.middleView)
        self.containerView.addSubview(self.bottomView)
        self.bottomView.addSubview(self.bottomlabel)
        self.containerView.backgroundColor = Color.light.value
        self.contentView.backgroundColor = Color.white.value
        self.bottomView.backgroundColor = Color.white.value
        let bottomTap = UITapGestureRecognizer(target: self, action: #selector(self.bottomClick))
        //self.containerView.dropShadow()
        self.bottomView.addGestureRecognizer(bottomTap)
        self.setUpConstarints()
        
        // set the shadow properties
        
        
        // self.setupConstraints()
    }
    var first1 = true
    var first2 = true
    @objc func bottomClick(){
        self.viewModel?.bottomSelection?(self.photosession!)
    }
    override func configure(viewModel: TableViewCell.ViewModelType) {
        var viewModel = viewModel as! PhotoPhotosessionsCellModelType2
        self.selection = viewModel.selectionHandler
        self.viewModel = viewModel
        self.photosession = viewModel.photo
        self.topView.onComplain = {
            viewModel.complainHandler?(viewModel.photo.id)
        }
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        dateFormatter.locale =  Locale(identifier: "ru_MD")
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let date = dateFormatter.date(from: viewModel.photo.date)!
        
        dateFormatter.dateFormat = "dd MMMM"
        let weekDay = calendar.component(.weekday, from: date)
        var weekString = ""
        switch weekDay {
        case 1:
            weekString = "воскресенье"
        case 2:
            weekString = "понедельник"
        case 3:
            weekString = "вторник"
        case 4:
            weekString = "среда"
        case 5:
            weekString = "четверг"
        case 6:
            weekString = "пятница"
        case 7:
            weekString = "суббота"
            
        default:
            weekString = ""
        }
        let dateString = dateFormatter.string(from: date)
        self.topView.set(avatarUrl: viewModel.photo.author!.avatar?.original.absoluteString ?? "", name: "\(viewModel.photo.author!.givenName) \(viewModel.photo.author!.familyName)", date: "")
        var placeType = ""
        switch viewModel.photo.place.type {
        case .home:
            placeType = "дома"
        case .outdoors:
            placeType = "на выезде"
        case .studio:
            placeType = "в студии"
            
            
        }
        var durationString = ""
        switch viewModel.photo.duration {
        case 1:
            durationString = "1 час"
        case 2:
            durationString = "2 часа"
        case 3:
            durationString = "3 часа"
        case 4:
            durationString = "4 часа"
        default:
            durationString = "\(viewModel.photo.duration) часов"
        }
        
        //var bottomString = ""
        switch viewModel.photo.status {
        case .execution:
            self.bottomlabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Позвонить")
            self.middleView.set(top: "\(viewModel.photo.specialization.name!), \(placeType)", middle: "\(dateString), \(weekString)", bottom: "с \(viewModel.photo.time), \(durationString)", cost: "\(viewModel.photo.price!) ₽", endStage: false)
        case .confirmation:
            self.topView.messageImageView.isHidden = false
            self.bottomlabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Подтвердить проведение")
            self.middleView.set(top: "\(viewModel.photo.specialization.name!), \(placeType)", middle: "\(dateString), \(weekString)", bottom: "с \(viewModel.photo.time), \(durationString)", cost: "\(viewModel.photo.price!) ₽", endStage: false)
        case .results:
            
            self.bottomlabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Загрузить результаты")
            self.middleView.set(top: "\(viewModel.photo.specialization.name!), \(placeType)", middle: "\(dateString), \(weekString)", bottom: "с \(viewModel.photo.time), \(durationString)", cost: "\(viewModel.photo.price!) ₽", endStage: false)
        case .pretension:
            self.bottomlabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Ожидается проверка модератором")
            self.middleView.set(top: "\(viewModel.photo.specialization.name!), \(placeType)", middle: "\(dateString), \(weekString)", bottom: "с \(viewModel.photo.time), \(durationString)", cost: "\(viewModel.photo.price!) ₽", endStage: false)
        case .unselected:
            self.bottomlabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Утвержден другой фотограф")
            self.middleView.set(top: "\(viewModel.photo.specialization.name!), \(placeType)", middle: "\(dateString), \(weekString)", bottom: "с \(viewModel.photo.time), \(durationString)", cost: "\(viewModel.photo.price!) ₽", endStage: true)
        case .canceled:
            self.bottomlabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Фотосессия отменена клиентом")
            self.middleView.set(top: "\(viewModel.photo.specialization.name!), \(placeType)", middle: "\(dateString), \(weekString)", bottom: "с \(viewModel.photo.time), \(durationString)", cost: "\(viewModel.photo.price!) ₽", endStage: true)
        case .annuled:
            self.bottomlabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Фотосессия отменена модератором")
            self.middleView.set(top: "\(viewModel.photo.specialization.name!), \(placeType)", middle: "\(dateString), \(weekString)", bottom: "с \(viewModel.photo.time), \(durationString)", cost: "\(viewModel.photo.price!) ₽", endStage: true)
        case .completed:
            self.bottomlabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Результаты отправлены")
            self.middleView.set(top: "\(viewModel.photo.specialization.name!), \(placeType)", middle: "\(dateString), \(weekString)", bottom: "с \(viewModel.photo.time), \(durationString)", cost: "\(viewModel.photo.price!) ₽", endStage: false)
        default:
            self.middleView.set(top: "\(viewModel.photo.specialization.name!), \(placeType)", middle: "\(dateString), \(weekString)", bottom: "с \(viewModel.photo.time), \(durationString)", cost: "\(viewModel.photo.price!) ₽", endStage: false)
        }
            
        
        
    }
    func setUpConstarints(){
        self.baseView.snp.makeConstraints{
            $0.top.equalTo(self.contentView).offset(8)
            $0.left.equalTo(self.contentView).offset(ViewSize.sideOffset)
            $0.right.equalTo(self.contentView).offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.contentView).offset(-8)
            // $0.height.equalTo(169)
        }
        self.containerView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.topView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(40)
        }
        self.middleView.snp.makeConstraints{
            $0.top.equalTo(self.topView.snp.bottom).offset(1)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(86)
            //$0.bottom.equalToSuperview()
        }
        self.bottomView.snp.makeConstraints{
            $0.top.equalTo(self.middleView.snp.bottom).offset(1)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.bottomlabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(12)
            $0.centerY.equalToSuperview()
        }
        
    }
    
    override func didSelect() {
        self.selection?(self.photosession!)
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        return 186
        
    }
    
    
    
    
}
struct PhotoPhotosessionsCellModelType2 : TableCellViewModel {
    var cellType: TableViewCell.Type {return PhotoPhotosessionsCellType2.self}
    var photo : Photosession
    var bottomSelection : ((Photosession) -> Void)?
    let selectionHandler: ((Photosession) -> Void)?
    var complainHandler : ((String) -> Void)? 
}


