//
//  PhotosessionTableSlideView.swift
//  Taoka
//
//  Created by Роман Макеев on 05/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotosessionTableSlideView : SlideView {
    
    var onActive : ((UIView) -> Void)?
    var textField = OMGTextField.makeChangeCostField()
    private lazy var tableViewController = TableViewController()
    var titleLabel = UILabel()
    var selection : (() -> Void)?
    var onTextField : ((Int) -> Void)?
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.titleLabel)
        self.titleLabel.attributedText = CustomFont.bodyBold20.attributesWithParagraph.make(string: "Подскажите пожалуйста, почему не заинтересовало?"
        )
        self.titleLabel.numberOfLines = 0
        self.tableViewController.tableView.register(cellClass: PhotosessionSliddeTableViewCell.self)
        self.addSubview(self.tableViewController.view)
        self.addSubview(self.textField)
        self.textField.isHidden = true
        self.setupConstraintss()
        
        self.textField.onBeginEditing = {[weak self] smth in
            self?.onActive?(smth)
        }
        self.textField.onReturnHandler = {[weak self] in
            guard let string = self?.textField.text else {return}
            if string != "" {
                guard let response : Int = Int(string) else {return}
                self?.onTextField?(response)
            }
        }
        self.render()
    }
    func openTableView(){
        self.tableViewController.tableView.isHidden = false
        self.titleLabel.isHidden = false
        self.textField.isHidden = true
        self.open?()
        self.setRightTitle(title: "", completion: nil)
    }
    func openTextField(){
        self.tableViewController.tableView.isHidden = true
        self.textField.isHidden = false
        self.titleLabel.isHidden = true
        self.textField.becomeFirstResponder()
        self.open?()
        let completion : Action = {
            if !self.textField.isHidden {
                guard let string = self.textField.text else {return}
                if string != "" {
                    guard let response : Int = Int(string) else {return}
                    self.onTextField?(response)
                }
            } else {
                self.close?()
            }
        }
        self.setRightTitle(title: "Готово", completion: completion)
    }
    func render(){
        let selection : Action? = {[weak self] in
            self?.selection?()
        }
        let viewModels = [PhotosessionSliddeTableViewCellModel(name: "Особой причины нет", selectionHandler: selection), PhotosessionSliddeTableViewCellModel(name: "Неудобное время", selectionHandler: selection), PhotosessionSliddeTableViewCellModel(name: "Неудобное место", selectionHandler: selection), PhotosessionSliddeTableViewCellModel(name: "Другое", selectionHandler: selection) ]
        let section = DefaultTableSectionViewModel(cellModels: viewModels)
        self.tableViewController.update(viewModels: [section])
    }
     func setupConstraintss() {
        //super.setupConstraints()
        self.titleLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(54)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.tableViewController.view.snp.makeConstraints{
            $0.top.equalTo(self.titleLabel.snp.bottom).offset(5)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.textField.snp.makeConstraints{
            $0.top.equalToSuperview().offset(44)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
    }
}
