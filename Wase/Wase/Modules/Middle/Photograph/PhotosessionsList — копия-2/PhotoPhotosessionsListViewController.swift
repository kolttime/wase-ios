//
//  PhotoPhotosessionsListViewController.swift
//  Taoka
//
//  Created by Minic Relocusov on 23/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit


class PhotographMiddleViewController : SimpleNavigationBar {
    
    private let emptyView = EmptyPhotosessionsView(mainString: "Расскажите о Wase в своих социальных сетях, чтобы привлечь еще больше людей в наше сообщество любителей создавать прекрасные фотографии", buttonString: "Рассказать о сервисе")
    let buttonView = UIView()
    private var inboxModels : [PhotoPhotosessionsCellModelType1]?
    private var waitingModels : [PhotoPhotosessionsCellModelType1]?
    private var activeModels : [PhotoPhotosessionsCellModelType2]?
    private var endedModels : [PhotoPhotosessionsCellModelType2]?
    var onFill : Action?
    let headerView = UserPhotosessionsScrollView()
    private lazy var tableViewController = TableViewController()
    let separator = UIView()
    let activeView = UIView()
    var onNext : ((Photosession) -> Void)?
    private var loadingView = TaokaLoadingView()
    var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 16
        layout.minimumLineSpacing = 16
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
   // var sessionManager : TaokaUserSessionManager
    private lazy var defaultView = UIView()
    var tabIndex : TabIndex = .inbox
    var userModel : User
    private var apiService : ApiService
    init(apiService : ApiService, userModel : User) {
        self.apiService = apiService
       // self.sessionManager = sessionManager
        self.userModel = userModel
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func initialSetup() {
        super.initialSetup()
        self.add(self.tableViewController)
        self.tableViewController.tableView.register(cellClass: PhotoPhotosessionsCellType1.self)
        self.tableViewController.tableView.register(cellClass: PhotoPhotosessionsCellType2.self)
        self.emptyView.isHidden = true
        self.collectionView.register(PhotoPhotosessionsCollectionViewCell.self, forCellWithReuseIdentifier: "hello")
        self.toastPresenter.targetView = self.view
        self.collectionView.backgroundColor = Color.white.value
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        //self.collectionView.setContentOffset(CGPoint(x: 24, y: 0), animated: false)
        //self.tableViewController.tableView.setContentOffset(CGPoint(x: 0, y: 50), animated: false)
        self.setTitle(title: "Фотосессии")
        self.setBackTitle(title: "Фотосессии")
        self.setLargeTitle()
        //self.tableViewController.view.addSubview(self.headerView)
        self.tableViewController.tableView.contentInset = UIEdgeInsets(top: 48, left: 0, bottom: 0, right: 0)
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: ViewSize.sideOffset, bottom: 0, right: ViewSize.sideOffset)
        //self.collectionViewController
        //self.headerView.backgroundColor = Color.red.value
      //  self.tableViewController.tableView.tableHeaderView = self.headerView
        self.collectionView.showsVerticalScrollIndicator = false
        self.collectionView.showsHorizontalScrollIndicator = false
        let width : CGFloat = {
            let lbl = UILabel()
            lbl.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Входящие")
            return lbl.textWidth()
        }()
        self.separator.backgroundColor = Color.grey.value
        self.collectionView.addSubview(self.separator)
        //self.separator.layer.zPosition = 999999
        self.collectionView.bringSubviewToFront(self.separator)
        self.collectionView.addSubview(self.activeView)
        self.activeView.frame = CGRect(x: 0, y: 38, width: width, height: 2)
        self.activeView.backgroundColor = Color.black.value
        self.collectionView.bringSubviewToFront(self.activeView)
        self.navigationController!.view.addSubview(self.collectionView)
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        self.view.addSubview(self.loadingView)
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.loadingView.isHidden = true
        self.setUpConstraints()
        
//        let selection = {[weak self] in
//            self?.onNext?("fdsfds")
//            print("FFFFFFFFFFFFFF")
//        }
//
//        let viewModels = [PhotoPhotosessionsCollectionViewCellModel(name: "Входящие", selectionHandler: selection), PhotoPhotosessionsCollectionViewCellModel(name: "Ожидаемые", selectionHandler: selection), PhotoPhotosessionsCollectionViewCellModel(name: "Активные", selectionHandler: nil), PhotoPhotosessionsCollectionViewCellModel(name: "Завершенные", selectionHandler: nil)]
//        let section = DefaultCollectionSectionViewModel(cellModels: viewModels)
       // self.collectionViewController.update(viewModels: [section])
        
       // self.view.addSubview(self.collectionView)
        self.view.addSubview(self.emptyView)
        self.emptyView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.emptyView.layoutIfNeeded()
        let onClick : Action? = {[weak self] in
            print("Заебок!")
            guard let self = self else {return}
            if let name = URL(string: "https://wase.photo"), !name.absoluteString.isEmpty {
                let objectsToShare = ["Подключайся в Wase и получай памятные фотографии", name] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                self.present(activityVC, animated: true, completion: nil)
            }else  {
                // show alert for not available
            }
        }
        self.emptyView.onClick = onClick
        self.view.addSubview(self.defaultView)
        self.collectionView.layoutSubviews()
        self.collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .right)
        self.render(tabIndex: .inbox)
        if self.userModel.checkFills() {
            //self.render(tabIndex: .inbox)
        } else {
            self.tableViewController.tableView.isHidden = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.fillTap(recognizer:)))
            self.collectionView.isHidden = true
            
           // self.view.backgroundColor = Color.green.value
            let defaultImageView = UIImageView.init(image: UIImage.init(named: "Group")!)
            defaultImageView.contentMode = .scaleAspectFill
            self.defaultView.addSubview(defaultImageView)
            let label = UILabel()
            label.attributedText  = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Чтобы начать принимать заявки расскажите о некоторых деталях вашей работы и занятости.")
            self.defaultView.addSubview(label)
            //let buttonView = UIView()
            buttonView.backgroundColor = Color.white.value
            let buttonLabel = UILabel()
            buttonLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Заполнить")
            let arrowImageView = UIImageView.init(image: UIImage(named: "Arrow2")!)
            buttonView.addSubview(buttonLabel)
            buttonView.addSubview(arrowImageView)
            self.defaultView.addSubview(buttonView)
           // self.defaultView.backgroundColor = Color.red.value
            self.defaultView.snp.makeConstraints{
                $0.height.equalToSuperview()
                $0.width.equalToSuperview()
                $0.top.equalToSuperview().offset(self.getTopOffset())
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
                $0.bottom.equalToSuperview
            }
            defaultImageView.snp.makeConstraints{
                $0.top.equalToSuperview().offset(52)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.width.equalTo(152)
                $0.height.equalTo(104)
            }
            label.numberOfLines = 0
            label.snp.makeConstraints{
                $0.top.equalTo(defaultImageView.snp.bottom).offset(24)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset - 10)
            }
            buttonView.snp.makeConstraints{
                $0.top.equalTo(label.snp.bottom).offset(24)
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
                $0.height.equalTo(64)
            }
            buttonView.addGestureRecognizer(tap)
            buttonView.isUserInteractionEnabled = true
           // self.view.addGestureRecognizer(tap)
           // self.defaultView.addGestureRecognizer(tap)
            self.view.bringSubviewToFront(defaultView)
            buttonLabel.snp.makeConstraints{
                $0.top.equalToSuperview().offset(8)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                
            }
            arrowImageView.snp.makeConstraints{
                $0.top.equalToSuperview().offset(15.5)
                $0.right.equalToSuperview().offset(-23)
                $0.height.equalTo(13)
                $0.width.equalTo(8)
            }
            self.collectionView.isHidden = true
        }
        self.fetcUser()
        
       // self.render()
    }
    func forceUpdate(){
        switch self.tabIndex {
        case .inbox:
            self.inboxModels = nil
            self.render(tabIndex: .inbox)
        case .waiting:
            self.waitingModels = nil
            self.render(tabIndex: .waiting)
        case .active:
            self.activeModels = nil
            self.render(tabIndex: .active)
        case .ended:
            self.endedModels = nil
            self.render(tabIndex: .ended)
            
        }
    }
    func updateInbox(id : String){
        
        switch self.tabIndex {
        case .inbox:
            guard let index = self.inboxModels?.firstIndex(where: {$0.photo.id == id}) else {return}
            self.inboxModels?.remove(at: index)
        case .waiting:
            guard let index = self.waitingModels?.firstIndex(where: {$0.photo.id == id}) else {return}
            self.waitingModels?.remove(at: index)
        case .active:
            guard let index = self.activeModels?.firstIndex(where: {$0.photo.id == id}) else {return}
            self.activeModels?.remove(at: index)
        case .ended:
            guard let index = self.endedModels?.firstIndex(where: {$0.photo.id == id}) else {return}
            self.endedModels?.remove(at: index)
        
        }
        
    }
    func firstFetch(){
        self.loadingView.isHidden = false
        self.tableViewController.tableView.isHidden = true
        self.collectionView.isHidden = true
        self.apiService.getPhotoCount{
            [weak self] response in
            guard let self = self else {return}
            switch response {
            case .success(let result):
                print(result.photosessions)
                self.loadingView.isHidden = true
                if result.photosessions ==  0 {
                    //self.defaultView.isHidden = true
                   
                    self.emptyView.isHidden = false
                } else {
                    self.emptyView.isHidden = true
                    self.collectionView.isHidden = false
                    self.tableViewController.tableView.isHidden = false
                }
            case .failure(let error):
                self.showToastErrorAlert(error)
            }
        }
    }
    func fetcUser(){
        //self.loadingView.isHidden = false
        
        if UserClass.shared.user != nil {
            self.userModel = UserClass.shared.user!
            if self.userModel.checkFills() {
                
                    self.collectionView.isHidden = false
                    self.defaultView.isHidden = true
                    self.tableViewController.tableView.isHidden = false
                    self.firstFetch()
                
            } else {
                self.defaultView.isHidden = false
                self.tableViewController.tableView.isHidden = true
                self.collectionView.isHidden = true
            }
        } else {
            
                self.defaultView.isHidden = true
                self.tableViewController.tableView.isHidden = false
                self.collectionView.isHidden = false
            
            self.buttonView.isUserInteractionEnabled = false
            self.apiService.getProfile(userId: nil) { [weak self] response in
                switch response {
                case .success(let user):
                    print("USER \(user)")
                    self?.buttonView.isUserInteractionEnabled = true
                    self?.userModel = user
                    UserClass.shared.user = user
                    guard let self = self else {return}
                    if self.userModel.checkFills() {
                        
                            self.collectionView.isHidden = false
                            self.defaultView.isHidden = true
                            self.tableViewController.tableView.isHidden = false
                            self.firstFetch()
                    } else {
                        self.defaultView.isHidden = false
                        self.tableViewController.tableView.isHidden = true
                        self.collectionView.isHidden = true
                    }
                case .failure(let error):
                    self?.showToastErrorAlert(error)
                    self?.loadingView.isHidden = false
                }
            }
        }
    }
    @objc func fillTap(recognizer : UITapGestureRecognizer){
       
        
        self.onFill?()
        
        
    }
    func patchPhotosession(photo : Photosession){
        if self.waitingModels != nil {
           
         
            let newModel = PhotoPhotosessionsCellModelType1(photo: photo) { (photo) in
                self.onNext?(photo)
            }
            var allModels : [PhotoPhotosessionsCellModelType1] = [newModel]
            allModels.append(contentsOf: self.waitingModels ?? [])
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
        
            self.waitingModels?.sort(by: { (photo1, photo2) -> Bool in
                //2019-07-08T07:45:10
                let date1 = dateFormatter.date(from: photo1.photo.createdAt)!
                let date2 = dateFormatter.date(from: photo2.photo.createdAt)!
                
                return date1 > date2
            })
        
        
        }
        var i = 0
        for model in self.inboxModels ?? [] {
            if model.photo.id == photo.id {
                self.inboxModels?.remove(at: i)
                self.render(tabIndex: .inbox)
                return
            }
            i += 1
        }
        
        
    }
    var onConfirmation : ((String) -> Void)?
    var onResult : ((String) -> Void)?
    var onError : ((String) -> Void)?
    func render(tabIndex : TabIndex) {
        
        var viewModels : [TableCellViewModel] = []
        
        self.loadingView.isHidden = false
        switch tabIndex {
        case .inbox:
            
                self.apiService.getPhotoPhotosessions(tabIndex: tabIndex, id: nil) {[weak self] response in
                    switch response {
                    case .success(let photosessions):
                        print("PHOTOSESSIONS : \(photosessions.count)")
                        self?.loadingView.isHidden = true
                        //self?.inboxModels = photosessions
                        var inboxModels : [PhotoPhotosessionsCellModelType1] = []
                        for model in photosessions {
                            let viewModel = PhotoPhotosessionsCellModelType1(photo: model, selectionHandler: { (photo) in
                                self?.onNext?(photo)
                            })
                            viewModels.append(viewModel)
                            inboxModels.append(viewModel)
                        }
                        
                        self?.inboxModels = inboxModels
                        let section = DefaultTableSectionViewModel(cellModels: viewModels)
                        self?.tableViewController.update(viewModels: [section])
                    case .failure(let error):
                        print(error)
                        self?.showToastErrorAlert(error)
                    }
                }
            
        case .waiting:
            
                self.apiService.getPhotoPhotosessions(tabIndex: tabIndex, id: nil) {[weak self] response in
                    switch response {
                    case .success(let photosessions):
                        print("")
                        self?.loadingView.isHidden = true
                        //self?.waitingModels = photosessions
                        var waitingModels : [PhotoPhotosessionsCellModelType1] = []
                        for model in photosessions {
                            let viewModel = PhotoPhotosessionsCellModelType1(photo: model, selectionHandler: { (photo) in
                                self?.onNext?(photo)
                            })
                            viewModels.append(viewModel)
                            waitingModels.append(viewModel)
                        }
                        
                        self?.waitingModels = waitingModels
                        let section = DefaultTableSectionViewModel(cellModels: viewModels)
                        self?.tableViewController.update(viewModels: [section])
                    case .failure(let error):
                        print(error)
                        self?.showToastErrorAlert(error)
                    }
                
            }
        case .active:
            let bottomSelection : ((Photosession) -> Void)? = {[weak self] photo in
                guard let self = self else {return}
                if photo.status == .execution {
                    guard let number = URL(string: "tel://" + photo.author!.phone!) else { return }
                    UIApplication.shared.open(number)
                } else if photo.status == .confirmation {
                    self.onConfirmation?(photo.id)
                } else if photo.status == .results {
                    self.onResult?(photo.id)
                }
            }
            
                self.apiService.getPhotoPhotosessions(tabIndex: tabIndex, id: nil) {[weak self] response in
                    switch response {
                    case .success(let photosessions):
                        print(photosessions)
                        for photo in photosessions {
                            print(photo.title)
                        }
                        self?.loadingView.isHidden = true
                        //self?.waitingModels = photosessions
                        var activeModels : [PhotoPhotosessionsCellModelType2] = []
                        let complainHandler : ((String) -> Void)? = {[weak self] id in
                            self?.onError?(id)
                        }
                        for model in photosessions {
                            let viewModel = PhotoPhotosessionsCellModelType2(photo: model, bottomSelection: bottomSelection, selectionHandler: { (photo) in
                                self?.onNext?(photo)
                            }, complainHandler: complainHandler)
                            viewModels.append(viewModel)
                            activeModels.append(viewModel)
                        }
                        
                        self?.activeModels = activeModels
                        let section = DefaultTableSectionViewModel(cellModels: viewModels)
                        self?.tableViewController.update(viewModels: [section])
                    case .failure(let error):
                        print(error)
                        self?.showToastErrorAlert(error)
                    }
                
            }
        case .ended:
            let bottomSelection : ((Photosession) -> Void)? = {[weak self] photo in
//                guard let self = self else {return}
//                if photo.status == .execution {
//                    guard let number = URL(string: "tel://" + photo.author!.phone!) else { return }
//                    UIApplication.shared.open(number)
//                } else if photo.status == .confirmation {
//                    self.onConfirmation?(photo.id)
//                } else if photo.status == .results {
//                    self.onResult?(photo.id)
//                }
            }
            
                self.apiService.getPhotoPhotosessions(tabIndex: tabIndex, id: nil) {[weak self] response in
                    switch response {
                    case .success(let photosessions):
                        print(photosessions)
                        for photo in photosessions {
                            print(photo.title)
                        }
                        self?.loadingView.isHidden = true
                        //self?.waitingModels = photosessions
                        var endedModels : [PhotoPhotosessionsCellModelType2] = []
                        for model in photosessions {
                            let viewModel = PhotoPhotosessionsCellModelType2(photo: model, bottomSelection: bottomSelection, selectionHandler: { (photo) in
                                self?.onNext?(photo)
                            }, complainHandler: nil)
                            viewModels.append(viewModel)
                            endedModels.append(viewModel)
                        }
                        
                        self?.endedModels = endedModels
                        let section = DefaultTableSectionViewModel(cellModels: viewModels)
                        self?.tableViewController.update(viewModels: [section])
                    case .failure(let error):
                        print(error)
                        self?.showToastErrorAlert(error)
                    }
                
            }
        default:
            print("fff")
            self.apiService.getPhotoPhotosessions(tabIndex: tabIndex, id: nil){[weak self] response in
                switch response {
                case .success(let result):
                    print(result)
                    for photo in result{
                        print(photo.title)
                    }
                case .failure(let error):
                    self?.showToastErrorAlert(error)
                }
            }
        }
        
        
    }
    func render(){
        
        let selection : (() -> Void) = {[weak self] in
            print("FFFFFFFF")
            //self?.onNext?()
        }

        
//        let viewModels : [PhotoPhotosessionsCellModelType1] = [PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection), PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection), PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection), PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection), PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection), PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection), PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection)]
//        let section = DefaultTableSectionViewModel(cellModels: viewModels)
//        self.tableViewController.update(viewModels: [section])
        
    }
    
    
    override func setUpConstraints() {
        super.setUpConstraints()
        
        self.tableViewController.view.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.collectionView.snp.makeConstraints{
            $0.top.equalTo(self.navigationController!.navigationBar.snp.bottom)
           // $0.top.equalToSuperview().offset(self.getTopOffset())
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            //$0.centerX.equalToSuperview()
            $0.height.equalTo(40)
        }
        self.separator.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview().offset(20)
            $0.bottom.equalToSuperview()
            $0.height.equalTo(1)
            $0.width.equalToSuperview()
            //$0.left.equalToSuperview()
            //$0.right.equalToSuperview()
            
        }
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.collectionView.removeFromSuperview()
        self.collectionView.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
      
        //self.userModel = self.sessionManager.user!
        print("PhotoUserMOdel \(self.userModel)")
        guard let user = UserClass.shared.user else {return}
        self.userModel = user
        if self.userModel.checkFills() {
            
                self.collectionView.isHidden = false
                self.defaultView.isHidden = true
                self.tableViewController.tableView.isHidden = false
                self.firstFetch()
        } else {
            self.defaultView.isHidden = false
            self.tableViewController.tableView.isHidden = true
            self.collectionView.isHidden = true
        }
    }
    
}


extension PhotographMiddleViewController : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "hello", for: indexPath) as! PhotoPhotosessionsCollectionViewCell
        let viewModel : PhotoPhotosessionsCollectionViewCellModel
        if indexPath.item == 0 {
            viewModel = PhotoPhotosessionsCollectionViewCellModel(name: "Входящие", selectionHandler: nil)
        } else if indexPath.item == 1 {
            viewModel = PhotoPhotosessionsCollectionViewCellModel(name: "Ожидаемые", selectionHandler: nil)
        } else if indexPath.item == 2 {
            viewModel = PhotoPhotosessionsCollectionViewCellModel(name: "Активные", selectionHandler: nil)
        } else {
            viewModel = PhotoPhotosessionsCollectionViewCellModel(name: "Завершенные", selectionHandler: nil)
        }
        cell.configure(viewModel: viewModel)
        return cell
        
    }
    func change(item : Int){
        for i in 0...3 {
            guard self.collectionView.cellForItem(at: IndexPath(item: i, section: 0)) != nil else {continue}
            let cell = self.collectionView.cellForItem(at: IndexPath(item: i, section: 0)) as! PhotoPhotosessionsCollectionViewCell
            let name = cell.nameLabel.text!
            if i != item {
                cell.nameLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: name)
            } else {
                cell.nameLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.make(string: name)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //collectionView.setContentOffset(CGPoint(x: -24, y: 0), animated: false)
    
            //self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -24)
            self.collectionView.scrollToItem(at: indexPath, at: .right, animated: true)
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoPhotosessionsCollectionViewCell
        self.change(item: indexPath.item)
        
        let width = cell.nameLabel.textWidth()
        UIView.animate(withDuration: Double(0.25),
                       delay: 0,
                       // 6
            options: UIView.AnimationOptions.curveEaseOut,
            animations: {[self.activeView.frame = CGRect(x: cell.center.x - (width/2), y: 38, width: width, height: 2)]},
            completion: nil)
    
        print(indexPath.item)
        if indexPath.item == 0 {
            render(tabIndex: .inbox)
            self.tabIndex = .inbox
        } else if indexPath.item == 1{
            render(tabIndex: .waiting)
            self.tabIndex = .waiting
        } else if indexPath.item == 2 {
            render(tabIndex: .active)
            self.tabIndex = .active
        }else {
            render(tabIndex: .ended)
            self.tabIndex = .ended
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthLabel = UILabel()
        
        if indexPath.item == 0 {
            widthLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Входящие")
            
        } else if indexPath.item == 1 {
            widthLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Ожидаемые")
        }
        else if indexPath.item == 2 {
            widthLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Активные")
        }
        else if indexPath.item == 3 {
            widthLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Завершенные")
        }
        return CGSize(width: widthLabel.textWidth(), height: 40)
    }
}

