//
//  AddAlbumViewController.swift
//  Taoka
//
//  Created by Minic Relocusov on 29/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit
import Gallery
import Photos


class PhotoResultsViewController : SimpleNavigationBar, UIScrollViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.specializations.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.specializations[row].name!
        
    }
    var count = 1 {
        didSet {
            print("COUNT \(count)")
        }
    }
    var bottomLabel = UILabel()
    var onBackWithImages : (([Image]) -> Void)?
    var onBackWithAlbum : ((Album) -> Void)?
    let addLabel = UILabel()
    //var typeString = ""
    //var dateString = ""
    var isExporting = true
    //let datePicker = UIDatePicker()
    //let customPicker = UIPickerView()
    //var activeField = UIView()
    //private lazy var slideView = SlideView()
    let linkField = OMGTextField.makeLinkTextField()
    var interactor: AddPhotoInteractorInput!
    private var viewModels : [SubCollectionViewCellModel] = []
    //private lazy var nameTextField = OMGTextField.makeAlbumName()
    //private lazy var dateField = TimeViewClass.makeDateAlbum()
    //private lazy var typeField = TimeViewClass.makeTypeAlbum()
    let switchView = SwitchView.init(textLabel: "Запросить публикацию \nв портфолио")
    var testView = UIView()
    var helperView = UIView()
    private lazy var addButton = SampleButton.init(textLabel: "Отправить")
    let loadingView = TaokaLoadingView()
    private var topLabel = UILabel()
    private lazy var scrollViewController = ScrollViewController()
    private lazy var collectionViewController = CollectionWaterfallViewController(layout: self.collectionViewLayout)
    private var collectionView: UICollectionView { return self.collectionViewController.collectionView! }
    private lazy var collectionViewLayout: AirdronCollectionViewWaterfallLayout = {
        let layout = AirdronCollectionViewWaterfallLayout()
        layout.minimumColumnSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -2)
        return layout
    }()
    
    var id : String
    private var apiService : ApiService
    init(apiService : ApiService, id : String) {
        self.apiService = apiService
        self.id = id
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func initialSetup() {
        super.initialSetup()
        self.setTitle(title: "Завершение")
        self.setLargeTitle()
        //self.view.addSubview(self.scrollViewController.scrollView)
        self.setTabBarHidden()
        //fbhdsjgfalsdfjdsabjfdsnbjfsda
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.scrollViewController.scrollView)
        self.view.addSubview(self.loadingView)
       
        self.scrollViewController.scrollView.addSubview(self.switchView)
        self.scrollViewController.scrollView.addSubview(self.addButton)
        self.scrollViewController.scrollView.alwaysBounceHorizontal = false
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.collectionViewController.collectionView.isScrollEnabled = false
        self.collectionViewController.collectionView.register(cellClass: SubCollectionViewCell.self)
        self.collectionViewController.collectionView.register(cellClass: AddAlbumCollectionViewCell.self)
        self.collectionViewController.collectionView.register(cellClass: AddPhotoCollectionViewCell.self)
        self.scrollViewController.scrollView.addSubview(self.collectionViewController.collectionView)
        self.addButton.backgroundColor = Color.light.value
        //self.dateField.setDefault(text: "Дата фотосессии")
        //self.typeField.setDefault(text: "Тип фотосессии")
        self.scrollViewController.scrollView.addSubview(self.helperView)
        self.helperView.isHidden = true
        self.scrollViewController.scrollView.addSubview(self.linkField)
        self.addButton.mainLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.black.value).make(string: "Отправить")
        self.addButton.layer.cornerRadius = 10
        self.addButton.layer.masksToBounds = true
        self.toastPresenter.targetView = self.view
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.topLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Загрузите лучшие фотографии для альбома фотосессии, до 9 шт.")
        self.topLabel.numberOfLines = 0
        self.scrollViewController.scrollView.addSubview(self.topLabel)
        //  self.slideView.open?()
        self.addButton.addSubview(self.addLabel)
        self.addLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.black.value).make(string: "Отправить")
        self.bottomLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Загрузите все результаты в облако (Яндекс Диск, Google Drive или др.)")
        self.scrollViewController.scrollView.addSubview(self.bottomLabel)
        self.bottomLabel.numberOfLines = 0
        self.switchView.switchView.setOn(true, animated: false)
        self.switchView.onSwitchOn = {[weak self] in
            guard let self = self else {return}
            UIView.animate(withDuration: 0.5, animations: {
                
               
            })
            
        }
        self.switchView.onSwitchOff = {[weak self] in
            guard let self = self else {return}
            UIView.animate(withDuration: 0.5, animations: {
                
                
                
            })
            
        }
        
        
        
        // self.collectionViewController.collectionView.backgroundColor = Color.red.value
        //        self.addButton.center.y = 100
        self.loadingView.isHidden = false
        self.scrollViewController.scrollView.layer.layoutIfNeeded()
        self.setupHandlers()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.uploadTouch))
        self.addButton.addGestureRecognizer(tap)
        self.render()
        
    }
    
    
    
    
    func setupHandlers(){
        
        self.addButton.isUserInteractionEnabled = false
        self.addButton.alpha = 0.7
        self.linkField.onTextDidChanged = {[weak self] response in
            guard let self = self else {return}
            self.storageString = response.text
            if self.storageString == "" {
                self.addButton.isUserInteractionEnabled = false
                self.addButton.alpha = 0.7
            } else {
                self.addButton.isUserInteractionEnabled = true
                self.addButton.alpha = 1
            }
            
        }
        
    }
    
    func add(images: [Gallery.Image]) {
        //   self.isExporting = true
        //  self.uploadButton?.update(state: .disabled)
        let viewModels = self.interactor.makeViewModels(images: images)
        self.collectionViewController.collectionView.performBatchUpdates({
            viewModels.forEach { viewModel in
                self.collectionViewController.insert(viewModel,
                                                     for: self.collectionViewController.collectionView.numberOfItems(inSection: 0) - 1,
                                                     in: 0)
            }
        },
                                                                         completion: nil)
    }
    var onAddImages : ((Int) -> Void)?
    private func initialViewModels() {
        let addViewModel = AddAlbumCollectionViewCellModel { [weak self] in
            guard let self = self else {return}
            self.onAddImages?(10 - self.count)
        }
        let section = DefaultCollectionSectionViewModel(cellModels: [addViewModel])
        self.collectionViewController.update(viewModels: [section])
        
    }
    
    var storageString = ""
    @objc func uploadTouch(){
        var canUpload = true
        canUpload = !isExporting
        if canUpload {
            self.interactor.uploadResult(id: self.id, storage: self.storageString, visibility: self.switchView.switchView.isOn)
        }
        
        
        self.isExporting = true
    }
    
    private var specializations : [Specialization] = []
    func render(){
        self.initialViewModels()
        self.apiService.specialization() {[weak self] result in
            switch result {
            case .success(let response):
                self?.specializations = response
                self?.loadingView.isHidden = true
            case .failure(let error):
                print(error)
                self?.showToastErrorAlert(error)
            }
        }
        self.setUpConstraints(fields: true)
    }
    
    func setUpConstraints(fields: Bool) {
        super.setUpConstraints()
        var count = self.count
        if count > 9 {
            count = 9
        }
        let height = (UIScreen.main.bounds.size.width - 2) / 3.0
        
        let counted : Int = roundNumber(num: Float(count) / 3.0)
        let finalHeight = ( Float(height) * Float(counted) ) + Float(counted - 1)
        
        
        let maxHeight = UIScreen.main.bounds.size.height
        var fHeight : CGFloat = CGFloat(finalHeight + 64)
        
        
        if Device.isIphoneXSmax || Device.isIphoneX {
            fHeight += 60
        }
        if fields {
            fHeight += 173
        }
        
        let btnHeight : CGFloat = 56 + 58
        var offset = (maxHeight - fHeight - btnHeight) < 40 ? 40 : (maxHeight - fHeight - btnHeight - 58)
        print("Height: \(height)\ncounted: \(counted)\nfinalHeight: \(finalHeight)\nПромежуток: \((Float(height) / Float(count)) / 3.0)" )
        print("OFFSET \(offset)")
        if offset < 40 {
            offset = 40
        }
        self.scrollViewController.scrollView.snp.removeConstraints()
        self.loadingView.snp.removeConstraints()
        self.helperView.snp.removeConstraints()
        self.addButton.snp.removeConstraints()
        self.linkField.snp.removeConstraints()
        self.collectionViewController.collectionView.snp.removeConstraints()
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.width.equalToSuperview()
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.helperView.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalToSuperview()
            $0.width.equalToSuperview().offset(0)
            
        }
        self.topLabel.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.collectionViewController.collectionView.snp.makeConstraints{
            $0.top.equalTo(self.topLabel.snp.bottom).offset(16)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(finalHeight)
            //  $0.bottom.equalToSuperview()
        }
        self.bottomLabel.snp.makeConstraints{
            $0.top.equalTo(self.collectionViewController.collectionView.snp.bottom).offset(24)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.linkField.snp.makeConstraints{
            $0.top.equalTo(self.bottomLabel.snp.bottom).offset(8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.switchView.snp.makeConstraints{
        
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalTo(self.linkField.snp.bottom).offset(32)
            $0.height.equalTo(44)
        }
        
        self.addButton.snp.makeConstraints{
            $0.top.equalTo(self.switchView.snp.bottom).offset(offset)
            // $0.centerY.equalToSuperview().offset(200)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
            $0.bottom.equalToSuperview().offset(-58)
        }
        
        self.addLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        
    }
    //var onBack : Action?
}
extension PhotoResultsViewController: AddPhotoInteractorOutput {
    
    
    
    
    func didUpdateViewModels(count: Int) {
        self.count += count
        self.setUpConstraints(fields: self.switchView.switchView.isOn)
        self.view.animateLayout(duration: 0.5)
    }
    
    func didUploadAlbum(album: Album) {
        print(album)
        self.onBackWithAlbum?(album)
    }
    
    func didUploadImagesInAlbum(images: [Image]) {
        print(images)
        self.onBackWithImages?(images)
    }
    
    
    
    
    func didReceive(error: AirdronError) {
        //self.uploadButton?.update(state: .normal)
        // self.showToastErrorAlert(error)
        print("ERROR HERE \n\n \(error) \n\n")
        self.isExporting = false
    }
    
    func didClear(asset: PHAsset, cell: UICollectionViewCell) {
        guard let index = self.collectionView.indexPath(for: cell) else { return }
        self.collectionViewController.delete(index.item, in: index.section)
        self.count -= 1
        self.setUpConstraints(fields: self.switchView.switchView.isOn)
    }
    
    func didSelect(image: UIImage) {
        //  self.onFullPhoto?(image)
    }
    
    func didUpdateUpload(count: Int) {
        
    }
    
    func didUploadUnpaidImages(images: [Image]) {
        //self.onUploadedImages?(response, images)
        print(images)
    }
    
    func didUploadPaidImages(images: [Image]) {
        //self.onPayment?(self.albumShort, images)
    }
    
    func didExportPhotos() {
        self.isExporting = false
        
    }
}
