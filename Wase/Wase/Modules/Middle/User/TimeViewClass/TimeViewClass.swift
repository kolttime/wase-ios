
//
//  ApproveView.swift
//  OMG
//
//  Created by Minic Relocusov on 21/04/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit


class TimeViewClass : AirdronView {
    
    
    var isDefault = true
    var textLabel  = UILabel()
    var textDecsr = UILabel()
    var separator = UIView()
    var descText : String?
    var onEdditing : Action?
    var onEndEdditing : Action!
    var didSet = false
    override func initialSetup() {
        super.initialSetup()
        
        self.textLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "")
        self.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "")
        self.backgroundColor = Color.white.value
        self.addSubview(self.textLabel)
        self.addSubview(self.textDecsr)
        self.addSubview(self.separator)
        self.textLabel.alpha = 0
        self.textDecsr.alpha = 1
        self.separator.backgroundColor = Color.light.value
        self.onEndEdditing = {[weak self] in
            self?.separator.backgroundColor = Color.light.value
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onBeginEdditing))
        self.addGestureRecognizer(tap)
        self.setupConstraints()
    }
    @objc func onBeginEdditing(){
        self.separator.backgroundColor = Color.black.value
        self.onEdditing?()
    }
    
    
    func SetDefault(text : String, animated : Bool = true){
        
        self.isDefault = true
        if animated {
            UIView.animate(withDuration: 0.5) {
                self.textLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: text)
                self.textLabel.alpha = 0
                self.textDecsr.alpha = 1
                self.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: text)
                self.separator.backgroundColor = Color.light.value
                
            }
        } else {
            self.textLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: text)
            self.textLabel.alpha = 0
            self.textDecsr.alpha = 1
            self.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: text)
            self.separator.backgroundColor = Color.light.value
        }
    }
    
    func Set(text : String){
        self.isDefault = false
        UIView.animate(withDuration: 0.5) {
            self.textLabel.alpha = 1
            self.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: text)
            self.separator.backgroundColor = Color.light.value
        }
    }
    
    func set(text : String){
        self.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: text)
        self.textDecsr.isHidden = false
        self.textLabel.isHidden = true
    }
    
    func setDefault(text : String){
        self.isDefault = true
        self.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: text)
        self.textDecsr.isHidden = false
        self.textLabel.isHidden = true
    }
    
    func setText(text : String){
        self.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: text)
        self.textDecsr.isHidden = false
        self.textLabel.isHidden = false
        self.textLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: text)
        self.didSet = true
        
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.textLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
        self.textDecsr.snp.makeConstraints{
            $0.top.equalTo(textLabel.snp.bottom).offset(4)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-44)
        }
        
        self.separator.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(1)
            $0.top.equalTo(textDecsr.snp.bottom).offset(7)
        }
        
        
    }
    
    override var intrinsicContentSize : CGSize{
        let width = UIView.noIntrinsicMetric
        return CGSize(width: width, height: 50)
    }
}


extension TimeViewClass {
    public static func makeDateAlbum() -> TimeViewClass{
        let field = TimeViewClass()
        field.textLabel.text = "Дата фотосессии"
        field.textDecsr.text = "Дата фотосессии"
        return field
    }
    
    public static func makeCityField() -> TimeViewClass{
        let field = TimeViewClass()
        field.textLabel.text = "Город"
        field.textDecsr.text = "Город"
        return field
    }
    
    public static func makeTypeAlbum() -> TimeViewClass{
        let field = TimeViewClass()
        field.textLabel.text = "Тип фотосессии"
        field.textDecsr.text = "Тип фотосессии"
        return field
    }
    public static func makePhotoTypeField() -> TimeViewClass{
        let field = TimeViewClass()
        field.SetDefault(text: "Тип фотосессии")
        return field
    }
    
    public static func makePhoneField() -> TimeViewClass{
        let field = TimeViewClass()
        field.SetDefault(text: "Телефон")
        return field
    }
    
    public static func makePhotoDayField() -> TimeViewClass {
        let field = TimeViewClass()
        field.SetDefault(text : "День")
        return field
    }
    public static func makePhotoBeginField() -> TimeViewClass{
        let field = TimeViewClass()
        field.SetDefault(text: "Начало")
        return field
    }
    public static func makePhotoDurationField() -> TimeViewClass{
        let field = TimeViewClass()
        field.SetDefault(text: "Продолжительность")
        return field
    }
    public static func makePhotoBudgetField() -> TimeViewClass {
        let field = TimeViewClass()
        field.SetDefault(text: "Бюджет")
        return field
    }
    public static func makePhotoCandidatesField() -> TimeViewClass {
        let field = TimeViewClass()
        field.SetDefault(text: "Подбор")
        return field
    }
    
}
