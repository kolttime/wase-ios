//
//  AuthModuleBuilder.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class UserMiddleModuleBuilder{
    
    
    private let apiService: ApiService
    private let sessionManager: TaokaUserSessionManager
    
    init(apiService: ApiService,
         sessionManager: TaokaUserSessionManager) {
        self.apiService = apiService
        self.sessionManager = sessionManager
    }
    
    func makePhotographProfileModule(id : String, photoId : String, status : OffersStatus? = nil, phoneNumber : String?, stage : Int?) -> PhotographProfileViewController {
        return PhotographProfileViewController(apiService: self.apiService, userId : id, photoId: photoId, status: status, phoneNumber: phoneNumber, stage : stage)
    }
    
    func makePhotoAlbum(images : [Image]?, id : String?, scrollTo : Int?, userId : String) -> PhotoAlbumViewController {
        return PhotoAlbumViewController(userId : userId ,images : images,  id: id, sctollTo: scrollTo , apiService: self.apiService )
    }
    func makeConfirmationmodule() -> UserConfirmationViewController {
        return UserConfirmationViewController()
    }
    func makeConfirmationCommentModule(id : String, rating : Int, type : TellPleaseType) -> PhotoSendErrorViewController{
        return PhotoSendErrorViewController(apiService: self.apiService, type: .clientConfirmation, rating: rating, photoId: id)
    }
    func makeLaunchModule() -> UserMiddleViewController {
        return UserMiddleViewController(apiService: self.apiService)
    }
    func makeAlbumModule(id : String) -> PhotoAlbumViewController {
        return PhotoAlbumViewController(images: nil, id: id, sctollTo: nil, apiService: self.apiService)
    }
    func makeUserMiddleModule() -> UserMiddleViewController {
        return UserMiddleViewController(apiService: self.apiService)
    }
    func makeUserAcceptViewModule(photoId : String, photoPrice : Int) -> UserAcceptViewController {
        return UserAcceptViewController(apiService: self.apiService, photoId: photoId, photoPrice: photoPrice)
    }
    func makePaymentModule(photoId : String) -> UserPaymentViewController {
        return UserPaymentViewController(apiService: self.apiService, photoId: photoId)
    }
    func makeUserPhotoSettingsModule(photoId : String) -> PhotosessiomSettingsViewController  {
        return PhotosessiomSettingsViewController(apiService: self.apiService, photoId: photoId)
       // return UserApproveViewController(apiService: self.apiService)
    }
    
    func makeUserApproveViewController(type : UserType, id : String) -> UserApproveViewController{
        return UserApproveViewController(apiService: self.apiService, userType: type, id: id )
    }
    
    func mskeUserCreateModule() -> UserCreatePhotoViewController {
        return UserCreatePhotoViewController(apiService: self.apiService)
    }
    
    func makeTineModule() -> TimeViewController {
        return TimeViewController()
    }
    
    func makePlaceModule() -> BringPlaceViewController {
        return BringPlaceViewController(apiService: self.apiService, userModel: self.sessionManager.user!)
    }
    
    func makeWorkerModule() -> WorkerTypeViewController {
        return WorkerTypeViewController()
    }
    
    func makeBudgetModule(photoCreation : CreatePhotosession) -> BudgetViewController {
        return BudgetViewController(photoCreation: photoCreation, apiService: self.apiService, userModel: self.sessionManager.user!)
    }
    
    func makeSecondStepModule(photoCreation : CreatePhotosession) -> ReadySecondStepViewController {
        return ReadySecondStepViewController(photoCreation: photoCreation, apiService: self.apiService)
    }
    
    func makeSelectionModule(type : UserType, id : String) -> SelectionViewController {
        return SelectionViewController(apiService: self.apiService, type: type, id: id)
    }
    
    func makePageUserCreateViewModule(initialViewControllers : [AirdronViewController], count : Int) -> AirdronPageViewController {
        let PageViewControllers = AirdronPageViewController(count: count)
        PageViewControllers.setViewContrtollers(viewControllers: initialViewControllers, count: count)
        return PageViewControllers
    }
    
    func makePhoneNumberViewController() -> PhoneNumberViewController{
        return PhoneNumberViewController(apiService: self.apiService, agreed: true)
    }
    
    func makePhoneCodeModule(phonenumber : String, key : String) -> PhoneCodeViewController {
        return PhoneCodeViewController(phoneNumber: phonenumber, apiService : self.apiService, key : key,  sessionManager: self.sessionManager, agreed: true)
    }
    func makePhotoDeleteModule(photoId : String) -> PhotoSendErrorViewController{
        return PhotoSendErrorViewController.init(apiService: self.apiService, type: .clientError, rating: nil, photoId: photoId)
    }
    
}


