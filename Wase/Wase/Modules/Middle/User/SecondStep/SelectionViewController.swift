//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class SelectionViewController : SimpleNavigationBar {
    
    var onBack : Action?
    var onNext : (() -> Void)?
    var onUser : ((String, Int) -> Void)?
    lazy var tableViewController = TableViewController()
    private var selectionTypes: [SelectionCellModel] = []
    
    private var apiService : ApiService
    private var type : UserType
    private var id : String
    init(apiService : ApiService, type : UserType, id : String){
        self.apiService = apiService
        self.type = type
        self.id = id
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.onBack?()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(tableViewController.tableView)
        self.tableViewController.tableView.register(cellClass: SelectionCell.self)
        self.toastPresenter.targetView = self.view
        //self.setBackTitle(title: "Фотографы")
    
        self.setTitle(title: "Подбор")
        self.view.backgroundColor = Color.white.value
        self.setTabBarHidden()
        self.setLargeTitleHidden()
        self.navigationItem.setHidesBackButton(false, animated: false)
        self.setUpConstraints()
        self.fetchData()
        
    }
    
    func update(id : String){
        //self.fetchData()
        guard let index = self.viewModels.lastIndex(where: {$0.id == id}) else {return}
        self.viewModels[index].offer = true
        self.viewModels[index].selectionHandler = {[weak self] id in
            guard let self = self else {return}
            self.onUser?(id, 1)
        }
        let section = DefaultTableSectionViewModel(cellModels: self.viewModels)
        self.tableViewController.update(viewModels: [section])
    }
    func fetchData(){
        self.apiService.getCandidates(type: self.type, id: self.id) {[weak self] response in
            switch response {
            case .success(let result):
                print("Ебой \n\n\(result)\n\n")
                
                self?.render(users: result)
            case .failure(let error):
                print(error)
                self?.showToastErrorAlert(error)
                
            }
        }
    }
    var viewModels : [SelectionCellModel] = []
    func render(users : [CandidatesResponse2]){
        self.viewModels = []
        for user in users {
            let selection : ((String) -> Void)? = {[weak self] id in
                var stage = 0
                if user.candidate.offer {
                    stage = 1
                }
                self?.onUser?(id, stage)
            }
            var countString = ""
            switch user.candidate.user.photosessions {
            case 0:
                countString = "0 фотосессий"
            case 1:
                countString = "1 фотосессия"
            case 2:
                countString = "2 фотосессии"
            case 3:
                countString = "3 фотосессии"
            case 4:
                countString = "4 фотосессии"
            case 5:
                countString = "5 фотосессий"
            default:
                countString = "\(user.candidate.user.photosessions!) фотосессий"
            }
            let viewModel = SelectionCellModel(mainLabel: "\(user.candidate.user.givenName) \(user.candidate.user.familyName)", countLabel: countString, id: user.candidate.user.id, image: user.candidate.user.avatar, offer: user.candidate.offer, selectionHandler: selection, hasSeparator: true)
            viewModels.append(viewModel)
        }
        let section = DefaultTableSectionViewModel(cellModels: viewModels)
        self.tableViewController.update(viewModels: [section])
        self.selectionTypes = viewModels
    }

    @objc func nextView(){
        print("~~~~~")
        self.onNext?()
    }
    
    override func setUpConstraints(){
        
        tableViewController.tableView.snp.makeConstraints{
        $0.top.equalToSuperview().offset((self.navigationController?.navigationBar.bounds.size.height ?? 0) + UIApplication.shared.statusBarFrame.height + 10) //?
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
    }
}




