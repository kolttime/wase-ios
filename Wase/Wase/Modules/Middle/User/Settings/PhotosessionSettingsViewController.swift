//
//  PhotosessionSettingsViewController.swift
//  Wase
//
//  Created by Роман Макеев on 18/08/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit



class PhotosessiomSettingsViewController : SimpleNavigationBar {
  
    
    
    
    var onSave : ((LongPhotosession) -> Void)?
    private var typeArray : [Specialization] = []
    //private var dayArray : []
    private var durationArray = ["1 час", "2 часа", "3 часа", "4 часа", "5 часов", "6 часов", "7 часов", "8 часов", "9 часов", "10 часов", "11 часов", "12 часов"]
    private var fullBegin = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"]
    private var beginArray = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"]
    private var budgetArray : [Budget] = []
    private var candidatesArray = ["Фотограф", "Фотограф и визажист"]
    private lazy var datePicker = UIDatePicker()
    private var saveButton = UIView()
    private lazy var slideView = SlideView()
    private lazy var titleField = OMGTextField.makePhotoNameTextFiedl()
    private lazy var placeField = OMGTextField.makePhotoPlaceTextField()
    private lazy var commentField = OMGTextField.makePhotoCommenTextField()
    private lazy var typeField = TimeViewClass.makePhotoTypeField()
    private lazy var dayField = TimeViewClass.makePhotoDayField()
    private lazy var beginField = TimeViewClass.makePhotoBeginField()
    private lazy var durationField = TimeViewClass.makePhotoDurationField()
    private lazy var candidatesField = TimeViewClass.makePhotoCandidatesField()
    private lazy var budgetField = TimeViewClass.makePhotoBudgetField()
    private lazy var scrollViewController = ScrollViewController()
    private lazy var loadingView = TaokaLoadingView()
    private let keyObserver = KeyboardObserver()
    private var activeField = UIView()
    var photoModel : LongPhotosession?
    var testModel : LongPhotosession?
    var photoId : String
    var onDelete : ((String) -> Void)?
   
    private var apiService : ApiService
    init(apiService : ApiService, photoId : String) {
        self.apiService = apiService
       
        self.photoId = photoId
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let topLabel = UILabel()
    
    private lazy var pickerView = UIPickerView()
    override func initialSetup() {
        
        
        super.initialSetup()
        self.setLargeTitleHidden()
        self.setBackTitle(title: "Настройки")
        self.toastPresenter.targetView = self.view
        self.setTabBarHidden()
        self.add(self.scrollViewController)
        self.scrollViewController.scrollView.addSubview(self.topLabel)
        self.topLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: "При изменении данных фотосесии заявки будут отправлены еще раз.")
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.topLabel.numberOfLines = 0
        self.slideView.addSubview(self.pickerView)
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = false
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.slideView.setHeight(height: 304)
        self.slideView.animate = {[weak self] in
            self?.view.animateLayout(duration: 0.5, completion: nil)
        }
        self.saveButton.layer.cornerRadius = 10
        self.saveButton.layer.masksToBounds = true
        self.saveButton.backgroundColor = Color.light.value
        self.saveButton.alpha = 0.4
        let saveLabel = UILabel()
        saveLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: "Сохранить")
        saveButton.addSubview(saveLabel)
        saveLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.delegate = self
        self.scrollViewController.scrollView.addSubview(self.saveButton)
        self.view.addSubview(self.slideView)
        self.slideView.setLeftTitle(title: "Отмена", completion: {[weak self] in
            self?.slideView.close?()
        })
        self.slideView.setRightTitle(title: "Выбрать", completion: {[weak self] in
            self?.chooseSlideView()
        })
        
        //self.datePicker.maximumDate =
        var sevenDaysfromNow: Date {
            return (Calendar.current as NSCalendar).date(byAdding: .year, value: 1, to: Date(), options: [])!
        }
        self.slideView.isHidden = true
        self.typeField.isUserInteractionEnabled = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.saveTouch))
        self.saveButton.addGestureRecognizer(tap)
        self.saveButton.isUserInteractionEnabled = false
        self.datePicker.maximumDate = sevenDaysfromNow
        self.datePicker.minimumDate = Date()
        self.slideView.addSubview(self.datePicker)
        self.datePicker.datePickerMode = .date
        self.setupHandlers()
        self.fetchData()
        
        
        
       // convertDuration(duration: "24 часа")
        //self.setUpConstraints()
    }
    @objc func cancelTouch(){
        guard let photo = self.photoModel else {return}
        self.onDelete?(photo.id)
    }
    var status : PhotosessionStatus = .inbox
    func setStatus(status : PhotosessionStatus){
        self.status = status
        if self.status != .created && self.status != .selection && self.status != .execution {
            
            self.titleField.isUserInteractionEnabled = false
            self.placeField.isUserInteractionEnabled = false
            self.dayField.isUserInteractionEnabled = false
            self.typeField.isUserInteractionEnabled = false
            self.beginField.isUserInteractionEnabled = false
            self.activeField.isUserInteractionEnabled = false
            self.budgetField.isUserInteractionEnabled = false
            self.commentField.isUserInteractionEnabled = false
            self.durationField.isUserInteractionEnabled = false
            self.candidatesField.isUserInteractionEnabled = false
            
        }
    }
    
    @objc func saveTouch(){
        print("Saved")
        self.loadingView.isHidden = false
        self.apiService.patchPhotosessions(photosession: self.photoModel!) {[weak self] response in
            guard let self = self else {return}
            
            switch response {
            case .success(let photo):
                self.onSave?(photo)
                print("ОКЕЙ")
                print("Photoseesion \n\n\n\(photo)")
            case .failure(let error):
                self.showToastErrorAlert(error)
                print(error)
            }
            
            
            
        }
    }
    var budgetSelected = true
    func chooseSlideView(){
        print("Выбрать")
        self.slideView.close?()
        
        switch self.activeField {
        case self.typeField:
            if self.typeArray.count != 0 {
                self.photoModel?.specialization = self.typeArray[self.pickerView.selectedRow(inComponent: 0)]
                self.budgetField.SetDefault(text: "Бюджет")
                self.budgetSelected = false
                self.fetchBudget()
                if self.photoModel != nil {
                    self.typeField.Set(text: self.photoModel!.specialization.name!)
                }
            }
        case self.dayField:
            let dateFormatter = DateFormatter()
            dateFormatter.locale =  Locale(identifier: "ru_MD")
            //let date = dateFormatter.date(from: self.datePicker.date)
            let date = self.datePicker.date
            dateFormatter.dateFormat = "YYYY-MM-dd"
            self.photoModel?.date = dateFormatter.string(from: date)
            dateFormatter.dateFormat = "dd MMMM"
            self.dayField.Set(text: dateFormatter.string(from: date))
            let calendar = Calendar.current
            if calendar.component(.day, from: date) ==  calendar.component(.day, from: Date()) && calendar.component(.month, from: date) == calendar.component(.month, from: Date()) && calendar.component(.year, from: date) == calendar.component(.year, from: Date()) {
                let currentHour = calendar.component(.hour, from: Date())
                self.beginArray = []
                
                for i in (currentHour + 1)...23 {
                    self.beginArray.append(self.fullBegin[i])
                }
                self.beginField.SetDefault(text: "Начало")
                
            } else {
                self.beginArray = self.fullBegin
            }
            
            
            //self.photoModel?.date =
        case self.beginField:
            self.photoModel?.time = self.beginArray[self.pickerView.selectedRow(inComponent: 0)]
            self.beginField.Set(text: self.beginArray[self.pickerView.selectedRow(inComponent: 0)])
           // self.beginField.Set(text: self.photoModel.)
        case self.durationField:
            self.photoModel?.duration = self.pickerView.selectedRow(inComponent: 0) + 1
            self.budgetField.SetDefault(text: "Бюджет")
            self.budgetSelected = false
            self.fetchBudget()
            self.durationField.Set(text: self.durationArray[self.pickerView.selectedRow(inComponent: 0)])
        case self.budgetField:
            if self.budgetArray.count != 0 {
                self.photoModel?.budget = self.budgetArray[self.pickerView.selectedRow(inComponent: 0)].value!
                self.budgetField.Set(text: self.budgetArray[self.pickerView.selectedRow(inComponent: 0)].name!)
                self.budgetSelected = true
            }
        case self.candidatesField:
            self.photoModel?.participantsTypes = self.pickerView.selectedRow(inComponent: 0) == 0 ? [UserType.photographer] : [UserType.photographer, UserType.makeUpArtist]
            self.fetchBudget()
            self.budgetField.SetDefault(text: "Бюджет")
            self.budgetSelected = false
            self.candidatesField.Set(text: self.candidatesArray[self.pickerView.selectedRow(inComponent: 0)])
        default:
            print("Hi")
        }
        self.setSaveButton()
    }
    
    private var isButtonEnabled = false
    func setSaveButton(){
        if self.photoModel != nil {
            
            
            if HelpFunctions.checkPhotosessionChanged(first: self.photoModel!, second: self.testModel!)
                && self.titleField.text != nil && self.titleField.text != "" && !self.budgetField.isDefault && !self.beginField.isDefault {
                    
                print("SET DEFAULT : \(self.budgetField.isDefault)")
                    self.saveButton.alpha = 1
                    self.saveButton.isUserInteractionEnabled = true
                    
                    
                
            } else {
                
                self.saveButton.alpha = 0.4
                self.saveButton.isUserInteractionEnabled = false
            }
            
            
        }
    }
    
    func setupHandlers(){
        
        
        
        self.keyObserver.onKeyboardWillShow = {[weak self] frame, smth in
            guard let self = self else {return}
            if self.activeField == self.commentField {
                self.scrollViewController.scrollView.setContentOffset(CGPoint(x: self.scrollViewController.scrollView.contentOffset.x, y: self.scrollViewController.scrollView.contentOffset.y + frame.height), animated: true)
                
            }
        }
        
        
        self.placeField.onTextDidChanged = {[weak self] response in
            self?.photoModel?.place.address = response.text
            self?.setSaveButton()
        }
        self.titleField.onTextDidChanged = {[weak self] text in
            self?.photoModel?.title = text.text
            self?.setSaveButton()
        }
        self.titleField.onBeginEditing = {[weak self] field in
            self?.activeField = field
            //self?.setSaveButton()
        }
        self.commentField.onBeginEditing = {[weak self] field in
            self?.activeField = field
            //self?.setSaveButton()
        }
        self.commentField.onTextDidChanged = {[weak self] text in
            self?.photoModel?.description = text.text
            self?.setSaveButton()
        }
        
        self.typeField.onEdditing = {[weak self] in
            guard let self = self else {return}
            
            self.activeField = self.typeField
            self.resignResponsers()
            self.pickerView.isHidden = false
            self.datePicker.isHidden = true
            self.pickerView.reloadAllComponents()
            let index = self.typeArray.lastIndex(where: {$0.id == self.photoModel?.specialization.id})
            if index != nil {
                self.pickerView.selectRow(index!, inComponent: 0, animated: false)
            }
           // self.pickerView.selectRow(, inComponent: 0, animated: false)
            self.slideView.open?()
           // self.setSaveButton()
            
        }
        self.dayField.onEdditing = {[weak self] in
            guard let self = self else {return}
            self.activeField = self.dayField
            self.resignResponsers()
            self.pickerView.isHidden = true
            self.datePicker.isHidden = false
            //self.pickerView.reloadAllComponents()
            let dateFormatter = DateFormatter()
            dateFormatter.locale =  Locale(identifier: "ru_MD")
            dateFormatter.dateFormat = "YYYY-MM-dd"
            if self.photoModel != nil {
              let date = dateFormatter.date(from: self.photoModel!.date)!
              self.datePicker.setDate(date, animated: false)
            }
            self.slideView.open?()
           // self.setSaveButton()
        }
        self.beginField.onEdditing = {[weak self] in
            guard let self = self else {return}
            self.activeField = self.beginField
            let dateFormatter = DateFormatter()
            dateFormatter.locale =  Locale(identifier: "ru_MD")
            dateFormatter.dateFormat = "YYYY-MM-dd"
            if self.photoModel != nil {
                let date = dateFormatter.date(from: self.photoModel!.date)!
                let calendar = Calendar.current
                if calendar.component(.day, from: date) ==  calendar.component(.day, from: Date()) && calendar.component(.month, from: date) == calendar.component(.month, from: Date()) && calendar.component(.year, from: date) == calendar.component(.year, from: Date()){
                    let currentHour = calendar.component(.hour, from: Date())
                    self.beginArray = []
                    
                    for i in (currentHour + 1)...23 {
                        self.beginArray.append(self.fullBegin[i])
                    }
                    //self.beginField.SetDefault(text: "Начало")
                    
                } else {
                    self.beginArray = self.fullBegin
                }
            }
            self.resignResponsers()
            self.pickerView.isHidden = false
            self.datePicker.isHidden = true
            self.pickerView.reloadAllComponents()
            let index = self.beginArray.lastIndex(where: {$0 == self.photoModel?.time})
            if index != nil {
                self.pickerView.selectRow(index!, inComponent: 0, animated: false)
            }
            self.slideView.open?()
           // self.setSaveButton()
        }
        self.durationField.onEdditing = {[weak self] in
            guard let self = self else {return}
            self.activeField = self.durationField
            self.resignResponsers()
            self.pickerView.isHidden = false
            self.datePicker.isHidden = true
            self.pickerView.reloadAllComponents()
            if self.photoModel != nil {
                self.pickerView.selectRow(self.photoModel!.duration - 1, inComponent: 0, animated: false)
            }
//            let index = self.durationArray.lastIndex(where: {$0.})
//            if index != nil {
//                self.pickerView.selectRow(index!, inComponent: 0, animated: false)
//            }
            self.slideView.open?()
           // self.setSaveButton()
        }
        self.budgetField.onEdditing = {[weak self] in
            guard let self = self else {return}
            self.activeField = self.budgetField
            self.resignResponsers()
            self.pickerView.isHidden = false
            self.datePicker.isHidden = true
            self.pickerView.reloadAllComponents()
            
            self.slideView.open?()
           // self.setSaveButton()
        }
        self.candidatesField.onEdditing = {[weak self] in
            guard let self = self else {return}
            self.activeField = self.candidatesField
            self.resignResponsers()
            self.pickerView.isHidden = false
            self.datePicker.isHidden = true
            self.pickerView.reloadAllComponents()
            if self.photoModel != nil {
                self.pickerView.selectRow(self.photoModel!.participantsTypes.count == 1 ? 0 : 1, inComponent: 0, animated: false)
            }
            self.slideView.open?()
          //  self.setSaveButton()
        }
        
    }
    
    func convertDuration(duration : String) -> Int {
        var cutted = duration
        if cutted.hasSuffix("часoв") {
            cutted = String(cutted.dropLast(5))
        } else if cutted.hasSuffix("часа"){
            cutted = String(cutted.dropLast(4))
            
        } else if cutted.hasSuffix("час") {
            cutted = String(cutted.dropLast(3))
        }
        
        return Int(cutted)!
    }
    func render(model : LongPhotosession){
        self.scrollViewController.scrollView.addSubview(self.topLabel)
        self.scrollViewController.scrollView.addSubview(self.titleField)
        self.titleField.text = model.title
        self.scrollViewController.scrollView.addSubview(self.typeField)
        self.typeField.Set(text: model.specialization.name!)
        self.scrollViewController.scrollView.addSubview(self.dayField)
        let dateFormatter = DateFormatter()
        dateFormatter.locale =  Locale(identifier: "ru_MD")
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let date = dateFormatter.date(from: model.date)!
        dateFormatter.dateFormat = "dd MMMM"
        
        
        self.dayField.Set(text: dateFormatter.string(from: date))
        self.scrollViewController.scrollView.addSubview(self.beginField)
        self.beginField.Set(text: model.time)
        self.scrollViewController.scrollView.addSubview(self.durationField)
        var dString = ""
        switch model.duration {
        case 1:
            dString = "1 час"
        case 2:
            dString = "2 часа"
        case 3:
            dString = "3 часа"
        case 4:
            dString = "4 часа"
        default:
            dString = "\(model.duration) часов"
        }
        self.durationField.Set(text: dString)
        if model.place.type == .home {
            self.scrollViewController.scrollView.addSubview(self.placeField)
            self.placeField.text = model.place.address!
        }
        self.scrollViewController.scrollView.addSubview(self.budgetField)
        var bString = ""
        if model.budget[0] == 0 {
            bString = "Не больше \(model.budget[1]) ₽"
        } else if model.budget[1] == 999999 {
            bString = "Больше \(model.budget[0]) ₽"
        } else {
            bString = "От \(model.budget[0]) ₽ до \(model.budget[1]) ₽"
        }
        self.budgetField.Set(text: bString)
        self.scrollViewController.scrollView.addSubview(self.candidatesField)
        if model.offers!.count == 1 {
            self.candidatesField.Set(text: "Фотограф")
        } else {
            self.candidatesField.Set(text: "Фотограф и визажист")
        }
        self.scrollViewController.scrollView.addSubview(self.commentField)
        if model.description != nil && model.description != "" {
            self.commentField.text = model.description!
        }
        self.setUpConstraints(model: model)
    }
    func resignResponsers(){
        if self.activeField != self.titleField { self.titleField.resignResponder() }
        if self.activeField != self.typeField { self.typeField.onEndEdditing() }
        if self.activeField != self.dayField {self.dayField.onEndEdditing()}
        if self.activeField != self.beginField {self.beginField.onEndEdditing()}
        if self.activeField != self.durationField {self.durationField.onEndEdditing()}
        if self.activeField != self.budgetField {self.budgetField.onEndEdditing()}
        if self.activeField != self.candidatesField {self.candidatesField.onEndEdditing()}
        if self.activeField != self.commentField {self.commentField.resignResponder()}
    }
    func fetchBudget(){
        self.apiService.getBudgest(specialization: self.photoModel!.specialization.id!, duration: self.photoModel!.duration, type: (self.photoModel?.participantsTypes.map({$0.rawValue}))!){
            [weak self] response in
            switch response {
            case .success(let budget):
                self?.budgetArray = budget
                self?.pickerView.reloadAllComponents()
            case .failure(let error):
                print(error)
            }
        }
    }
    func fetchOthers(){
        self.apiService.specialization(){
            [weak self] response in
            guard let self = self else {return}
            switch response{
            case .success(let spec):
                self.typeArray = spec
                self.pickerView.reloadAllComponents()
                
            case .failure(let error):
                print(error)
            }
        }
        self.fetchBudget()
        
    }
    
    func fetchData(){
        self.apiService.getPhotoPhotosession(id: self.photoId) {
           
            
            [weak self] response in
            guard let self = self else {return}
            self.loadingView.isHidden = false
            switch response {
            case .success(let photosession):
                self.slideView.isHidden = false
                self.photoModel = photosession
                self.testModel = photosession
                self.setStatus(status: photosession.status)
                self.render(model: photosession)
                self.fetchOthers()
                self.loadingView.isHidden = true
                if photosession.status == .created || photosession.status == .selection || photosession.status == .prepayment || photosession.status == .execution {
                    let tapCancel = UITapGestureRecognizer(target: self, action: #selector(self.cancelTouch))
                    
                        let rightlabel = UILabel()
                        rightlabel.addGestureRecognizer(tapCancel)
                        rightlabel.isUserInteractionEnabled = true
                        rightlabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.red.value).make(string: "Отменить")
                         self.setRightButton(view: rightlabel)
                    
                }
            case .failure(let error):
                print(error)
                self.showToastErrorAlert(error)
            }
        }
    }
    
     func setUpConstraints(model : LongPhotosession) {
        super.setUpConstraints()
        
        
        self.topLabel.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.titleField.snp.makeConstraints{
            $0.top.equalTo(self.topLabel.snp.bottom).offset(16)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.typeField.snp.makeConstraints{
            $0.top.equalTo(self.titleField.snp.bottom).offset(16)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(60)
        }
        self.dayField.snp.makeConstraints{
            $0.top.equalTo(self.typeField.snp.bottom).offset(16)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(60)
        }
        self.beginField.snp.makeConstraints{
            $0.top.equalTo(self.dayField.snp.bottom).offset(16)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(60)
        }
        self.durationField.snp.makeConstraints{
            $0.top.equalTo(self.beginField.snp.bottom).offset(16)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(60)
        }
        if model.place.type == .home {
            self.placeField.snp.makeConstraints{
                $0.top.equalTo(self.durationField.snp.bottom).offset(16)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
                $0.height.equalTo(60)
            }
        }
        self.budgetField.snp.makeConstraints{
            if model.place.type == .home {
                $0.top.equalTo(self.placeField.snp.bottom).offset(16)
            } else {
                $0.top.equalTo(self.durationField.snp.bottom).offset(16)
            }
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(60)
        }
        self.candidatesField.snp.makeConstraints{
            $0.top.equalTo(self.budgetField.snp.bottom).offset(16)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(60)
        }
        self.commentField.snp.makeConstraints{
            $0.top.equalTo(self.candidatesField.snp.bottom).offset(16)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.saveButton.snp.makeConstraints{
            $0.top.equalTo(self.commentField.snp.bottom).offset(32)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
            $0.bottom.equalToSuperview().offset(-50)
        }
        self.slideView.snp.makeConstraints{
            self.slideView.topConstr = $0.top.equalTo(self.view.snp.bottom).constraint
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(304)
        }
        self.pickerView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(54)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(220)
        }
        self.datePicker.snp.makeConstraints{
            $0.edges.equalTo(self.pickerView)
        }
        
    }
    
    
}



extension PhotosessiomSettingsViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.isEqual(self.pickerView) {
            
            switch self.activeField{
            case self.typeField:
                return typeArray.count
            case self.beginField:
                return beginArray.count
            case self.durationField:
                return self.durationArray.count
            case self.budgetField:
                return self.budgetArray.count
            case self.candidatesField:
                return self.candidatesArray.count
            default:
                return 0
            }
            
            
        }
        return 10
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.isEqual(self.pickerView){
            
            
            switch self.activeField{
            case self.typeField:
                return typeArray[row].name!
            case self.beginField:
                return beginArray[row]
            case self.durationField:
                return self.durationArray[row]
            case self.budgetField:
                return self.budgetArray[row].name!
            case self.candidatesField:
                return self.candidatesArray[row]
            default:
                return ""
            }
            
            
        }
        return ""
    }
    
    
    
    
}


extension PhotosessiomSettingsViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.slideView.close?()
    }
}
