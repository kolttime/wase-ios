//
//  UserCommentConfirmationViewController.swift
//  Wase
//
//  Created by Роман Макеев on 14/09/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class UserCommentConfirmationViewController : SimpleNavigationBar {
    
    
    let headerLabel = UILabel()
    let sepoaratorView = UIView()
    let textField = UITextField()
    var rating : Int
    var id : String
    let keyObserver = KeyboardObserver()
    var apiService : ApiService
    var middleVC : PhotographMiddleViewController?
    var type : TellPleaseType
    init(id : String, rating : Int, apiService : ApiService, vc : PhotographMiddleViewController? = nil, type : TellPleaseType){
        self.rating = rating
        self.id = id
        self.middleVC = vc
        self.apiService = apiService
        self.type = type
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func initialSetup() {
        super.initialSetup()
        self.view.addSubview(self.headerLabel)
        self.headerLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.make(string: "Расскажите, пожалуйста")
        self.headerLabel.numberOfLines = 0
        self.setLargeTitleHidden()
        self.toastPresenter.targetView = self.view
        self.setTabBarHidden()
        self.view.addSubview(self.sepoaratorView)
        self.sepoaratorView.backgroundColor = Color.light.value
        self.view.addSubview(self.textField)
        self.textField.placeholder = "Подробности"
        var comp : Action? = {[weak self] in
            guard let self = self else {return}
            self.sendButton()
        }
        self.setRighttitle(title: "Отправить", completion: comp)
        self.setuphandlers()
        self.setUpConstraints()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onEndEdditing))
        self.view.addGestureRecognizer(tap)
    }
    var onSendPhoto : ((LongPhotosession) -> Void)?
    func sendButton(){
        let comment = self.textField.text ?? ""
        if self.type == .photoCancel {
            self.apiService.putPhotosessionOffer(photoId: self.id, comment: comment) {
                [weak self] response in
                guard let self = self else {return}
                switch response {
                case .success( _):
                    self.onSend?(self.id)
                case .failure(let error):
                    self.showToastErrorAlert(error)
                }
            }
        } else if self.type == .confirmation{
            self.apiService.postConfirmation(photoId: self.id, rating: self.rating, comment: comment, resultDate: nil) {
                [weak self] response in
                guard let self = self else {return}
                switch response {
                case .success(let result):
                    self.onSendPhoto?(result)
                case .failure(let error):
                    self.showToastErrorAlert(error)
                }
            }
        } else if self.type == .clientCancel {
            
        }
        
    }
    var onSend : ((String) -> Void)?
    func setuphandlers(){
        
    }
    @objc func onEndEdditing(){
        self.view.endEditing(true)
    }
    override func setUpConstraints() {
        super.setUpConstraints()
        self.headerLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(self.getTopOffset())
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.sepoaratorView.snp.makeConstraints{
            $0.top.equalTo(self.headerLabel.snp.bottom).offset(10)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
        }
        self.textField.snp.makeConstraints{
            $0.top.equalTo(self.sepoaratorView.snp.bottom)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
           // $0.height.equalTo(self.view.frame.height)
            //$0.bottom.equalToSuperview()
        }
        
    }
}
