//
//  File.swift
//  Wase
//
//  Created by Роман Макеев on 14/09/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class UserConfirmationViewController : SimpleNavigationBar {
    
    
    let headerLabel = UILabel()
    let checkView5 = checkPlanView()
    let checkView4 = checkPlanView()
    let checkView3 = checkPlanView()
    let checkView2 = checkPlanView()
    
    override func initialSetup() {
        super.initialSetup()
        self.setTitle(title: "")
        self.setBackTitle(title: "Назад")
        self.setLargeTitleHidden()
        self.setTabBarHidden()
        self.view.addSubview(self.headerLabel)
        self.headerLabel.numberOfLines = 0
        self.headerLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.make(string: "Как прошла фотосессия?")
        self.checkView2.nameLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Ужасно")
        self.checkView3.nameLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Можно было лучше")
        self.checkView4.nameLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Мне понравилось")
        self.checkView5.nameLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Всё было замечательно")
        
        self.view.addSubview(self.checkView2)
        self.view.addSubview(self.checkView3)
        self.view.addSubview(self.checkView4)
        self.view.addSubview(self.checkView5)
        self.view.backgroundColor = Color.white.value
        let tap5 = UITapGestureRecognizer(target: self, action: #selector(self.click5))
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(self.click4))
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.click3))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.click2))
        self.checkView5.addGestureRecognizer(tap5)
        self.checkView4.addGestureRecognizer(tap4)
        self.checkView3.addGestureRecognizer(tap3)
        self.checkView2.addGestureRecognizer(tap2)
        self.setUpConstraints()
    }
    var onTap : ((Int) -> Void)?
    @objc func click5(){
        self.onTap?(5)
    }
    @objc func click4(){
        self.onTap?(4)
    }
    @objc func click3(){
        self.onTap?(3)
    }
    @objc func click2(){
        self.onTap?(2)
    }
    
    override func setUpConstraints() {
        super.setUpConstraints()
        self.headerLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(self.getTopOffset())
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.checkView5.snp.makeConstraints{
            $0.top.equalTo(self.headerLabel.snp.bottom).offset(24)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(64)
        }
        self.checkView4.snp.makeConstraints{
            $0.top.equalTo(self.checkView5.snp.bottom).offset(8)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(64)
        }
        self.checkView3.snp.makeConstraints{
            $0.top.equalTo(self.checkView4.snp.bottom).offset(8)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(64)
        }
        self.checkView2.snp.makeConstraints{
            $0.top.equalTo(self.checkView3.snp.bottom).offset(8)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(64)
        }
        
    }
}
