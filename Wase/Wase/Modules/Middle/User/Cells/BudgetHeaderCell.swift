//
//  PortfolioHeaderCollectionView.swift
//  OMG
//
//  Created by Minic Relocusov on 26/03/2019.
//  Copyright © 2019 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit


class BudgetHeaderCell : TableHeaderFooterView {
    
    
    
    private var viewModel : BudgetHeaderCellModel?
    private lazy var titleLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(titleLabel)
        self.setupConstraints()
    }
    
    
    
    
    override func configure(viewModel: TableHeaderFooterView.ViewModelType) {
        
        let viewModel = viewModel as! BudgetHeaderCellModel
        
        self.viewModel = viewModel
        self.titleLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.colored(color: Color.black.value).make(string: viewModel.mainLabel)
        self.titleLabel.numberOfLines = 0
        
    }
    
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 100
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.titleLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalToSuperview()
            $0.right.equalToSuperview().offset(-45)
        }
        
    }
    
    
}





struct BudgetHeaderCellModel : TableHeaderFooterViewModel {
    var viewType: TableHeaderFooterView.Type { return BudgetHeaderCell.self }
    var mainLabel: String
}




