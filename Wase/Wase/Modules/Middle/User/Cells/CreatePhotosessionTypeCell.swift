//
//  SearchableCountryTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 13/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class CreatePhotosessionTypeCell: TableViewCell {
    var selection : ((Specialization?) -> Void)?
    var cityModel: CreatePhotosessionTableCellViewModel?
    var specialization : Specialization?
    var imageq = UIImageView(image: .init(UIImage(named: "Arrow2")?.withRenderingMode(.alwaysTemplate))!)
    
    private lazy var mainLabel = UILabel()
    private lazy var descrLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.mainLabel)
        self.contentView.addSubview(self.descrLabel)
        self.contentView.addSubview(self.imageq)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.mainLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(24)
        }
        
        self.descrLabel.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(4)
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        
        self.imageq.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-15.5)
            $0.top.equalToSuperview().offset(15)
            $0.width.equalTo(8)
            $0.height.equalTo(13)
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! CreatePhotosessionTableCellViewModel
        self.backgroundColor = Color.white.value
        self.imageq.tintColor = Color.greyL.value
        self.cityModel = viewModel
        self.selection = viewModel.selectionHandler
        self.specialization = viewModel.specialization
        self.mainLabel.text = viewModel.specialization.name
        self.descrLabel.text = viewModel.specialization.description
        self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: self.mainLabel.text ?? "")
        self.descrLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: self.descrLabel.text ?? "")
        self.descrLabel.numberOfLines = 0
        
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 75
    }
    
    override func didSelect() {
        self.selection?(self.specialization)
        self.backgroundColor = Color.light.value
        self.imageq.tintColor = Color.black.value
    }
}

struct CreatePhotosessionTableCellViewModel: TableCellViewModel {
    var cellType: TableViewCell.Type { return CreatePhotosessionTypeCell.self }
    var specialization : Specialization
    let selectionHandler: ((Specialization?) -> Void)?
}


