//
//  SearchableCountryTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 13/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class BudgetCell: TableViewCell {
    var selection : ((Budget) -> Void)?
    var cityModel: BudgetTableCellViewModel?
    var budget : Budget?
    var imageq = UIImageView(image: .init(UIImage(named: "Arrow2")?.withRenderingMode(.alwaysTemplate))!)
    
    private lazy var mainLabel = UILabel()
    private lazy var descrLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.mainLabel)
        self.contentView.addSubview(self.imageq)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.mainLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(24)
        }

        self.imageq.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-22)
            $0.top.equalToSuperview().offset(15)
            $0.width.equalTo(8)
            $0.height.equalTo(13)
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! BudgetTableCellViewModel
        self.cityModel = viewModel
        self.imageq.tintColor = Color.light.value
        self.backgroundColor = Color.white.value
        self.budget = viewModel.budget
        self.selection = viewModel.selectionHandler
        self.mainLabel.text = viewModel.budget.name
        self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: self.mainLabel.text ?? "")
        
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 70
    }
    
    override func didSelect() {
        self.selection?(self.budget!)
        self.backgroundColor = Color.light.value
        self.imageq.tintColor = Color.black.value
    }
}

struct BudgetTableCellViewModel: TableCellViewModel {
    var cellType: TableViewCell.Type { return BudgetCell.self }
    var budget : Budget
    let selectionHandler: ((Budget?) -> Void)?
}



