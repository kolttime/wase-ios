//
//  UserAcceptViewController.swift
//  Wase
//
//  Created by Роман Макеев on 28/08/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import WebKit



class UserAcceptViewController: SimpleNavigationBar {
    
    lazy var webView = WKWebView()
    let mainLabel = UILabel()
    let paymentButton = UIView()
    let appleButton = UIView()
    let paymentLabel = UILabel()
    let appleLabel = UILabel()
    var apiService : ApiService
    var loadingView : TaokaLoadingView = TaokaLoadingView()
    let promoView = PromoView()
    let costView = CostPayView()
    var photoId : String
    var price : Int
    //let costView = UIView()
    init(apiService : ApiService, photoId : String, photoPrice : Int){
        self.apiService = apiService
        self.photoId = photoId
        self.price = photoPrice
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var onReady : Action?
    
    override func initialSetup() {
        super.initialSetup()
        
        self.setTitle(title: "Подтверждение")
        self.setLargeTitle()
        self.toastPresenter.targetView = self.view
        webView.navigationDelegate = self
        self.view.addSubview(self.mainLabel)
        self.view.addSubview(self.paymentButton)
        self.view.addSubview(self.appleButton)
        self.view.addSubview(self.promoView)
        self.view.addSubview(self.costView)
        self.mainLabel.numberOfLines = 0
        self.view.backgroundColor = Color.white.value
        self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Текст про оплату и про случаи возврата")
        self.paymentButton.addSubview(self.paymentLabel)
        self.appleButton.addSubview(self.appleLabel)
        self.setTabBarHidden()
        self.setBackTitle(title: "Подтверждение")
        self.paymentLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: "Оплатить картой")
        self.appleLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.white.value).make(string: "Оплатить через Apple Pay")
        self.paymentButton.backgroundColor = Color.light.value
        self.paymentButton.layer.cornerRadius = 10
        self.paymentButton.layer.masksToBounds = true
        self.appleButton.backgroundColor = Color.accent.value
        self.appleButton.layer.cornerRadius = 10
        self.promoView.layer.masksToBounds = true
        self.promoView.layer.cornerRadius = 10
        self.promoView.layer.borderWidth = 1
        self.toastPresenter.targetView = self.view
        self.promoView.layer.borderColor = Color.light.value.cgColor
        self.view.addSubview(self.costView)
        self.costView.layer.masksToBounds = true
        self.costView.layer.cornerRadius = 10
        self.costView.layer.borderWidth = 1
        self.costView.layer.borderColor = Color.light.value.cgColor
        self.promoView.onAdd = {text in
            print(text)
            self.apiService.getCheckPromocode(code: text) {
                [weak self] response in
                guard let self = self else {return}
                switch response {
                case .success(let result):
                    if result.available {
                        self.promoView.setPromo(text: result.promocode!.title!)
                        let cost = Float(self.price) - (( Float(self.price) / 100.0) * Float(result.promocode!.discount!))
                        self.costView.setDiscount(cost: Int(cost), old: self.price)
                    } else {
                       self.promoView.showError(error: result.reason!)
                        self.costView.setNormal(cost: self.price)
                    }
                case .failure(let error):
                    self.showToastErrorAlert(error)
                }
            }
        }
        self.promoView.onError = {
            self.promoView.snp.remakeConstraints{
                
                    $0.bottom.equalTo(self.costView.snp.top).offset(-8)
                    $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                    $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                    $0.height.equalTo(68)
                
            }
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            })
        }
        self.promoView.onSet = {
            self.promoView.snp.remakeConstraints{
                
                $0.bottom.equalTo(self.costView.snp.top).offset(-8)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(68)
                
            }
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            })
        }
        self.promoView.onNormal = {
            self.promoView.snp.remakeConstraints{
                
                $0.bottom.equalTo(self.costView.snp.top).offset(-8)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
                
            }
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            })
            self.costView.setNormal(cost: self.price)
        }
        self.appleButton.layer.masksToBounds = true
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.paymentClick))
        self.paymentButton.addGestureRecognizer(tap1)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.appleClick))
        self.appleButton.addGestureRecognizer(tap2)
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = false
        self.fetch()
        self.setUpConstraints()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.resignResponser))
        self.view.addGestureRecognizer(tap)
    }
    @objc func resignResponser(){
        self.view.endEditing(true)
    }
    func fetch(){
        self.apiService.getPhotoPhotosession(id: self.photoId) {
            [weak self] response in
            switch response {
            case .success(let result):
                self?.price = result.price ?? 3600
                self?.fetchData()
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    func fetchData(){
        
        self.apiService.getProfilePromocode{
            [weak self] response in
            guard let self = self else {return}
            self.loadingView.isHidden = true
            switch response {
            case .success(let result):
                //self.promoView.setPromo(text: "")
                if result._id != nil {
                    self.promoView.setPromo(text: result.title!)
                    let cost = Float(self.price) - (( Float(self.price) / 100.0) * Float(result.discount!))
                    self.costView.setDiscount(cost: Int(cost), old: self.price)
                } else {
                    //self.promoView.setNormal()
                    self.costView.setNormal(cost: self.price)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.costView.setNormal(cost: self.price)
            }
        }
    }
    
    var onPayment : Action?
    var onApple : Action?
    func openWebView(){
        self.loadingView.isHidden = false
        self.apiService.postPrepayment(photoId: self.photoId) {
            [weak self] response in
            guard let self = self else {return}
            switch response {
            case .success(let result):
                print(result)
                let testUrl = URLRequest.init(url: URL.init(string: result.link)!)
                self.webView.load(testUrl)
                self.webView.isHidden = true
                self.webView.navigationDelegate = self
                self.view.addSubview(self.webView)
                self.webView.isHidden = true
                self.loadingView.isHidden = false
                self.webView.snp.makeConstraints {
                    $0.top.equalToSuperview().offset(self.getTopOffset())
                    $0.bottom.equalToSuperview()
                    $0.left.equalToSuperview()
                    $0.right.equalToSuperview()
                }
            case .failure(let error):
                self.showToastErrorAlert(error)
            }
            
        }
    }
    @objc func paymentClick(){
        print("Payment")
        //self.onPayment?()
        openWebView()
    }
    @objc func appleClick(){
        print("Apple")
        self.onApple?()
    }
    override func setUpConstraints() {
        super.setUpConstraints()
        
        self.mainLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(self.getTopOffset() + 12)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            
        }
        self.appleButton.snp.makeConstraints{
            $0.bottom.equalToSuperview().offset(-45)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
        }
        self.appleLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        self.paymentButton.snp.makeConstraints{
            $0.bottom.equalTo(self.appleButton.snp.top).offset(-8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
        }
        self.paymentLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.costView.snp.makeConstraints{
            $0.bottom.equalTo(self.paymentButton.snp.top).offset(-24)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(90)
        }
        self.promoView.snp.makeConstraints{
            $0.bottom.equalTo(self.costView.snp.top).offset(-8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
        }
    }
}


extension UserAcceptViewController : WKNavigationDelegate {
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.loadingView.isHidden = true
        self.webView.isHidden = false
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print(navigationAction.request.url)
        if (navigationAction.request.url?.absoluteString == "https://demo.moneta.ru/assistantWizard.widget") {
            self.loadingView.isHidden = true
            self.webView.isHidden = false
        }
        if navigationAction.request.url?.absoluteString.hasPrefix(Endpoints.host) ?? false{
            self.onReady?()
        }
        decisionHandler(.allow)
        
    }
}
