//
//  Promoview.swift
//  Wase
//
//  Created by Роман Макеев on 23/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit


class PromoView : AirdronView {
    
    
    let field = OMGTextField.makePromoField()
    let label = UILabel()
    var onAdd : ((String) -> Void)?
    let errorLabel = UILabel()
    var state = "normal"
    var onError : Action?
    var onNormal : Action?
    let promoLabel = UILabel()
    var onSet : Action?
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.field)
        self.addSubview(self.label)
        self.addSubview(self.promoLabel)
        self.label.alpha = 0
        self.label.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Применить")
        self.setupHandlers()
        self.setupConstraints()
        let tap = UITapGestureRecognizer(target: self, action: #selector(click))
        self.label.addGestureRecognizer(tap)
        self.label.isUserInteractionEnabled = true
        label.textAlignment = .right
        //self.addSubview(self.errorLabel)
        self.promoLabel.isHidden = true
        self.errorLabel.alpha = 0
        self.promoLabel.numberOfLines = 0
    }
    @objc func click(){
        if self.label.alpha != 0 {
            let text = self.field.text!
            if state == "normal" {
                self.onAdd?(text)
            } else if state == "set" {
                self.onNormal?()
                self.setNormal()
            }
        }
    }
    func setNormal(){
        self.promoLabel.isHidden = true
        self.field.isHidden = false
        self.field.text = nil
        self.label.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Применить")
        label.textAlignment = .right
        self.state = "normal"
    }
    func setPromo(text : String) {
        self.promoLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: text)
        self.promoLabel.isHidden = false
        self.label.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Удалить")
        label.textAlignment = .right
        self.field.isHidden = true
        self.state = "set"
        self.onSet?()
    }
    func showError(error : String) {
        self.onError?()
        state = "error"
        self.field.makeAnotherError(error: error)
        self.field.showError()
    }
    func setupHandlers(){
        field.onTextDidChanged = {[weak self] response in
            guard let self = self else {return}
            if self.state == "error" {
                self.field.deleteError()
                self.state = "normal"
                self.onNormal?()
            }
            if response.text.count > 2 {
                self.openLabel(open: true)
            } else {
                self.openLabel(open: false)
            }
        }
    }
    func openLabel(open : Bool){
        UIView.animate(withDuration: 0.5) {
            if open {
                self.label.alpha = 1
            } else {
                self.label.alpha = 0
            }
        }
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.label.snp.makeConstraints{
            $0.top.equalToSuperview().offset(17)
            $0.right.equalToSuperview().offset(-16)
            $0.width.equalTo(self.label.textWidth())
        }
        self.field.snp.makeConstraints{
            $0.top.equalToSuperview().offset(17)
            $0.left.equalToSuperview().offset(16)
            $0.right.equalTo(self.label.snp.left).offset(-5)
            
        }
        self.promoLabel.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.left.equalToSuperview().offset(16)
            $0.right.equalTo(self.label.snp.left).offset(-5)
        }
    }
}
