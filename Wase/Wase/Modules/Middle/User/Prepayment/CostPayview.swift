//
//  CostPayview.swift
//  Wase
//
//  Created by Роман Макеев on 23/10/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit


class CostPayView : AirdronView {
    
    let mainLabel = UILabel()
    let costLabel = UILabel()
    let oldLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.mainLabel)
        self.addSubview(self.costLabel)
        self.addSubview(self.oldLabel)
        self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Стоимость фотосессии")
        self.oldLabel.alpha = 0
        self.setupConstraints()
    }
    
    func setNormal(cost : Int){
        UIView.animate(withDuration: 0.3) {
            self.oldLabel.alpha = 0
            self.costLabel.attributedText = CustomFont.titleSecondary.attributesWithParagraph.colored(color: Color.black.value).make(string: "\(cost) ₽")
        }
    }
    func setDiscount(cost : Int, old : Int){
        UIView.animate(withDuration: 0.3) {
            self.oldLabel.alpha = 1
            //self.oldLabel.attributedText = CustomFont.titleSecondary.attributesWithParagraph.colored(color: Color.grey.value).make(string: "\(old) ₽")
           // self.oldLabel.attributedText.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, self.oldLabel.attributedText.length))
            self.oldLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "\(old) ₽")
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString()
            let string = CustomFont.titleSecondary.attributesWithParagraph.colored(color: Color.grey.value).make(string: "\(old) ₽")
            attributeString.append(string)
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
            self.oldLabel.attributedText = attributeString
            self.costLabel.attributedText = CustomFont.titleSecondary.attributesWithParagraph.colored(color: Color.accent.value).make(string: "\(cost) ₽")
        }
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.mainLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(12)
            $0.left.equalToSuperview().offset(16)
        }
        self.costLabel.snp.makeConstraints{
            $0.top.equalTo(self.mainLabel.snp.bottom).offset(2)
            $0.left.equalToSuperview().offset(16)
        }
        self.oldLabel.snp.makeConstraints{
            $0.top.equalTo(self.costLabel)
            $0.left.equalTo(self.costLabel.snp.right).offset(10)
        }
        
    }
}
