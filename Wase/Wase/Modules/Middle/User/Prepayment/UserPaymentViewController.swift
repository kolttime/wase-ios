//
//  UserPaymentViewController.swift
//  Wase
//
//  Created by Роман Макеев on 28/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit


class UserPaymentViewController: SimpleNavigationBar {
    
    
    var apiService : ApiService
    var photoId : String
    let cardNumberField = OMGTextField.makeСardNumber()
    let cardHolderField = OMGTextField.makeCardHolderField()
    let validField = OMGTextField.makeValidThru()
    let codeField = OMGTextField.makeCardCode()
    let saveLabel = UILabel()
    let switchView = UISwitch()
    let saveView = UIView()
    let keyObserver = KeyboardObserver()
    let payButton = UIView()
    var bottomConstraint : Constraint?
    init(apiService : ApiService, photoId : String) {
        self.apiService = apiService
        self.photoId = photoId
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        
        
        self.setTitle(title: "Оплата")
        self.setTabBarHidden()
        self.setLargeTitle()
        self.view.backgroundColor = Color.white.value
        self.saveView.addSubview(self.saveLabel)
        self.saveView.addSubview(self.switchView)
        self.view.addSubview(self.payButton)
        let payLabel = UILabel()
        payLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.white.value).make(string: "Оплатить")
        self.payButton.addSubview(payLabel)
        payLabel.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview()
        }
        self.toastPresenter.targetView = self.view
        self.payButton.backgroundColor = Color.accent.value
        self.payButton.layer.cornerRadius = 10
        self.payButton.layer.masksToBounds = true
        self.view.addSubview(self.cardNumberField)
        self.view.addSubview(self.cardHolderField)
        self.view.addSubview(self.validField)
        self.view.addSubview(self.codeField)
        //self.view.addSubview(self.saveLabel)
        //self.view.addSubview(self.switchView)
        self.view.addSubview(self.saveView)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.endEditingHandler))
        tap.cancelsTouchesInView = false
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.endEditingHandler))
        swipe.cancelsTouchesInView = false
        swipe.direction = [.down, .up]
        self.view.addGestureRecognizer(swipe)
        self.view.addGestureRecognizer(tap)
        self.payButton.alpha = 0.7
        self.payButton.isUserInteractionEnabled = false
        let payTap = UITapGestureRecognizer(target: self, action: #selector(self.payClicked))
        self.payButton.addGestureRecognizer(payTap)
        self.switchView.tintColor = Color.accent.value
        self.saveLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Сохранить карту")
        self.setupHandlers()
        self.setUpConstraints()
    }
    var onPay : ((Photosession) -> Void)?
    @objc func payClicked(){
        print("PAYAYAY")
        self.apiService.postPrepayment(photoId: self.photoId) {
            [weak self] response in
            guard let self = self else {return}
            switch response {
            case .success(let photosession):
                print(photosession)
                //self.onPay?(photosession.toShort())
            case .failure(let error):
                self.showToastErrorAlert(error)
            }
        }
    }
    
    @objc func endEditingHandler(){
        self.view.endEditing(true)
    }
    
    func setupHandlers(){
        self.cardHolderField.onTextDidChanged = { response in
            self.validate()
        }
        
        self.codeField.onTextDidChanged = { response in
            self.codeComplete = response.mandatoryComplete
        }
        
        self.cardNumberField.onTextDidChanged = { response in
            self.cardNumberComplete = response.mandatoryComplete
        }
        
        self.validField.onTextDidChanged = { response in
            self.validThruComplete = response.mandatoryComplete
            
        }
        
    }
    private func validate() {
        var validate = self.cardHolderField.text?.isEmpty == false
        validate = validate && self.cardNumberComplete
        validate = validate && self.validThruComplete
        validate = validate && self.codeComplete
        
        
        if validate {
           // self.payCreditCardButton.update(state: .normal)
            self.payButton.isUserInteractionEnabled = true
            self.payButton.alpha = 1
        } else {
            //self.payCreditCardButton.update(state: .disabled)
            self.payButton.isUserInteractionEnabled = false
            self.payButton.alpha = 0.7
        }
    }
    private var cardNumberComplete: Bool = false {
        didSet {
            self.validate()
        }
    }
    private var validThruComplete: Bool = false {
        didSet {
            self.validate()
        }
    }
    private var codeComplete: Bool = false {
        didSet {
            self.validate()
        }
    }

    
    override func setUpConstraints() {
        super.setUpConstraints()
        self.cardNumberField.snp.makeConstraints{
            $0.top.equalToSuperview().offset(self.getTopOffset() + 15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.validField.snp.makeConstraints{
            $0.top.equalTo(self.cardNumberField.snp.bottom).offset(16)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.height.equalTo(60)
            $0.width.equalTo((UIScreen.main.bounds.size.width / 2) - ViewSize.sideOffset - (17 / 2))
        }
        self.codeField.snp.makeConstraints{
            $0.top.equalTo(self.validField)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
            $0.width.equalTo(self.validField)
        }
        self.cardHolderField.snp.makeConstraints{
            $0.top.equalTo(self.validField.snp.bottom).offset(16)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.saveView.snp.makeConstraints{
            $0.top.equalTo(self.cardHolderField.snp.bottom).offset(24)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(32)
        }
        
        self.saveLabel.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.centerY.equalToSuperview()
            
        }
        self.switchView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalToSuperview()
            $0.width.equalTo(52)
        }
        self.payButton.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
            self.bottomConstraint = $0.bottom.equalToSuperview().offset(-24).constraint
        }
    }
}
