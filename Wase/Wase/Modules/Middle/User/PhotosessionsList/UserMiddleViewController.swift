//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class UserMiddleViewController: SimpleNavigationBar{
    
    
    private let emptyView = EmptyPhotosessionsView(mainString: "Потрясающие и памятные фотографии - вот результат, который вы получите после фотосессии. Наши фотографы и визажисты помогут запечатлеть вашу красоту и подберут уникальный стиль для приятных воспоминаний.", buttonString: "Хочу фотосессию")
    var selection : ((UserType, String) -> Void)?
    var onNew : Action?
    private lazy var tableViewController : TableViewController = TableViewController()
    private var photoModels : [Photosession] = []
    
    private var apiService : ApiService
    var userModel : User?
    init(apiService : ApiService){
        self.apiService = apiService
        super.init()
    }
    var refreshControl = UIRefreshControl()
    private lazy var loadingView = TaokaLoadingView()
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.setTitle(title: "Фотосессии")
        self.setLargeTitle()
        self.setBackTitle(title: "Фотосессии")
        self.setTabBar()
        
        self.toastPresenter.targetView = self.view
        self.add(self.tableViewController)
        self.tableViewController.tableView.register(cellClass: UserPhotosessionsCellType1.self)
        self.tableViewController.tableView.register(cellClass: UserPhotosessionsCellType2.self)
        self.tableViewController.tableView.register(cellClass: UserPhotosessionsCellType3.self)
        self.tableViewController.tableView.register(cellClass: UserPhotosessionsCellType4.self)
       // self.tableViewController.tableView.estimatedRowHeight = 400
       // self.tableViewController.tableView.rowHeight = UITableView.automaticDimension
        //self.setRighttitle(title: "Создать новую", completion: self.onNew)
        let addNewLabel = UILabel()
        addNewLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Хочу новую")
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onAddNewButton))
        addNewLabel.isUserInteractionEnabled = true
        addNewLabel.addGestureRecognizer(tap)
        self.setRightButton(view: addNewLabel)
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = true
        self.emptyView.isHidden = true
        self.setUpConstraints()
        //self.render()
        self.view.addSubview(self.emptyView)
        self.emptyView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        let onClick : Action? = {[weak self] in
            print("Заебок!")
            guard let self = self else {return}
//            if let name = URL(string: "https://wase.photo"), !name.absoluteString.isEmpty {
//                let objectsToShare = ["Подключайся в Wase и получай памятные фотографии", name] as [Any]
//                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//
//                self.present(activityVC, animated: true, completion: nil)
//            }else  {
//                // show alert for not available
//            }
            self.onNew?()
        }
        self.emptyView.onClick = onClick
        
        //refreshControl.attribute[]dTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.tableViewController.tableView.addSubview(refreshControl) // not required when using UIT
        self.fetchData()
    }
    
    var onPhoneNumber : Action?
    func updatePhotosession(photo : Photosession) {
        guard let index = self.photoModels.lastIndex(where: {$0.id == photo.id}) else {return}
        self.photoModels[index] = photo
        self.render()
        
    }
    @objc func refresh(){
        print("REFRESH")
        self.apiService.getUserPhotosessions(active: true) {
            [weak self] response in
            self?.refreshControl.endRefreshing()
            switch response {
            case .success(let photosessions):
                //print(photosessions)
                //print(photosessions[0].id)
                self?.photoModels = photosessions
                //self?.loadingView.isHidden = true
                self?.render()
            case .failure(let error):
                print(error)
                self?.showToastErrorAlert(error)
            }
        }
    }
    @objc func onAddNewButton(){
        
        
        guard let userModel = self.userModel else {return}
        
        if userModel.phone != nil && userModel.phone != "" {
        
            self.onNew?()
            
        } else {
            self.onPhoneNumber?()
        }
        
    }

    func fetchData(){
        self.loadingView.isHidden = false
        if self.userModel == nil {
            if UserClass.shared.user != nil {
                self.userModel = UserClass.shared.user
            } else {
                self.apiService.getProfile(userId: nil) {
                    [weak self] response in
                    guard let self = self else {return}
                    switch response {
                    case .success(let user):
                        self.userModel = user
                        UserClass.shared.user = user
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }
        self.apiService.getUserPhotosessions(active: true) {
            [weak self] response in
            switch response {
            case .success(let photosessions):
                //print(photosessions)
               // print(photosessions[0].id)
                self?.photoModels = photosessions
                self?.loadingView.isHidden = true
                self?.render()
            case .failure(let error):
                print(error)
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    func updateUser(){
        self.userModel = UserClass.shared.user!
    }
    func addNew(photosession : Photosession){
        var photos : [Photosession] = [photosession]
        photos.append(contentsOf: self.photoModels)
        self.photoModels = photos
        self.render()
    }
    func update(photosession : Photosession){
        guard let index = self.photoModels.lastIndex(where: {$0.id == photosession.id}) else {return}
        self.photoModels[index] = photosession
        self.render()
    }
    func forceupdate(){
        self.photoModels = []
        self.fetchData()
    }
    var onUser : ((String, String?) -> Void)?
    var onPhotoSettings : ((String) -> Void)?
    var onApprove : ((UserType, String) -> Void)?
    var onAccept : ((String, Int) -> Void)?
    var onConfirmation : ((String) -> Void)?
    var onAlbum : ((String) -> Void)?
    func render(){
        if self.photoModels.count == 0 {
           self.tableViewController.tableView.isHidden = true
           self.emptyView.isHidden = false
        } else {
            self.emptyView.isHidden = true
            self.tableViewController.tableView.isHidden = false
        }
        let viewModels2 : [TableCellViewModel] = HelpFunctions.convertModelToPhotosessionCell(photoModels: self.photoModels, settingSelection:{(id) in
            print("ID")
            self.onPhotoSettings?(id)
        }, candidatesSelection: { (type, id) in
            self.selection?(type, id)
        }, userSelection: onUser, accepcSelection: onAccept, approveSelection: {[weak self] type, id in
            self?.onApprove?(type, id)
            }, openAlbumSelection: self.onAlbum, openConfirmation: self.onConfirmation)
//        for model in self.photoModels {
//            var viewModel : TableCellViewModel
//            var stage = 0
//            switch model.status {
//            case .created:
//                stage = 0
//            case .prepayment:
//                stage = 2
//            case .selection:
//                stage = 1
//            default:
//                print("ИИИИИ")
//
//            }
//            let dateFormatter = DateFormatter()
//            let calendar = Calendar.current
//            //let DATE = Date()
//            let year = calendar.component(.year, from: Date())
//            dateFormatter.locale =  Locale(identifier: "ru_MD")
//            dateFormatter.dateFormat = "YYYY-MM-dd"
//            let date = dateFormatter.date(from: model.date)
//            let year2 = calendar.component(.year, from: date!)
//            if year != year2 {
//                dateFormatter.dateFormat = "dd MMMM YYYY"
//            } else {
//                dateFormatter.dateFormat = "dd MMMM"
//            }
//            let stringDate = dateFormatter.string(from: date!)
//
//
//            
//            let selection : ((UserType, String) -> Void)? = {[weak self] type, id in
//                self?.selection?(type, id)
//            }
//            var durationString = ""
//            switch model.duration {
//                case 1:
//                    durationString = "1 час"
//            case 2:
//                durationString = "2 часа"
//            case 3:
//                durationString = "3 часа"
//            case 4:
//                durationString = "4 часа"
//            default:
//                durationString = "\(model.duration) часов"
//
//            }
//
//                if model.offers!.count == 1 {
//               //     viewModel = UserPhotosessionsCellModelType1(name: model.title, date: "\(stringDate) c \(model.time), \(durationString)", urls: [""], type: "", count: model.offers![0].pending!, stage: stage, id: model.id, selection: selection)
//               //     viewModels2.append(viewModel)
//                } else if model.offers!.count == 2 {
//                  //  viewModel = UserPhotosessionsCellModelType2(name: model.title, date: "\(stringDate) c \(model.time), \(durationString)", urls: ["",""], count1: model.offers![0].pending!, count2: model.offers![1].pending!, stage: stage, id: model.id, selection: selection)
//               //     viewModels2.append(viewModel)
//                }
//
//
//
//        }
        let section = DefaultTableSectionViewModel(cellModels: viewModels2)
        self.tableViewController.update(viewModels: [section])
        print("PHOTOSESSIONS : \(viewModels2.count)")
    }
    override func viewDidAppear(_ animated: Bool) {
//        if self.photoModels.count == 0 {
//            self.tableViewController.tableView.isHidden = true
//            self.emptyView.isHidden = false
//        } else {
//            self.emptyView.isHidden = true
//            self.tableViewController.tableView.isHidden = false
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.setLargeTitle()
        self.setTabBar()
        guard let userModel = UserClass.shared.user else {return}
        self.userModel = userModel
    }
    
    
    override func setUpConstraints() {
        super.setUpConstraints()
        self.tableViewController.view.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
    
}




