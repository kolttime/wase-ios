//
//  UserPhotoTopView.swift
//  Taoka
//
//  Created by Minic Relocusov on 18/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class UserphotoTopView : AirdronView {
    
    
    let separatorView = UIView()
    let nameLabel : UILabel = UILabel()
    let dateLabel : UILabel = UILabel()
    let arrow : UIImageView = UIImageView()
    let messegaImageView = UIImageView()
    let img : Bool = false
    
    
    
    func set(nameLabel : String, dateLabel : String, img : Bool){
        if !img {
            self.arrow.isHidden = true
        }
        if img {
            self.arrow.isHidden = false
        }
        
        self.nameLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: nameLabel)
        self.dateLabel.attributedText = CustomFont.tech13.attributesWithParagraph.make(string: dateLabel)
    }
    var onSelect : Action?
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.nameLabel)
        self.addSubview(self.dateLabel)
        self.addSubview(self.separatorView)
        self.addSubview(self.messegaImageView)
        self.messegaImageView.image = UIImage.init(named: "Messages")!
        self.messegaImageView.isHidden = true
        self.separatorView.backgroundColor = Color.light.value
        self.backgroundColor = Color.white.value
        arrow.image = UIImage(named: "Arrow2")!
        self.addSubview(self.arrow)
        self.messegaImageView.alpha = 0
        arrow.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selected))
        self.addGestureRecognizer(tap)
        
        self.setupConstraints()
    }
    
    @objc func selected(){
        self.onSelect?()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.nameLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(12)
        }
        self.dateLabel.snp.makeConstraints{
            $0.top.equalTo(self.nameLabel.snp.bottom)
            $0.left.equalToSuperview().offset(12)
        }
        self.separatorView.snp.makeConstraints{
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
        }
        
            self.arrow.snp.makeConstraints{
                $0.top.equalToSuperview().offset(15)
                $0.right.equalToSuperview().offset(-17)
                $0.height.equalTo(13)
                $0.width.equalTo(8)
            }
        self.messegaImageView.snp.makeConstraints{
            $0.top.equalTo(self.arrow)
            $0.right.equalTo(self.arrow)
            $0.width.equalTo(25)
            $0.height.equalTo(23)
        }
        
    }
}
