//
//  UserPhotosessionCell.swift
//  Taoka
//
//  Created by Minic Relocusov on 18/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class UserPhotosessionsCellType1 : TableViewCell {
    
    
    var viewModel : UserPhotosessionsCellModelType1?
    var id : String?
    var selection : ((UserType, String) -> Void)?
    let baseView = UIView()
    let separatorView = UIView()
    let nameLabel = UILabel()
    let dateLabel = UILabel()
    let containerView = UIView()
    let firstMiddle = UserPhotoMiddleView()
    let secondMiddle = UserPhotoMiddleView()
    var topView = UserphotoTopView()
    var statusBarView = UserPhotoStatusBarView(stage: 1)
    override func initialSetup() {
        super.initialSetup()
        
        self.backgroundColor = Color.white.value
        // self.contentView.addSubview(self.label)
        self.contentView.addSubview(self.baseView)
        // self.contentView.addSubview(self.mainImageView)
        
        baseView.backgroundColor = UIColor.clear
        baseView.layer.shadowColor = UIColor.black.cgColor
        baseView.layer.shadowOffset = CGSize(width: 0, height: 0)
        baseView.layer.shadowOpacity = 0.1
        baseView.layer.shadowRadius = 8.0
        self.containerView.frame = baseView.bounds
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.masksToBounds = true
        baseView.addSubview(self.containerView)
      //  baseView.layer.shadowPath = UIBezierPath(roundedRect: baseView.bounds, cornerRadius: 10).cgPath
        baseView.layer.shouldRasterize = true
        baseView.layer.rasterizationScale = UIScreen.main.scale
        self.containerView.backgroundColor = Color.light.value
        self.firstMiddle.onTouch = {[weak self] in
            guard let self = self else {return}
            guard let viewModel = self.viewModel else {return}
           // if viewModel.
            self.selection?(UserType.photographer, self.id!)
        }
        self.firstMiddle.onCall = {[weak self] in
            guard let self = self else {return}
            self.viewModel?.onCall?(.photographer)
        }
        self.secondMiddle.onCall = {[weak self] in
            guard let self = self else {return}
            self.viewModel?.onCall?(.makeUpArtist)
        }
        self.topView.onSelect = {[weak self] in
            guard let self = self else {return}
            guard let viewModel = self.viewModel else {return}
            viewModel.settingsSelection?(viewModel.id)
        }
        self.containerView.addSubview(self.topView)
        self.containerView.addSubview(self.firstMiddle)
        self.containerView.addSubview(self.statusBarView)
        self.contentView.backgroundColor = Color.white.value
        //self.containerView.dropShadow()
        
        self.setUpConstarints()
        
        // set the shadow properties
        
        
       // self.setupConstraints()
    }
    var first1 = true
    var first2 = true
    override func configure(viewModel: TableViewCell.ViewModelType) {
        var viewModel = viewModel as! UserPhotosessionsCellModelType1
        self.viewModel = viewModel
        self.id = viewModel.id
        self.selection = viewModel.selection
        self.topView.set(nameLabel: viewModel.name, dateLabel: viewModel.date, img: true)
        
        if viewModel.stage == 3 {
            self.topView.arrow.isHidden = true
            self.topView.messegaImageView.isHidden = true
            self.firstMiddle.arrow.isHidden = true
            self.firstMiddle.callsImageVIew.isHidden = false
        } else {
            if viewModel.stage == 0 {
                self.topView.arrow.isHidden = false
                self.topView.messegaImageView.isHidden = true
                self.firstMiddle.arrow.isHidden = false
                self.firstMiddle.callsImageVIew.isHidden = true
            } else {
                self.topView.arrow.isHidden = true
                self.topView.messegaImageView.isHidden = true
                self.firstMiddle.callsImageVIew.isHidden = true
                self.firstMiddle.arrow.isHidden = true
            }
        }
        self.firstMiddle.set(count: viewModel.count, proffesionName: viewModel.proffesionName, avatarURL: viewModel.urls![0], new: viewModel.new)
        self.statusBarView.removeFromSuperview()
        self.statusBarView = UserPhotoStatusBarView(stage: viewModel.stage)
        self.containerView.addSubview(self.statusBarView)
        self.statusBarView.snp.makeConstraints{
            $0.top.equalTo(self.firstMiddle.snp.bottom).offset(1)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }

    }
    func setUpConstarints(){
        self.baseView.snp.makeConstraints{
            $0.top.equalTo(self.contentView).offset(8)
            $0.left.equalTo(self.contentView).offset(ViewSize.sideOffset)
            $0.right.equalTo(self.contentView).offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.contentView).offset(-8)
           // $0.height.equalTo(169)
        }
        self.containerView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.topView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(60)
        }
        self.firstMiddle.snp.makeConstraints{
            $0.top.equalTo(self.topView.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(56)
        }
        self.statusBarView.snp.makeConstraints{
            $0.top.equalTo(self.firstMiddle.snp.bottom).offset(1)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
    
    
    
    
    
    
    
    
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        return 185
        
    }
    
    
    
    
}
struct UserPhotosessionsCellModelType1 : TableCellViewModel {
    var cellType: TableViewCell.Type {return UserPhotosessionsCellType1.self}
    var name : String
    var date : String
    var urls : [String]?
    var type : String
    var count : NSAttributedString
    var proffesionName : String
    var stage : Int
    var id : String
    var selection : ((UserType, String) -> Void)?
    var settingsSelection : ((String) -> Void)?
    var new : Int
    var onCall : ((UserType) -> Void)?
}

