//
//  UserPhotoMiddleView.swift
//  Taoka
//
//  Created by Minic Relocusov on 19/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class UserPhotoMiddleView : AirdronView {
    
    
    var onTouch : Action?
    private lazy var avatarImageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = Color.light.value
        view.layer.cornerRadius = 36 / 2
        view.clipsToBounds = true
        view.isUserInteractionEnabled = true
        view.contentMode = .scaleAspectFill
        return view
    }()
    let topLabel = UILabel()
    let bottomLabel = UILabel()
    var newLabel = UILabel()
    let arrow = UIImageView()
    let separatorView = UIView()
    let callsImageVIew = UIImageView()
    
    func set(count : NSAttributedString, proffesionName : String, avatarURL : String, new : Int){
        self.topLabel.attributedText = count
        self.bottomLabel.attributedText = CustomFont.tech11.attributesWithParagraph.make(string: proffesionName)
        self.avatarImageView.kf.setImage(with: URL(string: avatarURL))
        if new != 0 {
            self.newLabel.isHidden = false
            self.newLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.white.value).make(string: " +\(new) ")
        } else {
            self.newLabel.isHidden = true
            
            //self.newLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph
        }
        self.setupConstraints()
        
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.avatarImageView)
        self.addSubview(self.topLabel)
        self.addSubview(self.bottomLabel)
        self.addSubview(self.arrow)
        self.addSubview(self.separatorView)
        self.addSubview(self.callsImageVIew)
        self.callsImageVIew.isHidden = true
        self.callsImageVIew.image = UIImage.init(named: "Calls")!
        self.backgroundColor = Color.white.value
        self.separatorView.backgroundColor = Color.grey.value
        self.arrow.image = UIImage(named: "Arrow2")!
       // self.arrow.contentMode = .scaleAspectFill
        self.callsImageVIew.isUserInteractionEnabled = true
        let callTap = UITapGestureRecognizer(target: self, action: #selector(self.makeCall))
        self.callsImageVIew.addGestureRecognizer(callTap)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.touched))
        self.addGestureRecognizer(tap)
        self.newLabel.layer.masksToBounds = true
        self.newLabel.layer.cornerRadius = 5
        self.newLabel.backgroundColor = Color.black.value
        self.newLabel.textAlignment = .center
        self.addSubview(self.newLabel)
        self.setupConstraints()
    }
    
    var onCall : Action?
    @objc func makeCall(){
        //let url =
       // print("Calll")
        self.onCall?()
    }
    @objc func touched(){
        self.onTouch?()
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        self.avatarImageView.snp.removeConstraints()
        self.topLabel.snp.removeConstraints()
        self.bottomLabel.snp.removeConstraints()
        self.newLabel.snp.removeConstraints()
        self.arrow.snp.removeConstraints()
        self.callsImageVIew.snp.removeConstraints()
        self.avatarImageView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(12)
            $0.width.equalTo(36)
            $0.height.equalTo(36)
        }
        self.topLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalTo(self.avatarImageView.snp.right).offset(6)
            $0.width.equalTo(self.topLabel.textWidth())
        }
        self.bottomLabel.snp.makeConstraints{
            $0.top.equalTo(self.topLabel.snp.bottom)
            $0.left.equalTo(self.avatarImageView.snp.right).offset(6)
            $0.width.equalTo(self.bottomLabel.textWidth())
        }
        self.newLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(11)
            $0.left.equalTo(self.topLabel.snp.right).offset(3)
            $0.width.equalTo(self.newLabel.textWidth())
            $0.height.equalTo(20)
            
        }
        self.arrow.snp.makeConstraints{
            $0.top.equalToSuperview().offset(15)
            $0.right.equalToSuperview().offset(-17)
            $0.height.equalTo(13)
            $0.width.equalTo(8)
        }
        self.callsImageVIew.snp.makeConstraints{
            $0.top.equalTo(self.arrow)
            $0.right.equalTo(self.arrow)
            $0.height.equalTo(20)
            $0.width.equalTo(20)
        }
    }
    
}
