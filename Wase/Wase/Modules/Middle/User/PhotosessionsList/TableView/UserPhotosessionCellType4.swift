//
//  UserPhotosessionCell.swift
//  Taoka
//
//  Created by Minic Relocusov on 18/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class UserPhotosessionsCellType4 : TableViewCell {
    
    
    
    var id : String?
    var selection : ((UserType, String) -> Void)?
    let baseView = UIView()
    let separatorView = UIView()
    let nameLabel = UILabel()
    let dateLabel = UILabel()
    let containerView = UIView()
    let firstMiddle = UserPhotoMiddleView()
    let secondMiddle = UserPhotoMiddleView()
    var topView = UserphotoTopView()
    var subLabel = UILabel()
    var subButtonView = UIView()
    var statusBarView = UserPhotoStatusBarView(stage: 1)
    var viewModel : UserPhotosessionsCellModelType4?
    override func initialSetup() {
        super.initialSetup()
        
        self.backgroundColor = Color.white.value
        // self.contentView.addSubview(self.label)
        self.contentView.addSubview(self.baseView)
        // self.contentView.addSubview(self.mainImageView)
        
        baseView.backgroundColor = UIColor.clear
        baseView.layer.shadowColor = UIColor.black.cgColor
        baseView.layer.shadowOffset = CGSize(width: 0, height: 0)
        baseView.layer.shadowOpacity = 0.1
        baseView.layer.shadowRadius = 8.0
        self.containerView.frame = baseView.bounds
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.masksToBounds = true
        baseView.addSubview(self.containerView)
       // baseView.layer.shadowPath = UIBezierPath(roundedRect: baseView.bounds, cornerRadius: 10).cgPath
        baseView.layer.shouldRasterize = true
        baseView.layer.rasterizationScale = UIScreen.main.scale
        self.containerView.backgroundColor = Color.light.value
        self.firstMiddle.onCall = {[weak self] in
            guard let self = self else {return}
            self.viewModel?.onCall?(.photographer)
        }
        self.secondMiddle.onCall = {[weak self] in
            guard let self = self else {return}
            self.viewModel?.onCall?(.makeUpArtist)
        }
        //self.containerView.backgroundColor = Color.red.value
        self.containerView.addSubview(self.topView)
        self.containerView.addSubview(self.firstMiddle)
        self.containerView.addSubview(self.statusBarView)
        self.containerView.addSubview(self.separatorView)
        self.containerView.addSubview(self.subButtonView)
        self.subButtonView.backgroundColor = Color.light.value
        self.subButtonView.addSubview(self.subLabel)
        self.subLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: "Подтвердить")
        self.separatorView.backgroundColor = Color.light.value
        self.contentView.backgroundColor = Color.white.value
        //self.containerView.dropShadow()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.bottomClick))
        self.subButtonView.addGestureRecognizer(tap)
        self.topView.onSelect = {[weak self]  in
            guard let self = self else {return}
            guard let viewModel = self.viewModel else {return}
            viewModel.settingsSelection?(viewModel.id)
        }
        self.firstMiddle.onTouch = {[weak self] in
            guard let self = self else {return}
            self.selection?(UserType.photographer, self.id!)
        }
        self.secondMiddle.onTouch = {[weak self] in
            guard let self = self else {return}
            self.selection?(UserType.makeUpArtist, self.id!)
        }
        self.setUpConstarints()
        
        // set the shadow properties
        
        
        // self.setupConstraints()
    }
    func makeCall(){
        
    }
    
    var first1 = true
    var first2 = true
    @objc func bottomClick(){
        guard let viewModel = self.viewModel else {return}
        viewModel.bottomSelection?(viewModel.id)
    }
    override func configure(viewModel: TableViewCell.ViewModelType) {
        var viewModel = viewModel as! UserPhotosessionsCellModelType4
        self.topView.set(nameLabel: viewModel.name, dateLabel: viewModel.date, img: true)
       // self.firstMiddle.set(count: "0 Заявок", proffesionName: "Хз", avatarURL: viewModel.urls![0])
        self.firstMiddle.set(count: viewModel.count, proffesionName: viewModel.proffesionName, avatarURL: viewModel.urls![0], new : viewModel.new)
        self.selection = viewModel.selection
        
        self.viewModel = viewModel
        self.id = viewModel.id
        if viewModel.stage == 3 {
            self.subLabel.text = "Подтвердить"
        } else if viewModel.stage == 4 {
            self.subLabel.text = "Оставить отзыв"
        } else if viewModel.stage == 6 {
            self.subLabel.text = "Открыть альбом"
        }
        if viewModel.stage == 3 {
            self.topView.arrow.isHidden = true
            self.topView.messegaImageView.isHidden = false
            self.firstMiddle.arrow.isHidden = true
            self.firstMiddle.callsImageVIew.isHidden = false
            self.secondMiddle.arrow.isHidden = true
            self.secondMiddle.callsImageVIew.isHidden = false
        } else {
            if viewModel.stage == 0 {
                self.topView.arrow.isHidden = false
                self.topView.messegaImageView.isHidden = true
                self.firstMiddle.arrow.isHidden = false
                self.firstMiddle.callsImageVIew.isHidden = true
                self.secondMiddle.arrow.isHidden = false
                self.secondMiddle.callsImageVIew.isHidden = true
            } else {
                self.topView.arrow.isHidden = true
                self.topView.messegaImageView.isHidden = true
                self.firstMiddle.callsImageVIew.isHidden = true
                self.firstMiddle.arrow.isHidden = true
                self.secondMiddle.callsImageVIew.isHidden = true
                self.secondMiddle.arrow.isHidden = true
            }
        }
        self.statusBarView.removeFromSuperview()
        self.statusBarView = UserPhotoStatusBarView(stage: viewModel.stage)
        self.containerView.addSubview(self.statusBarView)
        self.statusBarView.snp.makeConstraints{
            $0.top.equalTo(self.subButtonView.snp.bottom).offset(1)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
    }
    func setUpConstarints(){
        self.baseView.snp.makeConstraints{
            $0.top.equalTo(self.contentView).offset(8)
            $0.left.equalTo(self.contentView).offset(ViewSize.sideOffset)
            $0.right.equalTo(self.contentView).offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.contentView).offset(-8)
            // $0.height.equalTo(169)
        }
        self.containerView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.topView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(60)
        }
        self.firstMiddle.snp.makeConstraints{
            $0.top.equalTo(self.topView.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(56)
        }
        self.subButtonView.snp.makeConstraints{
            $0.top.equalTo(self.firstMiddle.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(56)
        }
        self.subLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        self.statusBarView.snp.makeConstraints{
            $0.top.equalTo(self.subButtonView.snp.bottom).offset(1)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
    
    
    
    
    
    
    
    
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        return 241
        
    }
    
    
    
    
}
struct UserPhotosessionsCellModelType4 : TableCellViewModel {
    var cellType: TableViewCell.Type {return UserPhotosessionsCellType4.self}
    var name : String
    var date : String
    var urls : [String]?
    var stage : Int
    var count : NSAttributedString
    var id : String
    var selection : ((UserType, String) -> Void)?
    var settingsSelection : ((String) -> Void)?
    var bottomSelection : ((String) -> Void)?
    var proffesionName : String
    var onCall : ((UserType) -> Void)?
    var new : Int
}

