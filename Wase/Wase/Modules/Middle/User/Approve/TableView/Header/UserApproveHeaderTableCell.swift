//
//  UserApproveHeaderTableCell.swift
//  Wase
//
//  Created by Роман Макеев on 19/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class UserApproveHeadertableCell : TableViewCell {
    
    
    private var avatarImageView = UIImageView()
    private var nameLabel = UILabel()
    private var priceLabel = UILabel()
    private var commentview = CommentView(top: true)
    private var separatorView = UIView()
    private var checkView = UIImageView()
    
    
    override func initialSetup() {
        super.initialSetup()
        
        self.contentView.addSubview(self.avatarImageView)
        self.contentView.addSubview(self.nameLabel)
        self.contentView.addSubview(self.priceLabel)
        self.contentView.addSubview(self.commentview)
        self.contentView.addSubview(self.separatorView)
        self.contentView.addSubview(self.checkView)
        self.contentView.backgroundColor = Color.white.value
        self.avatarImageView.contentMode = .scaleAspectFill
        self.checkView.contentMode = .scaleToFill
        self.avatarImageView.layer.cornerRadius = 22
        self.avatarImageView.layer.masksToBounds = true
        self.avatarImageView.backgroundColor = Color.greyL.value
        self.separatorView.backgroundColor = Color.light.value
        self.setupConstraints()
        
    }
    //var selection : ((String, String) -> Void)?
    var offerModel : UserApproveHeadertableCellModel?
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! UserApproveHeadertableCellModel
        self.offerModel = viewModel
        self.checkView.image = UIImage.init(named: "Ok")
        if viewModel.offer.status == .participant {
            self.checkView.isHidden = false
        } else {
            self.checkView.isHidden = true
        }
        self.priceLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.make(string: "\(viewModel.offer.price!) ₽")
        self.nameLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "\(viewModel.offer.user.givenName) \(viewModel.offer.user.familyName)")
        self.avatarImageView.kf.setImage(with: URL(string: viewModel.offer.user.avatar?.original.absoluteString ?? ""))
        self.separatorView.removeFromSuperview()
        if viewModel.offer.comment != nil && viewModel.offer.comment != ""{
            
            self.commentview.isHidden = false
            self.commentview.set(text: viewModel.offer.comment!, time: "")
            self.commentview.snp.removeConstraints()
            self.commentview.snp.makeConstraints{
                $0.top.equalTo(self.priceLabel.snp.bottom).offset(8)
                $0.left.equalToSuperview().offset(16)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                let commentheigh = self.commentview.textLabel.text!.height(width: UIScreen.main.bounds.size.width - ViewSize.sideOffset - 16 - 12 - 12, font: CustomFont.bodyRegular15.font) + 16
                $0.height.equalTo(commentheigh)
                $0.bottom.equalToSuperview().offset(-8)
                
            }
        }
        else {
            self.contentView.addSubview(self.separatorView)
            self.separatorView.snp.makeConstraints{
                $0.top.equalTo(self.priceLabel.snp.bottom).offset(8)
                $0.left.equalTo(self.priceLabel.snp.left)
                $0.right.equalToSuperview()
                $0.height.equalTo(1)
                $0.bottom.equalToSuperview()

            }
        }
    }
    override func didSelect() {
        self.offerModel?.selection?(self.offerModel!.offer.user.id, self.offerModel!.offer.status)
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.avatarImageView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(16)
            $0.height.equalTo(44)
            $0.width.equalTo(44)
        }
        self.nameLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(12)
            $0.left.equalTo(self.avatarImageView.snp.right).offset(8)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.priceLabel.snp.makeConstraints{
            $0.top.equalTo(self.nameLabel.snp.bottom)
            $0.left.equalTo(self.avatarImageView.snp.right).offset(8)
            
        }
        self.checkView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(20)
            $0.right.equalToSuperview().offset(-14)
            $0.height.equalTo(ViewSize.sideOffset)
            $0.width.equalTo(ViewSize.sideOffset)
        }
        self.commentview.isHidden = true
        self.separatorView.removeFromSuperview()
        self.contentView.addSubview(self.separatorView)
        self.separatorView.snp.makeConstraints{
            $0.top.equalTo(self.priceLabel.snp.bottom).offset(9)
            $0.left.equalTo(self.priceLabel.snp.left)
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
            $0.bottom.equalToSuperview()
        }
        self.commentview.snp.makeConstraints{
            $0.top.equalTo(self.priceLabel.snp.bottom).offset(8)
            $0.left.equalTo(self.priceLabel.snp.left)
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
            //$0.bottom.equalToSuperview().offset(-8)
        }
       
    }
    
    
}
struct  UserApproveHeadertableCellModel : TableCellViewModel{
    var cellType: TableViewCell.Type {return UserApproveHeadertableCell.self}
    var offer : UserOffers
    var selection : ((String, OffersStatus) -> Void)?
}
