//
//  UserApproveHeaderTableView.swift
//  Wase
//
//  Created by Роман Макеев on 19/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class UserApproveSectionTableView : TableViewCell {
    
    
    
    private lazy var tableViewController = ALTableViewController()
    private var separator1 = UIView()
    private var separator2 = UIView()
    var photoSelection : ((String, OffersStatus) -> Void)?
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.tableViewController.tableView)
        self.tableViewController.tableView.register(cellClass: UserApproveHeadertableCell.self)
        self.tableViewController.tableView.register(cellClass: UserApproveCellView.self)
        self.contentView.addSubview(self.separator1)
        self.contentView.addSubview(self.separator2)
        self.separator1.backgroundColor = Color.greyL.value
        self.separator2.backgroundColor = Color.light.value
        self.contentView.backgroundColor = Color.white.value
        self.setupConstraints()
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! UserApproveSectionTableViewModel
        self.photoSelection = viewModel.selection
        var models : [UserApproveHeadertableCellModel] = []
        var models2 : [UserApproveCellViewModel] = []
        viewModel.offers.map({
            
            if $0.status == .participant || $0.status == .accepted {
            
                models.append(UserApproveHeadertableCellModel(offer: $0, selection: {[weak self] id, status in
                    self?.photoSelection?(id, status)
                }))
            } else {
                models2.append(UserApproveCellViewModel(offers: $0, selection: {[weak self] id in
                    self?.photoSelection?(id, .inbox)
                }))
            }
            
        })
        var allModels : [TableCellViewModel] = []
        allModels.append(contentsOf: models)
        allModels.append(contentsOf: models2)
        let section = DefaultTableSectionViewModel(cellModels: allModels)
        self.tableViewController.update(viewModels: [section])
        self.tableViewController.tableView.snp.removeConstraints()
        var tableHeight : CGFloat = 0
        for model in models {
            if model.offer.comment == nil || model.offer.comment == ""{
                tableHeight += 64
            } else {
                let commentheigh = model.offer.comment!.height(width: UIScreen.main.bounds.size.width - ViewSize.sideOffset - 16 - 12 - 12, font: CustomFont.bodyRegular15.font) + 16
                tableHeight += 64 + commentheigh + 8
            }
        }
        tableHeight += CGFloat(models2.count * 64)
        
        self.tableViewController.tableView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(tableHeight + 10)
            $0.bottom.equalToSuperview().offset(-10)
        }
        self.separator1.snp.makeConstraints{
            $0.top.equalTo(self.tableViewController.tableView.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
        }
        self.separator2.snp.makeConstraints{
            $0.top.equalTo(self.separator1.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(9)
        }
       // self.tableViewController.tableView.layoutSubviews()
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.tableViewController.tableView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.greaterThanOrEqualTo(50)
            $0.bottom.equalToSuperview()
        }
//        self.separator1.snp.makeConstraints{
//            $0.top.equalTo(self.tableViewController.tableView.snp.bottom)
//            $0.left.equalToSuperview()
//            $0.right.equalToSuperview()
//            $0.height.equalTo(1)
//        }
//        self.separator2.snp.makeConstraints{
//            $0.top.equalTo(self.separator1.snp.bottom)
//            $0.left.equalToSuperview()
//            $0.right.equalToSuperview()
//            $0.height.equalTo(9)
//        }
    }
    
}
struct UserApproveSectionTableViewModel : TableCellViewModel {
    var cellType: TableViewCell.Type {return UserApproveSectionTableView.self}
    var offers : [UserOffers]
    var selection : ((String, OffersStatus) -> Void)?
}

