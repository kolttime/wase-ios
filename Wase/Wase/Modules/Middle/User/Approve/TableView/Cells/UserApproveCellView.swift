//
//  UserApproveCellView.swift
//  Wase
//
//  Created by Роман Макеев on 19/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit



class UserApproveCellView : TableViewCell {
    
    
    
    var userID : String?
    var selection : ((String) -> Void)?
    private lazy var avatarImageVIew = UIImageView()
    private var nameLabel = UILabel()
    private var descriptionLabel = UILabel()
    private var separatorView = UIView()
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.avatarImageVIew)
        self.addSubview(self.nameLabel)
        self.addSubview(self.descriptionLabel)
        self.addSubview(self.separatorView)
        self.separatorView.backgroundColor = Color.light.value
        self.avatarImageVIew.layer.cornerRadius = 22
        self.avatarImageVIew.layer.masksToBounds = true
        self.avatarImageVIew.contentMode = .scaleAspectFill
        self.avatarImageVIew.backgroundColor = Color.greyL.value
        self.setupConstraints()
    }
    
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! UserApproveCellViewModel
        let offer = viewModel.offers
        self.userID = viewModel.offers.user.id
        self.selection = viewModel.selection
        self.nameLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "\(offer.user.givenName) \(offer.user.familyName)")
        var descString = ""
        if offer.status == .rejected {
            descString = "Отклонил заявку"
        } else {
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            //var fff = "2019-08-16T12:46:06"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            let date = dateFormatter.date(from: offer.createdAt)!
            let calendar = Calendar.current
            if calendar.component(.year, from: date) == calendar.component(.year, from: Date()) && calendar.component(.day, from: date) == calendar.component(.day, from: Date()) && calendar.component(.month, from: date) == calendar.component(.month, from: Date()) {
                let currentHour = calendar.component(.hour, from: Date())
                let acceptedHour = calendar.component(.hour, from: date)
                let duration = currentHour - acceptedHour
                if duration == 0 {
                    let currentHour = calendar.component(.minute, from: Date())
                    let acceptedHour = calendar.component(.minute, from: date)
                    let duration = currentHour - acceptedHour
                    if duration == 0 {
                        let currentHour = calendar.component(.second, from: Date())
                        let acceptedHour = calendar.component(.second, from: date)
                        let duration = currentHour - acceptedHour
                        
                        switch duration{
                        case 0:
                            descString = "Ожидается отклик, 0 секунд"
                        case 1:
                            descString = "Ожидается отклик, 1 секунда"
                        case 2:
                            descString = "Ожидается отклик, 2 секунды"
                        case 3:
                            descString = "Ожидается отклик, 3 секунды"
                        case 4:
                            descString = "Ожидается отклик, 4 секунды"
                        case 11:
                            descString = "Ожидается отклик, 11 секунд"
                        case 12:
                            descString = "Ожидается отклик, 12 секунд"
                        case 13 :
                            descString = "Ожидается отклик, 13 секунд"
                        case 14 :
                            descString = "Ожидается отклик, 14 секунд"
                        default:
                            descString = "Ожидается отклик, \(duration) секунд"
                        }
                        let dString = String(duration)
                        let last = Int(dString.suffix(1))
                        if duration > 15 {
                            switch last {
                            case 0:
                                descString = "Ожидается отклик, 0 минут"
                            case 1:
                                descString = "Ожидается отклик, 1 минута"
                            case 2:
                                descString = "Ожидается отклик, 2 минуты"
                            case 3:
                                descString = "Ожидается отклик, 3 минуты"
                            case 4:
                                descString = "Ожидается отклик, 4 минуты"
                            default:
                                descString = "Ожидается отклик, \(duration) минут"
                            }
                        }
                    } else {
                        switch duration{
                        case 0:
                            descString = "Ожидается отклик, 0 минут"
                        case 1:
                            descString = "Ожидается отклик, 1 минута"
                        case 2:
                            descString = "Ожидается отклик, 2 минуты"
                        case 3:
                            descString = "Ожидается отклик, 3 минуты"
                        case 4:
                            descString = "Ожидается отклик, 4 минуты"
                        case 11:
                            descString = "Ожидается отклик, 11 минут"
                        default:
                            descString = "Ожидается отклик, \(duration) минут"
                        }
                        let dString = String(duration)
                        let last = Int(dString.suffix(1))
                        if duration > 15 {
                            switch last {
                            case 0:
                                descString = "Ожидается отклик, 0 минут"
                            case 1:
                                descString = "Ожидается отклик, 1 минута"
                            case 2:
                                descString = "Ожидается отклик, 2 минуты"
                            case 3:
                                descString = "Ожидается отклик, 3 минуты"
                            case 4:
                                descString = "Ожидается отклик, 4 минуты"
                            default:
                                descString = "Ожидается отклик, \(duration) минут"
                            }
                        }
                    }
                } else {
                    switch duration{
                    
                    case 1:
                        descString = "Ожидается отклик, 1 час"
                    case 2:
                        descString = "Ожидается отклик, 2 часа"
                    case 3:
                        descString = "Ожидается отклик, 3 часа"
                    case 4:
                        descString = "Ожидается отклик, 4 часа"
                    default:
                        descString = "Ожидается отклик, \(duration) часов"
                    }
                }
            } else {
                if calendar.component(.year, from: date) == calendar.component(.year, from: Date())  && calendar.component(.month, from: date) == calendar.component(.month, from: Date()) {
                    let currentHour = calendar.component(.day, from: Date())
                    let acceptedHour = calendar.component(.day, from: date)
                    let duration = currentHour - acceptedHour
                    switch duration{
                    case 0:
                        descString = "Ожидается отклик, 0 дней"
                    case 1:
                        descString = "Ожидается отклик, 1 день"
                    case 2:
                        descString = "Ожидается отклик, 2 дня"
                    case 3:
                        descString = "Ожидается отклик, 3 дня"
                    case 4:
                        descString = "Ожидается отклик, 4 дня"
                    default:
                        descString = "Ожидается отклик, \(duration) дней"
                    }
                } else {
                    dateFormatter.locale =  Locale(identifier: "ru_MD")
                    dateFormatter.dateFormat = "dd MMMM"
                    descString = "Ожидается отклик, \(dateFormatter.string(from: date))"
                }
            }
        }
        
        self.descriptionLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).makeMutable(string: descString)
        self.avatarImageVIew.kf.setImage(with: URL(string: offer.user.avatar?.original.absoluteString ?? ""))
    }
    override func didSelect() {
        self.selection?(self.userID!)
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.avatarImageVIew.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(16)
            $0.height.equalTo(44)
            $0.width.equalTo(44)
        }
        self.nameLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(12)
            $0.left.equalTo(self.avatarImageVIew.snp.right).offset(8)
            
        }
        self.descriptionLabel.snp.makeConstraints{
            $0.top.equalTo(self.nameLabel.snp.bottom)
            $0.left.equalTo(self.nameLabel.snp.left)
        }
        self.separatorView.snp.makeConstraints{
            $0.top.equalTo(self.descriptionLabel.snp.bottom).offset(9)
            $0.left.equalTo(self.descriptionLabel.snp.left)
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
            $0.bottom.equalToSuperview()
        }
    }
    
}

struct  UserApproveCellViewModel : TableCellViewModel {
    var cellType: TableViewCell.Type {return UserApproveCellView.self}
    var offers : UserOffers
    var selection : ((String) -> Void)?
    
}
