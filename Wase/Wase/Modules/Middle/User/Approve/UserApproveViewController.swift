//
//  UserApproveViewController.swift
//  Wase
//
//  Created by Роман Макеев on 19/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class UserApproveViewController : SimpleNavigationBar {
    
    
    
    private lazy var tableViewController = ALTableViewController()
    
    private var loadingView = TaokaLoadingView()
    var offers : [UserOffers] = []
    private var apiService : ApiService
    private var userType : UserType
    private var id : String
    var onUser : ((String, String) -> Void)?
    init(apiService : ApiService, userType : UserType, id : String){
        self.apiService = apiService
        self.userType = userType
        self.id = id
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        if self.userType == .photographer {
            self.setTitle(title: "Фотографы")
        } else {
            self.setTitle(title: "Визажисты")
        }
        self.setLargeTitle()
        self.add(self.tableViewController)
        
        self.tableViewController.tableView.register(cellClass: UserApproveSectionTableView.self)
        self.toastPresenter.targetView = self.view
        self.loadingView.isHidden = true
        self.view.addSubview(self.loadingView)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.candidatesClick))
        let candlabel = UILabel()
        candlabel.isUserInteractionEnabled = true
        candlabel.addGestureRecognizer(tap)
        candlabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Подбор")
        self.setRightButton(view: candlabel)
        self.setUpConstraints()
        //self.render()
        self.fetchData()
        
    }
    var onCandidates : (() -> Void)?
    @objc func candidatesClick(){
        self.onCandidates?()
        print("Подбор")
    }
    func fetchData(){
        self.loadingView.isHidden = false
        self.apiService.getPhotosessionsOffers(type: self.userType, id: self.id) {
            [weak self] reponse in
            guard let self = self else {return}
            switch reponse {
            case .success(let offers):
                self.offers = offers
                self.loadingView.isHidden = true
                self.render()
                print("OFFERS : \(offers)")
            case .failure(let error):
                self.showToastErrorAlert(error)
                print(error)
            }
        }
    }
    var participantSelection : ((String, Int) -> Void)?
    var userSelection : ((String, Int) -> Void)?
    func update(offers : [UserOffers]){
        self.fetchData()
    }
    func render(){
       // let viewModels : UserApproveSectionTableViewModel = UserApproveSectionTableViewModel(offers: [UserOffers.init(user: UserShort(id: "fdsfdsfsdf", type: .photographer, nickname: "fdsfdsf", givenName: "Roman", familyName: "Makeev", avatar: nil, city: City.init(id: "dasd", name: "dsadas"), phone: nil, description: "fdsafas"), status: .confirmation, price: 3400, comment: nil, createdAt: "Сегодня", acceptedAt: "Завтра", viewed: true), UserOffers.init(user: UserShort(id: "fdsfdsfsdf", type: .photographer, nickname: "fdsfdsf", givenName: "Roman", familyName: "Makeev", avatar: nil, city: City.init(id: "dasd", name: "dsadas"), phone: nil, description: "fdsafas"), status: .confirmation, price: 3400, comment: nil, createdAt: "Сегодня", acceptedAt: "Завтра", viewed: true)])
        var offers1 : [UserOffers] = []
        var offers2 : [UserOffers] = []
        var offers3 : [UserOffers] = []
        var sections : [DefaultTableSectionViewModel] = []
        
        for model in self.offers {
            if model.status == .participant || model.status == .accepted {
                offers1.append(model)
            } else if model.status == .inbox {
                offers2.append(model)
            } else {
                offers3.append(model)
            }
        }
        if offers1.count != 0 {
            sections.append(DefaultTableSectionViewModel(cellModels: [UserApproveSectionTableViewModel(offers: offers1, selection: {
                [weak self]  userId, status in
                var stage = 2
                if status == .participant {
                    stage = 3
                }
                self?.participantSelection?(userId, stage)
            })]))
        }
        if offers2.count != 0 {
            sections.append(DefaultTableSectionViewModel(cellModels: [UserApproveSectionTableViewModel(offers: offers2, selection: {
                [weak self] userId, status in
                self?.userSelection?(userId, 1)
            })]))
        }
        if offers3.count != 0 {
            sections.append(DefaultTableSectionViewModel(cellModels: [UserApproveSectionTableViewModel(offers: offers, selection: {
                [weak self] userId, status in
                self?.userSelection?(userId, 4)
            })]))
        }
        
       // let section = DefaultTableSectionViewModel(cellModels: [viewModels])
        self.tableViewController.update(viewModels: sections)
    }
    
    override func setUpConstraints() {
        super.setUpConstraints()
        self.tableViewController.view.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
    
    
}
