//
//  AuthModuleBuilder.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class UserProfileModuleBuilder{
    
    public let apiService: ApiService
    private let sessionManager: TaokaUserSessionManager
    
    init(apiService: ApiService,
         sessionManager: TaokaUserSessionManager) {
        self.apiService = apiService
        self.sessionManager = sessionManager
    }
    
    func makePhoneNumberChangeModule() -> PhoneNumberViewController {
        return PhoneNumberViewController(apiService: self.apiService, agreed: true)
    }
    
    func makePhoneCodeChangeModule(phonenumber : String, key : String) -> PhoneCodeViewController {
        return PhoneCodeViewController(phoneNumber: phonenumber, apiService : self.apiService, key : key,  sessionManager: self.sessionManager, agreed: true)
    }
    
    func makeUserProfileModule() -> UserProfileViewController {
        return UserProfileViewController(apiService: self.apiService)
    }
    func makeUserProfileSettings(userModel : User) -> UserSettingsViewController {
        return UserSettingsViewController(userModel: userModel, apiService: self.apiService)
    }
    func makeUserAgreementSettings() -> photoAgreementViewController{
        return photoAgreementViewController(apiService: self.apiService)
    }
    
    func makeUserNotifSettings() -> NotificationSettingsViewController {
        return NotificationSettingsViewController()
    }
    
    func makeUserErrorsSettings() -> PhotoSendErrorViewController {
        return PhotoSendErrorViewController.init(apiService: self.apiService, type: .feedBack)
    }
    func makeUserAccountSettings(userModel : User) -> PhotographAccountSettingsViewController {
        return  PhotographAccountSettingsViewController(apiService: self.apiService, userModel: userModel)
    }
    
    func makeUserTermsOfUseSettings() -> PhotoTermsOfUseViewController{
        return PhotoTermsOfUseViewController(apiService: self.apiService)
    }
    func makePhotoAlbum(images : [Image]?, id : String?, scrollTo : Int?) -> PhotoAlbumViewController {
        return PhotoAlbumViewController( images : images,  id: id, sctollTo: scrollTo , apiService: self.apiService )
        
    }
    
}
