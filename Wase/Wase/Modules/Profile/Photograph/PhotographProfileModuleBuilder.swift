//
//  AuthModuleBuilder.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class PhotographProfileModuleBuilder{
    
    private let apiService: ApiService
    private let sessionManager: TaokaUserSessionManager
    
    init(apiService: ApiService,
         sessionManager: TaokaUserSessionManager) {
        self.apiService = apiService
        self.sessionManager = sessionManager
    }
    
    func makePhotographProfileModule() -> PhotographProfileViewController {
        return PhotographProfileViewController(apiService: self.apiService)
    }
    
    func makeAddAlbumModule() -> AddAlbumViewController{
        let interactor = AddPhotoInteractor(apiService: self.apiService)
        let vc = AddAlbumViewController(apiService: self.apiService)
        vc.interactor = interactor
        interactor.output = vc
        return vc
    }
    
    func makePhotoProfileSettings(userModel : User) -> PhotographSettingsViewController {
        return PhotographSettingsViewController(userModel: userModel, apiService: self.apiService)
    }
    
    func makePhotoAccountSettings(userModel : User) -> PhotographAccountSettingsViewController {
        return  PhotographAccountSettingsViewController(apiService: self.apiService, userModel: userModel)
    }
    func makePhotoServiceSettings(userModel : User) -> PhotoServiceSettingsViewController {
        return PhotoServiceSettingsViewController(apiService: self.apiService, userModel: userModel)
    }
    
    func makePhoneNumberChangeModule() -> PhoneNumberViewController {
        return PhoneNumberViewController(apiService: self.apiService, agreed: true)
    }
    
    func makePhoneCodeChangeModule(phonenumber : String, key : String) -> PhoneCodeViewController {
        return PhoneCodeViewController(phoneNumber: phonenumber, apiService : self.apiService, key : key,  sessionManager: self.sessionManager, agreed: true)
    }
    
    func makeNotifSettings() -> NotificationSettingsViewController {
        return NotificationSettingsViewController()
    }
    
    func makePhotoErrorsSettings() -> PhotoSendErrorViewController {
        return PhotoSendErrorViewController(apiService: self.apiService, type: .feedBack)
    }
    
    func makeUserEgreementSettings() -> photoAgreementViewController{
        return photoAgreementViewController(apiService: self.apiService)
    }
    func makeTermsOfUseSettings() -> PhotoTermsOfUseViewController{
        return PhotoTermsOfUseViewController(apiService: self.apiService)
    }
    
    func makePhotoFinanceSettings() -> PhotoFinanceSettingsViewController{
        return PhotoFinanceSettingsViewController(apiService: self.apiService)
    }
    func makePhotoAlbum(images : [Image]?, id : String?, scrollTo : Int?) -> PhotoAlbumViewController {
        return PhotoAlbumViewController( images : images,  id: id, sctollTo: scrollTo , apiService: self.apiService )
    }
    func makeCardAddingModel() -> AccountBindingViewController{
        return AccountBindingViewController(apiService: self.apiService)
    }
}
