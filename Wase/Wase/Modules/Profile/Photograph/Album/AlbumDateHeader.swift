//
//  AlbumDateHeader.swift
//  Wase
//
//  Created by Роман Макеев on 13/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit



class AlbumDateHeader : TableHeaderFooterView {
    let label = UILabel()
    
    
    override func initialSetup() {
        super.initialSetup()
        
        self.contentView.backgroundColor = Color.white.value
        self.contentView.addSubview(self.label)
        
        
        
        
        self.setupConstraints()
    }
    
    override func configure(viewModel: TableHeaderFooterView.ViewModelType) {
        let model = viewModel as! AlbumDateHeaderModel
        let dateFormatter = DateFormatter()
        dateFormatter.locale =  Locale(identifier: "ru_MD")
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let date = dateFormatter.date(from: model.mainLabel)!
        dateFormatter.dateFormat = "dd MMMM"
        let dateString = dateFormatter.string(from: date)
        self.label.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: dateString)
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 38 // ???
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.label.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
    }
    
}

struct AlbumDateHeaderModel : TableHeaderFooterViewModel {
    var viewType: TableHeaderFooterView.Type { return AlbumDateHeader.self }
    var mainLabel: String
}
