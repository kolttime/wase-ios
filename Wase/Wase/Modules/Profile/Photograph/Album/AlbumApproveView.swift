//
//  AlbumApproveView.swift
//  Wase
//
//  Created by Роман Макеев on 26/09/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class AlbumApproveView : AirdronView {
    
    
    let mainLabel = UILabel()
    let contentView = UIView()
    let leftButton = UIView()
    let rightButton = UIView()
    let leftlabel = UILabel()
    let rightLabel = UILabel()
    let mainView = UIView()
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.contentView)
        self.contentView.backgroundColor = Color.light.value
        self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "Могут-ли фотограф и визажист опубликовать эту фотосессию у себя в портфолио?")
        self.leftlabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.make(string: "Нет, нельзя")
        self.rightLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.make(string: "Да, конечно")
        self.leftButton.addSubview(self.leftlabel)
        self.rightButton.addSubview(self.rightLabel)
        self.contentView.addSubview(self.leftButton)
        self.contentView.addSubview(self.rightButton)
        self.mainView.backgroundColor = Color.white.value
        self.rightButton.backgroundColor = Color.white.value
        self.leftButton.backgroundColor = Color.white.value
        self.mainLabel.numberOfLines = 0
        self.mainView.addSubview(mainLabel)
        self.addSubview(self.mainView)
        let leftTap = UITapGestureRecognizer(target: self, action: #selector(self.rejectClick))
        let rightTap = UITapGestureRecognizer(target: self, action: #selector(self.acceptClick))
        self.leftButton.addGestureRecognizer(leftTap)
        self.rightButton.addGestureRecognizer(rightTap)
        self.setupConstraints()
    }
    var onAccept : Action?
    var onReject : Action?
    @objc func acceptClick(){
        self.onAccept?()
    }
    @objc func rejectClick(){
        self.onReject?()
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.contentView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.mainView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(98)
        }
        self.mainLabel.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.left.equalTo(16)
            $0.right.equalTo(-16)
        }
        self.leftButton.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.top.equalTo(self.mainView.snp.bottom).offset(1)
            $0.width.equalToSuperview().dividedBy(2).offset(-0.5)
            $0.bottom.equalToSuperview()
        }
        self.rightButton.snp.makeConstraints{
            $0.right.equalToSuperview()
            $0.top.equalTo(self.mainView.snp.bottom).offset(1)
            $0.bottom.equalToSuperview()
            $0.width.equalTo(self.leftButton)
        }
        self.leftlabel.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview()
        }
        self.rightLabel.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview()
        }
    }
    
}
