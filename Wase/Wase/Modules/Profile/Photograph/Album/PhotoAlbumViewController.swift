//
//  PhotoAlbumViewController.swift
//  Taoka
//
//  Created by Minic Relocusov on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class PhotoAlbumViewController : SimpleNavigationBar {
    

    lazy var tableViewController = TableViewController()

    let approveView = AlbumApproveView()
    var id : String?
    var scrollTo : Int?
    private var apiService : ApiService
    var images : [Image]?
    let loadingView = TaokaLoadingView()
    var userId : String?
    init(userId : String? = nil, images : [Image]?, id : String?, sctollTo : Int?, apiService : ApiService){
        self.id = id
        self.scrollTo = sctollTo
        self.images = images
        self.apiService = apiService
        self.userId = userId
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let shadowView = UIView()
    override func initialSetup() {
        super.initialSetup()
        //self.tabBarController?.tabBar.isHidden = true
        self.setTabBarHidden()
       // self.setLargeTitle()
        //self.setTitle(title: "Портфолио")
        self.add(self.tableViewController)
        //self.scrollingView = self.tableViewController.scrollView
        //self.scrollingView.scrollView = self.tableViewController.scrollView
        self.view.backgroundColor = Color.white.value
        //self.tableViewController.scrollView.delegate = self
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = true
        self.onLargetitle = {[weak self] in
          //  self?.setOffset()
        }
        self.onNormalTitle = {[weak self] in
         //   self?.setOffset()
        }
        self.tableViewController.tableView.register(cellClass: PhotoAlbumViewCell.self)
        self.tableViewController.tableView.register(headerFooterViewClass: AlbumDateHeader.self)
       // self.render()
        self.shadowView.layer.masksToBounds = true
        shadowView.backgroundColor = UIColor.clear
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 0)
        shadowView.layer.shadowOpacity = 0.1
        shadowView.layer.shadowRadius = 8.0
        self.view.addSubview(self.shadowView)
        self.approveView.frame = self.shadowView.bounds
        self.shadowView.addSubview(self.approveView)
        self.approveView.layer.cornerRadius = 10
        self.approveView.layer.masksToBounds = true
        self.setUpConstraints(approve: false)
        self.approveView.onAccept = {
            [weak self] in
            guard let self = self else {return}
            self.onAccept()
        }
        self.approveView.onReject = {[weak self] in
            guard let self = self else {return}
            self.onReject()
        }
        self.fetchData()
    }
   // override func scrollViewDidScroll(_ scrollView: UIScrollView) {

//    }
    var onReady : Action?
    func onAccept(){
        self.setUpConstraints(approve: false)
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        self.apiService.patchPhotosessionsAlbum(id: self.id!, visibility: true) { (response) in
            switch response {
            case .success(let result):
                print(result)
                self.onReady?()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    func onReject(){
        self.setUpConstraints(approve: false)
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        self.apiService.patchPhotosessionsAlbum(id: self.id!, visibility: false) { (response) in
            switch response {
            case .success(let result):
                self.onReady?()
                print(result)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    func fetchData(){
        if self.id != nil {
            self.loadingView.isHidden = false
            self.apiService.getAlbum(userId : self.userId, id: self.id!) {
                [weak self] response in
                switch response {
                case .success(let album):
                    self?.images = album.images
                    self?.setTitle(title: album.title)
                    //self?.setMultilineNavigationBar(topText: "Игорь", bottomText: "сука")
                    self?.setLargeTitle(subTitle: "Игорь")
                    self?.loadingView.isHidden = true
                    
                    //album.visibility
                    print(album.id)
                    if album.storage != nil {
                        let onStorage : Action? = {
                            [weak self] in
                            guard let self = self else {return}
                            if let name = URL(string: album.storage!), !name.absoluteString.isEmpty {
                                let objectsToShare = [name]
                                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                                
                                self.present(activityVC, animated: true, completion: nil)
                            }else  {
                                // show alert for not available
                            }
                        }
                        self?.setRighttitle(title: "Оригиналы", completion: onStorage)
                    }
                    if album.visibility != nil {
                        if album.visibility!.rawValue == "PENDING"  {
                            self?.setLargeTitleHidden()
                            self?.setUpConstraints(approve: true)
                        }
                    }
                        self?.render(date: album.date)
                    
                case .failure(let error):
                    print(error)
                
                }
            }
        } else {
            self.setTitle(title: "Портфолио")
            self.render()
        }
    }

    func render(date : String? = nil){
        var viewModels : [PhotoAlbumViewCellModel] = []
        for image in self.images! {
            let viewModel = PhotoAlbumViewCellModel.init(image: image)
            viewModels.append(viewModel)
        }
        var header : AlbumDateHeaderModel?
        if self.id != nil {
             header = AlbumDateHeaderModel(mainLabel: date!)
            
        }
        var section : DefaultTableSectionViewModel
        if header == nil {
             section = DefaultTableSectionViewModel(cellModels: viewModels)
        } else {
            section = DefaultTableSectionViewModel(cellModels: viewModels, headerViewModel: header!, footerViewModel: nil)
        }
        self.tableViewController.update(viewModels: [section])
        if self.scrollTo != nil {
            self.tableViewController.tableView.scrollToRow(at: IndexPath(row: self.scrollTo!, section: 0), at: .middle, animated: false)
        }
    }
    private var offsetView = UIView()
     func setUpConstraints(approve : Bool) {
        super.setUpConstraints()
        self.approveView.snp.removeConstraints()
        self.shadowView.snp.removeConstraints()
        self.tableViewController.view.snp.removeConstraints()
        self.tableViewController.tableView.snp.removeConstraints()
        
        if !approve {
           // self.tableViewController.tableView.snp.removeConstraints()
            self.approveView.snp.makeConstraints{
                $0.height.equalTo(0)
            }
            self.tableViewController.view.snp.makeConstraints{
                $0.top.equalToSuperview()
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
                $0.bottom.equalToSuperview()
            }
            self.tableViewController.tableView.snp.makeConstraints{
                $0.edges.equalTo(self.tableViewController.view)
            }
            self.loadingView.snp.makeConstraints{
                $0.edges.equalToSuperview()
            }
        } else {
            self.shadowView.snp.makeConstraints{
                $0.top.equalToSuperview().offset(self.getTopOffset() )
                $0.left.equalToSuperview().offset(ViewSize.sideOffset - 10)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset + 10)
                $0.height.equalTo(169)
            }
            self.approveView.snp.makeConstraints{
                $0.top.equalToSuperview().offset(10)
                $0.left.equalToSuperview().offset(10)
                $0.right.equalToSuperview().offset(-10)
                $0.bottom.equalToSuperview().offset(-10)
            }
            self.tableViewController.view.snp.makeConstraints{
                $0.top.equalTo(self.approveView.snp.bottom).offset(24)
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
                $0.bottom.equalToSuperview()
            }
            
            self.loadingView.snp.makeConstraints{
                $0.edges.equalToSuperview()
            }
        }
    }

}
