//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import Gallery

class PhotographProfileViewController: SimpleNavigationBar{
    
    
    
    var onImages : (([Image], Int) -> Void)?
    var onAlbum : ((String) -> Void)?
    var imagesModel : [Image]?
    var onAvatar : Action?
    var onPhotosession : Action?
    var onSelectCell : Action?
    var onSettings : ((User) -> Void)?
    var userModel : User?
    var albumsModel : [ShortAlbum]?
    var onAdd : Action?
    var userId : String?
    var phoneNumber : String?
    var onAddForUser : ((String) -> Void)?
    var tableSlideView = ReviewSlideView()
    private lazy var loadingView = TaokaLoadingView()
    private lazy var loadingViewTable = TaokaLoadingView()
    private lazy var tableViewController = ALTableViewController()
    private lazy var scrollViewController = ScrollViewController()
    private lazy var collectionViewController = CollectionWaterfallViewController(layout: self.collectionViewLayout)
    private var collectionView: UICollectionView { return self.collectionViewController.collectionView! }
    private lazy var collectionViewLayout: AirdronCollectionViewWaterfallLayout = {
        let layout = AirdronCollectionViewWaterfallLayout()
        layout.minimumColumnSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -2)
        return layout
    }()
    private var profileHeader : TakoaProfileView?
    private lazy var addWorks = AddWorksButton(frame: .zero)
    private lazy var pickerView = PagePickerView()
    var albumsFetched = false
    var imagesFetched = false
    var photoId : String?
    var status : OffersStatus?
    var apiService : ApiService
    var isReview : Bool
    var stage : Int?
    init(apiService : ApiService, userId : String? = nil, photoId : String? = nil, status : OffersStatus? = nil, phoneNumber : String? = nil, isReview : Bool = false, stage : Int? = nil){
        self.apiService = apiService
        self.userId = userId
        self.photoId = photoId
        self.status = status
        self.isReview = isReview
        self.phoneNumber = phoneNumber
        self.stage = stage
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var getSetting : Action?
    override func initialSetup() {
        super.initialSetup()
        
        // self.add(self.tableViewController)
        //self.add(self.scrollViewController)
        self.setLargeTitleHidden()
        self.getSetting = {[weak self] in
            guard let userModel = self?.userModel else {return}
            self?.onSettings?(userModel)
        }
        self.view.addSubview(self.scrollViewController.scrollView)
        
        // self.scrollViewController.scrollView.addSubview(self.tableViewController.view)
        self.tableViewController.tableView.register(cellClass: SubTableViewCell.self)
        self.collectionViewController.collectionView.register(cellClass: SubCollectionViewCell.self)
        //self.tableViewController.tableView.estimatedRowHeight = 400
        //self.tableViewController.tableView.rowHeight = UITableView.automaticDimension
        self.tableViewController.tableView.contentSize.height = 4000
        self.scrollViewController.scrollView.alwaysBounceHorizontal = false
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        //self.render()
        if self.userId == nil {
            self.setRighttitle(title: "Настройки", completion : getSetting)
        }
        self.setBackTitle(title: "Профиль")
        self.toastPresenter.targetView = self.view
        self.scrollViewController.scrollView.layer.layoutIfNeeded()
        self.tableViewController.tableView.layer.layoutIfNeeded()
        self.tableViewController.tableView.backgroundColor = .purple
        self.tableViewController.tableView.isScrollEnabled  = false
        self.collectionViewController.collectionView.isScrollEnabled = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onAddButton))
        self.addWorks.addGestureRecognizer(tap)
        self.addWorks.layer.cornerRadius = 10
        self.pickerView.layer.cornerRadius = 10
        self.addWorks.layer.masksToBounds = true
        self.pickerView.layer.masksToBounds = true
        self.pickerView.onLeftPick = {[weak self] in
            self?.render2()
        }
        self.pickerView.onRightPick = {[weak self] in
            self?.render()
        }
        self.profileHeader?.onAvarat = {[weak self] in
            self?.onAvatar?()
        }
        // self.pickerView.rightPick()
        //  self.render()
        self.view.addSubview(self.loadingViewTable)
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = true
        self.loadingViewTable.isHidden = true
        self.view.addSubview(self.tableSlideView)
        self.tableSlideView.setRecognizer(height: 385)
        self.tableSlideView.animate = {[weak self] in
            self?.view.animateLayout(duration: 0.5)
            
        }
        self.tableSlideView.selection = {[weak self] id in
            //self?.cancel1()
            print("Click!")
            guard let self = self else {return}
            if id == "иии" {
                self.onCreate?(self.userId!)
            }
        }
        // self.setUpConstraints()
        //self.setuphandlers()
        var cancel : Action? = {
            [weak self] in
            print("cancel")
            self?.tableSlideView.open?()
        }
        self.tableSlideView.snp.makeConstraints{
            self.tableSlideView.topConstr = $0.top.equalTo(self.view.snp.bottom).constraint
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(385)
        }
        var slideCancel : Action? = {[weak self] in
            self?.tableSlideView.close?()
            self?.addWorks.mainLabel.textColor = Color.black.value
            self?.addWorks.isUserInteractionEnabled = true
        }
        
        self.tableSlideView.setLeftTitle(title: "Отмена", completion: slideCancel)
        if !self.isReview {
                if self.phoneNumber == nil {
                    if self.userId != nil {
                        self.addWorks.backgroundColor = Color.accent.value
                        if self.stage == 0 {
                            self.addWorks.mainLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.white.value).make(string: "Отправить заявку")
                        } else if self.stage == 2 {
                            self.addWorks.mainLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.white.value).make(string: "Выбрать")
                        } else if self.stage == 3 {
                            self.addWorks.mainLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.white.value).make(string: "Выбран на фотосессию")
                            self.addWorks.alpha = 0.7
                            self.addWorks.isUserInteractionEnabled = false
                        } else if self.stage == 1{
                            self.addWorks.alpha = 0.7
                            self.addWorks.isUserInteractionEnabled = false
                            self.addWorks.mainLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.white.value).make(string: "Заявка отправлена")
                        }
                        
                    }
                } else {
                    self.addWorks.backgroundColor = Color.accent.value
                    self.addWorks.isUserInteractionEnabled = true
                    self.addWorks.mainLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.white.value).make(string: HelpFunctions.formattedNumber(number: self.phoneNumber!))
                }
        } else {
            self.addWorks.mainLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.make(string: "Предложить фотосессию")
            self.addWorks.backgroundColor = Color.light.value
            self.addWorks.alpha = 1
            self.addWorks.isUserInteractionEnabled = true
        }
        if self.userId != nil {
            self.setTabBarHidden()
        } else {
            self.setTabBar()
        }
        //self.setUpConstraints()
        //////////////////////////////////////// self.firstFetch()
        // self.render2()
        // self.tempFetch()
        self.firstFetch()
    }
    var onBackParty : (([UserOffers]) -> Void)?
    
    var clicked = false
    var onCreate : ((String) -> Void)?
    @objc func onAddButton() {
        
        if !self.isReview {
                    if self.phoneNumber == nil {
                        if self.userId == nil {
                            self.onAdd?()
                        } else {
                            if !self.clicked {
                                if self.stage == 0 {
                                    self.apiService.postOffers(photoId: self.photoId!, users: [self.userId!]) { [weak self] result in
                                        switch result {
                                        case .success(let offers):
                                            print(offers)
                                            guard let strongself = self else {return}
                                            self?.onBackParty?(offers)
                                            self?.addWorks.mainLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.white.value).make(string: "Заявка отправлена")
                                            print(result)
                                        case .failure(let error):
                                            print(error)
                                            self?.showToastErrorAlert(error)
                                        }
                                    }
                                    self.onAddForUser?(self.userId!)
                                    
                                } else if self.stage == 2 {
                                    print("Accept")
                                    self.apiService.postParticipants(photoId: self.photoId!, users: [self.userId!]) {[weak self] response in
                                        switch response {
                                        case .success(let offers):
                                            self?.onBackParty?(offers)
                                            self?.addWorks.mainLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.white.value).make(string: "Выбран на фотосессию")
                                            self?.addWorks.alpha = 0.7
                                            self?.addWorks.isUserInteractionEnabled = false
                                        case .failure(let error):
                                            self?.showToastErrorAlert(error)
                                            
                                        }
                                    }
                                    //self.onBackParty?(self.photoId!, .accepted)
                                    
                                }
                                
                                self.clicked = true
                                self.addWorks.alpha = 0.7
                            }
                            
                        }
                    } else {
                        guard let number = URL(string: "tel://" + self.phoneNumber!) else { return }
                        UIApplication.shared.open(number)
                    }
        } else {
            self.addWorks.mainLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Предложить фотосессию")
            self.addWorks.isUserInteractionEnabled = false
            self.apiService.getPhotoPhotosessions(tabIndex: .waiting, id : self.userId!) {
                [weak self] response in
                guard let self = self else {return}
                switch response {
                case .success(let result):
                    print("")
                    self.tableSlideView.render(photos: result)
                    self.tableSlideView.open?()
                case .failure(let error):
                    
                    print(error.localizedDescription)
                    self.showToastErrorAlert(error)
                }
            }
        }
    }
    func tempFetch(){
//        self.loadingView.isHidden = false
//        self.apiService.getProfile(){[weak self] response in
//            switch response {
//            case .success(let user):
//                self?.userModel = user
//                self?.loadingView.isHidden = true
//
//            case .failure(let error):
//                print(error)
//            }
//        }
    }
    
    func firstFetch(){
        var userFetched = false
        var albumsFetched = false
        self.loadingViewTable.isHidden = false
        self.loadingView.isHidden = false
        if UserClass.shared.user != nil && self.userId == nil && !self.isReview {
            self.userModel = UserClass.shared.user!
            //userFetched = true
            let user = self.userModel!
            let typeString : String
            if user.type == .photographer {
                typeString = "Фотограф"
            } else {
                typeString = "Визажист"
            }
        
            self.profileHeader = TakoaProfileView.init(givenName: user.givenName, familyName: user.familyName, city: "\(typeString), \(user.city.name)", descryption: user.description ?? "", imgUrl: user.avatar?.original.absoluteString ??  "")
            
            self.scrollViewController.scrollView.addSubview(self.profileHeader!)
            self.scrollViewController.scrollView.addSubview(self.addWorks)
            self.scrollViewController.scrollView.addSubview(self.pickerView)
            self.scrollViewController.scrollView.isScrollEnabled = true
            self.setUpConstraints()
            //self.loadingView.isHidden = true
            userFetched = true
            if albumsFetched {
                self.pickerView.rightPick()
                self.loadingView.isHidden = true
                userFetched = false
            }
        } else {
            self.apiService.getProfile(userId: self.userId) {[weak self] result in
                switch result {
                case .success(let user):
                    print(user)
                    
                    guard let self = self else {return}
                    if self.userId == nil && !self.isReview{
                        UserClass.shared.user = user
                    }
                    self.userModel = user
                    let typeString : String
                    if user.type == .photographer {
                        typeString = "Фотограф"
                    } else {
                        typeString = "Визажист"
                    }
                    if !(self.isReview ?? false) {
                        self.profileHeader = TakoaProfileView.init(givenName: user.givenName, familyName: user.familyName, city: "\(user.city.name)", descryption: user.description ?? "", imgUrl: user.avatar?.original.absoluteString ??  "")
                    } else {
                        var dString = user.description ?? ""
                        if user.type == .photographer {
                            dString += "\nот \(user.price!) руб./час"
                        } else {
                            dString += "\nот \(user.price!) руб."
                        }
                        self.profileHeader = TakoaProfileView.init(givenName: user.givenName, familyName: user.familyName, city: "\(user.city.name)", descryption: dString, imgUrl: user.avatar?.original.absoluteString ??  "")
                    }
                    self.scrollViewController.scrollView.addSubview(self.profileHeader!)
                    self.scrollViewController.scrollView.addSubview(self.addWorks)
                    self.scrollViewController.scrollView.addSubview(self.pickerView)
                    self.scrollViewController.scrollView.isScrollEnabled = true
                    self.setUpConstraints()
                    //self.loadingView.isHidden = true
                    userFetched = true
                    if albumsFetched {
                        self.pickerView.rightPick()
                        self.loadingView.isHidden = true
                        userFetched = false
                    }
                case .failure(let error):
                    print(error)
                    self?.showToastErrorAlert(error)
                    
                    
                }
                
            }
        }
        
        self.apiService.getAlbums(id : self.userId){[weak self] response in
            switch response {
            case .success(let albums):
                print("\n\nАльБОМы: \n\n\(albums)\n\n")
                self?.albumsModel = albums
                                albumsFetched = true
                                if userFetched {
                                    
                                    self?.pickerView.rightPick()
                                    self?.loadingView.isHidden = true
                                    albumsFetched = false
                                }
                
            case .failure(let error):
                print(error)
                
                
            }
        }
        self.scrollViewController.scrollView.layer.layoutIfNeeded()
        self.tableViewController.tableView.layer.layoutIfNeeded()
    }
    
    func set(userModel : User){
        self.userModel = userModel
        self.profileHeader?.set(givenName: userModel.givenName, familyName: userModel.familyName, city: userModel.city.name, descryption: userModel.description ?? "", imgUrl: userModel.avatar?.original.absoluteString ?? "")
    }
    func addImages(images : [Image]){
        var imageModel = images
        imageModel.append(contentsOf: self.imagesModel ?? [])
        self.imagesModel = imageModel
        if self.pickerView.rightPicked {
            self.render()
        } else {
            self.render2()
        }
    }
    func addAlbum(album : Album){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        dateFormatter.locale =  Locale(identifier: "ru_MD")
        var albumModel : [ShortAlbum] = []
        albumModel.append(ShortAlbum(id: album.id, title: album.title, specialization: album.specialization, date: album.date, images: [album.images[0]]))
        albumModel.append(contentsOf: self.albumsModel ?? [])
        
        
        albumModel.sort { (alb1, alb2) -> Bool in
            let date1 = dateFormatter.date(from: alb1.date)!
            let date2 = dateFormatter.date(from: alb2.date)!
            return date1 > date2
        }
        self.albumsModel = albumModel
        var imageModel = album.images
        imageModel.append(contentsOf: self.imagesModel ?? [])
        self.imagesModel = imageModel
        if self.pickerView.rightPicked {
            self.render()
        } else {
            self.render2()
        }
        
        
        
    }
    
    func render(){
        self.collectionViewController.collectionView.removeFromSuperview()
        self.collectionViewController.collectionView.snp.removeConstraints()
        self.tableViewController.tableView.removeFromSuperview()
        self.tableViewController.tableView.snp.removeConstraints()
        self.scrollViewController.scrollView.addSubview(self.tableViewController.tableView)
        
        var selectionHandler : ((String) -> Void)? = {[weak self] id in
            self?.onAlbum?(id)
        }
        var viewModels : [SubTableViewCellModel] = []
        
        for album in self.albumsModel! {
            let date = DateFormatter()
            date.dateFormat = "YYYY-MM-dd"
            date.locale =  Locale(identifier: "ru_MD")
            let dateD = date.date(from: album.date)
            date.dateFormat = "dd MMMM"
            let string = date.string(from: dateD!)
            let viewModel = SubTableViewCellModel.init(imgUrl: album.images[0].original.absoluteString, nameText: album.title, dateText: "\(album.specialization.name ?? ""), \(string)", id: album.id, selectionHandler: selectionHandler)
            viewModels.append(viewModel)
        }
        let section = DefaultTableSectionViewModel(cellModels: viewModels)
        self.tableViewController.update(viewModels: [section])
        self.setUpTableConstraints(count : viewModels.count)
        self.tableViewController.tableView.contentSize.height = 4000
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard let userModel = UserClass.shared.user else {return}
        if self.userId == nil {
            self.userModel = userModel
        }
    }
    func render2(){
        self.collectionViewController.collectionView.removeFromSuperview()
        self.collectionViewController.collectionView.snp.removeConstraints()
        self.tableViewController.tableView.removeFromSuperview()
        self.tableViewController.tableView.snp.removeConstraints()
        self.scrollViewController.scrollView.addSubview(self.collectionViewController.collectionView)
        // let viewModels = [SubCollectionViewCellModel(imgUrl: "https://bloody-disgusting.com/wp-content/uploads/2019/02/hotline-miami-header-e1551364402366.jpg", selectionHandler: self.onSelectCell), SubCollectionViewCellModel(imgUrl: "https://bloody-disgusting.com/wp-content/uploads/2015/04/HOT2_2.jpg", selectionHandler: self.onSelectCell), SubCollectionViewCellModel(imgUrl: "https://www.datocms-assets.com/9739/1552560427-hotlinemiami1.jpg"), SubCollectionViewCellModel(imgUrl: "https://img4.goodfon.ru/wallpaper/nbig/f/24/toni-hotline-miami-hotline-miami-tony-igra-art-risunok-tigr.jpg", selectionHandler: self.onSelectCell), SubCollectionViewCellModel(imgUrl: "https://static1.squarespace.com/static/58cafb2db8a79b4189b3c76c/t/5c7c3e2a9140b7a1d3c6719e/1553114719598/hotline-miami-game-retro-style-dark-life-cityscape-5k-su.jpg", selectionHandler: self.onSelectCell)]
        var viewModels2 : [SubCollectionViewCellModel] = []
        if self.imagesModel == nil {
            
            self.apiService.getImages(id : self.userId) {[weak self] response in
                switch response {
                case .success(let images):
                    
                    self?.imagesModel = images
                    var i = 0
                    let selection : ((Int) -> Void)? = {[weak self] index in
                        self?.onImages?(self?.imagesModel! ?? [], index)
                    }
                    for image in self?.imagesModel ?? [] {
                        let viewModel = SubCollectionViewCellModel(imgUrl: image.original.absoluteString, index: i, selectionHandler: selection)
                        viewModels2.append(viewModel)
                        i += 1
                    }
                    let section = DefaultCollectionSectionViewModel(cellModels: viewModels2)
                    self?.collectionViewController.update(viewModels: [section])
                    self?.setUpColelctionConstraints(count: viewModels2.count)
                case .failure(let error):
                    print(error)
                    self?.showToastErrorAlert(error)
                    return
                }
            }
        } else {
          //  self.collectionViewController.update
            var i = 0
            let selection : ((Int) -> Void)? = {[weak self] index in
                self?.onImages?(self?.imagesModel! ?? [], index)
            }
            for image in self.imagesModel ?? [] {
                let viewModel = SubCollectionViewCellModel(imgUrl: image.original.absoluteString, index: i, selectionHandler: selection)
                viewModels2.append(viewModel)
                i += 1
            }
            let section = DefaultCollectionSectionViewModel(cellModels: viewModels2)
            self.collectionViewController.update(viewModels: [section])
            self.setUpColelctionConstraints(count: self.imagesModel!.count)
        }
        
        
        
        // self.tableViewController.tableView.contentSize.height = 4000
    }
    func setUpTableConstraints(count : Int){
        self.tableViewController.tableView.snp.makeConstraints{
            $0.top.equalTo(self.addWorks.snp.bottom).offset(8)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.height.equalTo(CGFloat(count) * (getCardHeight() + 16))
        }
    }
    func setUpColelctionConstraints(count : Int){
        let height = (UIScreen.main.bounds.size.width - 2) / 3.0
        
        let counted : Int = self.roundNumber(num: Float(count) / 3.0)
        let finalHeight = ( Float(height) * Float(counted) ) + Float(counted - 1)
        print("Height: \(height)\ncounted: \(counted)\nfinalHeight: \(finalHeight)\nПромежуток: \((Float(height) / Float(count)) / 3.0)" )
        self.collectionViewController.collectionView.snp.makeConstraints{
            $0.top.equalTo(self.addWorks.snp.bottom).offset(16)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.height.equalTo(finalHeight)
        }
    }
    override func setUpConstraints() {
        super.setUpConstraints()
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.width.equalToSuperview()
        }
        self.profileHeader!.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        self.pickerView.snp.makeConstraints{
            $0.top.equalTo(self.profileHeader!.snp.bottom).offset(12)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalTo(76)
            $0.height.equalTo(44)
        }
        self.addWorks.snp.makeConstraints{
            $0.top.equalTo(self.profileHeader!.snp.bottom).offset(12)
            $0.right.equalTo(self.pickerView.snp.left).offset(-16)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.height.equalTo(44)
        }
        self.loadingView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.right.equalToSuperview()
        }
        //        self.loadingViewTable.snp.makeConstraints{
        //
        //        }
        
    }
    func roundNumber(num : Float) -> Int{
        let full : Int = Int(num)
        if Float(full) == num {
            return full
        } else {
            return full + 1
        }
    }
}

extension PhotographProfileViewController : GalleryControllerDelegate {
    func galleryController(_ controller: GalleryController, didSelectImages images: [Gallery.Image]) {
        controller.dismiss(animated: true, completion: nil)
        if let image = images.first {
            self.upload(image: image)
        }
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) { }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Gallery.Image]) { }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func upload(image: Gallery.Image) {
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let avatar):
                // self?.updateAvatar(image: avatar)
                print("HERE AVATAR : \n\n \(avatar) \n\n")
            case .failure(let error):
                //self?.showToastErrorAlert(error)
                print("ERROR \n\n \(error)")
            }
        }
    }
    
    //    func updateAvatar(image: Image) {
    //        self.apiService.updateAvatar(id: image.id) { [weak self] result in
    //            switch result {
    //            case .success(let model):
    //                self?.model = model
    //                self?.render(model: model)
    //            case .failure(let error):
    //                self?.showToastErrorAlert(error)
    //            }
    //        }
    //    }
}

