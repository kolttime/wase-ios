//
//  RightPicker.swift
//  Taoka
//
//  Created by Minic Relocusov on 11/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class RightPicker : AirdronView {
    
    
    public var picked : Bool = false
    private lazy var topView  = UIView()
    private lazy var bottomView = UIView()
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = Color.light.value
        self.addSubview(self.topView)
        self.addSubview(self.bottomView)
        self.topView.layer.cornerRadius = 1.5
        self.bottomView.layer.cornerRadius = 1.5
        self.topView.layer.masksToBounds = true
        self.bottomView.layer.masksToBounds = true
        self.topView.backgroundColor = Color.greyL.value
        self.bottomView.backgroundColor = Color.greyL.value
        self.setupConstraints()
        
    }
    
    func normalState(){
        self.topView.backgroundColor = Color.greyL.value
        self.bottomView.backgroundColor = Color.greyL.value
        self.picked = false
    }
    func pickedState() {
        self.topView.backgroundColor = Color.black.value
        self.bottomView.backgroundColor = Color.black.value
        self.picked = true
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.topView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.height.equalTo(7)
            $0.width.equalTo(16)
        }
        self.bottomView.snp.makeConstraints{
            $0.top.equalTo(self.topView.snp.bottom).offset(2)
            $0.left.equalToSuperview()
            $0.width.equalTo(16)
            $0.height.equalTo(7)
        }
    }
}
