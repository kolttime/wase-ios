//
//  AddButton.swift
//  Taoka
//
//  Created by Minic Relocusov on 11/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class AddWorksButton : UIButton {
    
    lazy var mainLabel = UILabel()
    
    override init(frame : CGRect){
        super.init(frame: frame)
        self.initialSetup()
    }
    
    func initialSetup(){
        self.backgroundColor = Color.light.value
        self.mainLabel.text = "Добавить работы"
        self.addSubview(self.mainLabel)
        
        self.setupConstraints()
    }
    
    func setupConstraints(){
        self.mainLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
