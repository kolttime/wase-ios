//
//  UserPhotosessionCell.swift
//  Taoka
//
//  Created by Minic Relocusov on 18/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class CardTableViewCell : TableViewCell {
    
    
    
    var selection : Action?
    var viewModel : CardTableViewCellModel?
    let baseView = UIView()
    private var expirationlabel = UILabel()
    private var numberLabel = UILabel()
    private var changeLabel = UILabel()
    let containerView = UIView()
    
    override func initialSetup() {
        super.initialSetup()
        
        self.backgroundColor = Color.white.value
        // self.contentView.addSubview(self.label)
        self.contentView.addSubview(self.baseView)
        // self.contentView.addSubview(self.mainImageView)
        
        baseView.backgroundColor = UIColor.clear
        baseView.layer.shadowColor = UIColor.black.cgColor
        baseView.layer.shadowOffset = CGSize(width: 0, height: 0)
        baseView.layer.shadowOpacity = 0.1
        baseView.layer.shadowRadius = 8.0
        self.containerView.frame = baseView.bounds
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.masksToBounds = true
        baseView.addSubview(self.containerView)
        //baseView.layer.shadowPath = UIBezierPath(roundedRect: baseView.bounds, cornerRadius: 10).cgPath
        baseView.layer.shouldRasterize = true
        baseView.layer.rasterizationScale = UIScreen.main.scale
        self.containerView.backgroundColor = Color.white.value
        
        self.contentView.backgroundColor = Color.white.value
        //self.containerView.dropShadow()
        self.containerView.addSubview(self.expirationlabel)
        self.containerView.addSubview(self.numberLabel)
        self.containerView.addSubview(self.changeLabel)
        self.changeLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Изменить")
        self.changeLabel.isUserInteractionEnabled = true
        self.setUpConstarints()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selecT))
        self.changeLabel.addGestureRecognizer(tap)
        // set the shadow properties
        
        
        // self.setupConstraints()
    }
    @objc func selecT(){
        self.selection?()
    }
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! CardTableViewCellModel
        self.viewModel = viewModel
        self.selection = viewModel.selection
        self.expirationlabel.attributedText = CustomFont.tech11.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Карта действует до \(viewModel.expiration)")
        self.numberLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "\(viewModel.number)")
        
        
        
    }
    func setUpConstarints(){
        self.baseView.snp.makeConstraints{
            $0.top.equalTo(self.contentView).offset(20)
            $0.left.equalTo(self.contentView).offset(ViewSize.sideOffset)
            $0.right.equalTo(self.contentView).offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.contentView).offset(-24)
            // $0.height.equalTo(169)
        }
        self.containerView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.expirationlabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(12)
        }
        self.numberLabel.snp.makeConstraints{
            $0.top.equalTo(self.expirationlabel.snp.bottom).offset(4)
            $0.left.equalToSuperview().offset(12)
        }
        self.changeLabel.snp.makeConstraints{
            $0.centerY.equalTo(self.numberLabel)
            $0.right.equalToSuperview().offset(-12)
        }
        
    }
    
    
    
    
    
    
    
    
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        return 104
        
    }
    
    
    
    
}
struct CardTableViewCellModel : TableCellViewModel {
    var cellType: TableViewCell.Type {return CardTableViewCell.self}
    var number : String
    var expiration : String
    var selection : Action?

}

