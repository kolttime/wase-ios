import Foundation
import UIKit


class CardHeaderTableViewCell : TableHeaderFooterView {
    
    
    
    private var viewModel : CardHeaderTableViewCellModel?
    private lazy var titleLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(titleLabel)
        self.setupConstraints()
    }
    
    
    
    
    override func configure(viewModel: TableHeaderFooterView.ViewModelType) {
        
        let viewModel = viewModel as! CardHeaderTableViewCellModel
        
        self.viewModel = viewModel
        self.titleLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.black.value).make(string: viewModel.mainLabel)
        self.titleLabel.numberOfLines = 0
        
    }
    
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 20
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.titleLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalToSuperview()
            //$0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
    }
}


struct CardHeaderTableViewCellModel : TableHeaderFooterViewModel {
    var viewType: TableHeaderFooterView.Type { return CardHeaderTableViewCell.self }
    var mainLabel: String
}
