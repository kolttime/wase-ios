//
//  SearchableCountryTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 13/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class SelectionCell: TableViewCell {
    var selection : ((String) -> Void)?
    var selectModel: SelectionCellModel?
    var id : String?
    var imageq = UIImageView()
    private var checkImageView = UIImageView()
    private let separator = UIView()
    private lazy var mainLabel = UILabel()
    private lazy var countLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.imageq)
        self.contentView.addSubview(self.mainLabel)
        self.contentView.addSubview(self.countLabel)
        self.contentView.addSubview(self.checkImageView)
        self.contentView.addSubview(self.separator)
        self.checkImageView.contentMode = .scaleToFill
        self.checkImageView.image = UIImage.init(named: "Ok")
        self.checkImageView.isHidden = true
        self.separator.backgroundColor = Color.light.value
        self.imageq.backgroundColor = Color.light.value
        self.imageq.contentMode = .scaleAspectFill
        self.imageq.layer.cornerRadius = 22
        self.imageq.layer.masksToBounds = true
        self.separator.isHidden = true
        self.setupConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.imageq.snp.makeConstraints{
            $0.left.equalToSuperview().offset(16)
            $0.top.equalToSuperview().offset(10)
            $0.height.equalTo(44)
            $0.width.equalTo(44)
        }
        self.mainLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(13)
            $0.left.equalTo(imageq.snp.right).offset(8)
        }
        
        self.countLabel.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(0)
            $0.left.equalTo(imageq.snp.right).offset(8)
        }
        self.checkImageView.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-14)
            $0.top.equalToSuperview().offset(20)
            $0.height.equalTo(ViewSize.sideOffset)
            $0.width.equalTo(ViewSize.sideOffset)
        }
        self.separator.snp.makeConstraints{
            $0.left.equalTo(self.countLabel)
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
            $0.bottom.equalToSuperview()
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! SelectionCellModel
        self.selectModel = viewModel
        self.imageq.kf.setImage(with: (viewModel.image?.original) ?? URL(string: ""))
        self.id = viewModel.id
        if viewModel.offer {
            self.checkImageView.isHidden = false
        } else {
            self.checkImageView.isHidden = true
        }
        if viewModel.hasSeparator {
            self.separator.isHidden = false
        } else {
            self.separator.isHidden = true
        }
        self.selection = viewModel.selectionHandler
        self.mainLabel.text = viewModel.mainLabel
        self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: self.mainLabel.text ?? "")
        self.countLabel.text = viewModel.countLabel
        self.countLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: self.countLabel.text ?? "")
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 64
    }
    
    override func didSelect() {
        self.selection?(self.id!)
    }
}

struct SelectionCellModel : TableCellViewModel {
    var cellType: TableViewCell.Type { return SelectionCell.self }
    var mainLabel: String
    var countLabel : String
    var id : String
    var image : Image?
    var offer : Bool
    var selectionHandler: ((String) -> Void)?
    var hasSeparator : Bool = false
}




