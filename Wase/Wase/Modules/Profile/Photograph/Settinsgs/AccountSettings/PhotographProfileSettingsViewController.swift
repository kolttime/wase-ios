//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit
import Gallery
import Kingfisher

class PhotographAccountSettingsViewController : SimpleNavigationBar, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    var oChangeNumber : (() -> Void)?
    var changeNumberLabel = UILabel()
    var labelCity = UILabel()
    var slideView = SlideView()
    var testUser : User
    var account : (() -> Void)?
    
    var onLogout: Action?
    var image = UIImageView()
    var scrollViewController = ScrollViewController()
    var actInd = UIActivityIndicatorView()
    var isChanged = false
    let loadingView = TaokaLoadingView()
    var onBack : ((User) -> Void)?
    var onAvatar : Action?
    var customPicker = UIPickerView()
    var cityNames = [String]()
    var cityModels = [City]()
    var selectedRow = 0
    var cityChanged = false
    var deleteUser = false
    private lazy var givenNameTextField = OMGTextField.makeGivenName()
    private lazy var familyNameTextField = OMGTextField.makeFamilyName()
    private lazy var nicknameTextField = OMGTextField.makeNickName()
    private lazy var phoneTextFiled = OMGTextField.makeTelephoneTF()
    private lazy var cityTextField  = TimeViewClass.makeCityField()
    private lazy var descrTextFiled = OMGTextField.makeDescrTF()
    let keyObserver = KeyboardObserver()
    private var activeField = UIView() {
        didSet {
            
            self.setScrollOffset(offset: 0)
            
        }
    }
    var descrLabel = UILabel()
    var userModel : User
    public var apiService : ApiService
    init(apiService : ApiService,userModel: User){
        self.apiService = apiService
        self.userModel = userModel
        self.testUser = userModel
        super.init()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateUser(){
        self.userModel = UserClass.shared.user!
    }
    
    func onChange(){
        if self.givenNameTextField.text != userModel.givenName{
            self.isChanged = true
            self.userModel.givenName = self.givenNameTextField.text ?? ""
        }
        if self.familyNameTextField.text != userModel.familyName{
            self.isChanged = true
            self.userModel.familyName = self.familyNameTextField.text ?? ""
        }
        if self.nicknameTextField.text != userModel.nickname{
            self.isChanged = true
            self.userModel.nickname = self.nicknameTextField.text ?? ""
        }
        if self.phoneTextFiled.text != userModel.phone{
            self.isChanged = true
            self.userModel.phone = self.phoneTextFiled.text
        }
        if self.userModel.type != .client {
            if self.descrTextFiled.text != userModel.description{
                self.isChanged = true
                self.userModel.description = self.descrTextFiled.text
            }
        }
        
        
        if cityChanged != false {
            if self.userModel.city.name != self.cityTextField.descText {
                print("Город - \(self.cityModels[self.selectedRow].name)")
                self.isChanged = true
                self.userModel.city = self.cityModels[self.selectedRow]
            }
        }
        //        self.apiService.getCheckNickname(nickname: nicknameTextField.text!) {[weak self] response in
        //            switch response {
        //            case .success(let available):
        //                print(available.available)
        //            case .failure(let error):
        //                print("HERE ERROR \(error)")
        //            }
        //        }
        
    }
    var keyHeight = CGFloat.zero
    
    
    func makeSpace(){
        self.phoneTextFiled.text! += " "
    }
    var onViyti : Action?
    override func initialSetup() {
        
        self.phoneTextFiled.onTextDidChanged = { [weak self] response in
            print(response.text.count)
        }
        
        let actionOnReady  : Action? = {[weak self] in
            guard let self = self else {return}
            self.cityChanged = true
            print(self.cityModels[self.selectedRow])
            self.cityTextField.Set(text: self.cityModels[self.selectedRow].name)
            self.slideView.close?()
        }
        
        let actionOnCancel  : Action? = {[weak self] in
            guard let self = self else {return}
            if self.cityChanged == false {
                self.cityChanged = false
            } else {
                self.cityChanged = true
            }
            self.slideView.close?()
            
        }
        
        super.initialSetup()
        self.view.backgroundColor = Color.white.value
        self.setTabBarHidden()
        self.toastPresenter.targetView = self.view
        self.givenNameTextField.text = userModel.givenName
        self.familyNameTextField.text = userModel.familyName
        self.nicknameTextField.text = userModel.nickname
        self.phoneTextFiled.text = userModel.phone
        self.cityTextField.Set(text: userModel.city.name)
        if self.cityTextField.descText != ""{
            self.cityTextField.textLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Город")
        } else {
            self.cityTextField.textDecsr.text = "Город"
        }
        
        self.descrTextFiled.text = userModel.description
        self.onViyti = {[weak self] in
            self?.logoutted = true
            self?.onLogout?()
        }
        self.descrLabel.attributedText = CustomFont.tech11.attributesWithParagraph.colored(color: Color.black.value).make(string: "Используйте буквы и знаки препинания, до 80 символов")
        
        self.descrLabel.textColor = .gray
        self.descrLabel.numberOfLines = 0
        
        self.setRighttitle(title: "Выйти", completion: self.onViyti)
        self.image.layer.cornerRadius = 48
        self.image.backgroundColor = Color.light.value
        image.layer.masksToBounds = true
        if self.userModel.avatar != nil {
            self.image.kf.setImage(with: URL(string: self.userModel.avatar!.original.absoluteString))
        } else {
            self.image.image = UIImage(named: "BringPhoto")
        }
        
        self.setLargeTitle()
        self.setTitle(title: "Аккаунт")
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        
        
        
        self.slideView.addSubview(self.labelCity)
        self.labelCity.attributedText = CustomFont.bodyBold20.attributesWithParagraph.colored(color: Color.black.value).make(string: "Выберите город")
        
        self.slideView.setLeftTitle(title: "Отмена", completion: actionOnCancel)
        self.slideView.setRightTitle(title: "Готово", completion: actionOnReady)
        
        
        let slideTap = UITapGestureRecognizer(target: self, action: #selector(self.openCityViewSlide))
        self.cityTextField.addGestureRecognizer(slideTap)
        self.slideView.setRecognizer(height: 304)
        self.slideView.animate = {[weak self] in
            self?.view.animateLayout(duration: 0.5)
        }
        self.slideView.addSubview(customPicker)
        self.slideView.defaultCenter = self.view.frame.maxY + (230)
        
        self.customPicker.delegate = self
        self.customPicker.dataSource = self
        
        
        self.view.addSubview(self.scrollViewController.scrollView)
        self.scrollViewController.scrollView.addSubview(self.image)
        self.scrollViewController.scrollView.addSubview(self.givenNameTextField)
        self.scrollViewController.scrollView.addSubview(self.familyNameTextField)
        self.scrollViewController.scrollView.addSubview(self.nicknameTextField)
        self.scrollViewController.scrollView.addSubview(self.phoneTextFiled)
        self.scrollViewController.scrollView.addSubview(self.cityTextField)
        self.scrollViewController.scrollView.addSubview(self.descrTextFiled)
        self.scrollViewController.scrollView.addSubview(self.descrLabel)
        self.scrollViewController.scrollView.addSubview(self.changeNumberLabel)
        self.scrollViewController.scrollView.addSubview(self.slideView)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.avatarTap))
        
        
        self.image.isUserInteractionEnabled = true
        self.image.addGestureRecognizer(tap)
        self.image.contentMode = .scaleAspectFill
        //self.view.addSubview(self.loadingView)
        if self.userModel.type == .client {
            self.descrTextFiled.isHidden = true
            self.descrLabel.isHidden = true
        }
        
        
        self.phoneTextFiled.separatorView.isHidden = true
        self.phoneTextFiled.isUserInteractionEnabled = false
        self.changeNumberLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Изменить")
        let changeNumberTap = UITapGestureRecognizer(target: self, action: #selector(self.changeNumber))
        self.changeNumberLabel.isUserInteractionEnabled = true
        self.changeNumberLabel.addGestureRecognizer(changeNumberTap)
        
        
        self.fetchData()
        self.setUpConstraints()
        self.setupHandlers()
    }
    
    @objc func openCityViewSlide(){
        self.customPicker.reloadAllComponents()
        self.view.endEditing(true)
        self.slideView.open?()
    }
    
    @objc func avatarTap(){
        self.onAvatar?()
    }
    
    @objc func changeNumber(){
        print("Change")
        self.oChangeNumber?()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        print(phoneTextFiled.text)
    }
    
    override func setUpConstraints(){
        super.setUpConstraints()
        
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        self.image.snp.makeConstraints{
            $0.top.equalToSuperview().offset(20)
            $0.centerX.equalToSuperview()
            $0.width.equalTo(96)
            $0.height.equalTo(96)
        }
        
        self.givenNameTextField.snp.makeConstraints{
            $0.top.equalTo(self.image.snp.bottom).offset(ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        self.familyNameTextField.snp.makeConstraints{
            $0.top.equalTo(self.givenNameTextField.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        self.nicknameTextField.snp.makeConstraints{
            $0.top.equalTo(self.familyNameTextField.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        
        self.changeNumberLabel.snp.makeConstraints{
            $0.bottom.equalTo(self.phoneTextFiled.snp.bottom).offset(-8)
            $0.right.equalTo(self.phoneTextFiled.snp.right)
        }
        
        self.phoneTextFiled.snp.makeConstraints{
            $0.top.equalTo(self.nicknameTextField.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        self.cityTextField.snp.makeConstraints{
            $0.top.equalTo(phoneTextFiled.snp.bottom).offset(8)
            $0.height.equalTo(60)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        self.descrTextFiled.snp.makeConstraints{
            $0.top.equalTo(self.cityTextField.snp.bottom).offset(14)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        self.descrLabel.snp.makeConstraints{
            $0.top.equalTo(self.descrTextFiled.snp.bottom).offset(6)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalToSuperview().offset(-20)
        }
        
        self.slideView.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            self.slideView.topConstr = $0.top.equalTo(self.view.snp.bottom).constraint
            $0.height.equalTo(457)
        }
        
        self.labelCity.snp.makeConstraints{
            $0.top.equalTo(slideView.snp.top).offset(54)
            $0.left.equalTo(slideView.snp.left).offset(ViewSize.sideOffset)
        }
        
        self.customPicker.snp.makeConstraints{
            $0.left.equalTo(self.slideView.snp.left).offset(30)
            $0.top.equalTo(self.labelCity.snp.top).offset(20)
            $0.right.equalTo(self.slideView.snp.right).offset(-30)
        }
        
    }
    var logoutted = false
    override func viewWillDisappear(_ animated: Bool) {
        if logoutted {return}
        print(self.userModel.description)
        onChange()
        if HelpFunctions.userModelChanged(first: self.userModel, second: self.testUser) {
            self.apiService.patchProfile(userModel: userModel) {  [weak self] response in
                switch response {
                case .success(let user):
                    print("Заебись")
                    
                case .failure(let error):
                    print(error)
                    self?.showToastErrorAlert(error)
                }
            }
        }
        
        onBack?(userModel)
        print(userModel.type.rawValue)
    }
    
    func fetchData(){
        self.apiService.getCities(){[weak self] response in
            switch response {
            case .success(let cities):
                print(cities)
                guard let self = self else {return}
                self.cityModels = cities
                for i in self.cityModels{
                    self.cityNames.append(i.name)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedRow = row
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cityNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return cityNames[row]
    }
    
}


extension PhotographAccountSettingsViewController{
    
    
    
    
    func setScrollOffset(offset : CGFloat = 0){
        let keyMin = self.view.frame.height - self.keyHeight
        let textFrame  = self.scrollViewController.scrollView.convert(self.activeField.frame, to: self.view)
        
        print("KeyMin: \(keyMin)\ntextFrame: \(textFrame.maxY)")
        
        if (textFrame.maxY + offset ) > keyMin {
            
            let scrollOffset = (textFrame.maxY + offset) - keyMin
            self.scrollViewController.scrollView.setContentOffset(CGPoint(x: self.scrollViewController.scrollView.contentOffset.x, y: self.scrollViewController.scrollView.contentOffset.y + scrollOffset), animated: true)
        } else {
            //  self.scrollViewController.scrollView.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    
    
    
    func setupHandlers(){
        
        self.keyObserver.onKeyboardWillShow = {[weak self] frame, smth in
            guard let self = self else {return}
            self.keyHeight = frame.height
        }
        
        self.givenNameTextField.onBeginEditing = {[weak self] textfield in
            self?.slideView.close?()
            print("givenName")
            self?.activeField = textfield
            guard let self = self else {return}
            //HelpFunctions.setScrollOffset(scrollView: self.scrollViewController.scrollView, view: self.view, activeView: self.activeField, keyHeight: self.keyHeight, offset: 0)
            // self.setScrollOffset()
            
        }
        self.familyNameTextField.onBeginEditing = {[weak self] field in
            guard let self = self else {return}
            self.slideView.close?()
            self.activeField = field
            // HelpFunctions.setScrollOffset(scrollView: self.scrollViewController.scrollView, view: self.view, activeView: self.activeField, keyHeight: self.keyHeight, offset: 0)
            //self.setScrollOffset()
        }
        self.nicknameTextField.onBeginEditing = {[weak self] field in
            guard let self = self else {return}
            
            self.slideView.close?()
            HelpFunctions.setScrollOffset(scrollView: self.scrollViewController.scrollView, view: self.view, activeView: self.activeField, keyHeight: self.keyHeight, offset: 0)
            
            // HelpFunctions.setScrollOffset(scrollView: self.scrollViewController.scrollView, view: self.view, activeView: self.activeField, keyHeight: self.keyHeight, offset: 0)
            // self.setScrollOffset()
            
        }
        self.phoneTextFiled.onBeginEditing = {[weak self] field in
            guard let self = self else {return}
            self.slideView.close?()
            self.activeField = field
            // HelpFunctions.setScrollOffset(scrollView: self.scrollViewController.scrollView, view: self.view, activeView: self.activeField, keyHeight: self.keyHeight, offset: 0,navBar: self.navigationController!.navigationBar)
            //  self.setScrollOffset()
        }
        
        
        
        self.descrTextFiled.onBeginEditing = {[weak self] field in
            guard let self = self else {return}
            self.slideView.close?()
            self.activeField = self.descrLabel
            // HelpFunctions.setScrollOffset(scrollView: self.scrollViewController.scrollView, view: self.view, activeView: self.activeField, keyHeight: self.keyHeight, offset: 0)
            // self.setScrollOffset()
        }
        self.givenNameTextField.onReturnHandler = {[weak self] in
            self?.familyNameTextField.becomeResponder()
        }
        self.familyNameTextField.onReturnHandler = {[weak self] in
            self?.nicknameTextField.becomeResponder()
            self?.cityTextField.onEndEdditing?()
        }
        self.nicknameTextField.onReturnHandler = {[weak self] in
            self?.phoneTextFiled.becomeResponder()
        }
        
        self.descrTextFiled.onTextDidChanged = {[weak self] text in
            self?.userModel.description = text.text
        }
        self.descrTextFiled.onReturnHandler = {[weak self] in
            self?.view.endEditing(true)
            print("done")
        }
        
    }
}


extension PhotographAccountSettingsViewController: GalleryControllerDelegate {
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Gallery.Image]) {
        controller.dismiss(animated: true, completion: nil)
        if let image = images.first {
            self.upload(image: image)
        }
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) { }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Gallery.Image]) { }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func upload(image: Gallery.Image) {
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let avatar):
                // self?.updateAvatar(image: avatar)
                self?.userModel.avatar = avatar
                //self?.actInd.isHidden = false
                
                //self?.actInd.isHidden = false
                self?.image.kf.indicatorType = .activity
                self?.image.kf.setImage(with: URL(string: avatar.original.absoluteString)!)
                
                
                
            case .failure(let error):
                // self?.showToastErrorAlert(error)
                print(error)
            }
        }
    }
    
    
}

