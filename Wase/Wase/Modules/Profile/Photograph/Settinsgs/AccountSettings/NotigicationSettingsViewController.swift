//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class NotificationSettingsViewController : SimpleNavigationBar{
    
    var scrollViewController = ScrollViewController()
    
    
    private var lookForOrders : ServiceCheckView
    private var toOtherCity : ServiceCheckView
    private lazy var timeTextField = OMGTextField.timeField()
    
    override init(){
        self.lookForOrders = ServiceCheckView(textLabel: "Фотосессии", textDescr: "Изменение статуса")
        self.toOtherCity = ServiceCheckView(textLabel: "Новые отклики", textDescr: "Вас смогут находить люди из любого города")
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.view.backgroundColor = Color.white.value
        
        self.setLargeTitle()
        self.setTitle(title: "Уведомления")
        
        
        self.setTabBarHidden()
        
        scrollViewController.scrollView.addSubview(lookForOrders)
        scrollViewController.scrollView.addSubview(toOtherCity)
        scrollViewController.scrollView.addSubview(timeTextField)
        
        self.timeTextField.isHidden = true
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        self.view.addSubview(self.scrollViewController.scrollView)
        
        self.setUpConstraints()
    }

    
    override func setUpConstraints(){
        super.setUpConstraints()
        
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        self.lookForOrders.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        }
        
        self.toOtherCity.snp.makeConstraints{
            $0.top.equalTo(lookForOrders.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-20)
        }
        
        self.timeTextField.snp.makeConstraints{
            $0.top.equalTo(toOtherCity.snp.bottom).offset(32)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        
    }
    
}




