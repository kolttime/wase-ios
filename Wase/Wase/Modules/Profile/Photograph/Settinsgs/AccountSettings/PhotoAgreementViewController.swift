//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class photoAgreementViewController : SimpleNavigationBar {
    private let loadingView = TaokaLoadingView()
    var onNext : (() -> Void)?
    var crutchTF = OMGTextField.makeDate()
    var apiLabel = UILabel()
    var apiService : ApiService
    
    let mainLabel = UILabel()
    
    init(apiService : ApiService){
        self.apiService = apiService
        
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLayoutSubviews() {
        // self.scrollViewController.scrollView.setContentOffset(CGPoint.zero, animated: false)
    }
    var scrollViewController = ScrollViewController()
    
    func fetchData(){
        self.apiService.licenseAgreement(){[weak self] response in
            switch response {
            case .success(let rr):
                print(rr)
                self?.apiLabel.text = rr.text
                self?.loadingView.isHidden = true
            case .failure(let error):
                print(error)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        self.setLargeTitleHidden()
        
        self.mainLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.colored(color: Color.black.value).make(string: "Лицензионное соглашение")
        
        self.mainLabel.numberOfLines = 0
        
        
        self.apiLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "")
        
        self.apiLabel.numberOfLines = 0
        
        self.crutchTF.isHidden = true
        
        self.setTabBarHidden()
        
        self.view.backgroundColor = Color.white.value
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        self.view.addSubview(self.scrollViewController.scrollView)
        self.scrollViewController.scrollView.addSubview(mainLabel)
        self.scrollViewController.scrollView.addSubview(crutchTF)
        self.scrollViewController.scrollView.addSubview(apiLabel)
        self.scrollViewController.scrollView.addSubview(loadingView)
        self.view.addSubview(self.loadingView)
        
        self.loadingView.alpha = 1
        
        
        self.setUpConstraints()
        
        self.fetchData()
    }
    
    @objc func nextView(){
        print("~~~~~")
        self.onNext?()
    }
    
    override func setUpConstraints(){
        
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        self.mainLabel.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
        self.crutchTF.snp.makeConstraints{
            $0.top.equalTo(self.mainLabel.snp.bottom).offset(ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.apiLabel.snp.makeConstraints{
            $0.top.equalTo(self.mainLabel.snp.bottom).offset(ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-16)
            $0.bottom.equalToSuperview().offset(-20)
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
}








