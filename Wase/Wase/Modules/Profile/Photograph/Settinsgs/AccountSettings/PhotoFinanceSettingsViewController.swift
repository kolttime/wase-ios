//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit
import WebKit


class PhotoFinanceSettingsViewController : SimpleNavigationBar {
    
    var descrLabel = UILabel()
    var addCard = SampleButton()
    private lazy var loadingView = TaokaLoadingView()
    private lazy var tableViewController = TableViewController()
    var goToACardView : (() -> Void)?
    private let webView = WKWebView()
    private var link : String = ""
    private let defaultView = UIView()
    private var apiService : ApiService
    init(apiService : ApiService){
        self.apiService = apiService
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        self.addCard.mainLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.black.value).make(string: "Привязать счет")
        self.addCard.backgroundColor = Color.light.value
        self.addCard.layer.cornerRadius = 10
        self.addCard.layer.masksToBounds = true
        super.initialSetup()
        self.toastPresenter.targetView = self.view
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = false
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.tableViewController.tableView.register(cellClass: CardTableViewCell.self)
        self.tableViewController.tableView.register(headerFooterViewClass: CardHeaderTableViewCell.self)
        self.tableViewController.tableView.register(cellClass: SelectionCell.self)
        self.descrLabel.attributedText =  CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: "Чтобы получать оплату за выполненный заказ, необходимо указать данные вашего счета в банке, привязанного к карте")
        
        self.descrLabel.numberOfLines = 0
        self.add(self.tableViewController)
        
        self.setTabBarHidden()
        
        
        self.setLargeTitle()
        
        self.setTitle(title: "Финансы")
        for navItem in(self.navigationController?.navigationBar.subviews)! {
            for itemSubView in navItem.subviews {
                if let largeLabel = itemSubView as? UILabel {
                    largeLabel.text = self.title
                    largeLabel.numberOfLines = 0
                    largeLabel.lineBreakMode = .byWordWrapping
                }
            }
        }
        
        self.defaultView.addSubview(descrLabel)
        self.defaultView.addSubview(addCard)
        self.view.addSubview(self.defaultView)
        self.defaultView.isHidden = true
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.goCard))
        addCard.addGestureRecognizer(tap1)
        
        self.setUpConstraints()
        self.fetchData()
        
        
    }
    func fetchData(){
        self.apiService.getProfileCard() {[weak self] response in
            guard let self = self else {return}
            
            switch response {
            case .success(let result):
                print(result)
                self.loadingView.isHidden = true
                if result.card == nil {
                    self.defaultView.isHidden = false
                    self.tableViewController.tableView.isHidden = true
                } else {
                    self.defaultView.isHidden = true
                    self.tableViewController.tableView.isHidden = false
                    self.render(finanses: result)
                }
            case .failure(let error):
                self.showToastErrorAlert(error)
            }
        }
    }
    func render(finanses : Finances){
        guard let card = finanses.card else {return}
        let selection : Action? = {[weak self] in
            guard let self = self else {return}
            self.goCard()
        }
        let cardModel = CardTableViewCellModel.init(number: card.number, expiration: card.expiration, selection: selection)
        var label = (finanses.transactions == nil || finanses.transactions?.count == 0 ) ? "У вас пока нет транзакций" : "Транзакции"
        let headerModel = CardHeaderTableViewCellModel.init(mainLabel: label)
        var selectionModels : [SelectionCellModel] = []
        if let transactions = finanses.transactions {
            for trans in transactions {
                var count = ""
                let dateFormatter = DateFormatter()
                dateFormatter.locale =  Locale(identifier: "ru_MD")
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let date = dateFormatter.date(from: trans.datetime)!
                dateFormatter.dateFormat = "dd MMMM"
                count = dateFormatter.string(from: date)
                let model = SelectionCellModel.init(mainLabel: "\(trans.from.givenName) \(trans.from.familyName)", countLabel: "\(count), \(Int(trans.sent)) ₽", id: "", image: trans.from.avatar, offer: false, selectionHandler: nil, hasSeparator: false)
                selectionModels.append(model)
            }
        }
        let section1 = DefaultTableSectionViewModel(cellModels: [cardModel])
        let section2 = DefaultTableSectionViewModel(cellModels: selectionModels, headerViewModel: headerModel, footerViewModel: nil)
        self.tableViewController.update(viewModels: [section1, section2])
    }
    @objc func goCard(){
        print("caaard")
        //self.goToACardView?()
        self.openWebView()
    }
    
    func openWebView(){
        self.loadingView.isHidden = false
        self.apiService.postProfileCard() {
            [weak self] response in
            guard let self = self else {return}
            switch response {
            case .success(let result):
                print(result)
                self.link = result.link
                let testUrl = URLRequest.init(url: URL.init(string: result.link)!)
                self.webView.load(testUrl)
                self.webView.isHidden = true
                self.webView.navigationDelegate = self
                self.view.addSubview(self.webView)
                self.webView.snp.makeConstraints {
                    $0.top.equalToSuperview().offset(self.getTopOffset())
                    $0.bottom.equalToSuperview()
                    $0.left.equalToSuperview()
                    $0.right.equalToSuperview()
                }
            case .failure(let error):
                self.showToastErrorAlert(error)
            }
            
        }
    }
    
    override func setUpConstraints(){
        super.setUpConstraints()
        self.tableViewController.tableView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.defaultView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        descrLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalToSuperview().offset(332)
        }
     
        self.addCard.snp.makeConstraints{
            $0.top.equalTo(self.descrLabel.snp.bottom).offset(12)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
        }
        
    }
}

extension PhotoFinanceSettingsViewController : WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print(navigationAction.request.url)
        if (navigationAction.request.url?.absoluteString == self.link) {
            self.loadingView.isHidden = true
            self.webView.isHidden = false
        }
        if navigationAction.request.url?.absoluteString.hasPrefix("https://dev.wase.photo/api/v1/payments/success_url") ?? false{
           // self.onReady?()
            self.webView.removeFromSuperview()
            self.webView.snp.removeConstraints()
            self.loadingView.isHidden = false
            self.fetchData()
            
        } else if navigationAction.request.url?.absoluteString.hasPrefix(Endpoints.host) ?? false{
            print("Fail")
            self.webView.removeFromSuperview()
            self.webView.snp.removeConstraints()
            self.showToastErrorAlert(AirdronError.init(code: 500, reason: ""))
            self.loadingView.isHidden = false
        }
        decisionHandler(.allow)
        
    }
}

