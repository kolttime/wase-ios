
//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class AccountBindingViewController : SimpleNavigationBar {
    
    
    private let saveButton = UIView()
    private let saveLabel = UILabel()
    private let rosBankLabel = UILabel()
    private var bikBankField = OMGTextField.makeBikBankTextFiel()
    private var corrBillTextField = OMGTextField.makeCorrBillTextField()
    private var innBankTextField = OMGTextField.makeINNBankTextField()
    private var fioTextField = OMGTextField.makeFIOTextField()
    private let numberBillField = OMGTextField.makeNumberBillTextField()
    
    private let descLabel = UILabel()
   // private var helperView = UIView()
    private lazy var headerLabel = UILabel()
    private var apiService : ApiService
    private lazy var scrollViewController  = ScrollViewController()
    init(apiService : ApiService){
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func initialSetup() {
        super.initialSetup()
        self.view.backgroundColor = Color.white.value
        self.setTabBarHidden()
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.view.addSubview(self.scrollViewController.scrollView)
        // self.add(self.scrollViewController)
        self.setLargeTitleHidden()
        self.setTitle(title: "")
        //self.view.addSubview(self.headerLabel)
        self.scrollViewController.scrollView.addSubview(self.headerLabel)
        //self.scrollViewController.scrollView.addSubview(self.helperView)
        //self.helperView.alpha = 0
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.headerLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.make(string: "Привязка счета для выплат")
        self.headerLabel.numberOfLines = 0
        self.scrollViewController.scrollView.addSubview(self.descLabel)
        self.descLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Чтобы получать оплату за выполненный заказ, необходимо указать данные вашего счета в банке привязанного к карте. Реквезиты банка можно узнать в поддержке или в приложении банка.")
        self.descLabel.numberOfLines = 0
        self.scrollViewController.scrollView.addSubview(self.bikBankField)
        self.scrollViewController.scrollView.addSubview(self.corrBillTextField)
        self.scrollViewController.scrollView.addSubview(self.innBankTextField)
        self.scrollViewController.scrollView.addSubview(self.fioTextField)
        self.scrollViewController.scrollView.addSubview(self.numberBillField)
        self.scrollViewController.scrollView.addSubview(self.rosBankLabel)
        
        self.rosBankLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.white.value).make(string: "Ф РОКЕТБАНК КИВИ БАНК (АО) ")
        
        self.scrollViewController.scrollView.addSubview(self.saveButton)
        self.saveButton.addSubview(self.saveLabel)
        self.saveButton.backgroundColor = Color.light.value
        //self.saveButton.alpha = 0.7
        self.saveLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: "Сохранить")
        //self.saveButton.isUserInteractionEnabled = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.buttonClicked))
        self.saveButton.addGestureRecognizer(tap)
        self.saveButton.layer.cornerRadius = 10
        self.setButtonState(active: false)
        self.saveButton.layer.masksToBounds = true
        self.scrollViewController.scrollView.alwaysBounceHorizontal = false
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.setUpConstraints()
    }
    
    func setButtonState(active : Bool){
        if !active {
            self.saveButton.alpha = 0.7
            self.saveButton.isUserInteractionEnabled = false
        } else {
            self.saveButton.alpha = 1
            self.saveButton.isUserInteractionEnabled = true
        }
    }
    @objc func buttonClicked(){
        print("Save")
    }
    override func setUpConstraints(){
        super.setUpConstraints()
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.width.equalToSuperview()
        }
        self.headerLabel.snp.makeConstraints{
            //$0.top.equalToSuperview().offset(self.getTopOffset())
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            
        }
//        self.headerLabel.snp.makeConstraints{
//            //$0.top.equalToSuperview().offset(self.getTopOffset())
//            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
//            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
//        }
        
        self.descLabel.snp.makeConstraints{
            $0.top.equalTo(self.headerLabel.snp.bottom).offset(12)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.bikBankField.snp.makeConstraints{
            $0.top.equalTo(self.descLabel.snp.bottom).offset(23)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.rosBankLabel.snp.makeConstraints{
            $0.top.equalTo(self.bikBankField.snp.bottom).offset(6)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.corrBillTextField.snp.makeConstraints{
            $0.top.equalTo(self.rosBankLabel.snp.bottom).offset(7)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.innBankTextField.snp.makeConstraints{
            $0.top.equalTo(self.corrBillTextField.snp.bottom).offset(7)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.fioTextField.snp.makeConstraints{
            $0.top.equalTo(self.innBankTextField.snp.bottom).offset(6)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.numberBillField.snp.makeConstraints{
            $0.top.equalTo(self.fioTextField.snp.bottom).offset(7)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        self.saveButton.snp.makeConstraints{
            $0.top.equalTo(self.numberBillField.snp.bottom).offset(32)
            // $0.centerY.equalToSuperview().offset(200)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
            $0.bottom.equalToSuperview().offset(-58)
        }
        self.saveLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
    }
}


