//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotoSendErrorViewController : SimpleNavigationBar, UITextViewDelegate {
    
    private var textView = UITextView()
    var onNext : (() -> Void)?
    var crutchTF = OMGTextField.makeDate()
    let mainLabel = UILabel()
    var type : SendErrorType
    var apiService : ApiService
    var emptyString : String
    var rating : Int?
    var photoId : String?
    var loadingView = TaokaLoadingView()
    init(apiService : ApiService, type : SendErrorType, rating : Int? = nil, photoId : String? = nil){
        self.apiService = apiService
        self.rating = rating
        self.photoId = photoId
        self.type = type
        switch type {
        case .photoError:
            emptyString = "Опишите проблему"
        case .feedBack:
            emptyString = "О проблеме или ошибках"
        case .clientError:
            emptyString = "Опишите проблему"
        case .clientConfirmation:
            emptyString = "Подробности"
        }
        super.init()
    }
    
    var scrollViewController = ScrollViewController()
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var onReady : Action?
    
    func fetchData(){
        self.loadingView.isHidden = false
        self.setRighttitle(title: "", completion: nil)
        switch self.type {
            
        case .feedBack:
            self.apiService.postFeedback(feedback: self.textView.text){[weak self] response in
                switch response {
                case .success(let result):
                    self?.crutchTF.resignResponder()
                    self?.loadingView.isHidden = true
                    self?.onReady?()
                    print(result)
                case .failure(let error):
                    print(error)
                    self?.setRighttitle(title: "Отправить", completion: self?.fetchData)
                }
            }
        case .photoError:
            self.apiService.deletePhotosessionsOffers(photoId: self.photoId!, comment: self.textView.text) {
                [weak self] response in
                guard let self = self else {return}
                switch response {
                case .success(let result):
                    self.crutchTF.resignResponder()
                    self.onReady?()
                    self.loadingView.isHidden = true
                case .failure(let error):
                    self.showToastErrorAlert(error)
                    self.setRighttitle(title: "Отправить", completion: self.fetchData)
                }
            }
            
        case .clientError:
            print("Clint Error")
            self.apiService.putPhotoPhotosession(id: self.photoId!, comment: self.textView.text) {[weak self] response in
                switch response {
                case .success( _):
                    self?.crutchTF.resignResponder()
                    self?.onReady?()
                    self?.loadingView.isHidden = true
                case .failure(let error):
                    self?.showToastErrorAlert(error)
                    self?.setRighttitle(title: "Отправить", completion: self?.fetchData)
                }
            }
        case .clientConfirmation:
            self.apiService.postConfirmation(photoId: self.photoId!, rating: self.rating!, comment: self.textView.text, resultDate: nil) {[weak self] response in
                switch response {
                case .success( _):
                    self?.onReady?()
                    self?.loadingView.isHidden = true
                case .failure(let error):
                    self?.showToastErrorAlert(error)
                    print(error.localizedDescription)
                    self?.loadingView.isHidden = true
                    self?.setRighttitle(title: "Отправить", completion: self?.fetchData)
                }
            }
        
        
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.backgroundColor = Color.white.value
        self.setLargeTitleHidden()
        //        self.setTitle(title: "Расскажите, пожалуйста")
        self.onLayout = {[weak self] in
            guard let self = self else {return}
            self.scrollViewController.scrollView.setContentOffset(CGPoint(x: 0, y: -self.scrollViewController.scrollView.contentOffset.y), animated: false)
            self.scrollViewController.scrollView.layoutSubviews()
            self.view.layoutSubviews()
        }
        
        self.mainLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.colored(color: Color.black.value).make(string: "Расскажите, пожалуйста")
        //self.setBackTitle(title: "Назад")
        
        mainLabel.numberOfLines = 0
        self.toastPresenter.targetView = self.view
        self.setRighttitle(title: "Отправить", completion: fetchData)
        self.crutchTF.isHidden = true
        self.view.backgroundColor = Color.white.value
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        self.view.addSubview(self.scrollViewController.scrollView)
        self.scrollViewController.scrollView.addSubview(mainLabel)
        self.scrollViewController.scrollView.addSubview(crutchTF)
        self.scrollViewController.scrollView.addSubview(textView)
        
        self.adjustUITextViewHeight(arg: self.textView)
        
        self.textView.isScrollEnabled = false
        
        textView.text = self.emptyString
        textView.textColor = UIColor.lightGray
        textView.delegate = self
        textView.font = CustomFont.bodyRegular17.font
        
        
        self.setTabBarHidden()
        self.view.addSubview(self.loadingView)
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.loadingView.isHidden = true
        self.setUpConstraints()
        
        
    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = self.emptyString
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    @objc func nextView(){
        print("~~~~~")
        self.onNext?()
    }
    
    override func setUpConstraints(){
        
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        self.mainLabel.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
        self.crutchTF.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        self.textView.snp.makeConstraints{
            $0.top.equalTo(self.mainLabel.snp.bottom).offset(ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
    }
}

public enum SendErrorType : String {
    case feedBack = "FEEDBACK"
    case photoError = "PHOTOERROR"
    case clientError = "CLIENTERROR"
    case clientConfirmation = "CLIENTCONFIRMATION"
}




