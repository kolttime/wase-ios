//
//  InstagramLoginController.swift
//  OMG
//
//  Created by Roman Makeev on 15/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class InstagramLoginController: SimpleNavigationBar {
    
    var onCompletion: Action?
    var onActivation: Action?
    var onSuccess: Action?
    
    private var loadingView = TaokaLoadingView()
    
    lazy var webView = WKWebView()
    
    private let clientId: String = InstagramConstants.clientId
    private let redurectUrl: String
    private let apiService: ApiService
    private let sessionManager: TaokaUserSessionManager
    
    init(apiService: ApiService, sessionManager: TaokaUserSessionManager) {
        self.apiService = apiService
        self.sessionManager = sessionManager
        self.redurectUrl = Endpoints.instagramLogin.url
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.setLargeTitleHidden()
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = false
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=code",
            arguments: [Endpoints.instagramAuth.url,
                        self.clientId,
                        self.redurectUrl])
        let urlRequest = URLRequest.init(url: URL.init(string: authURL)!,cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData,
                                         timeoutInterval: 10.0)
        let testUrl = URLRequest.init(url: URL.init(string: "https://instagram.com/accounts/logout/")!)
        webView.load(testUrl)
        webView.navigationDelegate = self
        self.webView.isHidden = true
        self.view.addSubview(self.webView)
        self.webView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(self.getTopOffset())
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        }
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
}

extension InstagramLoginController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Here load")
        self.loadingView.isHidden = true
        self.webView.isHidden = false
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        print("URLINTAGRAM : \(navigationAction.request.url?.absoluteString)")
        
        if navigationAction.request.url?.absoluteString == "https://www.instagram.com/" {
            let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=code",
                                 arguments: [Endpoints.instagramAuth.url,
                                             self.clientId,
                                             self.redurectUrl])
            let urlRequest = URLRequest.init(url: URL.init(string: authURL)!)
            webView.load(urlRequest)
        }
        if navigationAction.request.url?.absoluteString.hasPrefix(Endpoints.instagramLogin.url + "?code") == true {
            decisionHandler(.cancel)
            self.apiService.instagramAuth(url: navigationAction.request.url!) { [weak self] result in
                switch result {
                case .success(let response):
//                    self?.dismiss(animated: true, completion: {
//                        print("ЕБОЙ")
//                    })
                    print("response: \(response.success), \(response.user), \(response.token)")
                    if let token = response.token {
                        self?.sessionManager.save(token: token)
                        if let user = response.user{
                            self?.sessionManager.save(user: user)
                            self?.sessionManager.setFirstLaunch()
                            self?.onCompletion?()
                        } else {
                            self?.onActivation?()
                        }
                    }
                case .failure(let error):
                   // self?.showToastErrorAlert(error)
                    print(error)
                }
            }
        } else {
            decisionHandler(.allow)
        }
    }
}
