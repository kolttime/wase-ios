//
//  AppDelegate.swift
//  Taoka
//
//  Created by Roman Makeev on 07/07/2019.
//  Copyright © 2019 Roman Makeev. All rights reserved.
//

import UIKit
import OneSignal
import YandexMobileMetrica
import YandexMobileMetricaCrashes

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    lazy var dependencyContainer: DependencyContainer = {
        return DependencyContainer()
    }()
    
    private lazy var mainCoordinator: MainCoordinator = {
        return MainCoordinator(presenter: self.window!, sessionManager: self.dependencyContainer.serviceContainer.sessionManager,
                               coordinatorFactory: self.dependencyContainer.coordinatorFactory)
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()
        self.mainCoordinator.start()
        UIApplication.shared.statusBarStyle = .default
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        let configuration = YMMYandexMetricaConfiguration.init(apiKey: "f3f02bb8-3208-4c09-a8a9-c4abe5680f49")
        YMMYandexMetrica.activate(with: configuration!)
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "4c7cb04d-249b-45f4-ab0f-fc30ef3d5caa",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
//        OneSignal.promptForPushNotifications(userResponse: { accepted in
//            print("User accepted notifications: \(accepted)")
//        })
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    
}

