//
//  AuthCoordinator.swift
//  OMG
//
//  Created by Roman Makeev on 11/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import Gallery

protocol PhotographMiddleCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startPhotographMiddleFlow(presenter: AirdronNavigationController)
}

extension PhotographMiddleCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startPhotographMiddleFlow(presenter: AirdronNavigationController) {
        let coordinator = self.coordinatorFactory.makePhotographMiddleCoordinator(presenter: presenter,
                                                                            coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.start()
    }
}

class PhotographMiddleCoordinator: BaseCoordinator {
    private let galleryImageSelector = GalleryMultiselectImageSelector()
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: PhotographMiddleModuleBuilder
    private let navigationPresenter = AirdronNavigationController()
    var onCompletion: Action?
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: PhotographMiddleModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        self.showPhotographMiddleModule()
    }
    func showImagePicker(count : Int, selectedImagesHandler: @escaping (([Gallery.Image]) -> Void)) {
        let galleryController = GalleryController()
        Config.tabsToShow = [.imageTab]
        Config.Camera.imageLimit = count
        galleryController.delegate = self.galleryImageSelector
        self.galleryImageSelector.onSelectImages = { images in
            selectedImagesHandler(images)
        }
        self.presenter.present(galleryController, animated: true) { [unowned galleryController] in
            // Appsee.markView(asSensitive: galleryController.view)
        }
    }
    
    
    func showImagePicker(viewController: PhotographAccountSettingsViewController) {
        let galleryController = GalleryController()
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.Camera.imageLimit = 1
        galleryController.delegate = viewController
        self.presenter.present(galleryController, animated: true) { [unowned galleryController] in
            
        }
    }
    func showPhotographMiddleModule() {
        let viewController = self.moduleBuilder.makePhotographMiddleModule()
        
        
        viewController.onFill = {[weak self] in
            self?.showServicesModule(vc : viewController)
        }
        
        viewController.onNext = {[weak self] photo in
            self?.showPhotosessionOfferModule(photosession : photo, vc : viewController)
        }
        viewController.onConfirmation = {[weak self] id in
            guard let self = self else {return}
            self.showConfirmationModule(id: id, vc: viewController)
        }
        viewController.onResult = {[weak self] id in
            guard let self = self else {return}
            self.showPhotoResultsModule(photoId: id, vc: viewController)
        }
        viewController.onError = {[weak self] id in
            guard let self = self else {return}
            self.showErrorModule(photoId: id, mainVc: viewController)
        }
        self.presenter.setViewControllers([viewController], animated: false)
        
        
    }
    func showErrorModule(photoId : String, mainVc : PhotographMiddleViewController){
        let viewController = self.moduleBuilder.makeErrorViewController(photoId: photoId)
        viewController.onReady = {
            [weak self] in
            guard let self = self else {return}
            mainVc.forceUpdate()
            self.presenter.popToViewController(mainVc, animated: true)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showPhotoResultsModule(photoId : String, vc : PhotographMiddleViewController){
        let viewController = self.moduleBuilder.makePhotographResultsModule(id: photoId)
        viewController.onAddImages = { [weak self, weak viewController] count in
            self?.showImagePicker (count : count){ images in
                viewController?.add(images: images)
            }
//            viewController?.onBackWithImages = {[weak self] images in
//                vc.addImages(images: images)
//                self?.presenter.popViewController(animated: true)
//            }
            viewController?.onBackWithAlbum = {[weak self] album in
                //vc.addAlbum(album: album)
                vc.forceUpdate()
                //self?.presenter.popViewController(animated: true)
                self?.presenter.popToViewController(vc, animated: true)
            }
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showCancelModule(id : String, vc : PhotographMiddleViewController) {
       //  let viewController = self.moduleBuilder.makeCancelTextModule(id: id, rating: 0, vc: vc)
        let viewController = self.moduleBuilder.makeErrorViewController(photoId: id)
        viewController.onReady = {[weak self]  in
           // vc.updateInbox(id: id)
            vc.forceUpdate()
            self?.presenter.popToViewController(vc, animated: true)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showConfirmationModule(id : String, vc : PhotographMiddleViewController){
        let viewController = self.moduleBuilder.makeConfirmationModule(id: id)
        viewController.onConfirm = {[weak self] photo in
            guard let self = self else {return}
            print(photo)
            vc.forceUpdate()
            self.presenter.popViewController(animated: true)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showPhoneNumberModule(vc : PhotosessionOfferView){
        let viewController = self.moduleBuilder.makePhoneNumberViewController()
        
        
        
        
        viewController.onTextField = {[weak self] number, key in
            self?.startPhoneCodeModule(phoneNumber: number, key: key, vc: vc)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func startPhoneCodeModule(phoneNumber : String, key : String, vc : PhotosessionOfferView){
        let viewController = self.moduleBuilder.makePhoneCodeModule(phonenumber: phoneNumber, key: key)
        
        
        viewController.onAgreed = {[weak self] userModel in
            guard let self = self else {return}
            
            UserClass.shared.user = userModel
            
            vc.updateUser()
            
            
            self.presenter.popToViewController(vc, animated: true)
            //self.presenter.popViewController(animated: true)
            //self.presenter.popViewController(animated: false)
            
            
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showServicesModule(vc : PhotographMiddleViewController){
        let viewController = self.moduleBuilder.makeServicesModule()
        viewController.onBack = {[weak self] userModel , bb in
            vc.userModel = userModel
            UserClass.shared.user = userModel
            self?.presenter.popViewController(animated: true)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showPhotosessionOfferModule(photosession : Photosession, vc : PhotographMiddleViewController){
        let viewController = self.moduleBuilder.makePhotosessionOfferModule(photo: photosession)
        viewController.onPatch = {[weak self] photo in
            print("Photosesion : \n\n \(photo) \n\n")
            vc.patchPhotosession(photo: photo.toShort())
        }
        viewController.backCancel = {[weak self] in
            self?.showCancelModule(id: photosession.id, vc: vc)
        }
        viewController.onCancel1 = {[weak self] id in
            vc.updateInbox(id: id)
            self?.presenter.popViewController(animated: true)
        }
        viewController.onPhonenNumber = {[weak self] in
            guard let self = self else {return}
            self.showPhoneNumberModule(vc: viewController)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
}



