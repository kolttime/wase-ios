//
//  AuthCoordinator.swift
//  OMG
//
//  Created by Roman Makeev on 11/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit


protocol UserMiddleCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startUserMiddleFlow(presenter: AirdronNavigationController, id : String?)
}

extension UserMiddleCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startUserMiddleFlow(presenter: AirdronNavigationController, id : String?) {
        let coordinator = self.coordinatorFactory.makeUserMiddleCoordinator(presenter: presenter,
                                                                         coordinatorFactory: self.coordinatorFactory,
                                                                         userId: id)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.start()
    }
}

class UserMiddleCoordinator: BaseCoordinator {
    
    
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: UserMiddleModuleBuilder
    private let navigationPresenter = AirdronNavigationController()
    var onCompletion: Action?
    var userId : String?
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: UserMiddleModuleBuilder,
         coordinatorFactory: CoordinatorFactory,
         userId : String?) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
        self.userId = userId
    }
    

    
    override func start() {
        self.showUserMiddleModule(userId: userId)
        
    }
    func showCommentConfirmationModule(id : String, rating : Int, vc : UserMiddleViewController, type : TellPleaseType){
        let viewController = self.moduleBuilder.makeConfirmationCommentModule(id: id, rating: rating, type : type)
        viewController.onReady = {[weak self] in
            vc.forceupdate()
            self?.presenter.popViewController(animated: true)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showConfirmationModule(id : String, vc : UserMiddleViewController){
        let viewController = self.moduleBuilder.makeConfirmationmodule()
        viewController.onTap = {[weak self] rating in
            self?.showCommentConfirmationModule(id: id, rating: rating, vc: vc, type: .confirmation)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showAlbumModule(id : String, vc : UserMiddleViewController){
        let viewController = self.moduleBuilder.makeAlbumModule(id: id)
        viewController.onReady = {[weak self] in
            guard let self = self else {return}
            vc.forceupdate()
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showPaymentModule(photoId : String, vc : UserMiddleViewController){
        let viewController = self.moduleBuilder.makePaymentModule(photoId: photoId)
        viewController.onPay = {[weak self] photo in
            vc.update(photosession: photo)
            self?.presenter.popToViewController(vc, animated: true)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showUserAcceptModule(photoId : String, vc : UserMiddleViewController, photoPrice : Int){
        let viewController = self.moduleBuilder.makeUserAcceptViewModule(photoId: photoId, photoPrice: photoPrice)
        viewController.onReady = {[weak self] in
            vc.forceupdate()
            self?.presenter.popToViewController(vc, animated: true)
        }
        viewController.onPayment = {[weak self] in
            self?.showPaymentModule(photoId: photoId, vc: vc)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showUserApproveModule(type : UserType, photoId : String, vc : UserMiddleViewController){
        let viewController = self.moduleBuilder.makeUserApproveViewController(type: type, id: photoId)
        viewController.onCandidates = {[weak self] in
            self?.showSelectionModule(type: type, id: photoId, photoId: photoId, vc: vc, approveVc: viewController)
        }
        viewController.participantSelection = {[weak self] id, status in
            self?.showPhotographProfileModule(userId: id, photoId: photoId, vc: viewController, approveVc: viewController, status: .accepted, userMiddleViewController: vc, phoneNumber: nil, stage: status)
        }
        viewController.userSelection = {[weak self] id, stage in
            self?.showPhotographProfileModule(userId: id, photoId: photoId, vc: viewController, approveVc: viewController, status: .inbox, userMiddleViewController: vc, phoneNumber: nil, stage: stage)
        }
        
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showPhotosessionSettingsModule(photoId : String, vc : UserMiddleViewController) {
        let viewController = self.moduleBuilder.makeUserPhotoSettingsModule(photoId: photoId)
        viewController.onSave = {[weak self] photo in
            guard let self = self else {return}
            vc.fetchData()
            self.presenter.popViewController(animated: true)
        }
        viewController.onDelete = {[weak self] id in
            guard let self = self else {return}
            self.showDeletePhotoModule(photoId: id, mainVc: vc)
        }
        
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showUserMiddleModule(userId : String? = nil) {
        let viewController = self.moduleBuilder.makeUserMiddleModule()
        viewController.onUser = {[weak self] id, phone in
            guard let self = self else {return}
            self.showPhotographProfileModule(userId: id, photoId: "", vc: viewController, status: .participant, userMiddleViewController: viewController, phoneNumber : phone, stage : nil)
        }
        viewController.onAccept = {[weak self] id, price in
            self?.showUserAcceptModule(photoId: id, vc : viewController, photoPrice: price)
        }
        viewController.onNew = {[weak self]  in
            self?.showUserCreatePhoto(vc : viewController)
        }
        viewController.onApprove = {[weak self] type , id in
            self?.showUserApproveModule(type: type, photoId: id, vc: viewController)
        }
        viewController.onPhotoSettings = {[weak self] id in
            guard let self = self else {return}
            self.showPhotosessionSettingsModule(photoId: id, vc : viewController)
        }
        viewController.selection = {[weak self] type, id in
            self?.showSelectionModule(type: type, id: id, photoId: id, vc: viewController, approveVc: nil)
        }
        viewController.onPhoneNumber = {[weak self] in
            guard let self = self else {return}
            self.showPhoneNumberModule(vc: viewController)
        }
        viewController.onConfirmation = {[weak self] id in
            self?.showConfirmationModule(id: id, vc: viewController)
        }
        viewController.onAlbum = {[weak self] id in
            self?.showAlbumModule(id: id, vc: viewController)
        }
        if userId != nil {
            viewController.onNew?()
        }
        self.presenter.setViewControllers([viewController], animated: false)
        
    }
    
    func showDeletePhotoModule(photoId : String, mainVc : UserMiddleViewController){
        let viewController = self.moduleBuilder.makePhotoDeleteModule(photoId: photoId)
        viewController.onReady = {[weak self] in
            guard let self = self else {return}
            mainVc.forceupdate()
            self.presenter.popToViewController(mainVc, animated: true)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showSelectionModule(type : UserType, id : String, photoId : String, vc : UserMiddleViewController, approveVc : UserApproveViewController?){
        let selectionStep = self.moduleBuilder.makeSelectionModule(type: type, id: id)
        selectionStep.onUser = {[weak self] id, stage in
            print("HERES IDS \(id)")
            //let status = offer ? PhotosessionStatus.selection : .prepayment
            
            self?.showPhotographProfileModule(userId: id, photoId: photoId, vc : selectionStep, approveVc: approveVc, status: nil, userMiddleViewController: vc, phoneNumber: nil, stage: stage)
        }
        selectionStep.onBack = {[weak self] in
            //vc.fetchData()
            //approveVc?.update()
        }
        self.presenter.pushViewController(selectionStep, animated: true)
    }
    
    
    func showPhotographProfileModule(userId : String, photoId : String, vc : UIViewController, approveVc : UserApproveViewController? = nil, status : OffersStatus?, userMiddleViewController : UserMiddleViewController, phoneNumber : String?, stage : Int?) {
        let viewController = self.moduleBuilder.makePhotographProfileModule(id : userId, photoId: photoId, status: status, phoneNumber: phoneNumber, stage: stage)
        
        viewController.onAlbum = {[weak self] id in
            self?.showPhotoAlbumModule(images: nil, id: id, scrollTo: nil, userId: userId)
        }
        viewController.onImages = {[weak self] images, index in
            self?.showPhotoAlbumModule(images: images, id: nil, scrollTo: index, userId: userId)
        }
        
        viewController.onAddForUser = {[weak self] id in
            if let vc = vc as? SelectionViewController {
                vc.update(id: id)
                //userMiddleViewController.fetchData()
               // self?.presenter.popViewController(animated: true)
            }
            //approveVc?.update()
        }
        viewController.onBackParty = {[weak self] offers in
            approveVc?.update(offers: offers)
            userMiddleViewController.fetchData()
            
        }
       
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    
    func showPhotoAlbumModule(images : [Image]?, id : String?, scrollTo : Int?, userId : String){
        let viewController = self.moduleBuilder.makePhotoAlbum(images : images, id: id, scrollTo: scrollTo, userId: userId)
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    
    
    func showSecondRedyModule(photosessionCreateModel : CreatePhotosession, vc : UserMiddleViewController){
        let secondReadyStep = self.moduleBuilder.makeSecondStepModule(photoCreation: photosessionCreateModel)
        secondReadyStep.onNext = { [weak self] id, photo in
            photosessionCreateModel.title = "tiiitle"
            //self.presenter.pushViewController(selectionStep, animated: true)
            vc.addNew(photosession: photo)
            guard let type = photosessionCreateModel.participantsTypes else {return}
            let userType = UserType.init(rawValue: type[0].rawValue)
            self?.presenter.popViewController(animated: false)
            self?.showSelectionModule(type: userType!, id: id, photoId: photo.id ,vc : vc, approveVc: nil)
        }
        self.presenter.pushViewController(secondReadyStep, animated: true)
    }
    func showPhoneNumberModule(vc : UserMiddleViewController){
        let viewController = self.moduleBuilder.makePhoneNumberViewController()
        
        
        
        
        viewController.onTextField = {[weak self] number, key in
            self?.startPhoneCodeModule(phoneNumber: number, key: key, vc: vc)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func startPhoneCodeModule(phoneNumber : String, key : String, vc : UserMiddleViewController){
        let viewController = self.moduleBuilder.makePhoneCodeModule(phonenumber: phoneNumber, key: key)
        
        
        viewController.onAgreed = {[weak self] userModel in
            guard let self = self else {return}
            
            UserClass.shared.user = userModel
            
            vc.updateUser()
            
            
            self.presenter.popToViewController(vc, animated: true)
            //self.presenter.popViewController(animated: true)
            //self.presenter.popViewController(animated: false)
            
            
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showUserCreatePhoto(vc : UserMiddleViewController){
        var photosessionCreateModel = CreatePhotosession()
        let userCreate = self.moduleBuilder.mskeUserCreateModule()
        let timeСreate = self.moduleBuilder.makeTineModule()
        let bringPlace = self.moduleBuilder.makePlaceModule()
        let workerCheck = self.moduleBuilder.makeWorkerModule()
        let budgetCreate = self.moduleBuilder.makeBudgetModule(photoCreation : photosessionCreateModel)
        let viewController = self.moduleBuilder.makePageUserCreateViewModule(initialViewControllers: [userCreate, timeСreate, bringPlace, workerCheck, budgetCreate], count: 5)
        self.presenter.pushViewController(viewController, animated: true)
        var comp : Action? = {[weak self] in
            if timeСreate.fullFields == true{
                print("GOOD")
//                print(timeСreate.datePicker.date, timeСreate.itemSelectedOne, timeСreate.buffer[1]+1)
                viewController.next()
                viewController.setBackTitle(title: "Фотосессии")
                viewController.setRighttitle(title: "", completion: nil)
                
                photosessionCreateModel.date = timeСreate.datePicker.date.dateString
                photosessionCreateModel.time = timeСreate.itemSelectedOne
                photosessionCreateModel.duration = timeСreate.buffer[1]+1
                
            } else{
                print("BAd")
            }
        }
        viewController.onBack = {[weak self] index in
    
            if index == 0 {
                self?.presenter.popViewController(animated: true)
            }
            print(index)
            if index == 2 {
                viewController.setRighttitle(title: "Далее", completion: comp)
            } else {
                viewController.setRighttitle(title: "", completion: nil)
            }
        }
        
        userCreate.onNext = {[weak self] specialization in
            photosessionCreateModel.specialization = specialization
            viewController.next()
            viewController.setRighttitle(title: "Далее", completion: comp)
        }
        
        bringPlace.onnStudio = {[weak self] type in
            photosessionCreateModel.place.type = type
            viewController.next()
        }
        
        bringPlace.onnHome = {[weak self] type, adress in
            photosessionCreateModel.place.type = type
            photosessionCreateModel.place.adress = adress
            viewController.next()
        }
        
        bringPlace.onnAdress = {[weak self] type, district in
            photosessionCreateModel.place.type = type
            photosessionCreateModel.place.district = district
            viewController.next()
        }
        
        workerCheck.onPhotograph = {[weak self] in
            print("photograph")
            photosessionCreateModel.participantsTypes = [ParticipantsTypes.photographer]
            viewController.next()
        }
        
        workerCheck.onPhotoandVizage = {[weak self] in
            print("photo + vizage")
            photosessionCreateModel.participantsTypes = [ParticipantsTypes.photographer, ParticipantsTypes.makeup]
            viewController.next()
        }
        
        budgetCreate.onNext = {[weak self] budget in
            print(budget.value)
            var array : [Int] = []
            for i in budget.value!{
                array.append(i)
            }
            photosessionCreateModel.budget = array
            //self?.presenter.pushViewController(secondReadyStep, animated: true)
            self?.presenter.popViewController(animated: false)
            self?.showSecondRedyModule(photosessionCreateModel: photosessionCreateModel, vc : vc)
        }
    }
}


