//
//  AuthCoordinator.swift
//  OMG
//
//  Created by Roman Makeev on 11/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

protocol AuthCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startAuthFlow(presenter: AirdronNavigationController, completion: Action?)
}

extension AuthCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startAuthFlow(presenter: AirdronNavigationController, completion: Action?) {
        let coordinator = self.coordinatorFactory.makeAuthCoordinator(presenter: presenter, coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.onCompletion = {
            completion?()
        }
        coordinator.start()
    }
}

class AuthCoordinator: BaseCoordinator {
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: AuthModuleBuilder
    private let navigationPresenter = AirdronNavigationController()
    var onCompletion: Action?
    private var userCreation = UserCreation()
    init(presenter: AirdronNavigationController,
         moduleBuilder: AuthModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        self.presenter.present(self.navigationPresenter, animated: false)
        self.startLaunchModule()
    }
    
    private func startLaunchModule() {
        let viewController = self.moduleBuilder.makeLaunchModule()
        viewController.onInstagram = {[weak self] in
            self?.startInstagramModule()
        }
        viewController.onButtonClicked = {[weak self] in
            self?.startPhoneNumberModule()
        }
        
      //  let viewController2 = self.moduleBuilder.makePhoneNumberModule()
       
        self.navigationPresenter.setViewControllers([viewController], animated: false)
        
    }
    private func startPhoneNumberModule(){
        let viewController = self.moduleBuilder.makePhoneNumberModule()
        viewController.onUse = {[weak self] in
            self?.showUserTermsOfUseModule()
        }
        
        viewController.onAgreement = {[weak self] in
            self?.showUserAgreement()
        }
        
        viewController.onTextField = {[weak self] number, key in
            self?.startPhoneCodeModule(phoneNumber: number, key : key)
        }

        self.navigationPresenter.pushViewController(viewController, animated: true)
    }
    
    private func startPhoneCodeModule(phoneNumber : String, key : String){
        let viewController = self.moduleBuilder.makePhoneCodeModule(phonenumber: phoneNumber, key: key)
        viewController.onCodeField = {[weak self] in
            self?.startPageViewControllersModule(agreed: false)
        }
        viewController.onCompletion = {[weak self] in
            self?.navigationPresenter.dismiss(animated: true, completion: nil)
            self?.onCompletion?()
            self?.onFlowFinished?()
        }
        self.navigationPresenter.pushViewController(viewController, animated: true)
    }
    private func startPageViewControllersModule(agreed : Bool){
        let initialViewController = self.moduleBuilder.makeIntroductionModule(agreed: agreed)
        let professionViewController = self.moduleBuilder.makeProfessionModule()
        let todoViewController = self.moduleBuilder.makeToDoModule()
        let notificationViewController = self.moduleBuilder.makeNotificationModule()
        let readyViewController = self.moduleBuilder.makeReadyModule(userCreation: self.userCreation)
        let errorViewController = self.moduleBuilder.makeErrorModule(type: 0)
        
        let cityViewController = self.moduleBuilder.makeCityModule(userCreation: self.userCreation, vc: errorViewController) //!
        
        let pageViewController = self.moduleBuilder.makePageViewModule(initialViewControllers: [initialViewController, professionViewController], count: 5)
    
        var comp : Action? = {[weak self] in
            if initialViewController.fullFields == true {
                self?.userCreation.givenName = initialViewController.givenNameTextField.text
                self?.userCreation.familyName = initialViewController.familyNameTextField.text
                self?.userCreation.nickname = initialViewController.nickNametextField.text
                pageViewController.next()
                pageViewController.setRighttitle(title: "", completion: nil)
            }
        }
        
        pageViewController.setRighttitle(title: "Далее", completion: comp)
        pageViewController.setRightTitleColor(color: Color.light.value)
        pageViewController.navigationItem.rightBarButtonItem?.isEnabled = false
        
//        initialViewController.onNext = {
//            [weak self] givenName, familyname, nickName in
//        }
        
        professionViewController.onNext = {
            [weak self] in
            pageViewController.viewControllers.append(todoViewController)
            pageViewController.viewControllers.append(cityViewController)
            pageViewController.viewControllers.append(notificationViewController)
            //pageViewController.viewControllers.append(readyViewController)
            pageViewController.next()
        }
        professionViewController.onUserType = {[weak self] in
            self?.userCreation.type = .client
            pageViewController.viewControllers.append(cityViewController)
            pageViewController.viewControllers.append(notificationViewController)
            pageViewController.viewControllers.append(readyViewController)
            //pageViewController.viewControllers.append(notificationViewController)
            pageViewController.next()
        }
        todoViewController.onPhoto = {
            [weak self] in
          
                self?.userCreation.type = .photographer
            
            pageViewController.next()
        }
        todoViewController.onMakeUp = {
            [weak self] in
            
                self?.userCreation.type = .makeUpArtist
            
            pageViewController.next()
        }
        todoViewController.onOther = {
            [weak self] in
           
                self?.userCreation.type = .other
            
            pageViewController.next()
        }
        
        cityViewController.onNext = { [weak self] city in
            pageViewController.next()
        }
        
        cityViewController.onError = {[weak self] in
            self?.navigationPresenter.dismiss(animated: true, completion: nil)
            self?.onCompletion?()
            self?.onFlowFinished?()
        }
        
        notificationViewController.onNext = {
            [weak self] in
            if pageViewController.currentIndex == 4 {
                self?.navigationPresenter.pushViewController(readyViewController, animated: true)
                pageViewController.setBackTitle(title: "")
            } else {
                pageViewController.next()
            }
        }
        
        initialViewController.onColor = {[weak self] booool in
            if booool{
                pageViewController.setRighttitle(title: "Далее", completion: comp)
                pageViewController.navigationItem.rightBarButtonItem?.isEnabled = true
            } else {
                
                pageViewController.navigationItem.rightBarButtonItem?.isEnabled = false
            }
        }
        
        pageViewController.onBack = {
            [weak self] index in
            if index == 0 {
                self?.navigationPresenter.popViewController(animated: true)
            }
            if index == 1 {
                pageViewController.setRighttitle(title: "Далее", completion: comp)
            } else if index == 2 {
                pageViewController.viewControllers.removeLast(3)
            }
            else {
                pageViewController.setRighttitle(title: "", completion: nil)
            }
            
        }
        readyViewController.onNext = {
            [weak self] in
            self?.navigationPresenter.dismiss(animated: true, completion: nil)
            self?.onCompletion?()
            self?.onFlowFinished?()
        }
        
        self.navigationPresenter.pushViewController(pageViewController, animated: true)
        
    }
    
    func startInstagramModule() {
        let module = self.moduleBuilder.makeInstagramModule()
        module.onActivation = {[weak self] in
            self?.startPageViewControllersModule(agreed: true)
        }
        module.onCompletion = {[weak self] in
            self?.navigationPresenter.dismiss(animated: true, completion: nil)
            self?.onCompletion?()
            self?.onFlowFinished?()
        }
        self.navigationPresenter.pushViewController(module, animated: true)
    }

    func showUserTermsOfUseModule(){
        let viewController = self.moduleBuilder.makeUserTermsOfUseModule()
        self.navigationPresenter.pushViewController(viewController, animated: true)
    }
    
    func showUserAgreement(){
        let viewController = self.moduleBuilder.makeUserAgreementModule()
        self.navigationPresenter.pushViewController(viewController, animated: true)
    }
    
}
