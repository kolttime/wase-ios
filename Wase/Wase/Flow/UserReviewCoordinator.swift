
//
//  AuthCoordinator.swift
//  OMG
//
//  Created by Roman Makeev on 11/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

protocol UserReviewCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startUserReviewFlow(presenter: AirdronNavigationController, profileHandler: ((String) -> Void)?)
}

extension UserReviewCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startUserReviewFlow(presenter: AirdronNavigationController, profileHandler: ((String) -> Void)?) {
        let coordinator = self.coordinatorFactory.makeUserReviewCoordinator(presenter: presenter,
                                                                          coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.profileHandler = { id in
            profileHandler?(id)
        }
        
        coordinator.start()
    }
}

class UserReviewCoordinator: BaseCoordinator {
    
    var profileHandler: ((String) -> Void)?
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: UserReviewModuleBuilder
    private let navigationPresenter = AirdronNavigationController()
    var onCompletion: Action?
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: UserReviewModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        self.showUserReviewModule()
    }
 
    func showUserReviewModule() {
        let viewController = self.moduleBuilder.makeUserReviewModule()
        viewController.onDiscount = { dis in
            //guard let self = self else {return}
            self.showDiscountViewController(discount: dis)
        }
        viewController.onAlbum = {[weak self] id in
            guard let self = self else {return}
            self.showAlbumModule(id: id)
        }
        viewController.onPhotouser = {[weak self] id in
            self?.showPhotoProfileModule(userId: id)
        }
        viewController.onUserUser = {[weak self] id in
            self?.showUserProfileModule(id: id)
        }
        self.presenter.setViewControllers([viewController], animated: false)
    }
    
    func showDiscountViewController(discount : Int){
        let viewController = self.moduleBuilder.makeDescountViewController()
        
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showAlbumModule(id : String){
        let viewController = self.moduleBuilder.makeAlbummodule(id: id)
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showPhotoProfileModule(userId : String){
        let viewController = self.moduleBuilder.makePhotoProfileModule(id: userId)
        viewController.onAlbum = {[weak self] id in
            self?.showPhotoAlbumModule(images: nil, id: id, scrollTo: nil, userId: userId)
        }
        viewController.onImages = {[weak self] images, index in
            self?.showPhotoAlbumModule(images: images, id: nil, scrollTo: index, userId: userId)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showPhotoAlbumModule(images : [Image]?, id : String?, scrollTo : Int?, userId : String? ){
        let viewController = self.moduleBuilder.makePhotoAlbum(images : images, id: id, scrollTo: scrollTo, userId: userId)
        
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showUserProfileModule(id : String){
        let viewController = self.moduleBuilder.makeUserProfileModule(id: id)
        viewController.onCreate = {[weak self] userId in
            guard let self = self else {return}
            self.onCompletion?()
            self.profileHandler?(userId)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
}


